﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Fluent;

namespace DebtforceUtil
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : RibbonWindow
    {
        public int timeOut;
        DispatcherTimer mIdle;

        public Main()
        {
            this.InitializeComponent();
            //toolBar.Visibility = System.Windows.Visibility.Collapsed;
            //ShowDialog();
            // LoginPage loginPage = new LoginPage();
            LoginWindow loginWindow;
            loginWindow = new LoginWindow();
            loginWindow.ShowDialog();

            //InputManager.Current.PreProcessInput += Activity;

            //DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
            /*System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
            myTimer.Interval = 1000;
            myTimer.Tick += new EventHandler(Timer_Tick);
            myTimer.Start();*/
            //DialogResult result = loginWindow.ShowDialog();
            if (loginWindow.DialogResult == true)
            {
                createComp(loginWindow.loginSfBox.Text, loginWindow.passwordSfBox.Password, loginWindow.tokenSfBox.Text, loginWindow.Processor_LoginBox.Text, loginWindow.Processor_PasswordBox.Password, loginWindow.productionRB.IsChecked.Value, loginWindow.Processor_combox.SelectedItem.ToString(), loginWindow.Processor_LoginID_AuthorizeNet.Text, loginWindow.Processor_AuthNet_Key.Password, loginWindow.cbxLogs.IsChecked);
                try
                {
                    timeOut = int.Parse(loginWindow.timeOutValue.Text);
                    InputManager.Current.PreProcessInput += Idle_PreProcessInput;
                    mIdle = new DispatcherTimer();
                    mIdle.Interval = new TimeSpan(timeOut, 0, 0);
                    mIdle.IsEnabled = true;
                    mIdle.Tick += Idle_Tick;

                    if (loginWindow.Processor_combox.SelectedItem.ToString().Equals("Authorize.Net") 
                        || loginWindow.Processor_combox.SelectedItem.ToString().Equals("FirstData")
                        || loginWindow.Processor_combox.SelectedItem.ToString().Equals("AffinityProgram"))
                    {
                        btnUpdateTransaction.IsEnabled = false;
                        btnUpdateClientBalance.IsEnabled = false;
                        btnViewTransaction.IsEnabled = false;
                        btnViewProcessorAccount.IsEnabled = false;
                    }

                    if (loginWindow.Processor_combox.SelectedItem.ToString().Equals("AffinityProgram"))
                        btnCreateClient.IsEnabled = false;

                    if (loginWindow.Processor_combox.SelectedItem.ToString().Equals("GCS"))
                    {
                        btnScheduleCreator.IsEnabled = true;
                    }
                }
                catch
                {
                    
                }
            }
            else
                Application.Current.Shutdown();
            //System.Windows.Window.sh
            //DWindow.sh
            //loginPage
            //this.loginWindow.Navigate(loginWindow);
        }
        void Idle_Tick(object sender, EventArgs e)
        {
            toolBar.IsEnabled = false;
            ProcessorTransactionsFrame.IsEnabled = false;
            ProcessorAccountFrame.IsEnabled = false;
            clientFrame.IsEnabled = false;
            paymentsFrame.IsEnabled = false;
            transactionFrame.IsEnabled = false;
            mIdle.IsEnabled = false;
            

            MessageBoxResult result = MessageBox.Show("Your window is being inactive for " + timeOut.ToString() + " minute(s). The application is about to close.");
            if (result == MessageBoxResult.OK)
                this.Close();

            //this.Close();
        }
        void Idle_PreProcessInput(object sender, PreProcessInputEventArgs e)
        {
            mIdle.IsEnabled = false;
            mIdle.IsEnabled = true;
        }

        public void createComp(string loginSf, string passSf, string tokenSf, string loginProcessor, string passProcessor, Boolean liveEnv, string Processor, string AuthnetID, string AuthNetPwd, bool? IsLoggingEnabled)
        {
            if (Processor.Equals("T2P-Hybrid / T2P-ProtectionProgram"))
            {
                Processor = "T2P";
            }
            // System.Windows.GridLength lgt = 
            // lgt.Value = 140;
            //MainGrid.RowDefinitions[0].Height = new GridLength(140);
            //(MainGrid.RowDefinitions[0]). = System.Windows.Visibility.Visible;
            /*string loginSf = "faithworks@selastech.com.fwfindev";
            string passSf = "W8f05rIT";
            string tokenSf = "86rdMBZnn2y0yCs2BNig8foR";
            string loginGcs = "websvctestselas";
            string passGcs = "Ko5z2XEx";*/
            TransactionPage transactionPage = new TransactionPage(Processor);
            ClientPage clientPage = new ClientPage(Processor);
            ProcessorTransactionPage ProcessorTranPage = new ProcessorTransactionPage(Processor);
            ProcessorAccountPage acctPage = new ProcessorAccountPage();
            PaymentsPage pmtPage = new PaymentsPage(Processor);


            TransactionViewModel tvm = new TransactionViewModel(loginSf, passSf, tokenSf, loginProcessor, passProcessor, liveEnv, Processor, AuthnetID, AuthNetPwd, IsLoggingEnabled);
            transactionPage.DataContext = tvm;
            clientPage.DataContext = tvm;
            ProcessorTranPage.DataContext = tvm;
            acctPage.DataContext = tvm;
            pmtPage.DataContext = tvm;
            this.DataContext = tvm;

            this.transactionFrame.Navigate(transactionPage);
            this.clientFrame.Navigate(clientPage);
            this.ProcessorAccountFrame.Navigate(acctPage);
            this.ProcessorTransactionsFrame.Navigate(ProcessorTranPage);
            this.paymentsFrame.Navigate(pmtPage);
        }
    }
}
