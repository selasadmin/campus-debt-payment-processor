﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using System.ComponentModel;
using DebtLibrary;
using SFDebt;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using System.Net.Mail;
using System.Windows.Threading;
using System.Threading;
using SelasDataClass;
using RAMSInterop;


namespace DebtforceUtil
{
    static public class Constants
    {
        static public Boolean live = false;
    }
    public class TransactionViewModel : BaseViewModel
    {
        #region Fields

        /// <summary>
        /// The payment class instance
        /// </summary>
        private PaymentClass paymentClass; // =
            // new PaymentClass("staging@selastech.dev", "W8f06rIT", "rZapxH7ffnmN3R3WFtiMjr7Q", "jrichner98002713", "Fwf33436");
             //new PaymentClass("webuserdemo@selastech.com", "w3bD3mo12", "AEqOfLUltDlzKJwP4StO49yt", "websvc98002293", "s5JDw24P");
             //new PaymentClass("webuser@prevail.selastech.com", "pr3V@1LTEST", "uJuzZxUcLwGz8LlGOEygtOdk", "websvc98002293", "s5JDw24P");
            //new PaymentClass("vantage@selastech.com", "W8f02rIT", "b0fByreYpf5MvyzcGR1cfEQl", "websvctestselas", "Ko5z2XEx");
            // new PaymentClass("faithworks@selastech.com", "W8f03rIT", "kxM1XiztiTjk5Rp8B50DeoB7u", "websvctestselas", "Ko5z2XEx");
            // new PaymentClass("staging@selastech.dev", "W8f06rIT", "rZapxH7ffnmN3R3WFtiMjr7Q", "websvctestselas", "Ko5z2XEx");
             // new PaymentClass("douedraogo@selastech.dev", "W8f03rIT", "yRPWiO6eBtMdJFfezZJhFAQo", "websvctestselas", "Ko5z2XEx");
        // new PaymentClass("faithworks@selastech.com.fwfindev", "W8f05rIT", "86rdMBZnn2y0yCs2BNig8foR", "websvctestselas", "Ko5z2XEx");

        /// <summary>
        /// Stores the transactions and client info
        /// </summary>
        private TransactionDS transactions = new TransactionDS();

        /// <summary>
        /// Stores the transactions and client info
        /// </summary>
        private TransactionDS account = new TransactionDS();

        /// <summary>
        /// The start date for payment processing
        /// </summary>
        private DateTime startDate = new DateTime();

        /// <summary>
        /// The cutoff date for payment processing
        /// </summary>
        private DateTime endDate = new DateTime();

        /// <summary>
        /// The Payment Processor selected from Login Page
        /// </summary>
        private string _Processor  = string.Empty;

        /// <summary>
        /// The start date for transaction search
        /// </summary>
        private DateTime searchStartDate = new DateTime();

        /// <summary>
        /// The cutoff date for transaction search
        /// </summary>
        private DateTime searchEndDate = new DateTime();

        /// <summary>
        /// Flag activated whenever payments are ready
        /// </summary>
        private bool paymentsReady = false;

        /// <summary>
        /// Flag activated whenever export are ready
        /// </summary>
        private bool exportReady = false;

        /// <summary>
        /// Flag activated whenever payments are ready
        /// </summary>
        private bool clientsReady = false;

        /// <summary>
        /// Flag for header checkbox
        /// </summary>
        private bool checkAll = false;

        /// <summary>
        /// Flag for header checkbox
        /// </summary>
        private bool checkClientAll = false;

        /// <summary>
        /// CheckBox Column Size for transaction page
        /// </summary>
        private int chkSize = 0;

        /// <summary>
        /// CheckBox Column Size for client page
        /// </summary>
        private int chkClientSize = 0;

        /// <summary>
        /// create transaction button text
        /// </summary>
        private string btnCreateTxn = "Pending Transactions";

        /// <summary>
        /// create table name text
        /// </summary>
        private string tableName = "None";

        /// <summary>
        /// create client button text
        /// </summary>
        private string btnCreateClients = "Pending Clients";

        /// <summary>
        /// Flag activated whenever all processes have finished running
        /// </summary>
        private bool isBusy = false;

        /// <summary>
        /// Flag activated whenever the update transaction is in progress
        /// </summary>
        private bool isUpdate = false;

        /// <summary>
        /// Flag activated whenever transaction view is visible
        /// </summary>
        private string visibleView = "Transactions";

        /// <summary>
        /// The account number to search for
        /// </summary>
        private string accountNumber = string.Empty;

        private BackgroundWorker worker = new BackgroundWorker();

        private delegate void AddMessageDelegate(string message);

        private Thread mainThread = Thread.CurrentThread;

        private AddMessageDelegate addMessageDelegate;

        #endregion

        public TransactionViewModel(string sfLogin, string sfPass, string sfToken, string ProcessorLogin, string ProcessorPass, Boolean liveEnv, string Processor, string AuthNet_LoginID, string AuthNetKey, bool? IsLogEnabled)
        {
            // 2013-11-05 BT : The default constructor (not the one below) REQUIRES NuDebt API Settings to be set, that constructor
            // TODO            is NOT used in this program
            paymentClass = new PaymentClass(sfLogin, sfPass, sfToken, ProcessorLogin, ProcessorPass, liveEnv, Processor, AuthNet_LoginID, AuthNetKey, IsLogEnabled);
            this.Load();
            this._Processor = Processor;
        }

        public TransactionViewModel()
        { }

        #region Properties


        public string Processor
        {
            get
            {
                return this._Processor;
            }

            set
            {
                this._Processor = value;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return this.endDate;
            }

            set
            {
                this.endDate = value;
                this.NotifyPropertyChanged("EndDate");
            }
        }

        public DateTime StartDate
        {
            get
            {
                return this.startDate;
            }

            set
            {
                this.startDate = value;
                this.EndDate = this.StartDate.AddDays(this.DateSpan);
                this.NotifyPropertyChanged("StartDate");
            }
        }

        public DateTime SearchEndDate
        {
            get
            {
                return this.searchEndDate;
            }

            set
            {
                this.searchEndDate = value;
                this.NotifyPropertyChanged("SearchEndDate");
            }
        }

        public DateTime SearchStartDate
        {
            get
            {
                return this.searchStartDate;
            }

            set
            {
                this.searchStartDate = value;
                this.NotifyPropertyChanged("SearchStartDate");
            }
        }

        public TransactionDS Transactions
        {
            get
            {
                return this.transactions;
            }

            set
            {
                this.transactions = value;
                this.NotifyPropertyChanged("Transactions");
            }
        }

        public TransactionDS TransactionsView
        {
            get;
            set;
        }

        public TransactionDS Account
        {
            get
            {
                return this.account;
            }

            set
            {
                this.account = value;
                this.NotifyPropertyChanged("Account");
            }
        }

        public TransactionDS.ClientsRow ClientRow
        {
            get;
            set;
        }

        public string TableName
        {
            get { return tableName; }
            set { tableName = value; }
        }

        public TransactionDS.ClientsDataTable Clients
        {
            get
            {
                return this.paymentClass.CreatedClients;
            }
        }

        public ObservableCollection<string> Messages
        {
            get;
            set;
        }

        public ObservableCollection<int> IntegerList
        {
            get;
            set;
        }

        public ObservableCollection<DateTime> DateList
        {
            get;
            set;
        }

        public ObservableCollection<DateTime> DateList1
        {
            get;
            set;
        }

        public bool PaymentsReady
        {
            get
            {
                return this.paymentsReady;
            }

            set
            {
                this.paymentsReady = value;
                this.NotifyPropertyChanged("PaymentsReady");
            }
        }

        public bool ExportReady
        {
            get
            {
                return this.exportReady;
            }

            set
            {
                this.exportReady = value;
                this.NotifyPropertyChanged("ExportReady");
            }
        }

        public bool ClientsReady
        {
            get
            {
                return this.clientsReady;
            }

            set
            {
                this.clientsReady = value;
                this.NotifyPropertyChanged("ClientsReady");
            }
        }

        public bool CheckAll
        {
            get
            {
                return this.checkAll;
            }

            set
            {
                this.checkAll = value;
                if (this.checkAll)
                {
                    foreach (TransactionDS.ClientDepositRow row in this.transactions.ClientDeposit)
                    {
                        row.IsChecked = true;
                    }
                }
                else
                {
                    foreach (TransactionDS.ClientDepositRow row in this.transactions.ClientDeposit)
                    {
                        row.IsChecked = false;
                    }
                }
                this.NotifyPropertyChanged("CheckAll");
            }
        }

        public bool CheckClientAll
        {
            get
            {
                return this.checkClientAll;
            }

            set
            {
                this.checkClientAll = value;
                if (this.checkClientAll)
                {
                    foreach (TransactionDS.ClientsRow row in this.Clients)
                    {
                        row.IsChecked = true;
                    }
                }
                else
                {
                    foreach (TransactionDS.ClientsRow row in this.Clients)
                    {
                        row.IsChecked = false;
                    }
                }
                this.NotifyPropertyChanged("CheckClientAll");
            }
        }

        public string BTNCreateTxn
        {
            get
            {
                return this.btnCreateTxn;
            }

            set
            {
                this.btnCreateTxn = value;
                this.NotifyPropertyChanged("BTNCreateTxn");
            }
        }

        public string BTNCreateClients
        {
            get
            {
                return this.btnCreateClients;
            }

            set
            {
                this.btnCreateClients = value;
                this.NotifyPropertyChanged("BTNCreateClients");
            }
        }

        public int CHKSize
        {
            get
            {
                return this.chkSize;
            }

            set
            {
                this.chkSize = value;
                this.NotifyPropertyChanged("CHKSize");
            }
        }

        public int CHKClientSize
        {
            get
            {
                return this.chkClientSize;
            }

            set
            {
                this.chkClientSize = value;
                this.NotifyPropertyChanged("CHKClientSize");
            }
        }

        public bool IsUpdate
        {
            get
            {
                return this.isUpdate;
            }

            set
            {
                this.isUpdate = value;
                this.NotifyPropertyChanged("IsUpdate");
            }
        }

        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }

            set
            {
                this.isBusy = value;
                this.NotifyPropertyChanged("IsBusy");
            }
        }

        public string VisibleView
        {
            get
            {
                return this.visibleView;
            }

            set
            {
                this.visibleView = value;
                this.NotifyPropertyChanged("VisibleView");
            }
        }

        public string Version
        {
            get;
            set;
        }

        public string AccountNumber
        {
            get
            {
                return this.accountNumber;
            }

            set
            {
                this.accountNumber = value;
                this.NotifyPropertyChanged("AccountNumber");
            }
        }

        public int DateSpan
        {
            get
            {
                return this.paymentClass.NumberOfDaysAhead;
            }

            set
            {
                this.paymentClass.NumberOfDaysAhead = value;
                this.EndDate = this.StartDate.AddDays(value);
                this.NotifyPropertyChanged("DateSpan");
            }
        }
        
        #endregion 

        #region Commands

        public RelayCommand CreateClientCommand
        {
            get;
            set;
        }

        public RelayCommand CreateTxCommand
        {
            get;
            set;
        }

        public RelayCommand UpdateClientCommand
        {
            get;
            set;
        }

        public RelayCommand UpdateTxCommand
        {
            get;
            set;
        }

        public RelayCommand RunTxCommand
        {
            get;
            set;
        }

        public RelayCommand ViewProcessorTxCommand
        {
            get;
            set;
        }

        public RelayCommand ViewProcessorAccountsCommand
        {
            get;
            set;
        }

        public RelayCommand ExportExcelCommand
        {
            get;
            set;
        }

        public RelayCommand PullProcessorTxCommand
        {
            get;
            set;
        }

        public RelayCommand PullProcessorAccountsCommand
        {
            get;
            set;
        }

        public RelayCommand CreateOfferPmtsCommand { get; set; }

        public RelayCommand ProcessPaymentsCommand { get; set; }

        #endregion 

        #region View Specific Functions

        private void AddMessage(string message)
        {
            this.Messages.Insert(0, DateTime.Now.ToShortTimeString() + " - " + message);
        }

        private void Load()
        {
            this.addMessageDelegate = this.AddMessage;
            this.Version = "3.0";
            this.Messages = new ObservableCollection<string>();

            this.CreateClientCommand = new RelayCommand(this.CreateClientClick, this.CanExecute);
            this.CreateTxCommand = new RelayCommand(this.CreateTransactionsClick, this.CanExecute);
            this.CreateOfferPmtsCommand = new RelayCommand(this.CreatePaymentsClick, this.CanExecute);
            this.ProcessPaymentsCommand = new RelayCommand(this.ProcessPaymentsClick, this.CanExecute);
            this.UpdateClientCommand = new RelayCommand(this.UpdateClientClick, this.CanExecute);
            this.UpdateTxCommand = new RelayCommand(this.UpdateTransactionsClick, this.CanExecute);
            this.RunTxCommand = new RelayCommand(this.RunTransactionsClick);
            this.ViewProcessorTxCommand = new RelayCommand(this.ViewProcessorTransactionsClick, this.CanExecute);
            this.ViewProcessorAccountsCommand = new RelayCommand(this.ViewProcessorAccountClick, this.CanExecute);
            this.ExportExcelCommand = new RelayCommand(this.ExportExcelCommandClick, this.CanExecute);
            this.PullProcessorAccountsCommand = new RelayCommand(this.PullProcessorAccountClick, this.CanExecute);
            this.PullProcessorTxCommand = new RelayCommand(this.PullProcessorTransactionsClick, this.CanExecute);

            this.DateList = new ObservableCollection<DateTime>();
            this.DateList1 = new ObservableCollection<DateTime>();
            this.IntegerList = new ObservableCollection<int>();

            for (int i = 0; i <= 30; i++)
            {
                this.IntegerList.Add(i);
            }
            for (int i = 50; i <= 120; )
            {
                this.IntegerList.Add(i);
                i += 10;
            }

            for (int i = 13; i >= 0; i--)
            {
                this.DateList.Add(DateTime.Today.AddDays(-i));
            }

            for (int i = 0; i < 14; i++)
            {
                this.DateList1.Add(DateTime.Today.AddDays(i));
            }

            this.StartDate = DateTime.Today;
            this.EndDate = DateTime.Today;

            this.DateSpan = this.paymentClass.NumberOfDaysAhead;
            this.PaymentsReady = false;
            this.IsUpdate = false;

            this.worker.DoWork += new DoWorkEventHandler(this.Worker_DoWork);
            this.worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_RunWorkerCompleted);
        }

        #endregion

        #region Background Worker Related Functions

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.CreateClientCommand.RaiseCanExecuteChanged();
            this.CreateTxCommand.RaiseCanExecuteChanged();
            this.UpdateClientCommand.RaiseCanExecuteChanged();
            this.UpdateTxCommand.RaiseCanExecuteChanged();
            this.ViewProcessorAccountsCommand.RaiseCanExecuteChanged();
            this.ExportExcelCommand.RaiseCanExecuteChanged();
            this.ViewProcessorTxCommand.RaiseCanExecuteChanged();
            this.CreateOfferPmtsCommand.RaiseCanExecuteChanged();
        }

        //private void Worker_DoWork1(object sender, DoWorkEventArgs e)
        //{
        //    if (e.Argument != null)
        //    {
        //        //switch (e.Argument.ToString())
        //        //{
        //        //    case "UpdateClient":
        //        //        this.UpdateClient();
        //        //        break;
        //        //    case "CreateTransactions":
        //        //        this.CreateTransactions();
        //        //        break;
        //        //    case "UpdateTransactions":
        //        //        this.IsUpdate = true;
        //        //        this.UpdateTransactions();
        //        //        break;
        //        //    case "RunTransactions":
        //        //        this.RunTransactions();
        //        //        break;
        //        //    case "CreatePayments":
        //        //        this.CreatePayments();
        //        //        break;
        //        //    case "ProcessPayments":
        //        //        this.ProcessPayments();
        //        //        break;
        //        //    case "CreateClient":
        //        //        this.CreateClient();
        //        //        break;
        //        //    case "ViewProcessorTransactions":
        //        //        this.ViewProcessorTransactions();
        //        //        break;
        //        //    case "ViewProcessorAccount":
        //        //        this.ViewProcessorAccount();
        //        //        break;
        //        //}
        //    }
        //}

        //private void Worker_DoWork2(object sender, DoWorkEventArgs e)
        //{
        //    if (e.Argument != null)
        //    {
        //        switch (e.Argument.ToString())
        //        {
        //            case "UpdateClient":
        //                this.UpdateClient();
        //                this.BTNCreateClients = "Pending Clients";
        //                this.CHKClientSize = 0;
        //                this.ClientsReady = false;
        //                break;
        //            case "CreateTransactions":
        //                {
        //                    if (!this.BTNCreateTxn.Equals("Create Transactions"))
        //                    {
        //                        this.ViewPendingTransactions();
        //                        if (this.transactions.ClientDeposit.Count > 0)
        //                        {
        //                            this.CHKSize = 30;
        //                            this.BTNCreateTxn = "Create Transactions";
        //                            this.PaymentsReady = true;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        this.CreateTransactions();
        //                        this.BTNCreateTxn = "Pending Transactions";
        //                        this.CHKSize = 0;
        //                        this.PaymentsReady = false;
        //                    }
        //                    this.CheckAll = false;
        //                }
        //                break;
        //            case "UpdateTransactions":
        //                this.IsUpdate = true;
        //                //this.UpdateTransactions();
        //                this.BTNCreateTxn = "Pending Transactions";
        //                this.CHKSize = 0;
        //                this.PaymentsReady = false;
        //                break;
        //            case "RunTransactions":
        //                this.RunTransactions();
        //                break;
        //            case "CreatePayments":
        //                this.CreatePayments();
        //                break;
        //            case "ProcessPayments":
        //                this.ProcessPayments();
        //                break;
        //            case "CreateClient":
        //                {
        //                    if (!this.BTNCreateClients.Equals("Create Clients"))
        //                    {
        //                        this.ViewPendingClient();
        //                        if (this.Clients.Count > 0)
        //                        {
        //                            this.CHKClientSize = 30;
        //                            this.BTNCreateClients = "Create Clients";
        //                            this.ClientsReady = true;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        this.CreateClient();
        //                        this.BTNCreateClients = "Pending Clients";
        //                        this.CHKClientSize = 0;
        //                        this.ClientsReady = false;
        //                    }
        //                    this.CheckClientAll = false;
        //                }
        //                break;
        //            case "ViewProcessorTransactions":
        //                this.ViewProcessorTransactions();
        //                break;
        //            case "ViewProcessorAccount":
        //                this.ViewProcessorAccount();
        //                break;
        //        }
        //    }
        //}

        private void ResetLabel()
        {
            this.BTNCreateTxn = "Pending Transactions";
            this.CHKSize = 0;
            this.PaymentsReady = false;
            this.BTNCreateClients = "Pending Clients";
            this.CHKClientSize = 0;
            this.ClientsReady = false;
        }

        //private void Worker_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    bool flag = true;

        //    if (e.Argument != null)
        //    {
        //        switch (e.Argument.ToString())
        //        {
        //            case "UpdateClient":
        //                this.UpdateClient();
        //                break;
        //            case "UpdateTransactions":
        //                this.IsUpdate = true;
        //                //this.UpdateTransactions();
        //                break;
        //            case "ViewProcessorTransactions":
        //                this.ViewProcessorTransactions();
        //                break;
        //            case "ViewProcessorAccount":
        //                this.ViewProcessorAccount();
        //                break;
        //            case "CreateTransactions":
        //                {
        //                    if (!this.BTNCreateTxn.Equals("Create Transactions"))
        //                    {
        //                        this.ViewPendingTransactions();
        //                        if (this.transactions.ClientDeposit.Count > 0)
        //                        {
        //                            this.CHKSize = 30;
        //                            this.BTNCreateTxn = "Create Transactions";
        //                            this.PaymentsReady = true;
        //                            this.BTNCreateClients = "Pending Clients";
        //                            this.CHKClientSize = 0;
        //                            this.ClientsReady = false;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        this.CreateTransactions();

        //                    }
        //                    this.CheckAll = false;
        //                }
        //                flag = false;
        //                break;
        //            case "CreateClient":
        //                {
        //                    if (!this.BTNCreateClients.Equals("Create Clients"))
        //                    {
        //                        this.ViewPendingClient();
        //                        if (this.Clients.Count > 0)
        //                        {
        //                            this.CHKClientSize = 30;
        //                            this.BTNCreateClients = "Create Clients";
        //                            this.ClientsReady = true;
        //                            this.BTNCreateTxn = "Pending Transactions";
        //                            this.CHKSize = 0;
        //                            this.PaymentsReady = false;
        //                        }
        //                    }
        //                    else
        //                    {
        //                        this.CreateClient();
        //                    }
        //                    this.CheckClientAll = false;
        //                }
        //                flag = false;
        //                break;

        //            case "RunTransactions":
        //                this.RunTransactions();
        //                break;
        //            case "CreatePayments":
        //                this.CreatePayments();
        //                break;
        //            case "ProcessPayments":
        //                this.ProcessPayments();
        //                break;

        //        }
        //        if (flag)
        //            this.ResetLabel();

        //    }
        //}

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool flag = true;
            this.ExportReady = false;
            if (e.Argument != null)
            {
                switch (e.Argument.ToString())
                {
                    case "UpdateClient":
                        this.UpdateClient();
                        break;
                    case "UpdateTransactions":
                        this.IsUpdate = true;
                        this.UpdateTransactions();
                        break;
                    case "ViewProcessorTransactions":
                        this.ViewProcessorTransactions();
                        break;
                    case "ViewProcessorAccount":
                        this.ViewProcessorAccount();
                        break;
                    case "CreateTransactions":
                        {
                            if (!this.BTNCreateTxn.Equals("Create Transactions"))
                            {
                                this.ViewPendingTransactions();
                                if (this.transactions.ClientDeposit.Count > 0)
                                {
                                    this.CHKSize = 30;
                                    this.BTNCreateTxn = "Create Transactions";
                                    this.PaymentsReady = true;                                   
                                }
                                this.BTNCreateClients = "Pending Clients";
                                this.CHKClientSize = 0;
                                this.ClientsReady = false;
                            }
                            else
                            {
                                this.CreateTransactions();
                                this.ExportReady = true;
                                TableName = "CreateTransactions";
                                this.ExportExcel("Automatic");                                
                            }
                            this.CheckAll = false;
                        }
                        flag = false;
                        break;
                    case "CreateClient":
                        {
                            if (!this.BTNCreateClients.Equals("Create Clients"))
                            {
                                this.ViewPendingClient();
                                if (this.Clients.Count > 0)
                                {
                                    this.CHKClientSize = 30;
                                    this.BTNCreateClients = "Create Clients";
                                    this.ClientsReady = true;                                    
                                }
                                this.BTNCreateTxn = "Pending Transactions";
                                this.CHKSize = 0;
                                this.PaymentsReady = false;
                            }
                            else
                            {
                                this.CreateClient();                                
                                this.ExportReady = true;
                                TableName = "CreateClient";
                                this.ExportExcel("Automatic");
                            }
                            this.CheckClientAll = false;
                        }
                        flag = false;
                        break;

                    case "RunTransactions":
                        this.RunTransactions();
                        break;
                    case "CreatePayments":
                        this.CreatePayments();
                        break;
                    case "ProcessPayments":
                        this.ProcessPayments();
                        break;
                    case "CreateExcel":
                        this.ExportExcel("Manual");
                        break;

                }
                if (flag)
                    this.ResetLabel();

            }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Writes any errors that failed to write to SalesForce to a text file with the date stamp
        /// </summary>
        /// <param name="ex">The System.Exception that occurred</param>
        private void WriteError(Exception ex)
        {
            try
            {
                MailMessage mm = new MailMessage();
                // TODO bt - Testing to memm.To.Add("errors@selastech.com");
                //mm.To.Add("bill.tessaro@selastech.com");
                mm.To.Add("raghurajs@chetu.com");
                mm.Body = "An API error occurred on " + DateTime.Now.ToShortDateString() + " at " +
                        DateTime.Now.ToShortTimeString() + " \r\n\r\nError Message: " + ex.Message +
                        "\r\n\r\nStack Trace: " + ex.StackTrace;
                mm.From = new MailAddress("PmtProcessor@selastech.com");
                mm.Subject = "Critical error occurred with Selas Payment System";

                SmtpClient client = new SmtpClient("smtpout.secureserver.net", 25);
                client.Credentials = new System.Net.NetworkCredential("errors@selastech.com", "sales2012");
                client.Send(mm);
            }
            catch (Exception ex2)
            {
                this.paymentClass.WriteError(ex2, "");
                this.paymentClass.WriteError(ex, "");
            }
        }

        private void RunWorker(string workerType)
        {
            this.IsUpdate = false;
            this.worker.RunWorkerAsync(workerType);

            this.CreateClientCommand.RaiseCanExecuteChanged();
            this.CreateTxCommand.RaiseCanExecuteChanged();
            this.UpdateClientCommand.RaiseCanExecuteChanged();
            this.UpdateTxCommand.RaiseCanExecuteChanged();
            this.ViewProcessorAccountsCommand.RaiseCanExecuteChanged();
            this.ExportExcelCommand.RaiseCanExecuteChanged();
            this.ViewProcessorTxCommand.RaiseCanExecuteChanged();
            this.CreateOfferPmtsCommand.RaiseCanExecuteChanged();
        }

        private void PullProcessorAccountClick()
        {
            this.VisibleView = "ProcessorAccount";
            this.RunWorker("ViewProcessorAccount");
        }

        private void PullProcessorTransactionsClick()
        {
            this.VisibleView = "ProcessorTransactions";
            this.RunWorker("ViewProcessorTransactions");
        }

        private void ViewProcessorAccountClick()
        {
            this.VisibleView = "ProcessorAccount";
        }

        private void ExportExcelCommandClick()
        {
            //this.VisibleView = "Excel";
            this.RunWorker("CreateExcel");
        }

        private void ViewProcessorTransactionsClick()
        {
            this.VisibleView = "ProcessorTransactions";
        }

        private void CreateClientClick()
        {
            this.VisibleView = "Client";
            this.RunWorker("CreateClient");
        }

        private void CreateTransactionsClick()
        {
            this.VisibleView = "Transactions";
            this.RunWorker("CreateTransactions");
        }

        private void CreatePaymentsClick()
        {
            this.VisibleView = "Payments";
            this.RunWorker("CreatePayments");
        }

        private void ProcessPaymentsClick()
        {
            this.VisibleView = "Payments";
            this.RunWorker("ProcessPayments");
        }

        private void UpdateClientClick()
        {
            this.VisibleView = "Client";
            this.RunWorker("UpdateClient");
        }

        private void UpdateTransactionsClick()
        {
            this.VisibleView = "Transactions";
            this.RunWorker("UpdateTransactions");
        }

        private void RunTransactionsClick()
        {
            this.VisibleView = "Transactions";
            this.RunWorker("RunTransactions");
        }

        private bool CanExecute()
        {
            this.IsBusy = this.worker.IsBusy;
            return !this.isBusy;
        }

        private void ViewProcessorAccount()
        {
            try
            {
                this.ClientRow = null;
                this.account = null;

                switch (this.Processor)
                {
                    case "RAMS":
                        {
                            this.paymentClass.CheckRAMSClientAccount(this.accountNumber, out this.account);
                            break;
                        }

                    case "GCS":
                    case "T2P":

                        {
                            this.paymentClass.CheckGCSClientAccount(this.accountNumber, out this.account);
                            break;
                        }

                    case "EPPS":
                        {
                            this.paymentClass.CheckEPPSClientAccount(this.accountNumber, out this.account);
                            break;
                        }
                }
                

                if (this.account != null && this.account.Clients != null && this.account.Clients.Rows.Count > 0)
                {
                    this.ClientRow = this.account.Clients.Rows[0] as TransactionDS.ClientsRow;
                }

                this.NotifyPropertyChanged("ClientRow");
                this.NotifyPropertyChanged("Account");
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
            }
        }

        private void ViewProcessorTransactions()
        {
            try
            {
                if (this.searchStartDate <= this.searchEndDate)
                {
                    this.TransactionsView = this.paymentClass.GetTransactions(this.searchStartDate, this.searchEndDate);
                    this.NotifyPropertyChanged("TransactionsView");
                }
                else
                {
                        System.Windows.Forms.MessageBox.Show("Please select correct date range.");
                }
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
        }

        private void ViewPendingClient()
        {
            try
            {
                TransactionDS.ClientsDataTable clientDT = this.paymentClass.RunViewClient(this.startDate, this.endDate);
                this.NotifyPropertyChanged("Clients");
                if (clientDT.Rows.Count == 0)
                    Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "No New Client Account Creation to process");
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
        }

        //private void CreateClient()
        //{
        //    try
        //    {
        //        DateTime start = DateTime.Now;
        //        this.paymentClass.RunCreateClient();
        //        this.NotifyPropertyChanged("Clients");
        //        Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "New Client Account Creation Process Completed!");
        //    }
        //    catch (Exception ex)
        //    {
        //        this.WriteError(ex);
        //        Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
        //    }
        //}
        private void CreateClient()
        {
            try
            {
                TransactionDS.ClientsDataTable temp = new TransactionDS.ClientsDataTable();
                foreach (TransactionDS.ClientsRow row in this.paymentClass.CreatedClients)
                {
                    if (row.IsChecked.Equals(true))
                    {
                        TransactionDS.ClientsRow newRow = temp.NewClientsRow();
                        row.RowNumber = temp.Count + 1;
                        newRow = row;
                        temp.ImportRow(newRow);
                    }
                }
                if (temp.Count == 0)
                    System.Windows.Forms.MessageBox.Show("Please select atleast one record.");
                else
                {
                    this.paymentClass.CreatedClients = temp;
                    TransactionDS.ClientsDataTable clientDT = this.paymentClass.RunCreateClient();
                    this.NotifyPropertyChanged("Clients");
                    Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "New Client Account Creation Process Completed!");
                }
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
            finally
            {
                this.BTNCreateClients = "Pending Clients";
                this.CHKClientSize = 0;
                this.ClientsReady = false;
            }
        }

        /// <summary>
        /// Manual process transactions button click event handler
        /// </summary>
        private void RunTransactions()
        {
            try
            {
                DateTime start = DateTime.Now;
                string result = "failed!";

                object[] outcome = paymentClass.ManualProcessTransactions(this.Transactions);

                if (Convert.ToBoolean(outcome[0]))
                {
                    result = "completed!";
                    // TODO bt - trashing the old ones?, so reset to way it was
                    this.Transactions = new TransactionDS();
                }
                else
                {
                    // TODO bt - trashing the old ones?, so reset to way it was
                    this.Transactions = this.paymentClass.UpdatedTransactions;
                }
                // this.Transactions = this.paymentClass.UpdatedTransactions;
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "Send Transactions " + result);
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
            finally
            {
                this.PaymentsReady = false;
            }
        }

        /// <summary>
        /// Retrieves the list of customer account debits that are scheduled within the specified date range
        /// </summary>
        private void ViewPendingTransactions()
        {
            try
            {
                if (this.DateSpan != 0)
                {
                    this.Transactions = paymentClass.RunViewPendingTransaction(this.StartDate, this.endDate);
                    if (this.Transactions.ClientDeposit.Count == 0)
                        Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "No transactions to process");
                }
                else
                    System.Windows.Forms.MessageBox.Show("Please select valid number of days");
            }
            catch (Exception ex)
            {
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
                this.WriteError(ex);
            }
            finally
            {
            }
        }

        /// <summary>
        /// Retrieves the list of customer account debits that are scheduled within the specified date range
        /// </summary>
        //private void CreateTransactions()
        //{
        //    try
        //    {
        //        this.Transactions = paymentClass.RunCreateTransaction(transactions, this.StartDate, this.endDate);
        //        if (this.Transactions != null && this.Transactions.ClientDeposit.Rows.Count > 0)
        //            Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "Create Transactions Completed!");               
        //    }
        //    catch (Exception ex)
        //    {
        //        this.PaymentsReady = false;
        //        Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
        //        this.WriteError(ex);
        //    }
        //    finally
        //    {
        //    }
        //}
        private void CreateTransactions()
        {
            try
            {
                TransactionDS tempDs = new TransactionDS();
                foreach (TransactionDS.ClientDepositRow row in this.Transactions.ClientDeposit)
                {
                    if (row.IsChecked.Equals(true))
                    {
                        TransactionDS.ClientDepositRow newRow = tempDs.ClientDeposit.NewClientDepositRow();
                        row.RowNumber = tempDs.ClientDeposit.Count + 1;
                        newRow = row;
                        tempDs.ClientDeposit.ImportRow(newRow);
                    }
                }

                if (tempDs.ClientDeposit.Count == 0)
                    System.Windows.Forms.MessageBox.Show("Please select atleast one record.");
                else
                {
                    transactions = tempDs;
                    this.Transactions = paymentClass.RunCreateTransaction(transactions, this.StartDate, this.endDate);
                    Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "Create Transactions Completed!");                   
                }
            }
            catch (Exception ex)
            {
                this.PaymentsReady = false;
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
                this.WriteError(ex);
            }
            finally
            {
                this.BTNCreateTxn = "Pending Transactions";
                this.CHKSize = 0;
                this.PaymentsReady = false;
            }
        }

        /// <summary>
        /// Retrieves the list of creditor payments that are scheduled within the specified date range
        /// </summary>
        private void CreatePayments()
        {
            try
            {
                this.Transactions = paymentClass.BuildPaymentList(this.StartDate, this.endDate);

                if (this.Transactions != null && this.Transactions.OfferPayment.Rows.Count > 0)
                {
                    this.PaymentsReady = true;
                }
                else
                {
                    Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "No creditor transactions to process");
                }
            }
            catch (Exception ex)
            {
                this.PaymentsReady = false;
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
                this.WriteError(ex);
            }
            finally
            {
            }
        }

        private void ProcessPayments()
        {
            try
            {
                DateTime start = DateTime.Now;
                string result = "failed!";

                object[] outcome = paymentClass.ManualProcessPayments(this.Transactions);

                if (Convert.ToBoolean(outcome[0]))
                {
                    result = "completed!";
                    // TODO bt - trashing the old ones?, so reset to way it was
                    // this.Transactions = new TransactionDS();
                }
                else
                {
                    // TODO bt - trashing the old ones?, so reset to way it was
                    // this.Transactions = this.paymentClass.UpdatedTransactions;
                }
                
                this.Transactions = this.paymentClass.UpdatedTransactions;
                this.NotifyPropertyChanged("TransactionsView");
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "Send Creditor transactions " + result);
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
            finally
            {
                this.PaymentsReady = false;
            }
        }

        private void ExportExcel(string option)
        {
            paymentClass.CreateExcle(this.TableName, this.Transactions, option);
        }

        private void UpdateClient()
        {
            try
            {
                DateTime start = DateTime.Now;
                paymentClass.RunClientUpdate();
                this.NotifyPropertyChanged("Clients");
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "Update Client Completed!");
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
        }

        private void UpdateTransactions()
        {
            try
            {
                paymentClass.RunUpdateTransaction(this.StartDate, this.endDate);
                this.Transactions = this.paymentClass.UpdatedTransactions;

                if (this.Transactions != null && this.Transactions.ClientDeposit.Rows.Count > 0)
                {
                    Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "Update Transactions Completed!");
                }
                else
                {
                    Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "No transactions to update");
                }
            }
            catch (Exception ex)
            {
                //this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
        }

        #endregion

    }
}
