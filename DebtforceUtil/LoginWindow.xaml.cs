﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using RAMSInterop;
using AuthorizeNet;
using FDIntrop;

namespace DebtforceUtil
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private delegate void AddMessageDelegate(string message);

        //private Thread mainThread = Thread.CurrentThread;

        //private AddMessageDelegate addMessageDelegate;


        public LoginWindow()
        {
           InitializeComponent();
           //this.DialogResult = false;
            //this.addMessageDelegate = new AddMessageDelegate("Successs");
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            errorSfLbl.Visibility = System.Windows.Visibility.Hidden;
            errorProcessorLbl.Visibility = System.Windows.Visibility.Hidden;

            if (String.IsNullOrEmpty(loginSfBox.Text) || (String.IsNullOrEmpty(passwordSfBox.Password)) || (String.IsNullOrEmpty(tokenSfBox.Text)))
            {
                errorSfLbl.Content = "Please fill all the Salesforce fields below";
                errorSfLbl.Visibility = System.Windows.Visibility.Visible;
            }
            switch (Processor_combox.SelectedItem.ToString())
            {
                case "RAMS":
                    {
                        if (String.IsNullOrEmpty(Processor_LoginBox.Text.Trim()))
                        {
                            errorProcessorLbl.Content = "Please fill unique vendor id";
                            errorProcessorLbl.Visibility = System.Windows.Visibility.Visible;
                        }
                        break;
                    }

                case "GCS":
                        if (String.IsNullOrEmpty(Processor_LoginBox.Text.Trim()) || String.IsNullOrEmpty(Processor_PasswordBox.Password.Trim()))
                        {
                            errorProcessorLbl.Content = "Please fill all the Payment Company fields above";
                            errorProcessorLbl.Visibility = System.Windows.Visibility.Visible;
                        }
                        break;
                case "EPPS":
                case "Authorize.Net":
                case "AffinityProgram":
                case "FirstData":
                    {
                        if (String.IsNullOrEmpty(Processor_LoginBox.Text.Trim()) || String.IsNullOrEmpty(Processor_PasswordBox.Password.Trim()))
                        {
                            errorProcessorLbl.Content = "Please fill all the Payment Company fields above";
                            errorProcessorLbl.Visibility = System.Windows.Visibility.Visible;
                        }
                        break;
                    }
                case "T2P-Hybrid":
                case "T2P-ProtectionProgram":
                case "T2P-Hybrid / T2P-ProtectionProgram":
                    {
                        //if (String.IsNullOrEmpty(Processor_LoginBox.Text.Trim()) || String.IsNullOrEmpty(Processor_PasswordBox.Password.Trim()) ||
                        //   (String.IsNullOrEmpty(Processor_LoginID_AuthorizeNet.Text.Trim())) || (String.IsNullOrEmpty(Processor_AuthNet_Key.Password.Trim())))
                        if (String.IsNullOrEmpty(Processor_LoginBox.Text.Trim()) || String.IsNullOrEmpty(Processor_PasswordBox.Password.Trim()))
                        {
                            errorProcessorLbl.Content = "Please fill all the Payment Company fields above";
                            errorProcessorLbl.Visibility = System.Windows.Visibility.Visible;
                        }
                        break;
                    }
            }

            if (errorProcessorLbl.Visibility == System.Windows.Visibility.Hidden && errorSfLbl.Visibility == System.Windows.Visibility.Hidden)
                processLogin();
        }
        private void processLogin()
        {
            Boolean success = true;
            errorSfLbl.Visibility = System.Windows.Visibility.Hidden;
            errorProcessorLbl.Visibility = System.Windows.Visibility.Hidden;
            try
            {
                SFInterop.SFInterop sf = new SFInterop.SFInterop(productionRB.IsChecked.Value);
                sf.Login(loginSfBox.Text.Trim(), passwordSfBox.Password.Trim(), tokenSfBox.Text.Trim());
                errorSfLbl.Visibility = System.Windows.Visibility.Visible;
                errorSfLbl.Content = "Connection to Salesforce successfull";
            }
            catch (Exception ex)
            {
                success = false;
                errorSfLbl.Content = ex.Message;
                errorSfLbl.Visibility = System.Windows.Visibility.Visible;
            }
            try
            {
                ProcessorLogin();
            }
            catch (Exception ex)
            {
                success = false;
                errorProcessorLbl.Content = ex.Message;
                
                errorProcessorLbl.Visibility = System.Windows.Visibility.Visible;
                //errorProcessorLbl.Foreground = new SolidColorBrush(Color.FromArgb(0, 0, 255, 0));
            }

            if (success)
                loginSucceeded();
            
        }

        public void ProcessorLogin()
        {
            try
            {
                switch (Processor_combox.SelectedItem.ToString())
                {
                    case "RAMS":
                        {
                            string vendorID = Processor_LoginBox.Text.Trim();
                            RAMS client = new RAMS(vendorID, productionRB.IsChecked.Value);
                            if (client.Login())
                            {
                                errorProcessorLbl.Visibility = System.Windows.Visibility.Visible;
                                errorProcessorLbl.Content = "Connection to Payment Company successful";
                                //errorProcessorLbl.Foreground = new SolidColorBrush(Color.FromArgb(0, 0, 255, 0));
                            }
                            else
                            {
                                throw new Exception("Please enter valid vendor id");
                            }
                            break;
                        }

                    case "GCS":
                        {
                            GCSInterop.GCS gcs = new GCSInterop.GCS(Processor_LoginBox.Text.Trim(), Processor_PasswordBox.Password.Trim(), productionRB.IsChecked.Value);
                            string res = gcs.Login();
                            if (res != "")
                                throw new Exception(res);
                            errorProcessorLbl.Visibility = System.Windows.Visibility.Visible;
                            errorProcessorLbl.Content = "Connection to Payment Company successful";
                            //errorProcessorLbl.Foreground = new SolidColorBrush(Color.FromArgb(100, 0, 255, 0));
                            break;
                        }
                    case "T2P-Hybrid":
                    case "T2P-ProtectionProgram":
                    case "T2P-Hybrid / T2P-ProtectionProgram":
                        {
                            GCSInterop.GCS gcs = new GCSInterop.GCS(Processor_LoginBox.Text.Trim(), Processor_PasswordBox.Password.Trim(), productionRB.IsChecked.Value);
                            string res = gcs.Login();
                            if (res != "")
                                throw new Exception(res);
                            errorProcessorLbl.Visibility = System.Windows.Visibility.Visible;
                            //errorProcessorLbl.Content = "Connection to Payment Company successful";
                            //errorProcessorLbl.Foreground = new SolidColorBrush(Color.FromArgb(100, 0, 255, 0));


                            //Test Authorize.Net Connection
                            if (!String.IsNullOrEmpty(Processor_LoginID_AuthorizeNet.Text) && !String.IsNullOrEmpty(Processor_AuthNet_Key.Password))
                            {
                                var request = new AuthorizationRequest("", "", 0M, "");
                                Gateway gateway = new Gateway(Processor_LoginID_AuthorizeNet.Text, Processor_AuthNet_Key.Password, Processor_combox.SelectedItem.ToString(), productionRB.IsChecked.Value);
                                var response = gateway.Send(request);
                                if (response.Message.ToUpper().Equals("AUTHENTICATION FAILED"))
                                {
                                    throw new Exception("Authorize.Net :" + response.Message);
                                }
                                else
                                { errorProcessorLbl.Content = "Connection to Payment Company successful"; }
                            }                            
                            break;
                        }
                    case "Authorize.Net":
                        {

                            var request = new AuthorizationRequest("", "", 0M, "");
                            Gateway gateway = new Gateway(Processor_LoginBox.Text, Processor_PasswordBox.Password, Processor_combox.SelectedItem.ToString(), productionRB.IsChecked.Value);
                            var response = gateway.Send(request);
                            if (response.Message.Equals("The merchant login ID or password is invalid or the account is inactive."))
                            { throw new Exception("Authorize.Net :" + response.Message); }
                            else
                            { errorProcessorLbl.Content = "Connection to Payment Company successful"; }
                            break;
                        }

                    case "FirstData":
                    case "AffinityProgram":
                        {
                            try
                            {
                                FDIntrop.Main fdMain = new FDIntrop.Main(Processor_LoginBox.Text, Processor_PasswordBox.Password, productionRB.IsChecked.Value);
                                fdMain.Login();    
                                errorProcessorLbl.Visibility = System.Windows.Visibility.Visible;
                                errorProcessorLbl.Content = "Connection to Payment Company successful";
                            }
                            catch (Exception)
                            {
                                throw new Exception("Invalid User ID and Password");
                            }                          
                            break;
                        }

                    case "EPPS":
                        {
                            EPPSInterop.EPPS EPPS = new EPPSInterop.EPPS(Processor_LoginBox.Text.Trim(), Processor_PasswordBox.Password.Trim());
                            EPPSInterop.EPPSService.CardHolderDetail Dt = new EPPSInterop.EPPSService.CardHolderDetail();
                            Dt.CardHolderID = "testid";
                            EPPSInterop.EPPSService.FindCardHolder CH = EPPS.RetrieveCardHolderById(Dt);
                            if (CH.Message == "Login Failed")
                            {
                                throw new Exception("Invalid User ID and Password");
                            }
                            else
                            {
                                errorProcessorLbl.Visibility = System.Windows.Visibility.Visible;
                                errorProcessorLbl.Content = "Connection to Payment Company successful";
                                //errorProcessorLbl.Foreground = new SolidColorBrush(Color.FromArgb(0, 0, 255, 0));
                            }

                            break;
                        }

                }
            }
            catch (Exception ex) { throw ex; }
        
        }

        private Boolean loginSucceeded()
        {
            this.DialogResult = true;
            this.Close();
            return true;
        }

        private void Processor_combox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string selection =  Processor_combox.SelectedItem.ToString();

            switch (selection)
            {
                case "RAMS":
                    {
                        lblProcessor_Username.Content = "Vendor Id";
                        Processor_LoginBox.Text = string.Empty;
                        lblProcessor_Password.Visibility = System.Windows.Visibility.Hidden;
                        Processor_PasswordBox.Visibility = System.Windows.Visibility.Hidden;
                        lblProcessor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Hidden;
                        lblProcessor_Key_AuthorizeNet.Visibility = System.Windows.Visibility.Hidden;
                        Processor_AuthNet_Key.Visibility = System.Windows.Visibility.Hidden;
                        Processor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Hidden;
                        Processor_PasswordBox.Password = "";
                        break;
                    }

                case "GCS":
                
                    {
                        lblProcessor_Username.Content = "GCS User Id";
                        Processor_LoginBox.Text = string.Empty;
                        lblProcessor_Password.Visibility = System.Windows.Visibility.Visible;
                        Processor_PasswordBox.Visibility = System.Windows.Visibility.Visible;
                        lblProcessor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Hidden;
                        lblProcessor_Key_AuthorizeNet.Visibility = System.Windows.Visibility.Hidden;
                        Processor_AuthNet_Key.Visibility = System.Windows.Visibility.Hidden;
                        Processor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Hidden;
                        Processor_PasswordBox.Password = "";
                        break;
                    }

                case "T2P-Hybrid":
                case "T2P-ProtectionProgram":
                case "T2P-Hybrid / T2P-ProtectionProgram":
                    {
                        lblProcessor_Username.Content = "T2P User Id";
                        Processor_LoginBox.Text = string.Empty;
                        lblProcessor_Password.Visibility = System.Windows.Visibility.Visible;
                        Processor_PasswordBox.Visibility = System.Windows.Visibility.Visible;
                        lblProcessor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Visible;
                        lblProcessor_Key_AuthorizeNet.Visibility = System.Windows.Visibility.Visible;
                        Processor_AuthNet_Key.Visibility = System.Windows.Visibility.Visible;
                        Processor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Visible;
                        Processor_AuthNet_Key.Password = "";
                        Processor_LoginID_AuthorizeNet.Text = "";
                        Processor_PasswordBox.Password = "";
                        break;
                    }

                case "EPPS":
                    {
                        lblProcessor_Username.Content = "EPPS User Id";
                        Processor_LoginBox.Text = string.Empty;
                        lblProcessor_Password.Visibility = System.Windows.Visibility.Visible;
                        Processor_PasswordBox.Visibility = System.Windows.Visibility.Visible;
                        Processor_PasswordBox.Password = "";

                        lblProcessor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Collapsed;
                        lblProcessor_Key_AuthorizeNet.Visibility = System.Windows.Visibility.Collapsed;
                        Processor_AuthNet_Key.Visibility = System.Windows.Visibility.Collapsed;
                        Processor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Collapsed;

                        break;
                    }
                case "Authorize.Net":
                    {
                        lblProcessor_Username.Content = "User Id";
                        Processor_LoginBox.Text = string.Empty;
                        lblProcessor_Password.Visibility = System.Windows.Visibility.Visible;
                        Processor_PasswordBox.Visibility = System.Windows.Visibility.Visible;
                        Processor_PasswordBox.Password = "";

                        lblProcessor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Collapsed;
                        lblProcessor_Key_AuthorizeNet.Visibility = System.Windows.Visibility.Collapsed;
                        Processor_AuthNet_Key.Visibility = System.Windows.Visibility.Collapsed;
                        Processor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Collapsed;

                        break;
                    }
                case "FirstData":
                case "AffinityProgram":
                    {
                        lblProcessor_Username.Content = "Exact ID";
                        Processor_LoginBox.Text = string.Empty;
                        lblProcessor_Password.Visibility = System.Windows.Visibility.Visible;
                        Processor_PasswordBox.Visibility = System.Windows.Visibility.Visible;
                        Processor_PasswordBox.Password = "";

                        lblProcessor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Collapsed;
                        lblProcessor_Key_AuthorizeNet.Visibility = System.Windows.Visibility.Collapsed;
                        Processor_AuthNet_Key.Visibility = System.Windows.Visibility.Collapsed;
                        Processor_LoginID_AuthorizeNet.Visibility = System.Windows.Visibility.Collapsed;

                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        private void Processor_combox_Loaded(object sender, RoutedEventArgs e)
        {
            List<string> processors = new List<string>();
            processors.Add("RAMS");
            processors.Add("GCS");
            processors.Add("EPPS");
            processors.Add("T2P-Hybrid / T2P-ProtectionProgram");
            processors.Add("AffinityProgram");
            processors.Add("Authorize.Net");
            processors.Add("FirstData");
            var comboBox = sender as ComboBox;
            comboBox.ItemsSource = processors;
            comboBox.SelectedIndex = 0;
        }

    }
}
