﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DebtforceUtil
{
    /// <summary>
    /// Interaction logic for GCSTransactionPage.xaml
    /// </summary>
    public partial class ProcessorTransactionPage : Page
    {
        public ProcessorTransactionPage(string Processor)
        {
            InitializeComponent();
            //Change the Grid Header as per Processor Selected
            var Listbox = this.FindName("listView1");
            GridView Gv = (System.Windows.Controls.GridView)((((System.Windows.Controls.ListView)(Listbox)).View));
            Gv.Columns[0].Header = Processor + " Acct#";
        }
    }
}
