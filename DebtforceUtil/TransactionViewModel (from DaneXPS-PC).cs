﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GalaSoft.MvvmLight;
using System.ComponentModel;
using DebtLibrary;
using SFDebt;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.Collections.ObjectModel;
using System.Net.Mail;
using System.Windows.Threading;
using System.Threading;

namespace DebtforceUtil
{
    public class TransactionViewModel : BaseViewModel
    {
        #region Fields

        /// <summary>
        /// The payment class instance
        /// </summary>
        private PaymentClass paymentClass = new PaymentClass("webuserdemo@selastech.com", "d3m0User", "SMNaP7wsaGOt99flXTqA2Z9k", "jduhaney", "23WEsd45RTfg");

        /// <summary>
        /// Stores the transactions and client info
        /// </summary>
        private TransactionDS transactions = new TransactionDS();

        /// <summary>
        /// Stores the transactions and client info
        /// </summary>
        private TransactionDS account = new TransactionDS();

        /// <summary>
        /// The start date for payment processing
        /// </summary>
        private DateTime startDate = new DateTime();

        /// <summary>
        /// The cutoff date for payment processing
        /// </summary>
        private DateTime endDate = new DateTime();

        /// <summary>
        /// The start date for transaction search
        /// </summary>
        private DateTime searchStartDate = new DateTime();

        /// <summary>
        /// The cutoff date for transaction search
        /// </summary>
        private DateTime searchEndDate = new DateTime();

        /// <summary>
        /// Flag activated whenever payments are ready
        /// </summary>
        private bool paymentsReady = false;

        /// <summary>
        /// Flag activated whenever all processes have finished running
        /// </summary>
        private bool isBusy = false;

        /// <summary>
        /// Flag activated whenever the update transaction is in progress
        /// </summary>
        private bool isUpdate = false;

        /// <summary>
        /// Flag activated whenever transaction view is visible
        /// </summary>
        private string visibleView = "Transactions";

        /// <summary>
        /// The account number to search for
        /// </summary>
        private string accountNumber = string.Empty;

        private BackgroundWorker worker = new BackgroundWorker();

        private delegate void AddMessageDelegate(string message);

        private Thread mainThread = Thread.CurrentThread;

        private AddMessageDelegate addMessageDelegate;

        #endregion

        public TransactionViewModel()
        {
            this.Load();
        }
        
        #region Properties

        public DateTime EndDate
        {
            get
            {
                return this.endDate;
            }

            set
            {
                this.endDate = value;
                this.NotifyPropertyChanged("EndDate");
            }
        }

        public DateTime StartDate
        {
            get
            {
                return this.startDate;
            }

            set
            {
                this.startDate = value;
                this.EndDate = this.StartDate.AddDays(this.DateSpan);
                this.NotifyPropertyChanged("StartDate");
            }
        }

        public DateTime SearchEndDate
        {
            get
            {
                return this.searchEndDate;
            }

            set
            {
                this.searchEndDate = value;
                this.NotifyPropertyChanged("SearchEndDate");
            }
        }

        public DateTime SearchStartDate
        {
            get
            {
                return this.searchStartDate;
            }

            set
            {
                this.searchStartDate = value;
                this.NotifyPropertyChanged("SearchStartDate");
            }
        }

        public TransactionDS Transactions
        {
            get
            {
                return this.transactions;
            }

            set
            {
                this.transactions = value;
                this.NotifyPropertyChanged("Transactions");
            }
        }

        public TransactionDS TransactionsView
        {
            get;
            set;
        }

        public TransactionDS Account
        {
            get
            {
                return this.account;
            }

            set
            {
                this.account = value;
                this.NotifyPropertyChanged("Account");
            }
        }

        public TransactionDS.ClientsRow ClientRow
        {
            get;
            set;
        }

        public TransactionDS.ClientsDataTable Clients
        {
            get
            {
                return this.paymentClass.CreatedClients;
            }
        }

        public ObservableCollection<string> Messages
        {
            get;
            set;
        }

        public ObservableCollection<int> IntegerList
        {
            get;
            set;
        }

        public ObservableCollection<DateTime> DateList
        {
            get;
            set;
        }

        public bool PaymentsReady
        {
            get
            {
                return this.paymentsReady;
            }

            set
            {
                this.paymentsReady = value;
                this.NotifyPropertyChanged("PaymentsReady");
            }
        }

        public bool IsUpdate
        {
            get
            {
                return this.isUpdate;
            }

            set
            {
                this.isUpdate = value;
                this.NotifyPropertyChanged("IsUpdate");
            }
        }

        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }

            set
            {
                this.isBusy = value;
                this.NotifyPropertyChanged("IsBusy");
            }
        }

        public string VisibleView
        {
            get
            {
                return this.visibleView;
            }

            set
            {
                this.visibleView = value;
                this.NotifyPropertyChanged("VisibleView");
            }
        }

        public string Version
        {
            get;
            set;
        }

        public string AccountNumber
        {
            get
            {
                return this.accountNumber;
            }

            set
            {
                this.accountNumber = value;
                this.NotifyPropertyChanged("AccountNumber");
            }
        }

        public int DateSpan
        {
            get
            {
                return this.paymentClass.NumberOfDaysAhead;
            }

            set
            {
                this.paymentClass.NumberOfDaysAhead = value;
                this.EndDate = this.StartDate.AddDays(value);
                this.NotifyPropertyChanged("DateSpan");
            }
        }
        
        #endregion 

        #region Commands

        public RelayCommand CreateClientCommand
        {
            get;
            set;
        }

        public RelayCommand CreateTxCommand
        {
            get;
            set;
        }

        public RelayCommand UpdateClientCommand
        {
            get;
            set;
        }

        public RelayCommand UpdateTxCommand
        {
            get;
            set;
        }

        public RelayCommand RunTxCommand
        {
            get;
            set;
        }

        public RelayCommand ViewGCSTxCommand
        {
            get;
            set;
        }

        public RelayCommand ViewGCSAccountsCommand
        {
            get;
            set;
        }

        public RelayCommand PullGCSTxCommand
        {
            get;
            set;
        }

        public RelayCommand PullGCSAccountsCommand
        {
            get;
            set;
        }

        #endregion 

        #region View Specific Functions

        private void AddMessage(string message)
        {
            this.Messages.Add(DateTime.Now.ToShortTimeString() + " - " + message);
        }

        private void Load()
        {
            this.addMessageDelegate = this.AddMessage;
            this.Version = "1.1";
            this.Messages = new ObservableCollection<string>();

            this.CreateClientCommand = new RelayCommand(this.CreateClientClick, this.CanExecute);
            this.CreateTxCommand = new RelayCommand(this.CreateTransactionsClick, this.CanExecute);
            this.UpdateClientCommand = new RelayCommand(this.UpdateClientClick, this.CanExecute);
            this.UpdateTxCommand = new RelayCommand(this.UpdateTransactionsClick, this.CanExecute);
            this.RunTxCommand = new RelayCommand(this.RunTransactionsClick);
            this.ViewGCSTxCommand = new RelayCommand(this.ViewGCSTransactionsClick, this.CanExecute);
            this.ViewGCSAccountsCommand = new RelayCommand(this.ViewGCSAccountClick, this.CanExecute);
            this.PullGCSAccountsCommand = new RelayCommand(this.PullGCSAccountClick, this.CanExecute);
            this.PullGCSTxCommand = new RelayCommand(this.PullGCSTransactionsClick, this.CanExecute);

            this.DateList = new ObservableCollection<DateTime>();
            this.IntegerList = new ObservableCollection<int>();

            for (int i = 0; i < 15; i++)
            {
                this.IntegerList.Add(i);
            }

            for (int i = 2; i >= 0; i--)
            {
                this.DateList.Add(DateTime.Today.AddDays(-i));
            }

            this.StartDate = DateTime.Today;
            this.EndDate = DateTime.Today;

            this.DateSpan = this.paymentClass.NumberOfDaysAhead;
            this.PaymentsReady = false;
            this.IsUpdate = false;

            this.worker.DoWork += new DoWorkEventHandler(this.Worker_DoWork);
            this.worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Worker_RunWorkerCompleted);
        }

        #endregion

        #region Background Worker Related Functions

        private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.CreateClientCommand.RaiseCanExecuteChanged();
            this.CreateTxCommand.RaiseCanExecuteChanged();
            this.UpdateClientCommand.RaiseCanExecuteChanged();
            this.UpdateTxCommand.RaiseCanExecuteChanged();
            this.ViewGCSAccountsCommand.RaiseCanExecuteChanged();
            this.ViewGCSTxCommand.RaiseCanExecuteChanged();
        }

        private void Worker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Argument != null)
            {
                switch (e.Argument.ToString())
                {
                    case "UpdateClient":
                        this.UpdateClient();
                        break;
                    case "CreateTransactions":
                        this.CreateTransactions();
                        break;
                    case "UpdateTransactions":
                        this.IsUpdate = true;
                        this.UpdateTransactions();
                        break;
                    case "RunTransactions":
                        this.RunTransactions();
                        break;
                    case "CreateClient":
                        this.CreateClient();
                        break;
                    case "ViewGCSTransactions":
                        this.ViewGCSTransactions();
                        break;
                    case "ViewGCSAccount":
                        this.ViewGCSAccount();
                        break;
                }
            }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Writes any errors that failed to write to SalesForce to a text file with the date stamp
        /// </summary>
        /// <param name="ex">The System.Exception that occurred</param>
        private void WriteError(Exception ex)
        {
            try
            {
                MailMessage mm = new MailMessage();
                mm.To.Add("selasrequest@gmail.com");
                mm.To.Add("jduhaney@selastech.com");
                mm.To.Add("jburns@selastech.com");
                mm.To.Add("kgunn@selastech.com");
                mm.Body = "An API error occurred on " + DateTime.Now.ToShortDateString() + " at " +
                        DateTime.Now.ToShortTimeString() + " \r\n\r\nError Message: " + ex.Message +
                        "\r\n\r\nStack Trace: " + ex.StackTrace;
                mm.From = new MailAddress("selasrequest@gmail.com");
                mm.Subject = "Critical error occurred with Selas Payment System";

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.Credentials = new System.Net.NetworkCredential("selasrequest@gmail.com", "big1daddy2");
                client.Send(mm);
            }
            catch (Exception ex2)
            {
                DebtLibrary.PaymentClass paymentClass = new DebtLibrary.PaymentClass();
                paymentClass.WriteError(ex2);
                paymentClass.WriteError(ex);
            }
        }

        private void RunWorker(string workerType)
        {
            this.IsUpdate = false;
            this.worker.RunWorkerAsync(workerType);

            this.CreateClientCommand.RaiseCanExecuteChanged();
            this.CreateTxCommand.RaiseCanExecuteChanged();
            this.UpdateClientCommand.RaiseCanExecuteChanged();
            this.UpdateTxCommand.RaiseCanExecuteChanged();
            this.ViewGCSAccountsCommand.RaiseCanExecuteChanged();
            this.ViewGCSTxCommand.RaiseCanExecuteChanged();
        }

        private void PullGCSAccountClick()
        {
            this.VisibleView = "GCSAccount";
            this.RunWorker("ViewGCSAccount");
        }

        private void PullGCSTransactionsClick()
        {
            this.VisibleView = "GCSTransactions";
            this.RunWorker("ViewGCSTransactions");
        }

        private void ViewGCSAccountClick()
        {
            this.VisibleView = "GCSAccount";
        }

        private void ViewGCSTransactionsClick()
        {
            this.VisibleView = "GCSTransactions";
        }

        private void CreateClientClick()
        {
            this.VisibleView = "Client";
            this.RunWorker("CreateClient");
        }

        private void CreateTransactionsClick()
        {
            this.VisibleView = "Transactions";
            this.RunWorker("CreateTransactions");
        }

        private void UpdateClientClick()
        {
            this.VisibleView = "Client";
            this.RunWorker("UpdateClient");
        }

        private void UpdateTransactionsClick()
        {
            this.VisibleView = "Transactions";
            this.RunWorker("UpdateTransactions");
        }

        private void RunTransactionsClick()
        {
            this.VisibleView = "Transactions";
            this.RunWorker("RunTransactions");
        }

        private bool CanExecute()
        {
            this.IsBusy = this.worker.IsBusy;
            return !this.isBusy;
        }

        private void ViewGCSAccount()
        {
            try
            {
                this.ClientRow = null;
                this.account = null;

                this.paymentClass.CheckClientAccount(this.accountNumber, out this.account);

                if (this.account != null && this.account.Clients != null && this.account.Clients.Rows.Count > 0)
                {
                    this.ClientRow = this.account.Clients.Rows[0] as TransactionDS.ClientsRow;
                }

                this.NotifyPropertyChanged("ClientRow");
                this.NotifyPropertyChanged("Account");
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
            }
        }

        private void ViewGCSTransactions()
        {
            try
            {
                this.TransactionsView = this.paymentClass.GetTransactions(this.searchStartDate, this.searchEndDate);
                this.NotifyPropertyChanged("TransactionsView");
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
        }

        private void CreateClient()
        {
            try
            {
                DateTime start = DateTime.Now;
                this.paymentClass.RunCreateClient();
                this.NotifyPropertyChanged("Clients");
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "New Client Account Creation Process Completed!");
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
        }

        /// <summary>
        /// Manual process transactions button click event handler
        /// </summary>
        private void RunTransactions()
        {
            try
            {
                DateTime start = DateTime.Now;
                string result = "failed!!";

                object[] outcome = paymentClass.ManualProcessTransactions(this.Transactions);

                if (Convert.ToBoolean(outcome[0]))
                {
                    result = "completed!!";
                }

                this.Transactions = this.paymentClass.UpdatedTransactions;
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "Queue payments " + result);
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
            finally
            {
                this.PaymentsReady = false;
            }
        }

        private void CreateTransactions()
        {
            try
            {
                this.Transactions = paymentClass.ManualCreateTransactions(this.StartDate, this.endDate);

                if (this.Transactions != null && this.Transactions.ClientDeposit.Rows.Count > 0)
                {
                    this.PaymentsReady = true;
                }
                else
                {
                    Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "No payments to process");
                }
            }
            catch (Exception ex)
            {
                this.PaymentsReady = false;
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
                this.WriteError(ex);
            }
            finally
            {
            }
        }

        private void UpdateClient()
        {
            try
            {
                DateTime start = DateTime.Now;
                paymentClass.RunClientUpdate();
                this.NotifyPropertyChanged("Clients");
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "Update Client Completed!");
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
        }

        private void UpdateTransactions()
        {
            try
            {
                DateTime start = DateTime.Now;
                paymentClass.CheckTransactions(start);
                this.Transactions = this.paymentClass.UpdatedTransactions;
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, "Update Transactions Completed!");
            }
            catch (Exception ex)
            {
                this.WriteError(ex);
                Dispatcher.FromThread(this.mainThread).Invoke(this.addMessageDelegate, ex.Message);
            }
        }

        #endregion

    }
}
