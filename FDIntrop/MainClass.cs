﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FDIntrop
{
    public class Main
    {
        #region Field
        /// <summary>   
        /// FDGG User ID
        /// </summary>
        private string exactID = string.Empty;

        /// <summary>
        /// FDGG password
        /// </summary>
        private string password = string.Empty;
        private Boolean liveEnvironment = false;
        private string cardHoldersName;
        private string cardNumber;
        private double amount;
        private string expiryDate;

        private string status;
        private bool transactionApproved;
        private bool TransactioError;
        private string message;
        private string transactionId;
       
        #endregion

        #region Property

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public bool TransactionApproved
        {
            get { return transactionApproved; }
            set { transactionApproved = value; }
        }

        public bool TransactioError1
        {
            get { return TransactioError; }
            set { TransactioError = value; }
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public string TransactionId
        {
            get { return transactionId; }
            set { transactionId = value; }
        }

        public string CardNumber
        {
            get { return cardNumber; }
            set { cardNumber = value; }
        }       

        public string CardHoldersName
        {
            get { return cardHoldersName; }
            set { cardHoldersName = value; }
        }       

        public double Amount
        {
            get { return amount; }
            set { amount = value; }
        }       

        public string ExpiryDate
        {
            get { return expiryDate; }
            set { expiryDate = value; }
        }


        #endregion

        #region Constructor

        public Main(string exactID, string password, Boolean liveEnv)
        {
            this.exactID = exactID;
            this.password = password;
            this.liveEnvironment = liveEnv;
        }

        #endregion

        #region Methods
       
        /// <summary>
        /// Perpose -> Method to login on Firsdata 
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>july/28/2014</createdDate>
        /// </summary>
        /// <param name="exactID"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public void Login()
        {
            try
            {
                dynamic txn;
                dynamic ws;
                dynamic result;
                if (this.liveEnvironment.Equals(true))
                {
                    ws = new FDGGProd.ServiceSoapClient();
                    txn = new FDGGProd.Transaction();
                }
                else
                {
                    ws = new FDGG.ServiceSoapClient();
                    txn = new FDGG.Transaction();
                }
                    // set correct credential values
                txn.ExactID = this.exactID;
                txn.Password = this.password;
                    txn.Transaction_Type = "00";
                    result = ws.SendAndCommit(txn);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Perpose -> Method to create transaction on FirstData 
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>july/28/2014</createdDate>
        /// </summary>
        /// <returns></returns>
        public void CreateTransaction(ref Main objInput)
        {
            dynamic txn;
            dynamic ws;
            dynamic result;
            try
            {
                if (this.liveEnvironment.Equals(true))
                {
                    ws = new FDGGProd.ServiceSoapClient();
                    txn = new FDGGProd.Transaction();
                }
                else
                {
                    ws = new FDGG.ServiceSoapClient();
                    txn = new FDGG.Transaction();
                }
                

                // set correct credential values
                txn.ExactID = exactID;
                txn.Password = password;
                txn.Transaction_Type = "00";
                txn.Card_Number = objInput.CardNumber;
                txn.CardHoldersName = objInput.CardHoldersName;
                txn.DollarAmount = Convert.ToString(objInput.Amount);
                txn.Expiry_Date = objInput.ExpiryDate;
                result = ws.SendAndCommit(txn);

                if (!result.Bank_Message.Equals(""))
                {
                    status = result.Bank_Message;
                    transactionId = result.Retrieval_Ref_No;
                }
                else
                {
                    status = string.Empty;
                }
                TransactioError = result.Transaction_Error;
                transactionApproved = result.Transaction_Approved;
                message = result.EXact_Message;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        #endregion
    }
}