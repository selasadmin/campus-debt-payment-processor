﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SFInterop.SForce;

namespace SFInterop
{
    public class SFInterop
    {
       
        #region Fields

        private SForce.SforceService _sForceService;
        private Boolean _isValidSession = false;
        private String _mode = string.Empty;
        private Boolean prodEnvironment = false;

        #endregion

        #region Constructors & Destructors

        /// <summary>
        /// Creates a new instance of the SFInterop class
        /// </summary>
        public SFInterop(Boolean prod)
        {
            this._sForceService = new SForce.SforceService();
            this.prodEnvironment = prod;
            this._sForceService.Timeout = 5 * 60 * 1000;

            // Little bit of hack here.  The Prod  WSDL is used; however we use a URL fixup from 
            // the SFInterop properties to fix it up.
            if (prod)
                this._sForceService.Url = global::SFInterop.Properties.Settings.Default.SFInterop_SForce_SforceServiceProd;
            else
                this._sForceService.Url = global::SFInterop.Properties.Settings.Default.SFInterop_SForce_SforceServiceTest;

        }

        /// <summary>
        /// Creates a new instance of the SFInterop class and logs in with the supplied credentials
        /// </summary>
        /// <param name="uName">The salesForce username</param>
        /// <param name="pwd">The salesForce password</param>
        /// <param name="token">The salesForce security token</param>
        public SFInterop(string uName, string pwd, string token, Boolean prod)
        {
            this._sForceService = new SForce.SforceService();
            this.prodEnvironment = prod;
            this._sForceService.Timeout = 5 * 60 * 1000;
            // Little bit of hack here.  The Prod  WSDL is used; however we use a URL fixup from 
            // the SFInterop properties to fix it up.
            if (prod)
                this._sForceService.Url = global::SFInterop.Properties.Settings.Default.SFInterop_SForce_SforceServiceProd;
            else
                this._sForceService.Url = global::SFInterop.Properties.Settings.Default.SFInterop_SForce_SforceServiceTest;
                        
            this.Login(uName, pwd, token);
        }

        /// <summary>
        /// Destructor for the SFInterop class
        /// </summary>
        ~SFInterop()
        {
            // before destructing, ensure that the connection with SalesForce is ended
            if (this._isValidSession)
            {
                this.Logout();
            }
        }
        
        #endregion

        #region Properties

        public bool IsValidSession
        {
            get { return this._isValidSession; }
            set { this._isValidSession = value; }
        }

        public string SessionID
        {
            get;
            set;
        }

        public string URL
        {
            get;
            set;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Logs into SalesForce
        /// </summary>
        /// <param name="username">The SalesForce User ID</param>
        /// <param name="password">The SalesForce password</param>
        /// <param name="token">The SalesForce security token</param>
        /// <exception cref="System.Exception">Thrown when login fails or password expired</exception>
        public Boolean Login(string username, string password, string token)
        {
            try
            {
                LoginResult lr = this._sForceService.login(username, password + token);

                if (lr.passwordExpired)
                {
                    throw new Exception("Password Expired");
                }
                else if (lr.sandbox)
                {
                    this._mode = "SandBox";
                }

                
                this._sForceService.Url = lr.serverUrl;
                this._sForceService.SessionHeaderValue = new SessionHeader();
                this._sForceService.SessionHeaderValue.sessionId = lr.sessionId;
                this._sForceService.Timeout = 5 * 60 * 1000;    // 5 minutes

                this.SessionID = lr.sessionId;
                this.URL = lr.serverUrl;

                this._isValidSession = true;
            }
            catch
            {
                this._isValidSession = false;
                throw;
            }
            return true;
        }

        /// <summary>
        /// Safely logs out of sales force
        /// </summary>
        public void Logout()
        {
            try
            {
                if (this._isValidSession)
                {
                    this._sForceService.logout();
                    this._isValidSession = false;
                }
            }
            catch { this.IsValidSession = false; }
        }

        /// <summary>
        /// Runs a SOQL query on the active sales force service
        /// </summary>
        /// <param name="query">The query string</param>
        /// <returns>The query results</returns>
        public QueryResult RunQuery(string query)
        {
            QueryResult qrt = null;

            try
            {
                if (!this._isValidSession)
                {
                    throw new Exception("Invalid Session");
                }
                else
                {
                    qrt = _sForceService.query(query);
                }
            }
            catch
            {
                throw;
            }

            return qrt;
        }

        /// <summary>
        /// Runs a SOQL query on the active sales force service
        /// </summary>
        /// <param name="query">The query string</param>
        /// <returns>The query results</returns>
        public QueryResult GetNextBatch(QueryResult qr)
        {
            QueryResult qrt = null;

            try
            {
                if (!this._isValidSession)
                {
                    throw new Exception("Invalid Session");
                }
                else
                {
                    qrt = _sForceService.queryMore(qr.queryLocator);
                }
            }
            catch
            {
                throw;
            }

            return qrt;
        }

        /// <summary>
        /// Runs an update query on a list of sObjects
        /// </summary>
        /// <param name="listToUpdate">The list to update</param>
        /// <returns>True if successful and false otherwise</returns>
        public SaveResult[] UpdateBatch(List<sObject> listToUpdate)
        {
            if (this._isValidSession)
            {
                List<SaveResult> results = new List<SaveResult>();

                foreach (sObject[] batch in this.MakeBatches(listToUpdate, 50))
                {
                    foreach (SaveResult sr in this._sForceService.update(batch))
                    {
                        results.Add(sr);
                    }
                }

                return results.ToArray();
            }
            else
            {
                throw new Exception("Invalid Session");
            }
        }

        /// <summary>
        /// Breaks up a list of sObjects into multiple lists of fixed size batches to prevent timeouts.
        /// </summary>
        /// <param name="listToUpdate">The list of all sObjects</param>
        /// <param name="batchSize">The batch size</param>
        /// <returns>The group of batches</returns>
        private List<sObject[]> MakeBatches(List<sObject> listToUpdate, int batchSize)
        {
            List<sObject[]> batches = new List<sObject[]>();

            for (int i = 0; i < listToUpdate.Count; )
            {
                sObject[] batch = new sObject[batchSize];

                for (int j = 0; i < listToUpdate.Count && j < batchSize; j++, i++)
                {
                    batch[j] = listToUpdate[i];
                }

                batches.Add(batch);
            }

            return batches;
        }

        /// <summary>
        /// Runs a delete query on a list of sObjects
        /// </summary>
        /// <param name="listToUpdate">The list to delete</param>
        /// <returns>True if successful and false otherwise</returns>
        public DeleteResult[] DeleteBatch(List<string> listToUpdate)
        {
            if (this._isValidSession)
            {
                return this._sForceService.delete(listToUpdate.ToArray());
            }
            else
            {
                throw new Exception("Invalid Session");
            }
        }

        /// <summary>
        /// Runs an upsert query on a list of sObjects
        /// </summary>
        /// <param name="listToUpdate">The list to update</param>
        /// <returns>True if successful and false otherwise</returns>
        public bool UpsertBatch(List<sObject> listToUpdate)
        {
            return true;
        }

        /// <summary>
        /// Runs an insert query on a list of sObjects
        /// </summary>
        /// <param name="listToUpdate">The list to insert</param>
        /// <returns>True if successful and false otherwise</returns>
        public SaveResult[] InsertBatch(List<sObject> listToUpdate)
        {
            if (this._isValidSession)
            {
                List<SaveResult> results = new List<SaveResult>();

                foreach (sObject[] batch in this.MakeBatches(listToUpdate, 50))
                {
                    foreach (SaveResult sr in this._sForceService.create(batch))
                    {
                        results.Add(sr);
                    }
                }

                return results.ToArray();
            }
            else
            {
                throw new Exception("Invalid Session");
            }
        }

        #endregion
    }
}