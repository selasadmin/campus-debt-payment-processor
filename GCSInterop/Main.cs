﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
// using GcsApi = GCSInterop.GCSTest;      // Test System
using GcsApi = GCSInterop.GCSProd;   // Live System

namespace GCSInterop
{

    public class GCS
    {
        /// <summary>
        /// GCS Username
        /// </summary>
        private string userName = string.Empty;

        /// <summary>
        /// GCS password
        /// </summary>
        private string password = string.Empty;

        /// <summary>
        /// GCS live environment ? if no then test environment
        /// </summary>
        private Boolean prodEnvironment = false;

        /// <summary>
        /// Stores the list of active clients in the GCS system
        /// </summary>
        private GcsApi.ClientsWSDS clientsList = null;

        /// <summary>
        /// Stores a list of available account numbers that can be used for 
        /// new client accounts
        /// </summary>
        private List<string> availableAcctNums = new List<string>();

        /// <summary>
        /// The maximum number of new account numbers to retrieve at any given time
        /// </summary>
        private const int numAcctNumsToRetrieve = 25;

        /// <summary>
        /// The maximum number of row count 
        /// </summary>
        public int rowCount = 5000;

        /// <summary>
        /// Constructor for the GCS class
        /// </summary>
        /// <param name="uName">The username to use</param>
        /// <param name="pwd">The password to use</param>
        public GCS(string uName, string pwd, Boolean prod)
        {
            this.userName = uName;
            this.password = pwd;
            this.prodEnvironment = prod;
        }

        public GcsApi.DebitsWSDS DebitResults { get; set; }

        public GcsApi.PaymentsWSDS PaymentResults { get; set; }

        /// <summary>
        /// Creates GCS transaction records for all payments from the client and to creditors
        /// </summary>
        /// <param name="debits">The debit transactions</param>
        /// <param name="payments">The creditor payment transactions</param>
        /// <returns>True if all were executed successfully</returns>
        public bool SetTransactions(GcsApi.DebitsWSDS debits, GcsApi.PaymentsWSDS payments)
        {
            try
            {
                GcsApi.WebServices client = this.GetSoapClient();

                this.DebitResults = new GcsApi.DebitsWSDS();
                this.PaymentResults = new GcsApi.PaymentsWSDS();

                // update the transaction set and store any errors
                if (debits != null && debits.DEBITS.Rows.Count > 0)
                {
                    for (int i = 0; i < debits.DEBITS.Rows.Count; )
                    {
                        GcsApi.DebitsWSDS debitsTemp = new GcsApi.DebitsWSDS();

                        for (int j = 0; j < 150 && i < debits.DEBITS.Rows.Count; j++, i++)
                        {
                            if ((debits.DEBITS.Rows[i] as GcsApi.DebitsWSDS.DEBITSRow).DEBIT_AMOUNT > 0)
                                debitsTemp.DEBITS.ImportRow(debits.DEBITS.Rows[i]);
                        }

                        if (debitsTemp.DEBITS.Rows.Count > 0)
                        {
                            debitsTemp = client.DraftsSetADO(this.userName, this.password, debitsTemp);

                            this.DebitResults.Merge(debitsTemp);
                        }
                    }
                }

                // process creditor payments and store any errors
                if (payments != null && payments.PAYMENTS.Rows.Count > 0)
                {
                    /*GcsApi.PaymentsWSDS paymentsTemp = new GcsApi.PaymentsWSDS();

                    for (int i = 0; i < payments.PAYMENTS.Rows.Count; i++)
                    {
                        if ((payments.PAYMENTS.Rows[i] as GcsApi.PaymentsWSDS.PAYMENTSRow).PAYMENT_AMOUNT > 0)
                            paymentsTemp.PAYMENTS.ImportRow(payments.PAYMENTS.Rows[i]);
                    }
                    if (paymentsTemp.PAYMENTS.Rows.Count > 0)
                    {
                        payments = client.PaymentsSetADO(this.userName, this.password, payments);
                    }
                    this.PaymentResults.Merge(payments);*/
                    payments = client.PaymentsSetADO(this.userName, this.password, payments);
                    this.PaymentResults = payments;
                }

                return true;
            }
            catch
            {
                throw;
            }

        }


        public bool T2PTransaction(GcsApi.DebitsWSDS debits)
        {

            GcsApi.WebServices client = this.GetSoapClient();
            try
            {
                if (debits != null && debits.DEBITS.Rows.Count > 0)
                {
                    for (int i = 0; i < debits.DEBITS.Rows.Count; )
                    {
                        GcsApi.DebitsWSDS debitsTemp = new GcsApi.DebitsWSDS();

                        for (int j = 0; j < 150 && i < debits.DEBITS.Rows.Count; j++, i++)
                        {
                            if ((debits.DEBITS.Rows[i] as GcsApi.DebitsWSDS.DEBITSRow).DEBIT_AMOUNT > 0)
                                debitsTemp.DEBITS.ImportRow(debits.DEBITS.Rows[i]);
                        }

                        if (debitsTemp.DEBITS.Rows.Count > 0)
                        {
                            debitsTemp = client.DraftsSetADO(this.userName, this.password, debitsTemp);

                            this.DebitResults.Merge(debitsTemp);
                        }
                    }
                }
                return true;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public GcsApi.DebitsWSDS SetDraft(GcsApi.DebitsWSDS debits)
        {

            GcsApi.WebServices client = this.GetSoapClient();
            try
            {
                debits = client.DraftsSetADO(this.userName, this.password, debits);
                return debits;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public System.Xml.XmlNode SetDraftXML(System.Xml.XmlNode debits)
        {

            GcsApi.WebServices client = this.GetSoapClient();
            try
            {
                debits = client.DraftsSetXML(this.userName, this.password, debits);
                return debits;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public bool SetT2PTransactions(GcsApi.DebitsWSDS debits, GcsApi.PaymentsWSDS payments)
        {
            try
            {
                GcsApi.WebServices client = this.GetSoapClient();
                // process creditor payments and store any errors
                if (payments != null && payments.PAYMENTS.Rows.Count > 0)
                {
                    payments = client.PaymentsSetADO(this.userName, this.password, payments);
                    this.PaymentResults = payments;
                }

                return true;
            }
            catch
            {
                throw;
            }

        }

        public GcsApi.DebitsWSDS GetDebits(DateTime start)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.DraftsGetADO(this.userName, this.password, null, missing, missing, missing, null, missing,
                                                null, null, missing, missing, start.ToString(), missing, missing, missing,
                                                missing, missing, missing,
                                                null, null, missing, missing, missing, this.rowCount, null,
                                                ref rowCount, ref pageCount);
        }

        public GcsApi.DebitsWSDS GetDebits(DateTime startDate, DateTime endDate)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.DraftsGetADO(this.userName, this.password, null, missing, startDate.ToString(), endDate.ToString(), null, missing,
                                                null, null, missing, missing, missing, missing, missing, missing,
                                                missing, missing, missing,
                                                null, null, missing, missing, missing, this.rowCount, null,
                                                ref rowCount, ref pageCount);
        }

        public System.Xml.XmlNode GetDebitsXML(DateTime start)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.DraftsGetXML(this.userName, this.password, null, missing, missing, missing, null, missing,
                                                null, null, missing, missing, start.ToString(), missing, missing, missing,
                                                missing, missing, missing,
                                                null, null, missing, missing, missing, null, null,
                                                ref rowCount, ref pageCount);
        }

        /// <summary>
        /// Retrieves a debit transaction based on its debit ID
        /// </summary>
        /// <param name="debitID">The unique ID assigned to the debit</param>
        /// <returns>The debit record</returns>
        public GcsApi.DebitsWSDS GetDebitsByID(decimal debitID)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.DraftsGetADO(this.userName, this.password, debitID, missing, missing, missing, null, missing,
                                                null, null, missing, missing, missing, missing, missing, missing,
                                                missing, missing, missing,
                                                null, null, missing, missing, missing, null, null,
                                                ref rowCount, ref pageCount);
        }

        /// <summary>
        /// Retrieves all debit transaction based on the client's account number
        /// </summary>
        /// <param name="debitID">The unique ID assigned to the client's account</param>
        /// <returns>The debit records</returns>
        public GcsApi.DebitsWSDS GetDebitsByAccountID(string accountID)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.DraftsGetADO(this.userName, this.password, null, accountID, missing, missing, null, missing,
                                                null, null, missing, missing, missing, missing, missing, missing,
                                                missing, missing, missing,
                                                null, null, missing, missing, missing, null, null,
                                                ref rowCount, ref pageCount);
        }

        /// <summary>
        /// Retrieves all debit transaction based on the client's account number
        /// </summary>
        /// <param name="debitID">The unique ID assigned to the client's account</param>
        /// <returns>The debit records</returns>
        public GcsApi.DebitsWSDS GetDebitsByTransactionID(string transactionID)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.DraftsGetADO(this.userName, this.password, null, missing, missing, missing, null, missing,
                                                null, null, missing, missing, missing, missing, missing, missing,
                                                transactionID, missing, missing,
                                                null, null, missing, missing, missing, null, null,
                                                ref rowCount, ref pageCount);
        }

        /// <summary>
        /// Retrieves all debit transaction based on the client's account number
        /// </summary>
        /// <param name="debitID">The unique ID assigned to the client's account</param>
        /// <returns>The debit records</returns>
        public GcsApi.DebitsWSDS GetDebitsByEffDateAmountDayAcctNB(string accountID, int day, DateTime date, decimal amount)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.DraftsGetADO(this.userName, this.password, null, accountID, date.ToString(), date.ToString(), day, missing,
                                                amount, amount, missing, missing, missing, missing, missing, missing,
                                                missing, missing, missing,
                                                null, null, missing, missing, missing, null, null,
                                                ref rowCount, ref pageCount);
        }

        /// <summary>
        /// Retrieves a debit transaction based on its debit ID
        /// </summary>
        /// <param name="debitID">The unique ID assigned to the debit</param>
        /// <returns>The debit record</returns>
        public GcsApi.DebitsWSDS GetFeesByID(decimal debitID)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.FeesGetADO(this.userName, this.password, debitID, missing, missing, missing, null, missing,
                                                null, null, missing, missing, missing, missing, missing, missing,
                                                missing, missing, missing,
                                                null, null, missing, missing, missing, this.rowCount, null,
                                                ref rowCount, ref pageCount);
        }
        public GcsApi.DebitsWSDS SetFees(GcsApi.DebitsWSDS fees)
        {
            GcsApi.WebServices client = this.GetSoapClient();

            return client.FeesSetADO(this.userName, this.password, fees);
        }

        /// <summary>
        /// Retrieves all fees based on the client's account number
        /// </summary>
        /// <param name="debitID">The unique ID assigned to the client's account</param>
        /// <returns>The debit records</returns>
        public GcsApi.DebitsWSDS GetFeesByAccountID(string accountID)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.FeesGetADO(this.userName, this.password, null, accountID, missing, missing, null, missing,
                                                null, null, missing, missing, missing, missing, missing, missing,
                                                missing, missing, missing,
                                                null, null, missing, missing, missing, null, null,
                                                ref rowCount, ref pageCount);
        }

        public GcsApi.DepositsWSDS GetDeposits(string depositID)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.DepositsGetADO(this.userName, this.password, depositID, missing, null, missing, missing, missing,
                                                missing, missing, missing, missing, missing, missing, missing,
                                                missing, missing, missing,
                                                missing, missing, missing, missing, missing, null, null,
                                                ref rowCount, ref pageCount);
        }

        public GcsApi.DepositsWSDS GetDepositsByAccount(string accountID)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.DepositsGetADO(this.userName, this.password, missing, accountID, null, missing, missing, missing,
                                                missing, missing, missing, missing, missing, missing, missing,
                                                missing, missing, missing,
                                                missing, missing, missing, missing, missing, null, null,
                                                ref rowCount, ref pageCount);
        }

        public GcsApi.DepositsWSDS GetDeposits(DateTime start)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.DepositsGetADO(this.userName, this.password, missing, missing, null, missing, missing, missing,
                                                missing, missing, missing, missing, missing, missing, missing,
                                                start.ToString(), missing, missing,
                                                missing, missing, missing, missing, missing, null, null,
                                                ref rowCount, ref pageCount);
        }

        public GcsApi.NsfStatusWSDS GetNSFReasons()
        {
            return this.GetSoapClient().NsfStatusGetADO(this.userName, this.password);
        }

        public GcsApi.TransactionsWSDS GetTransactions(DateTime start)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.TransactionsGetADO(this.userName, this.password, null, missing, missing, missing, missing, missing,
                                                missing, missing, null, missing, missing, missing, null, null, null,
                                                missing, missing, missing,
                                                missing, start.ToString(), missing, missing, null, null, this.rowCount, null,
                                                ref rowCount, ref pageCount);
        }

        public GcsApi.TransactionsWSDS GetTransactions(DateTime start, DateTime end)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            // set the end time to 11:59 PM on the selected day
            end = end.AddDays(1).AddMinutes(-1);

            GcsApi.WebServices client = this.GetSoapClient();
            return client.TransactionsGetADO(this.userName, this.password, null, missing, missing, missing, missing, missing,
                                                missing, missing, null, missing, missing, missing, null, null, null,
                                                missing, missing, missing,
                                                missing, start.ToString(), end.ToString(), missing, null, null, this.rowCount, null,
                                                ref rowCount, ref pageCount);
        }

        public GcsApi.TransactionsWSDS GetTransactions(decimal? transactionID)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.TransactionsGetADO(this.userName, this.password, transactionID, missing, missing, missing, missing, missing,
                                                missing, missing, null, missing, missing, missing, null, null, null,
                                                missing, missing, missing,
                                                missing, missing, missing, missing, null, null, null, null,
                                                ref rowCount, ref pageCount);
        }

        public GcsApi.TransactionsWSDS GetTransactionsByReferenceId(decimal? transactionID)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.TransactionsGetADO(this.userName, this.password, null, missing, missing, missing, missing, missing,
                                                missing, missing, null, missing, missing, missing, null, null, transactionID,
                                                missing, missing, missing,
                                                missing, missing, missing, missing, null, null, null, null,
                                                ref rowCount, ref pageCount);
        }

        public GcsApi.TransactionsWSDS GetTransactions(string accountID)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.TransactionsGetADO(this.userName, this.password, null, accountID, missing, missing, missing, missing,
                                                missing, missing, null, missing, missing, missing, null, null, null,
                                                missing, missing, missing,
                                                missing, missing, missing, missing, null, null, null, null,
                                                ref rowCount, ref pageCount);
        }

        public GcsApi.TransactionsWSDS GetTransactions(string accountID, DateTime start)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.TransactionsGetADO(this.userName, this.password, null, accountID, missing, missing, missing, missing,
                                                missing, missing, null, missing, missing, missing, null, null, null,
                                                missing, missing, missing,
                                                missing, start.ToString(), missing, missing, null, null, this.rowCount, null,
                                                ref rowCount, ref pageCount);
        }

        public GcsApi.PaymentsWSDS GetPayments(DateTime start)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();

            return client.PaymentsGetADO(this.userName, this.password, null, missing, missing, missing, missing, missing,
                null, null, missing, missing, missing, missing, null, missing, null, missing,
                missing, missing, missing, missing, missing, start.ToString(), missing, missing, missing, missing, null,
                null, ref rowCount, ref pageCount);
        }

        public string GetPaymentIDByFields(DateTime effectiveDate, decimal payeeID, string payeeClientNum, decimal amount)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();

            GcsApi.PaymentsWSDS res = client.PaymentsGetADO(this.userName, this.password, null, missing, missing, missing, missing, missing,
                null, null, missing, missing, missing, missing, null, missing, null, missing,
                missing, missing, missing, missing, missing, null, missing, missing, missing, missing, null,
                null, ref rowCount, ref pageCount);
            if (res.PAYMENTS.Rows.Count > 0)
                return (res.PAYMENTS.Rows[0] as GcsApi.PaymentsWSDS.PAYMENTSRow).PAYMENT_ID.ToString();
            else
                return "";
        }

        public GcsApi.DebitsWSDS GetFees(DateTime start)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();

            return client.FeesGetADO(this.userName, this.password, null, missing, missing, missing, null, missing,
                null, null, missing, missing, start.ToString(), missing, missing, missing, missing, missing,
                missing, null, null, missing, missing, missing, this.rowCount,
                null, ref rowCount, ref pageCount);
        }

        public GcsApi.DebitsWSDS GetFees(DateTime startDate, DateTime endDate)
        {
            int? rowCount = 0, pageCount = 0;
            string missing = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();

            return client.FeesGetADO(this.userName, this.password, null, missing, startDate.ToString(), endDate.ToString(), null, missing,
                null, null, missing, missing, missing, missing, missing, missing, missing, missing,
                missing, null, null, missing, missing, missing, this.rowCount,
                null, ref rowCount, ref pageCount);
        }


        /// <summary>
        /// This function is used to manage notes
        /// </summary>
        /// <param name="notes"></param>
        /// <returns></returns>
        public GcsApi.NotesWSDS SetClientNotes(GcsApi.NotesWSDS notes)
        {
            GcsApi.WebServices client = this.GetSoapClient();

            return client.NotesSetADO(this.userName, this.password, notes);
        }
        public GcsApi.NotesWSDS GetClientNotesByAccountID(string accountID)
        {
            int? rowCount = 0, pageCount = 0;
            string blank = string.Empty;
            GcsApi.WebServices client = this.GetSoapClient();

            return client.NotesGetADO(this.userName, this.password, null, accountID,
                                      blank, blank, blank, blank, blank, blank,
                                      null, null, ref rowCount, ref pageCount);
        }
        public GcsApi.NotesWSDS GetClientNotesByNoteID(decimal? noteID)
        {
            int? rowCount = 0, pageCount = 0;
            string blank = string.Empty;
            GcsApi.WebServices client = this.GetSoapClient();

            return client.NotesGetADO(this.userName, this.password, noteID, blank,
                                      blank, blank, blank, blank, blank, blank,
                                      null, null, ref rowCount, ref pageCount);
        }

        /// <summary>
        /// Gets a list of all clients 
        /// </summary>
        /// <returns>The client dataset including list and errors</returns>
        public GcsApi.ClientsWSDS GetClients()
        {
            int? rowCount = 0, pageCount = 0;
            string blank = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            this.clientsList = client.ClientsGetADO(this.userName, this.password, blank, blank, blank, blank, blank, blank, blank, blank, blank, blank, blank,
                                                    blank, blank, blank, blank, blank, blank, blank, null, blank, blank, blank, blank, blank, blank, blank, blank,
                                                    null, null, ref rowCount, ref pageCount);
            return this.clientsList;
        }

        /// <summary>
        /// Retrieves a client record by GCS account number
        /// </summary>
        /// <param name="accountID">The GCS account number</param>
        /// <returns>The client dataset including list and errors</returns>
        public GcsApi.ClientsWSDS GetClientByAccountID(string accountID)
        {
            int? rowCount = 0, pageCount = 0;
            string blank = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.ClientsGetADO(this.userName, this.password, accountID, blank, blank, blank, blank, blank, blank, blank, blank, blank, blank,
                                                    blank, blank, blank, blank, blank, blank, blank, null, blank, blank, blank, blank, blank, blank, blank, blank,
                                                    null, null, ref rowCount, ref pageCount);
        }

        /// <summary>
        /// Creates accounts for new clients
        /// </summary>
        /// <returns>The client dataset including list and errors</returns>
        public GcsApi.ClientsWSDS SetClients(GcsApi.ClientsWSDS clients)
        {
            GcsApi.WebServices client = this.GetSoapClient();

            return client.ClientsSetADO(this.userName, this.password, clients);
        }
        /// <summary>
        /// This functions calls a useless function to check if the connection to gcs is ok : login/password.
        /// If not an exception is thrown, else returns true.
        /// </summary>
        /// <returns>true</returns>
        public string Login()
        {
            GcsApi.WebServices client = this.GetSoapClient();
            try
            {
                GcsApi.PolicyGroupsWSDS res = client.PolicyGroupsGetADO(this.userName, this.password, "Y");
                if (res.ERRORS.Count == 0)
                    return "";
                else
                    return res.ERRORS[0].ERROR_TEXT;
            }
            catch
            {
                throw;
            }
            //return true;
        }

        private GcsApi.WebServices GetSoapClient()
        {
            GcsApi.WebServices client = new GcsApi.WebServices();
            // Little bit of hack here.  The Prod GCS WSDL is used; however we use a URL fixup from 
            // the GCSInterop properties to fix it up.
            if (prodEnvironment)
                client.Url = global::GCSInterop.Properties.Settings.Default.PmtInterop_GCS_WebServicesProd;
            else
                client.Url = global::GCSInterop.Properties.Settings.Default.PmtInterop_GCS_WebServicesTest;
            //client.Url = global::GCSInterop.Properties.Settings.Default.PmtInterop_T2P_WebServicesTest;
            client.Timeout = 2 * 60 * 1000;         // set timeout to 2 minutes
            return client;
        }

        private GcsApi.WebServices GetT2PSoapClient()
        {
            GcsApi.WebServices client = new GcsApi.WebServices();
            // Little bit of hack here.  The Prod GCS WSDL is used; however we use a URL fixup from 
            // the GCSInterop properties to fix it up.
            if (prodEnvironment)
                client.Url = global::GCSInterop.Properties.Settings.Default.PmtInterop_T2P_WebServicesProd;
            else
                client.Url = global::GCSInterop.Properties.Settings.Default.PmtInterop_T2P_WebServicesTest;
            client.Timeout = 2 * 60 * 1000;         // set timeout to 2 minutes
            return client;
        }

        public GcsApi.ClientsListWSDS.CLIENTSRow[] FindClient(string ssn)
        {
            ssn = ssn.Replace("-", string.Empty).Replace(" ", string.Empty).Trim();

            return (GcsApi.ClientsListWSDS.CLIENTSRow[])this.clientsList.CLIENTS.Select("SOC_SEC_NUM = '" + ssn + "'");
        }

        public GcsApi.ClientsWSDS.CLIENTSRow FindClientById(string clientID)
        {
            int? rowCount = 0, pageCount = 0;
            string blank = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            GcsApi.ClientsWSDS clients = client.ClientsGetADO(this.userName, this.password, blank, blank, blank, clientID, blank, blank, blank, blank, blank, blank, blank,
                                                    blank, blank, blank, blank, blank, blank, blank, null, blank, blank, blank, blank, blank, blank, blank, blank,
                                                    null, null, ref rowCount, ref pageCount);

            if (clients != null && clients.CLIENTS.Rows.Count > 0)
            {
                return (GcsApi.ClientsWSDS.CLIENTSRow)clients.CLIENTS.Rows[0];
            }
            else
            {
                return null;
            }
        }

        public decimal getDefaultPolicyID()
        {
            string blank = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            GcsApi.PolicyGroupsWSDS policies = client.PolicyGroupsGetADO(this.userName, this.password, "Y");
            //GcsApi.PolicyGroupsWSDS[] tab = policies.POLICY_GROUPS.Select("");

            if (policies != null && policies.POLICY_GROUPS.Rows.Count > 0)
            {
                return ((GcsApi.PolicyGroupsWSDS.POLICY_GROUPSRow)policies.POLICY_GROUPS.Rows[0]).POLICY_GROUP_ID;
            }
            else
            {
                return -1;
            }
        }

        public GcsApi.PayeesWSDS GetCreditors()
        {
            int? rowCount = 0, pageCount = 0;
            string blank = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            GcsApi.PayeesWSDS payees = client.PayeesGetADO(this.userName, this.password, blank, blank, blank, blank, blank, blank, blank, blank, blank,
                                                    null, null, ref rowCount, ref pageCount);
            return payees;
        }

        public GcsApi.PayeesWSDS GetCreditorsByName(string payeeName)
        {
            int? rowCount = 0, pageCount = 0;
            string blank = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            GcsApi.PayeesWSDS payees = client.PayeesGetADO(this.userName, this.password, blank, blank, payeeName, blank, blank, blank, blank, blank, blank,
                                                    null, null, ref rowCount, ref pageCount);
            return payees;
        }

        public GcsApi.PayeesWSDS SetCreditors(GcsApi.PayeesWSDS clients)
        {
            GcsApi.WebServices client = this.GetSoapClient();
            return client.PayeesSetADO(this.userName, this.password, clients);
        }

        public string GetNextAccountNumber()
        {
            if (this.availableAcctNums.Count > 0)
            {
                string acctNumber = this.availableAcctNums[0];
                this.availableAcctNums.RemoveAt(0);
                return acctNumber;
            }
            else
            {
                this.GetNextAccountNumbers();
                return this.GetNextAccountNumber();
            }
        }

        public void GetNextAccountNumbers()
        {
            int numAcctRem = 0;
            GcsApi.WebServices client = this.GetSoapClient();

            GcsApi.AccountsWSDS accounts = client.NextAccountGetADO(this.userName, this.password, numAcctNumsToRetrieve, ref numAcctRem);

            if (accounts.ACCOUNTS.Rows.Count == 0)
            {
                if (accounts.ERRORS.Rows.Count > 0)
                {
                    throw new Exception(accounts.ERRORS.Rows[0][7].ToString());
                }
                else
                {
                    throw new Exception("Unknown error retrieving next 20 account numbers from GCS/T2P");
                }
            }

            foreach (GcsApi.AccountsWSDS.ACCOUNTSRow row in accounts.ACCOUNTS.Rows)
            {
                this.availableAcctNums.Add(row.ACCOUNT_ID);
            }
        }

        public GcsApi.PayeesWSDS GetDPCreditors()
        {
            int? rowCount = 0, pageCount = 0;
            string blank = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            GcsApi.PayeesWSDS payees = client.PayeesDirectPayGetADO(this.userName, this.password, blank, blank, blank, blank, blank, blank, blank, blank, blank,
                                                    null, null, ref rowCount, ref pageCount);
            return payees;
        }

        public GcsApi.PayeesListWSDS GetPayeeList()
        {
            int? rowCount = 0, pageCount = 0;
            string blank = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            GcsApi.PayeesListWSDS payees = client.PayeesListGetADO(this.userName, this.password, blank, blank, blank, blank, blank, blank, blank, blank, blank,
                                                    null, null, ref rowCount, ref pageCount);
            return payees;
        }

        public GcsApi.PayeesWSDS.PAYEESRow GetPayeeByDRCID(string drcID)
        {
            int? rowCount = 0, pageCount = 0;
            string blank = string.Empty;

            GcsApi.WebServices client = this.GetSoapClient();
            GcsApi.PayeesWSDS payees = client.PayeesGetADO(this.userName, this.password, blank, drcID, blank, blank, blank, blank, blank, blank, blank,
                                                    null, null, ref rowCount, ref pageCount);
            if (payees.PAYEES.Count > 0)
                return payees.PAYEES[0];
            else
                return null;
        }

        /// <summary>
        /// Manage the payments
        /// </summary>
        /// <param name="fees"></param>
        /// <returns></returns>
        public GcsApi.PaymentsWSDS SetPayments(GcsApi.PaymentsWSDS payments)
        {
            GcsApi.WebServices client = this.GetSoapClient();

            return client.PaymentsSetADO(this.userName, this.password, payments);
        }
        public GcsApi.PaymentsWSDS GetPaymentsByID(decimal? paymentID)
        {
            string blank = string.Empty;
            int? rowCount = 0, pageCount = 0, pageNumber = 0, pageSize = 0;
            //decimal? checkNumber = 0, payeeId = 0;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.PaymentsGetADO(this.userName, this.password, paymentID, blank, blank, blank, blank, blank,
                                        null, null, blank, blank, blank, blank, null, blank, null, blank, blank, blank, blank,
                                        blank, blank, blank, blank, blank, blank, blank,
                                        pageSize, pageNumber, ref rowCount, ref pageCount);
        }
        public GcsApi.PaymentsWSDS GetPaymentsByTransactionID(string DRCTransactionID)
        {
            string blank = string.Empty;
            int? rowCount = 0, pageCount = 0, pageNumber = 0, pageSize = 0;
            //decimal? checkNumber = 0, payeeId = 0;

            GcsApi.WebServices client = this.GetSoapClient();
            return client.PaymentsGetADO(this.userName, this.password, null, blank, blank, blank, blank, blank,
                                        null, null, blank, blank, blank, blank, null, blank, null, blank, blank, blank, DRCTransactionID,
                                        blank, blank, blank, blank, blank, blank, blank,
                                        pageSize, pageNumber, ref rowCount, ref pageCount);
        }

        /// <summary>
        /// Gets client account number information by Social Security Number
        /// </summary>
        /// <param name="ssn">The client's social security number</param>
        /// <returns>The client information if found or null if the SSN is malformed</returns>
        public string GetClientAccountNumber(string ssn)
        {
            ssn = ssn.Replace("-", string.Empty).Replace(" ", string.Empty).Trim();

            if (ssn.Length == 9)
            {
                GcsApi.WebServices client = this.GetSoapClient();
                GcsApi.ClientsListWSDS list = client.ClientsListGetADO(this.userName, this.password, null, null, null, ssn);

                if (list.CLIENTS.Rows.Count > 0)
                {
                    return list.CLIENTS[0].ACCOUNT_ID;
                }

                return null;
            }
            else
            {
                return null;
            }
        }

    }
}
