﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Web;
using System.Web.Services.Protocols;
using SFDebt;
using System.Collections;
// using GcsApi = GCSInterop.GCSTest;      // Test System
using GcsApi = GCSInterop.GCSProd;     // Live System
using GCSInterop;
using System.IO;
using SelasDataClass;
using System.Data;
using RAMSApi = RAMSInterop.RAMSProd;
using RAMSInterop;
using EPPSInterop;
using EPPSApi = EPPSInterop.EPPSService;
using System.Configuration;
using AuthorizeNet;

namespace DebtLibrary
{
    public class PaymentClass
    {
        #region Fields

        private string _sForceLogin = string.Empty;
        private string _sForcePwd = string.Empty;
        private string _sForceToken = string.Empty;
        private string _ProcessorLogin = string.Empty;
        private string _ProcessorPwd = string.Empty;
        private string _Processor = string.Empty;
        private Boolean _LiveEnvironment = false;
        private string _AuthNetID = string.Empty;
        private string _AuthNetPwd = string.Empty;
        private bool? _IsLogEnabled = false;
        private string defaultProcessor;
        private bool _isOfferProcessingEnabled = false;
        private bool _isProcessingEnabled = false;
        private bool _isClientCheckingEnabled = false;
        private bool _isCheckTransactionEnabled = true;

        private DateTime _checkTransactionsCompleted = new DateTime();

        /// <summary>
        /// The number of days ahead that will be added into GCS
        /// </summary>
        private int _numberOfDaysAhead = 0;

        List<string> results = new List<string>();

        #endregion

        #region Constructors

        /// <summary>
        /// Explicit value constructor
        /// </summary>
        /// <param name="salesForceLogin">The salesforce login username</param>
        /// <param name="salesForcePwd">Password for salesforce login</param>
        /// <param name="salesForceSecToken">Security token associated with salesforce login</param>
        /// <param name="ProcessorLogin">GCS login username</param>
        /// <param name="ProcessorPwd">GCS login password</param>
        public PaymentClass(string salesForceLogin, string salesForcePwd, string salesForceSecToken, string ProcessorLogin, string ProcessorPwd, Boolean liveEnv, string Processor, string AuthNetLoginID, string AuthNetKey, bool? IsLogEnabled)
        {
            this.AccountNotFound = new List<string>();
            this._sForceLogin = salesForceLogin;
            this._sForcePwd = salesForcePwd;
            this._sForceToken = salesForceSecToken;

            this._ProcessorLogin = ProcessorLogin;
            this._ProcessorPwd = ProcessorPwd;
            this._LiveEnvironment = liveEnv;

            this._isCheckTransactionEnabled = true;
            this._isClientCheckingEnabled = true;
            this._isOfferProcessingEnabled = true;
            this._isProcessingEnabled = true;
            //this._RAMSKey = ProcessorLogin;
            this._Processor = Processor;
            this.defaultProcessor = Processor;
            this._AuthNetID = AuthNetLoginID;
            this._AuthNetPwd = AuthNetKey;
            this._IsLogEnabled = IsLogEnabled;
        }

        #endregion

        #region Properties

        public List<string> AccountNotFound { get; set; }

        public int NumberOfDaysAhead { get; set; }

        public TransactionDS.ClientsDataTable CreatedClients { get; set; }

        public TransactionDS UpdatedTransactions { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Retrieves the list of pending transactions for a specified date range and 
        /// creates these transactions with the appropriate processor
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        public TransactionDS ManualCreateTransactions(DateTime start, DateTime endDate)
        {
            // do a triple check of the payment statuses and update these in SalesForce
            if (this._checkTransactionsCompleted < DateTime.Today)
            {
                this.CheckTransactions(DateTime.Today.AddDays(-1));
                this._checkTransactionsCompleted = DateTime.Today;
            }

            TransactionDS transactions = this.GetPendingPayments(start, endDate);

            return transactions;
        }

        /// <summary>
        /// Retrieves the list of pending transactions for a specified date range and 
        /// creates these transactions with the appropriate processor
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        public TransactionDS ViewT2PTransactions(DateTime start, DateTime endDate)
        {
            try
            {
                TransactionDS transactions = this.GetPendingT2PPayments(start, endDate);
                DateTime currentDate = DateTime.Now;
                if (start <= currentDate && !String.IsNullOrEmpty(this._AuthNetID) && !String.IsNullOrEmpty(this._AuthNetPwd))
                {
                    TransactionDS transactionsCredit = this.ViewAuthorizeCredits(start, endDate);
                    foreach (TransactionDS.ClientDepositRow item in transactionsCredit.ClientDeposit)
                    {
                        item.RowNumber = transactions.ClientDeposit.Count + 1;
                        transactions.ClientDeposit.ImportRow(item);
                    }
                }
                return transactions;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Retrieves the list of pending transactions for a specified date range and 
        /// creates these transactions with the appropriate processor
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        public TransactionDS CreateT2PTransactions(TransactionDS ds, DateTime startDate, DateTime endDate)
        {
            try
            {
                TransactionDS t2pDS = new TransactionDS();
                TransactionDS tnbcDS = new TransactionDS();
                if (!String.IsNullOrEmpty(this._AuthNetID) && !String.IsNullOrEmpty(this._AuthNetPwd))
                {
                    foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit)
                    {
                        if (row.Type.Equals("A") && !row.IsCardNumberNull() && !row.IsCardExpDateNull())
                        {
                            TransactionDS.ClientDepositRow newRow = tnbcDS.ClientDeposit.NewClientDepositRow();
                            row.RowNumber = tnbcDS.ClientDeposit.Count + 1;
                            newRow = row;
                            tnbcDS.ClientDeposit.ImportRow(newRow);
                        }
                        else
                        {
                            TransactionDS.ClientDepositRow newRow = t2pDS.ClientDeposit.NewClientDepositRow();
                            row.RowNumber = t2pDS.ClientDeposit.Count + 1;
                            newRow = row;
                            t2pDS.ClientDeposit.ImportRow(newRow);
                        }
                    }
                    if (tnbcDS.ClientDeposit.Count > 0)
                        this.CreateAuthorizeCredits(ref tnbcDS);

                }
                else
                {
                    t2pDS = ds;
                }

                if (t2pDS.ClientDeposit.Count > 0)
                    this.CreateT2PPayments(ref t2pDS, startDate, endDate);

                if (!String.IsNullOrEmpty(this._AuthNetID) && !String.IsNullOrEmpty(this._AuthNetPwd))
                {
                    foreach (TransactionDS.ClientDepositRow item in tnbcDS.ClientDeposit)
                    {
                        t2pDS.ClientDeposit.ImportRow(item);
                    }
                    ds = t2pDS;
                }
                else
                {
                    ds = t2pDS;
                }
                int rowNoumber = 1;
                foreach (TransactionDS.ClientDepositRow item in ds.ClientDeposit)
                {
                    item.RowNumber = rowNoumber;
                    rowNoumber = rowNoumber + 1;
                }

                SFDebtClass sfObj = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                if (ds.ClientDeposit.Count > 0)
                    sfObj.UpdateT2PTransactions(ds);
                if (_IsLogEnabled.Equals(true))
                {
                    sfObj.WriteSuccessLog(ds.ClientDeposit);
                }
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Retrieves the list of pending payments for a specified date range and 
        /// creates these transactions with the appropriate processor
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        public TransactionDS BuildPaymentList(DateTime start, DateTime endDate)
        {
            return this.GetWithdrawals(start, endDate);
        }

        /// <summary>
        /// Retrieves the list of pending transactions for a specified date range and 
        /// creates these transactions with the appropriate processor
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        public object[] ManualProcessTransactions(TransactionDS transactions)
        {
            bool processed = false;

            processed = this.CreateGCSTransactions(transactions);

            // do a triple check of the payment statuses and update these in SalesForce
            this.CheckTransactions(DateTime.Today.AddDays(-1));

            return new object[] { processed, this.results };
        }

        /// <summary>
        /// Retrieves the list of pending transactions for a specified date range and 
        /// creates these transactions with the appropriate processor
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        public object[] ManualProcessPayments(TransactionDS transactions)
        {
            bool processed = false;

            this.results.Clear();
            Hashtable acctHash = new Hashtable();

            // if there are no new payments, do nothing
            if (transactions.OfferPayment.Rows.Count > 0)
            {
                GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);

                // refresh the internal list of all the clients active in GCS
                GcsApi.ClientsWSDS clientList = gcs.GetClients();

                // retrieve the creditor payments that are to be processed
                GcsApi.PaymentsWSDS payments = this.BuildPaymentsTable(transactions, gcs, ref acctHash);

                // transfer creditor transactions to GCS system
                try
                {
                    processed = gcs.SetTransactions(null, payments);
                }
                catch (SoapException ex)
                {
                    SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                    sf.SetStatus(SetTimeOutPayments(payments, "TimedOut", ex.Message));
                    sf.Logout();
                    throw;
                }
                catch (WebException ex)
                {
                    SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                    sf.SetStatus(SetTimeOutPayments(payments, "TimedOut", ex.Message));
                    sf.Logout();
                    throw;
                }
                // process results
                this.results = this.ProcessCreateTransactionResults(gcs, transactions);

                foreach (GcsApi.PaymentsWSDS.ERRORSRow error in gcs.PaymentResults.ERRORS)
                {
                    this.results.Add(error.ERROR_CODE + " : " + error.ERROR_TEXT);
                }
            }

            return new object[] { processed, this.results };
        }

        /// <summary>
        /// Writes critical errors to SalesForce
        /// </summary>
        /// <param name="ex">The system exception</param>
        public void WriteError(Exception ex, string id)
        {
            SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            salesForce.WriteCriticalError(ex, id);
            salesForce.Logout();
        }

        /// <summary>
        /// Writes a table of errors to SalesForce
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="objectName"></param>
        public void WriteError(System.Data.DataTable dt, string objectName)
        {
            if (dt == null || dt.Rows.Count == 0)
            {
                return;
            }

            SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            salesForce.WriteError(dt, objectName);
            salesForce.Logout();
        }

        /// <summary>
        /// Method to runs the transaction creation process
        /// </summary>
        public TransactionDS RunViewPendingTransaction(DateTime startDate, DateTime endDate)
        {
            TransactionDS transactions = new TransactionDS();
            if (this._isClientCheckingEnabled)
            {
                switch (this._Processor)
                {
                    case "RAMS":
                        {
                            transactions = this.ViewPendingRAMSTransactions(startDate, endDate);
                            break;
                        }

                    case "GCS":
                        {
                            transactions = this.ManualCreateTransactions(startDate, endDate);
                            break;
                        }

                    case "EPPS":
                        {
                            transactions = this.ViewPendingEPPSTransactions(startDate, endDate);
                            break;
                        }
                    case "T2P":
                        {
                            transactions = this.ViewT2PTransactions(startDate, endDate);
                            break;
                        }
                    case "Authorize.Net":
                        {
                            transactions = this.ViewAIMTransaction(startDate, endDate);
                            break;
                        }
                    case "FirstData":
                    case "AffinityProgram":
                        {
                            transactions = this.ViewFDTransaction(startDate, endDate);
                            break;
                        }
                }

            }

            return transactions;

        }

        /// <summary>
        /// Method to runs the transaction creation process
        /// </summary>
        public TransactionDS RunCreateTransaction(TransactionDS clientDeposits, DateTime startDate, DateTime endDate)
        {
            try
            {
                TransactionDS transactions = new TransactionDS();

                if (this._isClientCheckingEnabled)
                {
                    switch (this._Processor)
                    {
                        case "RAMS":
                            {
                                transactions = this.CreateRAMSTransactions(clientDeposits);
                                break;
                            }

                        case "GCS":
                            {
                                transactions = this.ManualCreateTransactions(startDate, endDate);
                                break;
                            }

                        case "EPPS":
                            {
                                transactions = this.EPPSCreateTransactions(clientDeposits);
                                break;
                            }

                        case "T2P":
                            {
                                transactions = this.CreateT2PTransactions(clientDeposits, startDate, endDate);
                                break;
                            }
                        case "Authorize.Net":
                            {
                                transactions = this.CreateAIMTransaction(clientDeposits);
                                break;
                            }
                        case "FirstData":
                        case "AffinityProgram":
                            {
                                transactions = this.CreateFDTransaction(clientDeposits);
                                break;
                            }
                    }

                }

                return transactions;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        /// <summary>
        /// Method to runs the transaction creation process
        /// </summary>
        public void RunUpdateTransaction(DateTime startDate, DateTime endDate)
        {
            if (this._isClientCheckingEnabled)
            {
                switch (this._Processor)
                {
                    case "RAMS":
                        {
                            this.UpdateRAMSTransactions(endDate);
                            break;
                        }

                    case "GCS":
                        {
                            this.CheckTransactions(endDate);
                            break;
                        }

                    case "EPPS":
                        {
                            this.EPPSUpdateTransactions(startDate, endDate);
                            break;
                        }

                    case "T2P":
                        {
                            this.CheckT2PTransactions(startDate, endDate);
                            break;
                        }
                }

            }

        }

        public TransactionDS ViewPendingRAMSTransactions(DateTime start, DateTime endDate)
        {
            try
            {
                TransactionDS transactions = this.GetRAMSPayments(start, endDate);

                return transactions;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public TransactionDS CreateRAMSTransactions(TransactionDS ds)
        {
            try
            {
                TransactionDS transactions = this.CreateRAMSPayments(ds);

                return transactions;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS GetRAMSPayments(DateTime start, DateTime end)
        {
            try
            {
                TransactionDS feeTemplate = new TransactionDS();
                List<TransactionInfo> list = new List<TransactionInfo>();
                SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                TransactionDS ds = salesForce.GetRAMSTransactions(start, end, false, ref list, ref feeTemplate);

                salesForce.Logout();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS CreateRAMSPayments(TransactionDS ds)
        {
            try
            {
                TransactionDS feeTemplate = new TransactionDS();
                List<TransactionInfo> list = new List<TransactionInfo>();

                RAMS client = new RAMS(this._ProcessorLogin, this._LiveEnvironment);
                List<ClientInfo> objInput = new List<ClientInfo>();
                object objOut = null;

                if (list.Count > 0)
                {


                    client.CreateTransaction(ref list, ref objOut);

                    foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit.Rows)
                    {
                        string description = Convert.ToString((from i in list where i.TrasactionId.Equals(row.RecordID) select i.Note).FirstOrDefault());
                        if (!string.IsNullOrEmpty(description) && !string.IsNullOrWhiteSpace(description))
                        {
                            row.SyncNotes = description;
                            if ((from i in list where i.TrasactionId.Equals(row.RecordID) select i.ProcessStatus).First().Equals(true))
                            {
                                int pID = (from i in list where i.TrasactionId.Equals(row.RecordID) select i.PID).First();
                                row.TransactionID = Convert.ToString(pID);
                                row.Status = "Processing";
                            }
                        }
                    }

                    foreach (TransactionDS.ClientDepositRow row in feeTemplate.ClientDeposit.Rows)
                    {
                        string description = Convert.ToString((from i in list where i.TrasactionId.Equals(row.ParentDRCID) select i.Note).FirstOrDefault());
                        if (!string.IsNullOrEmpty(description) && !string.IsNullOrWhiteSpace(description))
                        {
                            row.SyncNotes = description;
                            if ((from i in list where i.TrasactionId.Equals(row.ParentDRCID) select i.ProcessStatus).First().Equals(true))
                            {
                                int pID = (from i in list where i.TrasactionId.Equals(row.ParentDRCID) select i.PID).First();
                                row.TransactionID = Convert.ToString(pID);
                                row.Status = "Processing";
                            }
                        }
                    }
                    SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                    sf.UpdateRAMSPayment(list);
                    sf.UpdateRAMSPaymentFee(feeTemplate);
                    sf.Logout();
                }

                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Builds a list of strings from the values in the first column of the table
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public List<string>[] TableToLists(GcsApi.TransactionsWSDS.TRANSACTIONSDataTable dt)
        {
            List<string>[] list = new List<string>[2];

            foreach (GcsApi.TransactionsWSDS.TRANSACTIONSRow dr in dt.Rows)
            {
                if (dr.TRANSACTION_CLASS_DESCRIPTION == "Payment")
                {
                    list[0].Add(dr[0].ToString());
                }
                else
                {
                    list[1].Add(dr[0].ToString());
                }
            }

            return list;
        }

        /// <summary>
        /// Checks the status of all pending transactions in the GCS system
        /// </summary>
        /// <param name="start">The start date</param>
        /// <returns></returns>
        public List<string> CheckTransactions(DateTime start)
        {
            List<string> list = new List<string>();

            if (this._isCheckTransactionEnabled)
            {
                this.CheckGCSTransactions(start);
            }

            return list;
        }

        /// <summary>
        /// Checks the status of all pending transactions in the payment processors system
        /// </summary>
        /// <param name="start">The start date</param>
        /// <returns></returns>
        public List<string> CheckT2PTransactions(DateTime startDate, DateTime endDate)
        {
            List<string> list = new List<string>();

            if (this._isCheckTransactionEnabled)
            {
                // this.CheckT2PTransaction(start);
            }

            return list;
        }

        /// <summary>
        /// Checks the status of all pending transactions in the GCS system
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns></returns>
        public TransactionDS GetTransactions(DateTime start, DateTime end)
        {
            TransactionDS ds = new TransactionDS(); ;
            switch (this._Processor)
            {
                case "RAMS":
                    {
                        ds = this.GetRAMSTransactions(start, end);
                        break;
                    }

                case "GCS":
                case "T2P":
                    {
                        ds = this.GetGCSTransactions(start, end);
                        break;
                    }

                case "EPPS":
                    {
                        ds = this.GetEPPSTransactions(start, end);
                        break;
                    }
            }
            return ds;
        }

        /// <summary>
        /// Runs the client update process
        /// </summary>
        public void RunClientUpdate()
        {
            // update account balances - this should also clear out the system "Sync Notes" if necessary
            switch (this._Processor)
            {
                case "RAMS":
                    {
                        this.UpdateRAMSBalances();
                        break;
                    }

                case "GCS":
                case "T2P":
                    {
                        this.UpdateGCSBalances();
                        break;
                    }

                case "EPPS":
                    {
                        this.UpdateEPPSBalances();
                        break;
                    }
            }

            if (this._isClientCheckingEnabled)
            {
                switch (this._Processor)
                {
                    case "RAMS":
                        {
                            this.UpdateRAMSClients();
                            break;
                        }

                    case "GCS":
                    case "T2P":
                        {
                            this.UpdateGCSClients();
                            break;
                        }

                    case "EPPS":
                        {
                            this.UpdateEPPSClients();
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// View Pending Clients
        /// </summary>
        public TransactionDS.ClientsDataTable RunViewClient(DateTime startDate, DateTime endDate)
        {
            if (this._isClientCheckingEnabled)
            {
                switch (this._Processor)
                {
                    case "RAMS":
                        {
                            this.ViewPendingRAMClients();
                            break;
                        }

                    case "GCS":
                    case "T2P":
                        {
                            this.ViewPendingGCSClients();
                            break;
                        }

                    case "EPPS":
                        {
                            this.ViewPendingEPPSClients();
                            break;
                        }
                    case "Authorize.Net":
                        {
                            this.ViewAIMClients();
                            break;
                        }
                    case "FirstData":
                        {
                            this.ViewFDClients();
                            break;
                        }
                }

            }

            return this.CreatedClients;
        }

        /// <summary>
        /// Runs the client update process
        /// </summary>
        public TransactionDS.ClientsDataTable RunCreateClient()
        {
            //TransactionDS.ClientsDataTable temp = new TransactionDS.ClientsDataTable();
            //foreach (TransactionDS.ClientsRow row in CreatedClients)
            //{
            //    if (row.IsChecked.Equals(true))
            //    {
            //        TransactionDS.ClientsRow newRow = temp.NewClientsRow();
            //        row.RowNumber = temp.Count + 1;
            //        newRow = row;
            //        temp.ImportRow(newRow);
            //    }
            //}
            //CreatedClients = temp;
            if (this._isClientCheckingEnabled)
            {
                switch (this._Processor)
                {
                    case "RAMS":
                        {
                            this.CreateRAMClients();
                            break;
                        }

                    case "GCS":
                    case "T2P":
                        {
                            this.CreateGCSandT2PClients();
                            break;
                        }

                    case "EPPS":
                        {
                            this.CreateEPPSClients();
                            break;
                        }
                    case "Authorize.Net":
                        {
                            this.CreateAuthorizeDotNetClients();
                            break;
                        }
                    case "FirstData":
                        {
                            this.CreateFDClients();
                            break;
                        }
                }

            }

            return this.CreatedClients;
        }

        /// <summary>
        /// Runs the client update process
        /// </summary>
        public void CheckGCSClientAccount(string accountNumber, out TransactionDS tx)
        {
            // create an instance of the GCS interop class
            GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);

            tx = new TransactionDS();

            decimal testAcct = -1;

            // if the supplied account number is not a valid number, then return
            if (!decimal.TryParse(accountNumber, out testAcct))
            {
                return;
            }

            GcsApi.TransactionsWSDS clientTransactions = gcs.GetTransactions(accountNumber);
            GcsApi.ClientsWSDS account = gcs.GetClientByAccountID(accountNumber);

            if (account.Tables["Clients"].Rows.Count > 0 && clientTransactions.Tables.Count > 0)
            {
                System.Data.DataView tView = clientTransactions.TRANSACTIONS.DefaultView;
                tView.Sort = "TRANSACTION_DATE DESC, TRANSACTION_AMOUNT ASC";

                foreach (GcsApi.TransactionsWSDS.TRANSACTIONSRow row in tView.Table.Rows)
                {
                    TransactionDS.ClientDepositRow cd = tx.ClientDeposit.NewClientDepositRow();
                    cd.ClientID = string.Empty;
                    cd.RecordID = row.TRANSACTION_ID.ToString();
                    cd.ClientName = string.Empty;
                    cd.Date = row.TRANSACTION_DATE;
                    cd.ProcessorAccount = accountNumber;
                    cd.RecordName = row.TRANSACTION_TYPE_DESCRIPTION;
                    cd.Amount = row.IsTRANSACTION_AMOUNTNull() ? 0 : (double)row.TRANSACTION_AMOUNT;

                    if (!row.IsPOST_DATENull())
                    {
                        cd.ClearedDate = row.POST_DATE;
                    }

                    cd.Status = this.GetStatus(gcs, row);
                    tx.ClientDeposit.Rows.Add(cd);
                }

                foreach (GcsApi.ClientsWSDS.CLIENTSRow row in account.CLIENTS.Rows)
                {
                    TransactionDS.ClientsRow cr = tx.Clients.NewClientsRow();
                    cr.AccountBalance = (double)row.ACCOUNT_BALANCE;
                    cr.Status = row.CLIENT_STATUS_DESCRIPTION;
                    cr.LastSyncDate = row.MODIFIED_DATE;
                    cr.FirstName = row.FIRST_NAME;
                    cr.LastName = row.LAST_NAME;
                    cr.LastStatus = row.DRAFTS_AUTHORIZED;
                    cr.IsNewAccount = false;
                    tx.Clients.Rows.Add(cr);
                }
            }
        }
        public void CheckRAMSClientAccount(string accountNumber, out TransactionDS tx)
        {
            tx = new TransactionDS();

            int testAcct = -1;

            // if the supplied account number is not a valid number, then return
            if (!int.TryParse(accountNumber, out testAcct))
            {
                return;
            }

            RAMS RAMS = new RAMS(this._ProcessorLogin, this._LiveEnvironment);
            RAMS.Login();

            ClientInfo cInfo = new ClientInfo();
            cInfo.ClientID = testAcct;

            if (RAMS.FindClientById(ref cInfo))
            {
                RAMS.GetClientTransactionsByClientID(ref cInfo);
                if (cInfo.Transactions_dt.Rows.Count > 0)
                {
                    System.Data.DataView tView = cInfo.Transactions_dt.DefaultView;
                    tView.Sort = "paymentdate DESC, amount ASC";

                    //Create Datatable with single column containing all Client's IDs to pass as parameter in below method
                    DataTable dt_ClientIDs = new DataTable();

                    dt_ClientIDs = tView.ToTable("Clients", false, "clid");
                    cInfo.ClientBalances_dt = RAMS.GetClientBalances(dt_ClientIDs);
                    string[] ClientInfo = RAMS.GetClientInfobyID(cInfo.ClientID);

                    foreach (DataRow row in tView.Table.Rows)
                    {
                        TransactionDS.ClientDepositRow cd = tx.ClientDeposit.NewClientDepositRow();
                        cd.ClientID = string.Empty;
                        cd.RecordID = string.Empty;
                        cd.ClientName = string.Empty;
                        cd.Date = DateTime.Parse(row["paymentdate"].ToString());
                        cd.ProcessorAccount = row["clid"].ToString();
                        cd.Type = row["action"].ToString();
                        cd.Amount = Double.Parse(row["action"].ToString());
                        cd.ClearedDate = DateTime.Parse(row["postdate"].ToString());
                        cd.Status = row["status"].ToString();
                        cd.SyncNotes = row["notes"].ToString();
                        tx.ClientDeposit.Rows.Add(cd);
                    }


                    foreach (DataRow row in cInfo.ClientBalances_dt.Rows)
                    {
                        TransactionDS.ClientsRow cr = tx.Clients.NewClientsRow();
                        if (row["trustAccountBalance"].ToString() != "Error")
                        {
                            cr.AccountBalance = Double.Parse(row["trustAccountBalance"].ToString());
                        }

                        cr.Status = ClientInfo[22];
                        //cr.LastSyncDate = row.MODIFIED_DATE;
                        cr.FirstName = ClientInfo[1];
                        cr.LastName = ClientInfo[2];
                        //cr.LastStatus = row.DRAFTS_AUTHORIZED;
                        cr.IsNewAccount = false;
                        tx.Clients.Rows.Add(cr);
                    }
                }
            }
        }

        public void CheckEPPSClientAccount(string accountNumber, out TransactionDS tx)
        {
            tx = new TransactionDS();
            int testAcct = -1;

            // if the supplied account number is not a valid number, then return
            if (!int.TryParse(accountNumber, out testAcct))
            {
                return;
            }

            EPPS EPPS = new EPPSInterop.EPPS(this._ProcessorLogin, this._ProcessorPwd);

            EPPSApi.FindCardHolder Cardholder = new EPPSApi.FindCardHolder();
            EPPSApi.CardHolderDetail oDetails = new EPPSApi.CardHolderDetail();
            oDetails.CardHolderID = testAcct.ToString();

            Cardholder = EPPS.RetrieveCardHolderById(oDetails);

            if (!Cardholder.CardHolderList.Length.Equals(0) && Cardholder.Message.Equals("Success"))
            {
                EPPSApi.FindEFT FindEftDetails = new EPPSApi.FindEFT();
                FindEftDetails = EPPS.RetrieveEFTById(oDetails.CardHolderID);
                EPPSApi.FindEftChange EFTChange = new EPPSApi.FindEftChange();
                //EPPSApi.EFTTransactionDetail transDetails = FindEftDetails.EFTList.FirstOrDefault(item => item.CardHolderId == oDetails.CardHolderID);               

                foreach (EPPSApi.EFTTransactionDetail transDetail in FindEftDetails.EFTList)
                {
                    EFTChange = EPPS.RetrieveFindEftChangebyID(transDetail.EftTransactionID);
                    TransactionDS.ClientDepositRow cd = tx.ClientDeposit.NewClientDepositRow();
                    cd.ClientID = string.Empty;
                    cd.RecordID = transDetail.EftTransactionID;
                    cd.RecordName = EFTChange.EftChangeList.LastOrDefault(item => item.EftTransactionID.Equals(transDetail.EftTransactionID)).AccountType;
                    cd.ClientName = string.Empty;
                    if (!String.IsNullOrEmpty(transDetail.EftDate.ToString()))
                    { cd.Date = transDetail.CreatedDate; }
                    cd.ProcessorAccount = transDetail.CardHolderId;
                    cd.Amount = Double.Parse(transDetail.EftAmount.ToString());
                    if (!String.IsNullOrEmpty(transDetail.SettledDate.ToString()))
                    { cd.ClearedDate = DateTime.Parse(transDetail.SettledDate); }
                    cd.Status = transDetail.StatusCode;
                    cd.SyncNotes = transDetail.Memo;
                    cd.BankAccountNumber = transDetail.AccountNumber;
                    cd.BankName = transDetail.BankName;
                    cd.BankRoutingNumber = transDetail.RoutingNumber;
                    cd.LastSyncDate = DateTime.Now;
                    //cd.Type = EFTChange.EftChangeList.LastOrDefault(item => item.EftTransactionID.Equals(transDetail.EftTransactionID)).AccountType;
                    tx.ClientDeposit.Rows.Add(cd);

                }

                foreach (EPPSApi.CardHolderDetail CHDetails in Cardholder.CardHolderList)
                {
                    //EPPSApi.CardHolderDetail CH  =  Cardholder.CardHolderList.FirstOrDefault(item => item.CardHolderID == oDetails.CardHolderID) ;
                    TransactionDS.ClientsRow cr = tx.Clients.NewClientsRow();
                    cr.FirstName = CHDetails.FirstName;
                    cr.LastName = CHDetails.LastName;
                    cr.IsNewAccount = false;
                    cr.State = CHDetails.State;
                    cr.AccountBalance = Double.Parse(CHDetails.AccountBalance);
                    cr.ProcessorAccountNum = CHDetails.CardHolderID;
                    cr.LastSyncDate = DateTime.Now;
                    //cr.AccountType = EFTChange.EftChangeList.LastOrDefault(item => item.CardHolderId.Equals(CHDetails.CardHolderID)).AccountType;
                    cr.Status = "";
                    tx.Clients.Rows.Add(cr);
                }
            }
        }

        #endregion

        #region Helper Functions

        /// <summary>
        /// Retrieves all GCS transactions processed within a specified date range
        /// </summary>
        /// <param name="startDate">The start date</param>
        /// <param name="endDate">The end date</param>
        /// <returns>The transactions list</returns>
        private TransactionDS GetGCSTransactions(DateTime startDate, DateTime endDate)
        {
            List<string> errors = new List<string>();
            TransactionDS tds = new TransactionDS();

            // create an instance of the GCS interop class
            GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);
            GcsApi.TransactionsWSDS clientTransactions = gcs.GetTransactions(startDate, endDate);
            GcsApi.ClientsWSDS clientList = gcs.GetClients();

            foreach (GcsApi.TransactionsWSDS.TRANSACTIONSRow draft in clientTransactions.TRANSACTIONS)
            {
                GcsApi.ClientsWSDS.CLIENTSRow client = clientList.CLIENTS.FindByACCOUNT_ID(draft.ACCOUNT_ID);
                if (client != null)
                {
                    TransactionDS.ClientDepositRow row = tds.ClientDeposit.NewClientDepositRow();
                    try
                    {
                        row.RecordID = client.CLIENT_ID;
                        row.ClientID = client.CLIENT_ID;
                        row.ClientName = client.FIRST_NAME + " " + client.LAST_NAME;
                        row.BankAccountType = client.BANK_ACCOUNT_TYPE;
                        row.BankAccountNumber = client.BANK_ACCOUNT_NUM;
                        row.BankRoutingNumber = client.BANK_ROUTING_NUM;
                        row.ClientImportID = client.CLIENT_ID.Length < 8 ? client.CLIENT_ID : string.Empty;
                        row.LastSyncDate = DateTime.Today;
                        row.Processor = this._Processor; // For T2P-Hybrid & T2P-Protection Program .. this will display T2P Only
                    }
                    catch
                    {
                        row.HasSyncError = "True";
                        row.SyncNotes = "Unknown account - Please Check Manually";
                    }

                    row.ProcessorAccount = draft.ACCOUNT_ID;
                    row.TransactionID = draft.IsREFERENCE_IDNull() ? string.Empty : draft.REFERENCE_ID.ToString();
                    row.Status = this.GetStatus(gcs, draft);
                    row.Date = draft.TRANSACTION_DATE;
                    row.Amount = (double)draft.TRANSACTION_AMOUNT;
                    tds.ClientDeposit.Rows.Add(row);
                }
                else
                {

                }

            }
            return tds;
        }

        private TransactionDS GetRAMSTransactions(DateTime startDate, DateTime endDate)
        {
            List<string> errors = new List<string>();
            TransactionDS tds = new TransactionDS();

            // create an instance of the RAMS interop class
            RAMS RAMS = new RAMS(this._ProcessorLogin, this._LiveEnvironment);
            RAMS.Login();
            DataTable clientTransactions = new DataTable();
            clientTransactions = RAMS.GetAllTransactionsByDate(startDate, endDate);

            //Create Datatable with single column containing all Client's IDs to pass as parameter in below method
            DataTable dt_ClientIDs = new DataTable();
            System.Data.DataView view = new System.Data.DataView(clientTransactions);
            dt_ClientIDs = view.ToTable("Clients", false, "clid");

            string[] ClientIDs = null;
            if (dt_ClientIDs.Rows.Count > 0)
            {
                ClientIDs = dt_ClientIDs.AsEnumerable().Select(r => r[0].ToString()).ToArray();
            }

            // retrieve the list of all client Information
            DataTable dt_ClientInfo = RAMS.GetAllClients(ClientIDs);

            foreach (DataRow draft in clientTransactions.Rows)
            {
                TransactionDS.ClientDepositRow row = tds.ClientDeposit.NewClientDepositRow();
                // if the account exists, retrieve the client record 
                System.Data.DataRow[] dr_clientRow = dt_ClientInfo.Select("ClientID = '" + draft["clid"].ToString() + "'");

                if (dr_clientRow.Length > 0 && String.IsNullOrEmpty(dr_clientRow[0]["Error"].ToString()))
                {
                    try
                    {
                        row.RecordID = dr_clientRow[0]["ClientID"].ToString();
                        row.ClientID = dr_clientRow[0]["ClientID"].ToString();
                        row.ClientName = dr_clientRow[0]["FirstName"].ToString() + " " + dr_clientRow[0]["LastName"].ToString();
                        //row.BankAccountType = dr_clientRow[0]["ClientID"].ToString();         //Not provided in methods
                        //row.BankAccountNumber = dr_clientRow[0]["ClientID"].ToString();
                        //row.BankRoutingNumber = dr_clientRow[0]["ClientID"].ToString();
                        //row.ClientImportID = client.CLIENT_ID.Length < 8 ? client.CLIENT_ID : string.Empty;
                        row.LastSyncDate = DateTime.Today;
                        row.Processor = "RAMS";

                    }
                    catch
                    {
                        row.HasSyncError = "True";
                        row.SyncNotes = "Unknown account - Please Check Manually";
                    }

                    row.ProcessorAccount = draft["clid"].ToString();
                    row.TransactionID = draft["Id"].ToString();
                    row.Status = draft["status"].ToString();
                    row.Date = DateTime.Parse(draft["paymentdate"].ToString());
                    row.Amount = Double.Parse(draft["amount"].ToString());
                    row.SyncNotes = draft["notes"].ToString();
                    tds.ClientDeposit.Rows.Add(row);
                }
            }
            return tds;
        }

        private TransactionDS GetEPPSTransactions(DateTime startDate, DateTime endDate)
        {
            List<string> errors = new List<string>();
            TransactionDS tds = new TransactionDS();

            EPPS EPPS = new EPPSInterop.EPPS(this._ProcessorLogin, this._ProcessorPwd);

            EPPSApi.FindEFT EFTResults = new EPPSApi.FindEFT();
            EFTResults = EPPS.RetrieveEFTByCreateDate(startDate, endDate);

            foreach (EPPSApi.EFTTransactionDetail TransDetails in EFTResults.EFTList)
            {
                EPPSApi.CardHolderDetail oDetails = new EPPSApi.CardHolderDetail();
                oDetails.CardHolderID = TransDetails.CardHolderId;
                EPPSApi.FindCardHolder Cardholder = new EPPSApi.FindCardHolder();
                Cardholder = EPPS.RetrieveCardHolderById(oDetails);

                TransactionDS.ClientDepositRow row = tds.ClientDeposit.NewClientDepositRow();
                try
                {
                    row.RecordID = TransDetails.EftTransactionID;
                    row.ClientName = String.Concat(Cardholder.CardHolderList.LastOrDefault(item => item.CardHolderID.Equals(TransDetails.CardHolderId)).FirstName, " ", Cardholder.CardHolderList.LastOrDefault(item => item.CardHolderID.Equals(TransDetails.CardHolderId)).LastName);
                    row.ClientID = TransDetails.CardHolderId;
                    row.BankName = TransDetails.BankName;
                    row.BankAccountNumber = TransDetails.AccountNumber;
                    row.BankRoutingNumber = TransDetails.RoutingNumber;
                    row.LastSyncDate = DateTime.Today;
                    row.Processor = this._Processor;
                    row.TransactionID = TransDetails.EftTransactionID;
                    row.Status = TransDetails.StatusCode;
                    row.ProcessorAccount = TransDetails.CardHolderId;
                    row.Amount = Double.Parse(TransDetails.EftAmount.ToString());
                    row.Date = TransDetails.EftDate;
                    if (!String.IsNullOrEmpty(TransDetails.SettledDate.ToString()))
                        row.ClearedDate = DateTime.Parse(TransDetails.SettledDate);
                    row.SyncNotes = TransDetails.Memo;
                }
                catch
                {
                    row.HasSyncError = "True";
                    row.SyncNotes = "Unknown account - Please Check Manually";
                }
                tds.ClientDeposit.Rows.Add(row);
            }
            return tds;
        }

        /// <summary>
        /// Checks the status of all GCS transactions processed after a specified date
        /// </summary>
        /// <param name="start">The start date</param>
        /// <returns>The results list</returns>
        private List<string> CheckGCSTransactions(DateTime start)
        {
            List<string> errors = new List<string>();
            TransactionDS tds = new TransactionDS();

            SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            sf.GetDraftsForUpdate(ref tds, this._Processor);

            // create an instance of the GCS interop class
            GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);
            GcsApi.TransactionsWSDS clientTransactions = gcs.GetTransactions(start.AddDays(-7));

            if (clientTransactions.TRANSACTIONS.Count > 0)
            {
                foreach (TransactionDS.ClientDepositRow cdR in tds.ClientDeposit.Rows)
                {
                    // retrieve all the transactions updated since the start date
                    string accountNumber = cdR.ProcessorAccount;
                    decimal testTranID = -1;
                    //DateTime testDate = new DateTime();

                    if (!cdR.IsTransactionIDNull() && !string.IsNullOrEmpty(cdR.TransactionID) && decimal.TryParse(cdR.TransactionID, out testTranID) && testTranID > 0)
                    {
                        System.Data.DataRow[] test = clientTransactions.TRANSACTIONS.Select("REFERENCE_ID = '" + cdR.TransactionID + "'");
                        if (test != null && test.Length > 0)
                        {
                            cdR.Status = this.GetStatus(gcs, (GcsApi.TransactionsWSDS.TRANSACTIONSRow)test[0]);
                            cdR.AmountPaid = (double)((GcsApi.TransactionsWSDS.TRANSACTIONSRow)test[0]).TRANSACTION_AMOUNT;
                            if ((cdR.Status == "Cleared" || cdR.Status == "Completed") && (!((GcsApi.TransactionsWSDS.TRANSACTIONSRow)test[0]).IsTRANSACTION_DATENull()))
                                cdR.ClearedDate = ((GcsApi.TransactionsWSDS.TRANSACTIONSRow)test[0]).POST_DATE;
                        }
                    }
                    else
                    {
                        System.Data.DataRow[] test = clientTransactions.TRANSACTIONS.Select("TRANSACTION_AMOUNT = " + cdR.Amount + " AND DEBIT_DATE = '" + cdR.Date.AddHours(1) + "' AND ACCOUNT_ID ='" + cdR.ProcessorAccount + "'");

                        if (test != null && test.Length > 0)
                        {
                            cdR.Status = this.GetStatus(gcs, (GcsApi.TransactionsWSDS.TRANSACTIONSRow)test[0]);
                            cdR.TransactionID = ((GcsApi.TransactionsWSDS.TRANSACTIONSRow)test[0]).REFERENCE_ID.ToString();
                            cdR.AmountPaid = (double)((GcsApi.TransactionsWSDS.TRANSACTIONSRow)test[0]).TRANSACTION_AMOUNT;
                            if ((cdR.Status == "Cleared" || cdR.Status == "Completed") && (!((GcsApi.TransactionsWSDS.TRANSACTIONSRow)test[0]).IsTRANSACTION_DATENull()))
                                cdR.ClearedDate = ((GcsApi.TransactionsWSDS.TRANSACTIONSRow)test[0]).POST_DATE;
                        }
                    }
                    cdR.AcceptChanges();
                }
            }

            if (tds.ClientDeposit.Rows.Count > 0)
            {
                SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                List<string> results = salesForce.SetStatus(tds);
                salesForce.Logout();

                this.UpdatedTransactions = tds;
                return results;
            }
            else
            {
                return new List<string>();
            }
        }

        /// <summary>
        /// Checks the status of all GCS transactions processed after a specified date
        /// </summary>
        /// <param name="start">The start date</param>
        /// <returns>The results list</returns>
        private List<string> UpdateRAMSTransactions(DateTime start)
        {
            TransactionDS tds = new TransactionDS();
            TransactionDS tdsOut = new TransactionDS();
            SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            salesForce.GetDraftsRAMSForUpdate(ref tds);
            salesForce.Logout();

            RAMS Rams = new RAMS(this._ProcessorLogin, this._LiveEnvironment);
            Rams.Login();

            DataTable ClientTrans_Rams = Rams.GetClientTransByPaymentDateRange(start.AddDays(-7), start.AddDays(10));

            if (ClientTrans_Rams != null && ClientTrans_Rams.Rows.Count > 0)
            {
                foreach (TransactionDS.ClientDepositRow row in tds.ClientDeposit.Rows)
                {
                    // retrieve all the transactions updated since the start date
                    string accountNumber = row.ProcessorAccount;
                    decimal testTranID = -1;

                    if (!row.IsTransactionIDNull() && !string.IsNullOrEmpty(row.TransactionID) && decimal.TryParse(row.TransactionID, out testTranID) && testTranID > 0)
                    {
                        System.Data.DataRow[] test = ClientTrans_Rams.Select("Id = '" + row.TransactionID + "'");

                        if (test != null && test.Length > 0)
                        {
                            TransactionDS.ClientDepositRow rowdata = tdsOut.ClientDeposit.NewClientDepositRow();
                            try { rowdata.RecordID = row.RecordID; }
                            catch (Exception) { }

                            try { rowdata.ClientID = row.ClientID; }
                            catch (Exception) { }

                            try { rowdata.ClientName = row.ClientName; }
                            catch (Exception) { }

                            try { rowdata.BankAccountType = row.BankAccountType; }
                            catch (Exception) { }

                            try { rowdata.BankAccountNumber = row.BankAccountNumber; }
                            catch (Exception) { }

                            try { rowdata.BankRoutingNumber = row.BankRoutingNumber; }
                            catch (Exception) { }

                            try { rowdata.BankName = row.BankName; }
                            catch (Exception) { }

                            try { rowdata.Amount = row.Amount; }
                            catch (Exception) { }

                            try { rowdata.Date = row.Date; }
                            catch (Exception) { }

                            try { rowdata.ProcessorAccount = row.ProcessorAccount; }
                            catch (Exception) { }

                            try { rowdata.TransactionID = row.TransactionID; }
                            catch (Exception) { }

                            try { rowdata.ParentDRCID = row.ParentDRCID; }
                            catch (Exception) { }

                            try { rowdata.Type = row.Type; }
                            catch (Exception) { }

                            try { rowdata.TypeDesc = row.TypeDesc; }
                            catch (Exception) { }

                            try { rowdata.Status = row.Status; }
                            catch (Exception) { }

                            try { rowdata.ClientImportID = row.ClientImportID; }
                            catch (Exception) { }

                            try { rowdata.ProcessorFee = row.ProcessorFee; }
                            catch (Exception) { }

                            try { rowdata.Processor = row.Processor; }
                            catch (Exception) { }

                            try { rowdata.LastSyncDate = row.LastSyncDate; }
                            catch (Exception) { }
                            rowdata.Status = "Cleared";
                            rowdata.SyncNotes = test[0]["notes"].ToString();
                            rowdata.AmountPaid = Double.Parse(test[0]["amount"].ToString());
                            if ((row.Status == "Cleared" || row.Status == "Completed") && !String.IsNullOrEmpty(test[0]["postdate"].ToString()))
                                rowdata.ClearedDate = DateTime.Parse(test[0]["postdate"].ToString());
                            rowdata.AcceptChanges();
                            tdsOut.ClientDeposit.AddClientDepositRow(rowdata);
                        }
                    }
                    //rowdata.AcceptChanges();
                    //dsOut.ClientDeposit.AddClientDepositRow(rowdata);   
                }
            }


            if (tdsOut.ClientDeposit.Rows.Count > 0)
            {
                salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                List<string> results = salesForce.SetStatus(tds);
                //salesForce.UpdateProcessRAMSPayment(tdsOut);
                salesForce.Logout();

                this.UpdatedTransactions = tdsOut;
                return results;
            }
            else
            {
                return new List<string>();
            }
        }

        private TransactionDS.ClientsDataTable ViewPendingEPPSClients()
        {
            TransactionDS.ClientsDataTable dt = null;
            try
            {
                // create a new instance of the salesForce interop class
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                dt = sf.GetPendingClients(this._Processor);
                sf.Logout();
            }
            catch (Exception ex)
            {
                this.WriteError(ex, "");
            }
            finally
            {
                this.CreatedClients = dt;
            }
            return dt;
        }

        private TransactionDS.ClientsDataTable CreateEPPSClients()
        {
            TransactionDS.ClientsDataTable dt = null;

            // create a new connection to EPPS
            EPPS Epps = new EPPS(this._ProcessorLogin, this._ProcessorPwd);

            try
            {
                int EPPSClientCount = 1;

                foreach (TransactionDS.ClientsRow row in dt.Rows)
                {
                    if (!string.IsNullOrEmpty(row.Processor) && row.Processor.ToUpper().Equals("EPPS"))
                    {
                        try
                        {
                            EPPSApi.FindCardHolder Cardholder = new EPPSApi.FindCardHolder();
                            EPPSApi.CardHolderDetail oDetails = new EPPSApi.CardHolderDetail();



                            oDetails.CardHolderID = row.ClientIDforRAMandEPPS.ToString();
                            oDetails.FirstName = row.FirstName;
                            oDetails.LastName = row.LastName;
                            oDetails.DateOfBirth = row.DateOfBirth;
                            oDetails.Phone = row.PhoneNumber.Replace("-", string.Empty).Trim(); ;
                            oDetails.SSN = row.SocialSecurityNumber.Replace("-", string.Empty).Trim();
                            oDetails.Address = row.Address1;
                            oDetails.City = row.City;
                            oDetails.State = row.State;
                            oDetails.Zip = row.Zip;
                            oDetails.Email = row.Email;

                            Cardholder = Epps.RetrieveCardHolderById(oDetails);
                            if (Cardholder.CardHolderList.Length.Equals(0) && Cardholder.Message.Equals("Success"))
                            {
                                EPPSApi.CardHolder CH = Epps.CreateCardHolder(oDetails);
                                if (CH.Message == "Success" && CH.CardHolderID == oDetails.CardHolderID)
                                {
                                    row.ProcessorStatus = "Active";
                                    row.LastSyncDate = DateTime.Now;
                                    row.RowNumber = EPPSClientCount++;
                                    row.ProcessorAccountNum = row.ClientIDforRAMandEPPS.ToString(); // Doubt 
                                    row.SyncDescription = "Account Created sucessfully";
                                    row.AcceptChanges();
                                }
                                else
                                {
                                    Exception ex = new Exception(CH.Message);
                                    throw ex;
                                }
                            }
                            else
                            {
                                row.ProcessorStatus = "Active";
                                row.LastSyncDate = DateTime.Now;
                                row.RowNumber = EPPSClientCount++;
                                row.ProcessorAccountNum = row.ClientIDforRAMandEPPS.ToString(); // Doubt 
                                row.SyncDescription = "Account already exist";
                                row.AcceptChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            string ErrorMessage = ex.Message.Replace("DBNull", "empty");
                            if (ErrorMessage.Contains("SqlDateTime overflow"))
                            {
                                ErrorMessage = "Incorrect Date of Birth";
                            }
                            //this.WriteError(ex, row.ClientID);
                            row.ProcessorAccountNum = string.Empty;
                            row.AccountBalance = 0;
                            row.LastSyncDate = DateTime.Now;
                            row.SyncDescription = "Error creating new Payment Processor account: " + ErrorMessage;
                            row.ProcessorStatus = row.LastStatus;
                            row.AcceptChanges();
                        }
                    }
                }

                // re-connect to SalesForce to update the processor account numbers
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.UpdateClientList(dt);
                if (_IsLogEnabled.Equals(true))
                {
                    sf.WriteClientLog(dt);
                }
                sf.Logout();
            }
            catch (Exception ex)
            {
                this.WriteError(ex, "");
            }
            finally
            {
                this.CreatedClients = dt;
            }
            return dt;
        }

        private TransactionDS.ClientsDataTable ViewAIMClients()
        {
            TransactionDS.ClientsDataTable dt = null;

            try
            {
                // create a new instance of the salesForce interop class
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                dt = sf.GetAuthorizePendingClients(this._Processor);
                sf.Logout();
            }
            catch (Exception ex)
            {
                this.WriteError(ex, "");
            }
            finally
            {
                this.CreatedClients = dt;
            }
            return dt;
        }

        private TransactionDS.ClientsDataTable CreateAuthorizeDotNetClients()
        {
            TransactionDS.ClientsDataTable dt = null;

            try
            {
                foreach (TransactionDS.ClientsRow row in dt.Rows)
                {
                    if (!string.IsNullOrEmpty(row.Processor) && row.Processor.Equals("Authorize.Net"))
                    {
                        try
                        {
                            if (
                                !string.IsNullOrEmpty(row.FirstName) && !string.IsNullOrEmpty(row.LastName)
                                && !string.IsNullOrEmpty(row.Address1) && !string.IsNullOrEmpty(row.City)
                                && !string.IsNullOrEmpty(row.State) && !string.IsNullOrEmpty(row.Zip)
                                && !string.IsNullOrEmpty(row.BankRoutingNumber) && !string.IsNullOrEmpty(row.BankAccountNumber)
                                && !string.IsNullOrEmpty(row.BankName) && !string.IsNullOrEmpty(row.NameonAccount)
                                )
                            {
                                row.ProcessorStatus = "Active";
                                row.SyncDescription = "Account Created sucessfully";
                            }
                            else
                            {
                                row.SyncDescription = "Please provide";
                                row.SyncDescription += string.IsNullOrEmpty(row.FirstName) ? ", First Name" : string.Empty;
                                row.SyncDescription += string.IsNullOrEmpty(row.LastName) ? ", Last Name" : string.Empty;
                                row.SyncDescription += string.IsNullOrEmpty(row.Address1) ? ", Address" : string.Empty;
                                row.SyncDescription += string.IsNullOrEmpty(row.City) ? ", City" : string.Empty;
                                row.SyncDescription += string.IsNullOrEmpty(row.State) ? ", State" : string.Empty;
                                row.SyncDescription += string.IsNullOrEmpty(row.Zip) ? ", Postal Code" : string.Empty;
                                row.SyncDescription += string.IsNullOrEmpty(row.BankRoutingNumber) ? ", Routing Number" : string.Empty;
                                row.SyncDescription += string.IsNullOrEmpty(row.BankAccountNumber) ? ", Account Number" : string.Empty;
                                row.SyncDescription += string.IsNullOrEmpty(row.BankName) ? ", Bank Name" : string.Empty;
                                row.SyncDescription += string.IsNullOrEmpty(row.NameonAccount) ? ", Account Name" : string.Empty;
                                row.SyncDescription = row.SyncDescription.Replace("provide,", "provide");
                                row.SyncDescription += ".";
                                row.ProcessorStatus = "Errored";
                            }
                            row.ProcessorAccountNum = string.Empty;
                            row.LastSyncDate = DateTime.Now;
                            row.AcceptChanges();
                        }
                        catch (Exception ex)
                        {

                            row.ProcessorAccountNum = string.Empty;
                            row.AccountBalance = 0;
                            row.LastSyncDate = DateTime.Now;
                            row.SyncDescription = ex.Message;
                            row.ProcessorStatus = row.LastStatus;
                            row.AcceptChanges();
                        }
                    }
                }

                // re-connect to SalesForce to update the processor account numbers
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.UpdateClientList(dt);
                sf.Logout();
            }
            catch (Exception ex)
            {
                this.WriteError(ex, "");
            }
            finally
            {
                this.CreatedClients = dt;
            }
            return dt;
        }

        private TransactionDS.ClientsDataTable ViewFDClients()
        {
            TransactionDS.ClientsDataTable dt = null;

            try
            {
                // create a new instance of the salesForce interop class
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                dt = sf.GetFDClients(this._Processor);
                sf.Logout();
            }
            catch (Exception ex)
            {
                this.WriteError(ex, "");
            }
            finally
            {
                this.CreatedClients = dt;
            }
            return dt;
        }

        private TransactionDS.ClientsDataTable CreateFDClients()
        {
            TransactionDS.ClientsDataTable dt = null;
            dt = this.CreatedClients;
            try
            {
                foreach (TransactionDS.ClientsRow row in dt.Rows)
                {
                    if (!string.IsNullOrEmpty(row.Processor) && row.Processor.Equals("FirstData"))
                    {
                        try
                        {
                            if (
                                !string.IsNullOrEmpty(row.ClientNameID) && !row.CardExpiaryDate.Equals("1/1/0001 12:00:00 AM")
                                && !string.IsNullOrEmpty(row.DebitCardNumber)
                                )
                            {
                                row.ProcessorStatus = "Active";
                                row.SyncDescription = "Account Created sucessfully";
                            }
                            else
                            {
                                row.SyncDescription = "Please provide";
                                row.SyncDescription += string.IsNullOrEmpty(row.ClientNameID) ? ", Card Holder Name" : string.Empty;
                                row.SyncDescription += !row.CardExpiaryDate.Equals("1/1/0001 12:00:00 AM") ? ", Card Expiry Date" : string.Empty;
                                row.SyncDescription += string.IsNullOrEmpty(row.DebitCardNumber) ? ", Card Number" : string.Empty;
                                row.SyncDescription = row.SyncDescription.Replace("provide,", "provide");
                                row.SyncDescription += ".";
                                row.ProcessorStatus = "Errored";
                            }
                            row.ProcessorAccountNum = string.Empty;
                            row.LastSyncDate = DateTime.Now;
                            row.AcceptChanges();
                        }
                        catch (Exception ex)
                        {

                            row.ProcessorAccountNum = string.Empty;
                            row.AccountBalance = 0;
                            row.LastSyncDate = DateTime.Now;
                            row.SyncDescription = ex.Message;
                            row.ProcessorStatus = row.LastStatus;
                            row.AcceptChanges();
                        }
                    }
                }

                // re-connect to SalesForce to update the processor account numbers
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.UpdateFDClientList(dt);
                sf.Logout();
            }
            catch (Exception ex)
            {
                this.WriteError(ex, "");
            }
            finally
            {
                this.CreatedClients = dt;
            }
            return dt;
        }

        /// <summary>
        /// Creates records in the RAMS system for clients marked to be created in SalesForce
        /// </summary>
        private TransactionDS.ClientsDataTable ViewPendingRAMClients()
        {
            TransactionDS.ClientsDataTable dt = null;

            try
            {
                // create a new instance of the salesForce interop class
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                dt = sf.GetPendingClients(this._Processor);
                sf.Logout();

            }
            catch (Exception ex)
            {
                this.WriteError(ex, "");
            }
            finally
            {
                this.CreatedClients = dt;
            }

            return dt;
        }

        /// <summary>
        /// Creates records in the RAMS system for clients marked to be created in SalesForce
        /// </summary>
        private TransactionDS.ClientsDataTable CreateRAMClients()
        {
            TransactionDS.ClientsDataTable dt = null;

            //// create a new connection to RAMS
            RAMS RAMS = new RAMS(this._ProcessorLogin, this._LiveEnvironment);
            RAMS.Login();

            try
            {

                int RAMSClientCount = 1;

                // do not continue if there are no new clients to create
                if (dt.Rows.Count == 0)
                {
                    return dt;
                }

                foreach (TransactionDS.ClientsRow row in dt.Rows)
                {
                    //if (string.IsNullOrEmpty(row.Processor) && (this.defaultProcessor == "RAMS"))
                    //{
                    //    row.Processor = this.defaultProcessor;
                    //}
                    if (!string.IsNullOrEmpty(row.Processor) && row.Processor.ToUpper().Equals("RAMS"))
                    {
                        try
                        {
                            //Name is used in place of Client ID
                            RAMSInterop.ClientInfo oClientInfo = new ClientInfo();
                            oClientInfo.ClientID = row.ClientIDforRAMandEPPS;
                            oClientInfo.Firstname = row.FirstName;
                            oClientInfo.Lastname = row.LastName;
                            oClientInfo.emailAddress = row.Email;
                            oClientInfo.taxID = row.SocialSecurityNumber.Length > 4 ? row.SocialSecurityNumber.Substring(row.SocialSecurityNumber.Length - 4, 4) : "";
                            oClientInfo.streetAddress = row.Address1;
                            oClientInfo.city = row.City;
                            oClientInfo.zip = row.Zip;
                            oClientInfo.stateAbbreviation = row.State;   //2 CHAR
                            oClientInfo.Phone1 = row.PhoneNumber;
                            oClientInfo.Phone2 = row.Phone2;
                            oClientInfo.Phone3 = row.Phone3;
                            oClientInfo.Dob1 = row.DateOfBirth.ToString("MM-dd-yyyy");
                            oClientInfo.AccountStatus = RAMSApi.AccountStatus.ACTIVE;
                            //oClientInfo.AffiliateID = 99630;
                            oClientInfo.AffiliateID = int.Parse(row.AffiateID);
                            oClientInfo.feeSplitGroupID = string.IsNullOrEmpty(row.FeeSplitGroupID) == true ? 0 : int.Parse(row.FeeSplitGroupID);
                            oClientInfo.AccountHoldStatus = RAMSApi.ACHStatus.ACTIVE;
                            oClientInfo.AccountNumber = row.BankAccountNumber;
                            oClientInfo.NameonAccount = row.NameonAccount;
                            oClientInfo.Routing = row.BankRoutingNumber;
                            oClientInfo.BankName = row.BankName;
                            oClientInfo.AccountType = row.AccountType.ToUpper() == "CHECKING" ? RAMSApi.BankAccountType.CONSUMER_DEBIT_CHECKING : RAMSApi.BankAccountType.CONSUMER_DEBIT_SAVINGS;

                            string SearchResponse = string.Empty;
                            if (!RAMS.FindClientById(ref oClientInfo))
                            {
                                RAMS.AddNewClient(ref oClientInfo);
                                if (oClientInfo.IsBankingCreated && oClientInfo.IsClientCreated)
                                {
                                    row.ProcessorStatus = "Active";
                                    row.LastSyncDate = DateTime.Now;
                                    row.RowNumber = RAMSClientCount++;
                                    row.ProcessorAccountNum = oClientInfo.ClientID.ToString();
                                    row.SyncDescription = "Account Created successfully";
                                    row.AcceptChanges();
                                }
                            }
                            else
                            {
                                //RAMS.UpdateClient(ref oClientInfo);
                                //if (oClientInfo.IsBankingCreated && oClientInfo.IsClientCreated)
                                //{
                                row.ProcessorStatus = "Active";
                                row.LastSyncDate = DateTime.Now;
                                row.RowNumber = RAMSClientCount++;
                                row.ProcessorAccountNum = oClientInfo.ClientID.ToString();
                                row.SyncDescription = "Account Updated successfully";
                                row.AcceptChanges();
                                //}
                            }
                        }
                        catch (Exception ex)
                        {
                            string ErrorMessage = ex.Message.Replace("DBNull", "empty");
                            if (ErrorMessage.Contains("SqlDateTime overflow"))
                            {
                                ErrorMessage = "Incorrect Date of Birth";
                            }
                            //this.WriteError(ex, row.ClientID);
                            row.ProcessorAccountNum = string.Empty;
                            row.AccountBalance = 0;
                            row.LastSyncDate = DateTime.Now;
                            row.SyncDescription = "Error creating new Payment Processor account: " + ErrorMessage;
                            row.ProcessorStatus = row.LastStatus;
                            row.RowNumber = RAMSClientCount++;
                            row.AcceptChanges();
                        }
                    }
                    else
                    {
                        row.SyncDescription = "Please select payment processor";
                        row.AcceptChanges();
                    }
                }

                // create the clients in the GCS system
                //this.CreateGCSClients(clientList, gcs, ref dt);

                // re-connect to SalesForce to update the processor account numbers
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.UpdateClientList(dt);
                sf.Logout();
            }
            catch (Exception ex)
            {
                this.WriteError(ex, "");
            }
            finally
            {
                this.CreatedClients = dt;
                RAMS.Logout();
            }

            return dt;
        }

        /// <summary>
        /// Creates records in the GCS system for clients marked to be created in SalesForce
        /// </summary>
        /// <exception cref="System.Exception">
        /// Sends any system exceptions through the standard error handling system
        /// </exception>
        private TransactionDS.ClientsDataTable ViewPendingGCSClients()
        {
            TransactionDS.ClientsDataTable dt = null;

            try
            {
                // create a new instance of the salesForce interop class
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                dt = sf.GetPendingClients(this._Processor);
                sf.Logout();
            }
            catch (Exception ex)
            {
                this.WriteError(ex, "");
            }
            finally
            {
                this.CreatedClients = dt;
            }

            return dt;
        }

        /// <summary>
        /// Creates records in the GCS system for clients marked to be created in SalesForce
        /// </summary>
        /// <exception cref="System.Exception">
        /// Sends any system exceptions through the standard error handling system
        /// </exception>
        private TransactionDS.ClientsDataTable CreateGCSandT2PClients()
        {
            TransactionDS.ClientsDataTable dt = null;
            dt = CreatedClients;
            try
            {
                // stores the list of clients to be created in the GCS system
                GcsApi.ClientsWSDS clientList = new GcsApi.ClientsWSDS();

                int gcsCount = 1;

                // create a new connection to GCS
                GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);

                foreach (TransactionDS.ClientsRow row in dt.Rows)
                {
                    if (!string.IsNullOrEmpty(row.Processor) &&
                        (row.Processor == "GCS" || row.Processor == "T2P-Hybrid" || row.Processor == "T2P-ProtectionProgram"))
                    {
                        try
                        {
                            GcsApi.ClientsWSDS.CLIENTSRow client = gcs.FindClientById(row.ClientNameID);

                            if (client == null)
                            {
                                client = clientList.CLIENTS.NewCLIENTSRow();

                                // if this account does not currently exist in the processor's system create it
                                if (string.IsNullOrEmpty(row.ProcessorAccountNum))
                                {
                                    client.ACCOUNT_ID = this.GetNewGCSAccount(gcs);
                                    row.ProcessorAccountNum = client.ACCOUNT_ID;
                                }
                                else
                                    client.ACCOUNT_ID = row.ProcessorAccountNum;
                                if (row.ClientNameID != null)
                                    client.CLIENT_ID = row.ClientNameID;
                                client.LAST_NAME = row.LastName;
                                client.FIRST_NAME = row.FirstName;
                                client.SOC_SEC_NUM = row.SocialSecurityNumber.Replace("-", string.Empty).Trim();
                                client.DATE_OF_BIRTH = row.DateOfBirth.AddDays(1);  // added a day as it was saving Actual b'date - 1 in record 
                                client.ADDRESS1 = row.Address1;
                                client.CITY = row.City;
                                client.STATE = row.State;
                                if (row.Country == null)
                                    row.Country = "US";
                                client.COUNTRY = row.Country;
                                client.ZIPCODE = row.Zip;
                                client.PHONE_NUM = row.PhoneNumber;
                                client.BANK_ACCOUNT_NUM = row.BankAccountNumber;
                                client.BANK_ROUTING_NUM = row.BankRoutingNumber;
                                client.BANK_ACCOUNT_TYPE = row.AccountType.Length > 0 ? row.AccountType[0].ToString() : "C";
                                client.ACTIVE_FLAG = "Y";
                                if (row.PolicyID != "")
                                    client.POLICY_GROUP_ID = Decimal.Parse(row.PolicyID);
                                else
                                    client.POLICY_GROUP_ID = gcs.getDefaultPolicyID();

                                clientList.CLIENTS.Rows.Add(client);

                                row.ProcessorStatus = "Active";
                                row.AccountBalance = 0;
                                row.LastSyncDate = DateTime.Now;
                                row.RowNumber = gcsCount++;
                            }
                            else
                            {
                                if (client.ACCOUNT_BALANCE > 0)
                                    row.AccountBalance = (double)client.ACCOUNT_BALANCE;
                                else
                                    row.AccountBalance = 0;
                                row.LastSyncDate = DateTime.Now;
                                row.ProcessorStatus = "Active";
                                row.ProcessorAccountNum = client.ACCOUNT_ID;
                            }
                            row.AcceptChanges();
                        }
                        catch (Exception ex)
                        {
                            //this.WriteError(ex, row.ClientID);
                            row.ProcessorAccountNum = string.Empty;
                            row.AccountBalance = 0;
                            row.LastSyncDate = DateTime.Now;
                            row.SyncDescription = "Error creating new Payment Processor account: " + ex.Message.Replace("DBNull", "empty");
                            row.ProcessorStatus = row.LastStatus;
                            row.AcceptChanges();
                        }
                    }
                }

                // create the clients in the GCS system
                this.CreateGCSClients(clientList, gcs, ref dt);

                foreach (TransactionDS.ClientsRow cRow in dt)
                {
                    if (String.IsNullOrEmpty(cRow.SyncDescription) && cRow.ProcessorStatus == "Active")
                    {
                        cRow.SyncDescription = "Account Created successfully";
                        cRow.AcceptChanges();
                    }
                }

                // re-connect to SalesForce to update the processor account numbers
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.UpdateClientList(dt);
                if (_IsLogEnabled.Equals(true))
                {
                    sf.WriteClientLog(dt);
                }
                sf.Logout();
            }
            catch (Exception ex)
            {
                this.WriteError(ex, "");
            }
            finally
            {
                this.CreatedClients = dt;
            }

            return dt;
        }

        /// <summary>
        /// Converts GCS status codes to the description
        /// </summary>
        /// <param name="code">GCS payment status code</param>
        /// <param name="depositID">The deposit # -- this is needed to check for specific NSF reasons</param>
        /// <returns>A description based on the status code</returns>
        private string GetStatus(GCS gcs, GcsApi.TransactionsWSDS.TRANSACTIONSRow transaction)
        {
            string depositID = transaction.IsEVALUCHECK_IDNull() ? string.Empty : transaction.EVALUCHECK_ID.ToString();
            string code = transaction.TRANSACTION_STATUS;
            string tempStatus = string.Empty;

            if (transaction.TRANSACTION_STATUS == "N")
            {
                if (!string.IsNullOrEmpty(depositID))
                {
                    GcsApi.DepositsWSDS deposits = gcs.GetDeposits(depositID);

                    if (deposits.DEPOSITS.Rows.Count > 0)
                    {
                        code = code + ((GcsApi.DepositsWSDS.DEPOSITSRow)deposits.DEPOSITS.Rows[0]).NSF_CODE;
                        tempStatus = ((GcsApi.DepositsWSDS.DEPOSITSRow)deposits.DEPOSITS.Rows[0]).NSF_REASON;
                    }
                }
            }

            string status = this.GetStatusFromCode(code);
            return string.IsNullOrEmpty(status) ? tempStatus : status;
        }

        /// <summary>
        /// Converts GCS status codes to the description
        /// </summary>
        /// <param name="code">GCS payment status code</param>
        /// <returns>A description based on the status code</returns>
        private string GetStatusFromCode(string code)
        {
            switch (code)
            {
                case "C":
                    return "Cleared";
                case "H":
                    return "Held";
                case "N":
                case "NI":
                case "N01":
                    return "Insufficient Funds";
                case "NC":
                case "N02":
                    return "Account Closed";
                case "NK":
                    return "Unknown Reason";
                case "NN":
                case "N04":
                    return "Account Not Found";
                case "NO":
                    return "NSF - Other";
                case "NR":
                case "N06":
                    return "Refer To Maker";
                case "NS":
                case "N07":
                    return "Payment Stopped";
                case "NT":
                case "N08":
                    return "Two Signatures Required";
                case "NU":
                case "N09":
                    return "Unauthorized";
                case "NV":
                case "N10":
                    return "Invalid Account";
                case "P":
                    return "Processing";
                case "R":
                    return "Reversed";
                case "V":
                    return "Voided";
            }

            return string.Empty;
        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS GetPendingPayments(DateTime start, DateTime end)
        {
            SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            TransactionDS ds = salesForce.GetTransactions(start, end, false, this._Processor);
            salesForce.Logout();

            // create an instance of the GCS interop class
            GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);

            TransactionDS tds2 = new TransactionDS();

            // retrieve all debits, deposits, payments and fees that were modified within the last two days
            /* BUGBUG ?? bt - Only checks for debits, deposits, payments, fees in last 2 days
             * */
            GcsApi.DebitsWSDS debits = gcs.GetDebits(start.AddDays(-29));

            // if the payment is already in the system, delete it from the list to be processed and mark it for update in SalesForce
            // as 0 the draft won't be sent to GCS but 
            foreach (GcsApi.DebitsWSDS.DEBITSRow debit in debits.DEBITS.Rows)
            {
                if (!debit.IsDRC_TRANSACTION_IDNull())
                {
                    TransactionDS.ClientDepositRow[] rows = ds.ClientDeposit.Select("RecordID = '" + debit.DRC_TRANSACTION_ID + "'") as TransactionDS.ClientDepositRow[];
                    if (rows.Length > 0)
                    {
                        for (int i = 0; i < rows.Length; i++)
                        {
                            rows[i].TransactionID = debit.DEBIT_ID.ToString();
                            rows[i].Status = "Processing";
                            rows[i].AmountPaid = (double)debit.DEBIT_AMOUNT;
                            tds2.ClientDeposit.ImportRow(rows[i]);

                            rows[i].Delete();
                        }
                    }
                }
            }

            ds.AcceptChanges();

            for (int i = 0; i < ds.ClientDeposit.Rows.Count; i++)
            {
                TransactionDS.ClientDepositRow deposit = ds.ClientDeposit.Rows[i] as TransactionDS.ClientDepositRow;

                if (deposit.IsProcessorAccountNull() || string.IsNullOrEmpty(deposit.ProcessorAccount.Trim()))
                {
                    // attempt to locate and populate the client's account ID
                    if (!this.FindAccountID(gcs, ref deposit, ref ds))
                    {
                        // remove the row from the list of payments to process
                        deposit.HasSyncError = "True";
                        deposit.SyncNotes = "Payment Processor account number not found!";
                        deposit.Status = "Pending";

                        tds2.ClientDeposit.ImportRow(deposit);
                    }
                }
            }

            int rowNumber = 1;

            // Remove all payments where there are no valid account numbers
            foreach (TransactionDS.ClientDepositRow deposit in ds.ClientDeposit.Rows)
            {
                if (!deposit.IsSyncNotesNull() && deposit.SyncNotes == "Payment Processor account number not found!")
                {
                    deposit.Delete();
                }
                else
                {
                    deposit.RowNumber = rowNumber++;
                }
            }

            // re-connect to SalesForce to update the payment statuses
            if (tds2.ClientDeposit.Rows.Count > 0 || tds2.ClientDepositFee.Rows.Count > 0)
            {
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.SetStatus(tds2);
                sf.Logout();
            }

            // re-connect to SalesForce to update the processor account numbers
            if (ds.Clients.Rows.Count > 0)
            {
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.UpdateClientList(ds.Clients);
                sf.Logout();
            }

            ds.ClientDeposit.AcceptChanges();
            return ds;
        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS GetPendingT2PPayments(DateTime start, DateTime end)
        {
            Hashtable acctHash = new Hashtable();
            SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            bool flag = false;
            if (String.IsNullOrEmpty(this._AuthNetID) || String.IsNullOrEmpty(this._AuthNetPwd))
            {
                flag = true;
            }
            TransactionDS ds = salesForce.GetT2PTransactions(start, end, flag);
            salesForce.Logout();
            return ds;
        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS CreateT2PPayments(ref TransactionDS ds, DateTime start, DateTime end)
        {
            Hashtable acctHash = new Hashtable();
            //SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            //bool flag = false;
            //if (String.IsNullOrEmpty(this._AuthNetID) || String.IsNullOrEmpty(this._AuthNetPwd))
            //{
            //    flag = true;
            //}
            //TransactionDS ds = salesForce.GetT2PTransactions(start, end, flag);
            //salesForce.Logout();
            // create an instance of the GCS interop class
            GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);

            TransactionDS tds2 = new TransactionDS();

            // retrieve all debits, deposits, payments and fees that were modified within the last two days
            /* BUGBUG ?? bt - Only checks for debits, deposits, payments, fees in last 2 days
             * */
            GcsApi.DebitsWSDS debitsRetrieve = gcs.GetFees(start.AddDays(-29));



            // if the payment is already in the system, delete it from the list to be processed and mark it for update in SalesForce
            // as 0 the draft won't be sent to GCS but 
            foreach (GcsApi.DebitsWSDS.DEBITSRow debit in debitsRetrieve.DEBITS.Rows)
            {
                if (!debit.IsDRC_TRANSACTION_IDNull())
                {
                    TransactionDS.ClientDepositRow[] rows = ds.ClientDeposit.Select("RecordID = '" + debit.DRC_TRANSACTION_ID + "'") as TransactionDS.ClientDepositRow[];
                    if (rows.Length > 0)
                    {
                        for (int i = 0; i < rows.Length; i++)
                        {
                            rows[i].TransactionID = debit.DEBIT_ID.ToString();
                            rows[i].Status = "Processing";
                            rows[i].AmountPaid = (double)debit.DEBIT_AMOUNT;
                            tds2.ClientDeposit.ImportRow(rows[i]);

                            rows[i].Delete();

                        }
                    }
                }
            }

            ds.AcceptChanges();

            //Remove duplicate payments
            debitsRetrieve = gcs.GetDebits(start.AddDays(-29));
            foreach (GcsApi.DebitsWSDS.DEBITSRow debit in debitsRetrieve.DEBITS.Rows)
            {
                if (!debit.IsDRC_TRANSACTION_IDNull())
                {
                    TransactionDS.ClientDepositRow[] rows = ds.ClientDeposit.Select("RecordID = '" + debit.DRC_TRANSACTION_ID + "'") as TransactionDS.ClientDepositRow[];
                    if (rows.Length > 0)
                    {
                        for (int i = 0; i < rows.Length; i++)
                        {
                            rows[i].TransactionID = debit.DEBIT_ID.ToString();
                            rows[i].Status = "Processing";
                            rows[i].AmountPaid = (double)debit.DEBIT_AMOUNT;
                            tds2.ClientDeposit.ImportRow(rows[i]);
                            rows[i].Delete();
                        }
                    }
                }
            }

            ds.AcceptChanges();

            for (int i = 0; i < ds.ClientDeposit.Rows.Count; i++)
            {
                TransactionDS.ClientDepositRow deposit = ds.ClientDeposit.Rows[i] as TransactionDS.ClientDepositRow;

                if (deposit.IsProcessorAccountNull() || string.IsNullOrEmpty(deposit.ProcessorAccount.Trim()))
                {
                    // attempt to locate and populate the client's account ID
                    if (!this.FindAccountID(gcs, ref deposit, ref ds))
                    {
                        // remove the row from the list of payments to process
                        deposit.HasSyncError = "True";
                        deposit.SyncNotes = "Payment Processor account number not found!";
                        deposit.Status = "Pending";

                        tds2.ClientDeposit.ImportRow(deposit);
                    }
                }
            }

            GcsApi.ClientsWSDS t2PClients = gcs.GetClients();
            for (int i = 0; i < ds.ClientDeposit.Rows.Count; i++)
            {
                TransactionDS.ClientDepositRow deposit = ds.ClientDeposit.Rows[i] as TransactionDS.ClientDepositRow;

                foreach (GcsApi.ClientsWSDS.CLIENTSRow client in t2PClients.CLIENTS.Rows)
                {
                    if (!client.IsCLIENT_IDNull())
                    {
                        if (client.CLIENT_ID == deposit.ClientName || client.CLIENT_ID == deposit.ClientID)
                        {
                            if (deposit.ProcessorAccount != client.ACCOUNT_ID)
                            {
                                // remove the row from the list of payments which have wrong account number
                                deposit.HasSyncError = "True";
                                deposit.SyncNotes = "Payment Processor account number found mismatch";
                                deposit.Status = "Pending";
                                deposit.ParentDRCID = null;
                                tds2.ClientDeposit.ImportRow(deposit);
                            }
                            break;
                        }
                    }
                }
            }

            int rowNumber = 1;

            // Remove all payments where there are no valid account numbers
            foreach (TransactionDS.ClientDepositRow deposit in ds.ClientDeposit.Rows)
            {
                if (!deposit.IsSyncNotesNull() && deposit.SyncNotes == "Payment Processor account number not found!")
                {
                    deposit.Delete();
                }
                else
                {
                    deposit.RowNumber = rowNumber++;
                }
            }

            // re-connect to SalesForce to update the payment statuses
            if (tds2.ClientDeposit.Rows.Count > 0 || tds2.ClientDepositFee.Rows.Count > 0)
            {
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.SetStatus(tds2);
                sf.Logout();
            }

            // re-connect to SalesForce to update the processor account numbers
            if (ds.Clients.Rows.Count > 0)
            {
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.UpdateClientList(ds.Clients);
                sf.Logout();
            }

            ds.ClientDeposit.AcceptChanges();

            // retrieve all debits, deposits, payments and fees that were modified within the last two days
            /* BUGBUG ?? bt - Only checks for debits, deposits, payments, fees in last 2 days
             * */
            if (ds.ClientDeposit.Count > 0)
            {
                GcsApi.DebitsWSDS debits = this.BuildDebitsTable(ref ds, gcs, ref acctHash);

                GcsApi.DebitsWSDS debitsReturn = gcs.SetFees(debits);
                foreach (GcsApi.DebitsWSDS.DEBITSRow item in debitsReturn.DEBITS)
                {
                    TransactionDS.ClientDepositRow[] rows = ds.ClientDeposit.Select("RecordID = '" + item.DRC_TRANSACTION_ID + "'") as TransactionDS.ClientDepositRow[];
                    if (!item.IsCREATION_DATENull())
                    {
                        if (rows.Length > 0)
                        {
                            for (int i = 0; i < rows.Length; i++)
                            {
                                rows[i].TransactionID = item.DEBIT_ID.ToString();
                                rows[i].Status = "Processing";
                                rows[i].SyncNotes = "Transaction Created Successfully";
                            }
                        }
                        else
                        {
                            foreach (GcsApi.DebitsWSDS.ERRORSRow errors in debitsReturn.ERRORS)
                            {
                                if (errors.TABLE_ID == Convert.ToString(item.DEBIT_ID))
                                {
                                    for (int i = 0; i < rows.Length; i++)
                                    {
                                        rows[i].SyncNotes += errors.ERROR_TEXT.ToString();
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (GcsApi.DebitsWSDS.ERRORSRow errors in debitsReturn.ERRORS)
                        {
                            if (errors.TABLE_ID == Convert.ToString(item.DEBIT_ID))
                            {
                                for (int i = 0; i < rows.Length; i++)
                                {
                                    rows[i].SyncNotes += errors.ERROR_TEXT.ToString();
                                }
                            }
                        }
                    }
                }
                ds.AcceptChanges();
            }
            return ds;
        }

        private void RefreshCreditors()
        {
            // Get all payees from SF
            SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            System.Data.DataTable accounts = salesForce.GetCreditors();
            salesForce.Logout();
            if (accounts.Rows.Count > 0)
            {
                GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);
                decimal payeeID = -1;
                GcsApi.PayeesWSDS payees = new GcsApi.PayeesWSDS();
                GcsApi.PayeesWSDS newPayees = new GcsApi.PayeesWSDS();

                // Create the list of payees to add to GCS from SF
                for (int i = 0; i < accounts.Rows.Count; i++)
                {
                    System.Data.DataRow cred = accounts.Rows[i];
                    if (cred["ProcessorAccount"].ToString() == "")
                    {
                        GcsApi.PayeesWSDS.PAYEESRow tmp = gcs.GetPayeeByDRCID(cred["Id"].ToString());
                        if (tmp == null)
                        {
                            tmp = payees.PAYEES.NewPAYEESRow();
                            tmp.ACTIVE_FLAG = "Y";
                            tmp.PAYEE_NAME = cred["Name"].ToString();
                            tmp.DRC_PAYEE_ID = cred["Id"].ToString();
                            tmp.PAYEE_ID = payeeID--;
                            payees.PAYEES.AddPAYEESRow(tmp);
                        }
                        else
                        {
                            cred["ProcessorAccount"] = tmp.PAYEE_ID.ToString();
                            payees.PAYEES.ImportRow(tmp);
                        }
                    }
                }
                // Add the new payees to GCS and update the new one. Returns the full data of the payees
                if (payees.PAYEES.Count > 0)
                    payees = gcs.SetCreditors(payees);

                // Save the gcs IDs to SF
                if (payees.PAYEES.Count > 0)
                {
                    System.Data.DataTable table2 = new System.Data.DataTable();
                    table2.Columns.Add("Id");
                    table2.Columns.Add("ProcessorAccount");
                    table2.Columns.Add("ProcessorBank");
                    table2.Columns.Add("ProcessorAddress");

                    for (int j = 0; j < payees.PAYEES.Rows.Count; j++)
                    {
                        GcsApi.PayeesWSDS.PAYEESRow row = (GcsApi.PayeesWSDS.PAYEESRow)payees.PAYEES.Rows[j];
                        System.Data.DataRow dt = table2.NewRow();
                        dt["ProcessorAccount"] = row.PAYEE_ID.ToString();
                        dt["Id"] = row.DRC_PAYEE_ID;
                        table2.Rows.Add(dt);
                    }
                    SFDebtClass sf2 = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                    sf2.SetCreditorAccount(table2);
                    sf2.Logout();

                    // Update the list of SF accounts with the appropriates GCS IDs
                    for (int i = 0; i < payees.PAYEES.Count; i++)
                    {
                        GcsApi.PayeesWSDS.PAYEESRow row = (GcsApi.PayeesWSDS.PAYEESRow)payees.PAYEES.Rows[i];
                        for (int j = 0; j < accounts.Rows.Count; j++)
                        {
                            System.Data.DataRow cred = accounts.Rows[j];
                            if (row.DRC_PAYEE_ID == cred["Id"].ToString())
                                cred["ProcessorAccount"] = row.PAYEE_ID.ToString();
                        }
                    }
                }

                // Process the banks/adresses
                decimal payeeAddrId = -1;
                decimal payeeBankId = -1;

                // for each SF accounts line create the banks and addresses
                for (int i = 0; i < accounts.Rows.Count; i++)
                {
                    System.Data.DataRow cred = accounts.Rows[i];
                    // cred["ProcessorAccount"] = row.PAYEE_ID.ToString();
                    // If all the fields for the address are filled, update the payee
                    if (cred["BillingCity"].ToString() != "" && cred["BillingStreet"].ToString() != "" &&
                        cred["BillingState"].ToString() != "" && cred["BillingPostalCode"].ToString() != "" &&
                        cred["BillingCountry"].ToString() != "")
                    {
                        GcsApi.PayeesWSDS.ADDRESSESRow addr;
                        GcsApi.PayeesWSDS.ADDRESSESRow[] addrs;

                        if (cred["ProcessorAddress"].ToString() != "" && (addrs = payees.ADDRESSES.Select("PAYEE_ADDRESS_ID = '" + Decimal.Parse(cred["ProcessorAddress"].ToString()) + "'") as GcsApi.PayeesWSDS.ADDRESSESRow[]).Length > 0)
                            addr = addrs[0];
                        else
                        {
                            addr = payees.ADDRESSES.NewADDRESSESRow();
                            addr.PAYEE_ADDRESS_ID = payeeAddrId--;
                            addr.PAYEE_ID = Decimal.Parse(cred["ProcessorAccount"].ToString());
                        }

                        addr.CITY = cred["BillingCity"].ToString();
                        addr.COUNTRY = cred["BillingCountry"].ToString();
                        addr.ZIPCODE = cred["BillingPostalCode"].ToString();
                        addr.STATE = cred["BillingState"].ToString();
                        addr.ADDRESS1 = cred["BillingStreet"].ToString();
                        addr.ADDRESS2 = cred["BillingStreet2"].ToString();
                        addr.ACTIVE_FLAG = "Y";
                        payees.ADDRESSES.AddADDRESSESRow(addr);
                    }
                    // if all the fields for the payee bank are filled, then update the payee
                    if (cred["BankAccount"].ToString() != "" && cred["BankAccountType"].ToString() != "" &&
                        cred["BankType"].ToString() != "" && cred["BankRouting"].ToString() != "")
                    {
                        GcsApi.PayeesWSDS.BANKSRow bank;
                        GcsApi.PayeesWSDS.BANKSRow[] banks;
                        if (cred["ProcessorBank"].ToString() != "" && (banks = payees.BANKS.Select("PAYEE_BANK_ID = '" + Decimal.Parse(cred["ProcessorBank"].ToString()) + "'") as GcsApi.PayeesWSDS.BANKSRow[]).Length > 0)
                            bank = banks[0];
                        else
                        {
                            bank = payees.BANKS.NewBANKSRow();
                            bank.PAYEE_BANK_ID = payeeBankId--;
                            bank.PAYEE_ID = Decimal.Parse(cred["ProcessorAccount"].ToString());
                        }

                        bank.BANK_ACCOUNT_NUM = cred["BankAccount"].ToString();
                        bank.BANK_ACCOUNT_TYPE = cred["BankAccountType"].ToString();
                        bank.BANK_ROUTING_NUM = cred["BankRouting"].ToString();
                        bank.BANK_TYPE = cred["BankType"].ToString();
                        bank.ACTIVE_FLAG = "Y";
                        payees.BANKS.AddBANKSRow(bank);
                    }
                }

                //payees.PAYEES.Clear();
                payees = gcs.SetCreditors(payees);
                System.Data.DataTable table = new System.Data.DataTable();
                table.Columns.Add("Id");
                table.Columns.Add("ProcessorAccount");
                table.Columns.Add("ProcessorBank");
                table.Columns.Add("ProcessorAddress");
                for (int i = 0; i < accounts.Rows.Count; i++)
                {
                    System.Data.DataRow cred = accounts.Rows[i];
                    System.Data.DataRow dt = table.NewRow();

                    dt["ProcessorAccount"] = cred["ProcessorAccount"];
                    dt["Id"] = cred["Id"];
                    GcsApi.PayeesWSDS.BANKSRow[] banks = payees.BANKS.Select("PAYEE_ID = '" + Decimal.Parse(cred["ProcessorAccount"].ToString()) + "'") as GcsApi.PayeesWSDS.BANKSRow[];
                    if (banks.Length > 0)
                        dt["ProcessorBank"] = banks[0].PAYEE_BANK_ID.ToString();
                    GcsApi.PayeesWSDS.ADDRESSESRow[] addrs = payees.ADDRESSES.Select("PAYEE_ID = '" + Decimal.Parse(cred["ProcessorAccount"].ToString()) + "'") as GcsApi.PayeesWSDS.ADDRESSESRow[];
                    if (addrs.Length > 0)
                        dt["ProcessorAddress"] = addrs[0].PAYEE_ADDRESS_ID.ToString();
                    table.Rows.Add(dt);
                }
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.SetCreditorAccount(table);
                sf.Logout();
            }
        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS GetWithdrawals(DateTime start, DateTime end)
        {
            RefreshCreditors();
            SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            TransactionDS ds = new TransactionDS();
            salesForce.GetWithdrawals(start, end, ds.OfferPayment);
            salesForce.Logout();

            // create an instance of the GCS interop class
            GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);

            TransactionDS tds2 = new TransactionDS();

            // retrieve all debits, deposits, payments and fees that were modified within the last two days
            /* BUGBUG ?? bt - Only checks for Withdrawls in last 2 days
             * */
            GcsApi.PaymentsWSDS payments = gcs.GetPayments(start.AddDays(-2));

            // if the payment is already in the system, delete it from the list to be processed and mark it for update in SalesForce
            foreach (GcsApi.PaymentsWSDS.PAYMENTSRow pmt in payments.PAYMENTS.Rows)
            {
                if (!pmt.IsDRC_TRANSACTION_IDNull())
                {
                    TransactionDS.OfferPaymentRow[] rows = ds.OfferPayment.Select("RecordID = '" + pmt.DRC_TRANSACTION_ID + "'") as TransactionDS.OfferPaymentRow[];

                    if (rows.Length > 0)
                    {
                        for (int i = 0; i < rows.Length; i++)
                        {
                            rows[i].TransactionID = pmt.PAYMENT_ID.ToString();
                            rows[i].Status = "Processing";
                            tds2.OfferPayment.ImportRow(rows[i]);

                            rows[i].Delete();
                        }
                    }
                }
            }

            // re-connect to SalesForce to update the payment statuses
            if (tds2.OfferPayment.Rows.Count > 0)
            {
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.SetStatus(tds2);
                sf.Logout();
            }

            ds.AcceptChanges();
            return ds;
        }

        /// <summary>
        /// Retrieves a new GCS account number
        /// </summary>
        /// <param name="gcs">The GCS Connection</param>
        /// <returns>A new GCS account number</returns>
        private string GetNewGCSAccount(GCS gcs)
        {
            string accountNumber = gcs.GetNextAccountNumber();
            return accountNumber;
        }

        /// <summary>
        /// Builds a list of debits that are to be processed
        /// </summary>
        /// <param name="transactions"></param>
        /// <param name="gcs"></param>
        /// <param name="acctHash"></param>
        /// <returns></returns>
        private GcsApi.DebitsWSDS BuildDebitsTable(ref TransactionDS transactions, GCS gcs, ref Hashtable acctHash)
        {
            List<GcsApi.DebitsWSDS> debitSets = new List<GcsApi.DebitsWSDS>();
            GcsApi.DebitsWSDS debits = new GcsApi.DebitsWSDS();
            this.AccountNotFound = new List<string>();
            int debitID = -1;

            List<string> duplicates = new List<string>();

            // iterate through the client payment transactions and create 
            // GCS debits with the appropriate data 
            for (int i = 0; i < transactions.ClientDeposit.Rows.Count; i++)
            {
                TransactionDS.ClientDepositRow deposit = transactions.ClientDeposit.Rows[i] as TransactionDS.ClientDepositRow;

                // if this is a GCS client, then process the transaction//
                if (deposit.Processor == "GCS" || deposit.Processor == "T2P" || deposit.Processor == "T2P-Hybrid" || deposit.Processor == "T2P-ProtectionProgram" ||
                    (string.IsNullOrEmpty(deposit.Processor) && (this.defaultProcessor == "GCS" || this.defaultProcessor == "T2P" || this.defaultProcessor == "T2P-ProtectionProgram")))
                {
                    deposit.RowNumber = -(debitID + 1);

                    if (!deposit.IsProcessorAccountNull() && !string.IsNullOrEmpty(deposit.ProcessorAccount.Trim()))
                    {
                        string acctNumber = deposit.ProcessorAccount.Trim();

                        // Managing the client deposit-- amount to savings + fees part
                        GcsApi.DebitsWSDS.DEBITSRow row = debits.DEBITS.NewDEBITSRow();
                        row.DEBIT_ID = debitID--;            // Unique debit ID
                        row.ACCOUNT_ID = acctNumber;              // The Client's 16 digit GCS Account Number
                        row.EFFECTIVE_DATE = deposit.Date.AddDays(1);      // Date debit is to become effective, added 1 day because of date goes -1
                        row.DAY_OF_MONTH = deposit.Date.Day;    // Day of month debit is to processed on
                        row.DEBIT_TYPE = deposit.Type;               // Two character debit type 
                        row.OCCURS_NUM = 1;                 // The number of times debit is to occur
                        row.MEMO = deposit.TypeDesc;                    // Description of debit
                        row.ACTIVE_FLAG = "Y";             // Char indicating whether debit is active
                        row.DRC_TRANSACTION_ID = deposit.RecordID;      // Company's identifier for this debit transaction
                        row.DEBIT_AMOUNT = (decimal)deposit.Amount;

                        GcsApi.DebitsWSDS.DEBITSRow[] check = debits.DEBITS.Select("DRC_TRANSACTION_ID = '" + deposit.RecordID + "'") as GcsApi.DebitsWSDS.DEBITSRow[];
                        //GcsApi.DebitsWSDS check2 = gcs.GetDebitsByTransactionID(deposit.RecordID);

                        // potential duplicate transaction (Draft)
                        if (check != null && check.Length > 0)
                            duplicates.Add("Possible duplicate transaction: Acct#" + acctNumber + " -- GCS/T2P Debit#" + check[0].DEBIT_ID + " and SF#" + deposit.RecordID);
                        else
                            //    if (check2 != null && check2.DEBITS.Count > 0)
                            //    {
                            //        deposit.TransactionID = check2.DEBITS[0].DEBIT_ID.ToString();
                            //        deposit.Status = "Processing";
                            //        transactions.AcceptChanges();
                            //        duplicates.Add("Possible duplicate transaction : Acct#" + acctNumber + " -- GCS/T2P Debit#" + check2.DEBITS[0].DEBIT_ID + " and SF#" + deposit.RecordID);
                            //    }
                            //    else
                            debits.DEBITS.Rows.Add(row);
                    }
                    else
                    {
                        // attempt to locate and populate the client's account ID
                        if (this.FindAccountID(gcs, ref deposit, ref transactions))
                        {
                            // re-add the payment row so that it can get processed
                            transactions.ClientDeposit.ImportRow(deposit);
                        }
                    }
                }
            }

            return debits;
        }

        /// <summary>
        /// Retrieves the GCS account number based on either the SalesForce ID or the ImportID
        /// </summary>
        /// <param name="gcs"></param>
        /// <param name="deposit"></param>
        /// <param name="transactions"></param>
        /// <returns></returns>
        private bool FindAccountID(GCS gcs, ref TransactionDS.ClientDepositRow deposit, ref TransactionDS transactions)
        {
            GcsApi.ClientsWSDS.CLIENTSRow clientRow = gcs.FindClientById(deposit.ClientID);

            if (clientRow == null && !string.IsNullOrEmpty(deposit.ClientImportID))
            {
                // try looking up client by their import ID
                clientRow = gcs.FindClientById(deposit.ClientImportID);
            }

            if (clientRow == null)
            {
                this.AccountNotFound.Add(deposit.ClientID);
                return false;
            }
            else
            {
                if (transactions.Clients.Select("ClientID = '" + deposit.ClientID + "'").Length == 0)
                {
                    // create a new client row with the processor account information
                    TransactionDS.ClientsRow newClientRow = transactions.Clients.NewClientsRow();
                    newClientRow.ClientID = deposit.ClientID;
                    newClientRow.ProcessorAccountNum = clientRow.ACCOUNT_ID;
                    newClientRow.AccountBalance = (double)clientRow.ACCOUNT_BALANCE;
                    newClientRow.IsNewAccount = false;
                    newClientRow.ExistsInSalesForce = false;
                    newClientRow.ProcessorStatus = "Active";
                    newClientRow.SyncDescription = "Added Payment Processor Account # to SalesForce";
                    transactions.Clients.Rows.Add(newClientRow);
                }

                // add the processor account information
                deposit.ProcessorAccount = clientRow.ACCOUNT_ID;
            }

            return true;
        }

        /// <summary>
        /// Builds a list of creditor payments that are to be processed
        /// </summary>
        /// <param name="transactions"></param>
        /// <param name="gcs"></param>
        /// <param name="acctHash"></param>
        /// <returns></returns>
        private GcsApi.PaymentsWSDS BuildPaymentsTable(TransactionDS transactions, GCS gcs, ref Hashtable acctHash)
        {
            GcsApi.PaymentsWSDS payments = new GcsApi.PaymentsWSDS();
            int paymentID = -1;

            // iterate through the client payment transactions and create 
            // GCS debits with the appropriate data 
            for (int i = 0; i < transactions.OfferPayment.Rows.Count; i++)
            {
                TransactionDS.OfferPaymentRow deposit = transactions.OfferPayment.Rows[i] as TransactionDS.OfferPaymentRow;

                try
                {
                    // if this is a GCS client, then process the transaction
                    if (deposit.Processor == "GCS" || deposit.Processor == "T2P-Hybrid" || deposit.Processor == "T2P-ProtectionProgram" ||
                        (string.IsNullOrEmpty(deposit.Processor) && (this.defaultProcessor == "GCS" || this.defaultProcessor == "T2P-Hybrid" || this.defaultProcessor == "T2P-ProtectionProgram")))
                    {
                        string acctNumber = deposit.ProcessorAccount;

                        if (!string.IsNullOrEmpty(acctNumber))
                        {
                            deposit.RowNumber = -(paymentID + 1);

                            GcsApi.PaymentsWSDS.PAYMENTSRow row = payments.PAYMENTS.NewPAYMENTSRow();
                            row.PAYMENT_ID = paymentID--;
                            row.ACCOUNT_ID = acctNumber;              // The Client's 16 digit GCS Account Number
                            row.PAYMENT_CLASS = "P";            // Payment or withdrawal
                            row.EFFECTIVE_DATE = deposit.Date;  // Date payment is to become effective
                            row.PAYMENT_AMOUNT = Convert.ToDecimal(deposit.Amount);     // amount of payment
                            // row.PAYMENT_TYPE = "A";              // Payment type : ACH, wire, check, etc.
                            row.PAYMENT_TYPE = "M";              // Payment type : ACH, wire, check, etc.
                            row.ACTIVE_FLAG = "Y";             // Char indicating whether debit is active
                            row.PAYEE_CLIENT_NUM = deposit.AccountNumber;   // Client's account number at payee
                            row.DRC_TRANSACTION_ID = deposit.RecordID;      // Company's identifier for this debit transaction
                            row.PAYEE_ID = Convert.ToDecimal(deposit.PayeeID);    // payeeID
                            if (deposit.PayeeAddressID != "")
                                row.PAYEE_ADDRESS_ID = Convert.ToDecimal(deposit.PayeeAddressID);
                            if (deposit.PayeeBankID != "")
                                row.PAYEE_BANK_ID = Convert.ToDecimal(deposit.PayeeBankID);

                            GcsApi.PaymentsWSDS check2 = gcs.GetPaymentsByTransactionID(deposit.RecordID);

                            // potential duplicate transaction (Draft)
                            if (check2 != null && check2.PAYMENTS.Count > 0)
                            {
                                deposit.TransactionID = check2.PAYMENTS[0].PAYMENT_ID.ToString();
                                if (check2.PAYMENTS[0].IsPAYMENT_STATUSNull())
                                    deposit.Status = "Processing";
                                else
                                    deposit.Status = check2.PAYMENTS[0].PAYMENT_STATUS;
                                deposit.AmountPaid = (double)check2.PAYMENTS[0].PAYMENT_AMOUNT;

                                transactions.AcceptChanges();
                                //duplicates.Add("Possible duplicate transaction : Acct#" + acctNumber + " -- GCS Debit#" + check2.DEBITS[0].DEBIT_ID + " and SF#" + deposit.RecordID);
                            }
                            else
                                payments.PAYMENTS.Rows.Add(row);
                        }
                        else
                        {
                            this.AccountNotFound.Add(deposit.ClientID);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.WriteError(ex, deposit.RecordID);
                }
            }

            return payments;
        }

        /// <summary>
        /// Creates pending transactions in GCS
        /// </summary>
        /// <param name="transactions">The pending transactions</param>
        /// <returns>True if successful, and false otherwise</returns>
        private bool CreateGCSTransactions(TransactionDS transactions)
        {
            this.results.Clear();
            Hashtable acctHash = new Hashtable();

            // if there are no new payments, do nothing
            if (transactions.ClientDeposit.Rows.Count == 0 && transactions.OfferPayment.Rows.Count == 0)
            {
                return true;
            }

            GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);

            // refresh the internal list of all the clients active in GCS
            GcsApi.ClientsWSDS clientList = gcs.GetClients();

            // retrieve the debit and fee transactions that are to be processed 
            GcsApi.DebitsWSDS debits = this.BuildDebitsTable(ref transactions, gcs, ref acctHash);

            // retrieve the creditor payments that are to be processed
            GcsApi.PaymentsWSDS payments = new GcsApi.PaymentsWSDS();

            // re-connect to SalesForce to update the transactions
            if (transactions.Clients.Rows.Count > 0)
            {
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.UpdateClientList(transactions.Clients);
                sf.Logout();
            }

            // transfer debit and creditor transactions to GCS system
            bool retVal;
            try
            {
                retVal = gcs.SetTransactions(debits, payments);
            }
            catch (SoapException ex)
            {
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.SetStatus(SetTimeOutDebits(debits, "TimedOut", ex.Message));
                sf.Logout();
                throw;
            }
            catch (WebException ex)
            {
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.SetStatus(SetTimeOutDebits(debits, "TimedOut", ex.Message));
                sf.Logout();
                throw;
            }
            catch
            {
                throw;
            }


            // process results
            this.results = this.ProcessCreateTransactionResults(gcs, transactions);

            foreach (GcsApi.DebitsWSDS.ERRORSRow error in gcs.DebitResults.ERRORS)
            {
                this.results.Add(error.ERROR_CODE + " : " + error.ERROR_TEXT);
            }

            // return true if was successful, otherwise, return false
            return retVal;
        }

        private TransactionDS SetTimeOutDebits(GcsApi.DebitsWSDS debits, string newStatus, string notes)
        {
            TransactionDS ds = new TransactionDS();
            for (int i = 0; i < debits.DEBITS.Count; i++)
            {
                GcsApi.DebitsWSDS.DEBITSRow row = debits.DEBITS[i] as GcsApi.DebitsWSDS.DEBITSRow;
                if (row.DEBIT_TYPE == "A")
                {
                    TransactionDS.ClientDepositRow cdR = ds.ClientDeposit.NewClientDepositRow();
                    cdR.RecordID = row.DRC_TRANSACTION_ID;
                    cdR.Status = newStatus;
                    cdR.SyncNotes = notes;
                    ds.ClientDeposit.AddClientDepositRow(cdR);
                }
                else
                {
                    TransactionDS.ClientDepositFeeRow cdR = ds.ClientDepositFee.NewClientDepositFeeRow();
                    cdR.RecordID = row.DRC_TRANSACTION_ID;
                    cdR.Status = newStatus;
                    cdR.SyncNotes = notes;
                    ds.ClientDepositFee.AddClientDepositFeeRow(cdR);
                }
            }
            return ds;
        }
        private TransactionDS SetTimeOutPayments(GcsApi.PaymentsWSDS payments, string newStatus, string notes)
        {
            TransactionDS ds = new TransactionDS();
            for (int i = 0; i < payments.PAYMENTS.Count; i++)
            {
                GcsApi.PaymentsWSDS.PAYMENTSRow row = payments.PAYMENTS[i] as GcsApi.PaymentsWSDS.PAYMENTSRow;

                TransactionDS.OfferPaymentRow cdR = ds.OfferPayment.NewOfferPaymentRow();
                cdR.RecordID = row.DRC_TRANSACTION_ID;
                cdR.Status = newStatus;
                cdR.SyncNotes = notes;
                ds.OfferPayment.AddOfferPaymentRow(cdR);

            }
            return ds;
        }

        /// <summary>
        /// Sets the Payment Status of all the payments modified during this session to 'processing'
        /// </summary>
        /// <param name="gcs"></param>
        private List<string> ProcessCreateTransactionResults(GCS gcs, TransactionDS transactions)
        {
            TransactionDS ds = new TransactionDS();

            // process successful debits
            for (int i = 0; i < gcs.DebitResults.DEBITS.Rows.Count; i++)
            {
                GcsApi.DebitsWSDS.DEBITSRow row = gcs.DebitResults.DEBITS.Rows[i] as GcsApi.DebitsWSDS.DEBITSRow;

                TransactionDS.ClientDepositRow cdR = ds.ClientDeposit.NewRow() as TransactionDS.ClientDepositRow;
                System.Data.DataRow[] test = ds.ClientDeposit.Select("RecordID = '" + row.DRC_TRANSACTION_ID + "'");
                // There is already an error for that payment ?
                if (test.Length > 0)
                    cdR = test[0] as TransactionDS.ClientDepositRow;
                else
                {
                    cdR.RecordID = row.DRC_TRANSACTION_ID;
                    cdR.Type = row.DEBIT_TYPE;
                    cdR.ParentDRCID = "";
                    ds.ClientDeposit.Rows.Add(cdR);
                }

                GcsApi.DebitsWSDS.ERRORSRow[] errors = gcs.DebitResults.ERRORS.Select("ROW_ID = '" + i + "' AND ERROR_TYPE = 'E'") as GcsApi.DebitsWSDS.ERRORSRow[];

                // if no fatal errors occurred while creating this transaction, add it to the list of records whose status need
                // to be set to processing
                if (errors.Length == 0)
                {
                    // populate the information
                    cdR.LastSyncDate = DateTime.Today;
                    if (row.DEBIT_ID > 0)
                        cdR.TransactionID = row.DEBIT_ID.ToString();
                    else
                        cdR.TransactionID = "";
                    cdR.AmountPaid = (double)row.DEBIT_AMOUNT;
                    cdR.Status = "Processing";
                }
                else
                {
                    cdR.TransactionID = "";
                    cdR.HasSyncError = "True";
                    cdR.Status = "Errored";
                    cdR.SyncNotes = "Error processing payment: " + errors[0].ERROR_TEXT;
                    errors[0].ERROR_TEXT += " SFID: " + row.DRC_TRANSACTION_ID;
                    errors[0].ERROR_TYPE = row.DEBIT_TYPE;
                }

                cdR.AcceptChanges();
            }

            GcsApi.PaymentsWSDS existingPayments = new GcsApi.PaymentsWSDS();
            // process successful payments
            for (int i = 0; i < gcs.PaymentResults.PAYMENTS.Rows.Count; i++)
            {
                GcsApi.PaymentsWSDS.PAYMENTSRow row = gcs.PaymentResults.PAYMENTS.Rows[i] as GcsApi.PaymentsWSDS.PAYMENTSRow;

                TransactionDS.OfferPaymentRow cdR = ds.OfferPayment.NewRow() as TransactionDS.OfferPaymentRow;
                int k = i + 1;
                GcsApi.PaymentsWSDS.ERRORSRow[] errors = gcs.PaymentResults.ERRORS.Select("ROW_ID = '" + k + "' AND ERROR_TYPE = 'E'") as GcsApi.PaymentsWSDS.ERRORSRow[];

                System.Data.DataRow[] existingRow = ds.OfferPayment.Select("RecordID = '" + row.DRC_TRANSACTION_ID + "'");

                if (existingRow.Length > 0)
                {
                    cdR = existingRow[0] as TransactionDS.OfferPaymentRow;
                }
                else
                {
                    TransactionDS.OfferPaymentRow existingData = transactions.OfferPayment.FindByRecordID(row.DRC_TRANSACTION_ID) as TransactionDS.OfferPaymentRow;

                    if (existingData != null)
                    {
                        ds.OfferPayment.ImportRow(existingData);
                        cdR = ds.OfferPayment.FindByRecordID(existingData.RecordID);
                        //cdR.TransactionID = existingData.TransactionID;
                    }
                    else
                    {

                        cdR.RecordID = row.DRC_TRANSACTION_ID;
                        //cdR.TransactionID = row.PAYMENT_ID.ToString();
                        ds.OfferPayment.Rows.Add(cdR);
                    }
                }

                // if no fatal errors occurred while creating this transaction, add it to the list of records whose status need
                // to be set to processing
                if (errors.Length == 0 && row.PAYMENT_ID > 0)
                {
                    // populate the information
                    if (row.PAYMENT_ID > 0)
                        cdR.TransactionID = row.PAYMENT_ID.ToString();
                    else
                        cdR.TransactionID = "";
                    cdR.RecordID = row.DRC_TRANSACTION_ID;
                    cdR.Status = "Processing";
                    cdR.AmountPaid = (double)row.PAYMENT_AMOUNT;
                    cdR.LastSyncDate = DateTime.Today;

                    //if (existingRow.Length < 1)
                    //ds.OfferPayment.Rows.Add(cdR);
                }
                else
                {
                    cdR.TransactionID = "";
                    cdR.HasSyncError = true;
                    cdR.SyncNotes = "Error processing payment: " + errors[0].ERROR_TEXT;
                    cdR.Status = "Errored";
                    errors[0].ERROR_TEXT += " SFID: " + row.DRC_TRANSACTION_ID;
                }
            }



            // process debit errors
            this.WriteError(gcs.DebitResults.ERRORS, "nudebt__Client_Payment__c");

            // process payment errors
            this.WriteError(gcs.PaymentResults.ERRORS, "nudebt__Offer_Payment__c");

            // write transactions to SalesForce
            SFDebtClass sfd = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            List<string> results = sfd.SetStatus(ds);
            //QuickFix
            if (gcs.PaymentResults.PAYMENTS.Count == 0 && gcs.DebitResults.DEBITS.Count == 0)
            {
                sfd.SetStatus(transactions);
            }
            sfd.Logout();

            this.UpdatedTransactions = ds;
            return results;
        }

        //private GCSApi.pa

        /// <summary>
        /// Creates new client accounts in the GCS system. Uses same error handler as is used app-wide
        /// </summary>
        /// <param name="clientList">The list of new client accounts to be created</param>
        /// <param name="gcs">The GCS connection</param>
        /// <param name="clients">Original list of clients to be created</param>
        private void CreateGCSClients(GcsApi.ClientsWSDS clientList, GCS gcs, ref TransactionDS.ClientsDataTable clients)
        {
            if (clientList.CLIENTS.Rows.Count > 0)
            {
                GcsApi.ClientsWSDS results = gcs.SetClients(clientList);

                foreach (GcsApi.ClientsWSDS.ERRORSRow row in results.ERRORS)
                {
                    if (row.ERROR_TYPE == "E")
                    {
                        System.Data.DataRow[] rows = clients.Select("RowNumber = '" + row.ROW_ID + "' AND (Processor = 'GCS' OR Processor = 'T2P-Hybrid' OR Processor = 'T2P-ProtectionProgram')");

                        if (rows.Length > 0)
                        {
                            foreach (TransactionDS.ClientsRow cRow in rows)
                            {
                                cRow.SyncDescription += row.ERROR_TEXT + " ";
                                cRow.ProcessorStatus = cRow.LastStatus;
                                cRow.ProcessorAccountNum = string.Empty;
                            }

                            row.ERROR_TEXT += " SFID: " + rows[0]["ClientNameID"].ToString();
                        }
                        clients.AcceptChanges();
                    }
                }

                this.WriteError(results.ERRORS, "nudebt__Clients__c");
            }
        }

        /// <summary>
        /// Updates existing client accounts in the GCS system. Uses same error handler as is used app-wide
        /// </summary>
        /// <param name="clientList">The list of new client accounts to be created</param>
        /// <param name="gcs">The GCS connection</param>
        /// <param name="clients">Original list of clients to be created</param>
        /// <param name="batchNumber">The current batch</param>
        private void UpdateGCSClients(GcsApi.ClientsWSDS clientList, GCS gcs, ref TransactionDS.ClientsDataTable clients, short batchNumber)
        {
            if (clientList.CLIENTS.Rows.Count > 0)
            {
                GcsApi.ClientsWSDS results = gcs.SetClients(clientList);

                foreach (TransactionDS.ClientsRow clientRow in clients.Rows)
                {
                    if (!clientRow.IsBatchNumberNull() && clientRow.BatchNumber == batchNumber)
                    {
                        GcsApi.ClientsWSDS.ERRORSRow[] error = results.ERRORS.Select("ERROR_TYPE = 'E' AND ROW_ID = '" + clientRow.RowNumber + "'") as GcsApi.ClientsWSDS.ERRORSRow[];

                        if (error != null && error.Length > 0)
                        {
                            foreach (GcsApi.ClientsWSDS.ERRORSRow cRow in error)
                            {
                                clientRow.SyncDescription += cRow.ERROR_TEXT + " ";
                                clientRow.ProcessorStatus = clientRow.LastStatus;
                            }
                        }
                        else if (results.CLIENTS.FindByACCOUNT_ID(clientRow.ProcessorAccountNum) != null)
                        {
                            clientRow.MarkForSync = false;
                            clientRow.LastSyncDate = DateTime.Now;
                            clientRow.SyncDescription = "Account changes synced with Payment Processor.";
                        }
                    }
                    else
                    {
                        clientRow.MarkForSync = false;
                        clientRow.LastSyncDate = DateTime.Now;
                        clientRow.SyncDescription = "No account changes found for sync. Please manually update any account changes to Payment Processor or try again.";
                    }
                }

                // write errors to sales force
                this.WriteError(results.ERRORS, "nudebt__Clients__c");
            }
        }

        /// <summary>
        /// Updates client information in GCS
        /// </summary>
        /// <param name="transactions"></param>
        /// <param name="gcs"></param>
        private void UpdateGCSClients()
        {
            // create a new connection to GCS
            GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);

            // maintain a list of clients to be updated
            GcsApi.ClientsWSDS clientList = new GcsApi.ClientsWSDS();

            // create a new instance of the salesForce interop class
            SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            TransactionDS.ClientsDataTable sfClients = sf.GetModifiedClients(this._Processor);
            sf.Logout();

            // count the number of GCS updates
            int gcsCount = 0;
            short batchNumber = 1;

            foreach (TransactionDS.ClientsRow row in sfClients.Rows)
            {
                if (!string.IsNullOrEmpty(row.Processor) && (row.Processor == "GCS" || row.Processor == "T2P-Hybrid" || row.Processor == "T2P-ProtectionProgram"))
                {
                    // if the account exists, retrieve the client record from GCS and store the account balance and account number  
                    GcsApi.ClientsWSDS temp = gcs.GetClientByAccountID(row.ProcessorAccountNum);

                    if (temp != null && temp.CLIENTS.Rows.Count > 0)
                    {
                        clientList.CLIENTS.ImportRow(temp.CLIENTS.Rows[0]);

                        GcsApi.ClientsWSDS.CLIENTSRow clientRow = clientList.CLIENTS.Rows[clientList.CLIENTS.Rows.Count - 1] as GcsApi.ClientsWSDS.CLIENTSRow;
                        bool hasChanges = false;

                        // if the banking information changed, push these changes to GCS
                        if (row.BankAccountNumber != clientRow.BANK_ACCOUNT_NUM || row.BankRoutingNumber != clientRow.BANK_ROUTING_NUM)
                        {
                            hasChanges = true;
                            clientRow.BANK_ACCOUNT_NUM = row.BankAccountNumber;
                            clientRow.BANK_ROUTING_NUM = row.BankRoutingNumber;
                            clientRow.BANK_ACCOUNT_TYPE = row.AccountType.Length > 0 ? row.AccountType[0].ToString() : "C";
                        }

                        // if the client's address changed, push these changes to GCS
                        if (row.Address1 != clientRow.ADDRESS1 || row.City != clientRow.CITY || row.State != clientRow.STATE)
                        {
                            hasChanges = true;
                            clientRow.ADDRESS1 = row.Address1;
                            clientRow.CITY = row.City;
                            clientRow.STATE = row.State;
                            clientRow.COUNTRY = row.Country;
                            clientRow.ZIPCODE = row.Zip;
                        }

                        // if the client's contact info or date of birth change, push the changes to GCS
                        try
                        {
                            if (clientRow.DATE_OF_BIRTH == null || row.DateOfBirth.Date != clientRow.DATE_OF_BIRTH.Date || row.PhoneNumber.Replace("-", string.Empty) != clientRow.PHONE_NUM.Replace("-", string.Empty))
                            {
                                hasChanges = true;
                                clientRow.DATE_OF_BIRTH = row.DateOfBirth.AddDays(1); // added a day as it was saving Actual b'date - 1 in record 
                                clientRow.PHONE_NUM = row.PhoneNumber;
                            }
                        }
                        catch
                        {
                            hasChanges = true;
                            clientRow.DATE_OF_BIRTH = row.DateOfBirth;
                            clientRow.PHONE_NUM = row.PhoneNumber;
                        }

                        if (hasChanges)
                        {
                            row.BatchNumber = batchNumber;
                            row.MarkForSync = true;
                            row.RowNumber = gcsCount++;
                            row.AcceptChanges();

                            // process in batches of 150 until completed
                            if (gcsCount == 150)
                            {
                                this.UpdateGCSClients(clientList, gcs, ref sfClients, batchNumber);
                                clientList.CLIENTS.Rows.Clear();
                                gcsCount = 0;
                                batchNumber++;
                            }
                        }
                        else
                        {
                            row.MarkForSync = false;
                            row.SyncDescription = "No changes detected to account for sync";
                            clientRow.AcceptChanges();
                            clientList.CLIENTS.RemoveCLIENTSRow(clientRow);
                        }
                    }
                }
            }

            this.UpdateGCSClients(clientList, gcs, ref sfClients, batchNumber);

            // create a new instance of the salesForce interop class to record changes to the client record
            SFDebtClass sfC = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            sfC.UpdateClientList(sfClients);
            sfC.Logout();
        }
        private void UpdateRAMSClients()
        {
            try
            {
                // create a new connection to RAMS and get Logged In to RAM System with a valid Session and Vendor key 
                RAMS RAMS = new RAMS(this._ProcessorLogin, this._LiveEnvironment);
                if (RAMS.Login())
                {
                    // create a new instance of the salesForce interop class
                    SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                    TransactionDS.ClientsDataTable sfClients = sf.GetModifiedClients(this._Processor);
                    sf.Logout();

                    //Create Datatable with single column containing all Client's IDs to pass as parameter in below method
                    DataTable dt_ClientIDs = new DataTable();

                    System.Data.DataView view = new System.Data.DataView(sfClients);
                    dt_ClientIDs = view.ToTable("Clients", false, "ClientIDforRAMandEPPS");

                    string[] ClientIDs = null;
                    if (dt_ClientIDs.Rows.Count > 0)
                    {
                        ClientIDs = dt_ClientIDs.AsEnumerable().Select(r => r[0].ToString()).ToArray();
                    }

                    // If any client exist 
                    if (ClientIDs != null)
                    {
                        // retrieve the list of all client Information
                        DataTable dt_ClientInfo = RAMS.GetAllClients(ClientIDs);

                        //List<ClientInfo> ListClientsInfo = new List<ClientInfo>();

                        foreach (TransactionDS.ClientsRow row in sfClients.Rows)
                        {
                            if (this.defaultProcessor == "RAMS")
                            {
                                row.Processor = this.defaultProcessor;
                            }
                            if (row.Processor == "RAMS")
                            {
                                // if the account exists, retrieve the client record 
                                System.Data.DataRow[] dr_clientRow = dt_ClientInfo.Select("ClientID = '" + row["ClientIDforRAMandEPPS"].ToString() + "'");
                                RAMSInterop.ClientInfo oClientInfo = new ClientInfo();
                                //TransactionDS.ClientsRow cr = (TransactionDS.ClientsRow)dr_clientRow[0];

                                // if the account exists, retrieve the client record from RAMS and store the account balance and account number  
                                if (dr_clientRow.Length > 0 && String.IsNullOrEmpty(dr_clientRow[0]["Error"].ToString()))
                                {
                                    bool hasChanges = false;

                                    // if the client's address changed, push these changes to RAM
                                    if (row.Address1 != dr_clientRow[0]["Address"].ToString() || row.City != dr_clientRow[0]["City"].ToString() || row.State != dr_clientRow[0]["State"].ToString() || String.IsNullOrEmpty(dr_clientRow[0]["DOB"].ToString()) || row.DateOfBirth.Date.ToString("MM-dd-yyyy") != dr_clientRow[0]["DOB"].ToString() || row.PhoneNumber.Replace("-", string.Empty) != dr_clientRow[0]["Phone1"].ToString().Replace("-", string.Empty))
                                    {
                                        hasChanges = true;
                                        oClientInfo.ClientID = row.ClientIDforRAMandEPPS;
                                        oClientInfo.Firstname = row.FirstName;
                                        oClientInfo.Lastname = row.LastName;
                                        oClientInfo.emailAddress = row.Email;
                                        oClientInfo.taxID = row.SocialSecurityNumber.Length > 4 ? row.SocialSecurityNumber.Substring(row.SocialSecurityNumber.Length - 4, 4) : "";
                                        oClientInfo.streetAddress = row.Address1;
                                        oClientInfo.city = row.City;
                                        oClientInfo.zip = row.Zip;
                                        oClientInfo.stateAbbreviation = row.State;   //2 CHAR
                                        oClientInfo.Phone1 = row.PhoneNumber;
                                        oClientInfo.Dob1 = row.DateOfBirth.ToString("MM-dd-yyyy");

                                        //Reset with exisitng Status in RAMS
                                        switch (dr_clientRow[0]["Status"].ToString())
                                        {
                                            case "1":
                                                {
                                                    oClientInfo.AccountStatus = RAMSApi.AccountStatus.ACTIVE;
                                                    break;
                                                }
                                            case "6":
                                                {
                                                    oClientInfo.AccountStatus = RAMSApi.AccountStatus.CANCELLED;
                                                    break;
                                                }
                                            case "21":
                                                {
                                                    oClientInfo.AccountStatus = RAMSApi.AccountStatus.ON_HOLD;
                                                    break;
                                                }
                                        }


                                        switch (dr_clientRow[0]["ACHStatus"].ToString())
                                        {
                                            case "0":
                                                {
                                                    oClientInfo.AccountHoldStatus = RAMSApi.ACHStatus.ACTIVE;
                                                    break;
                                                }
                                            case "99":
                                                {
                                                    oClientInfo.AccountHoldStatus = RAMSApi.ACHStatus.ON_HOLD_RETURN;
                                                    break;
                                                }
                                            case "98":
                                                {
                                                    oClientInfo.AccountHoldStatus = RAMSApi.ACHStatus.ON_HOLD_REQUEST;
                                                    break;
                                                }
                                        }

                                        oClientInfo.AffiliateID = 99630;
                                        oClientInfo.feeSplitGroupID = int.Parse(row.FeeSplitGroupID);
                                        oClientInfo.AccountNumber = row.BankAccountNumber;
                                        oClientInfo.NameonAccount = row.NameonAccount;
                                        oClientInfo.Routing = row.BankRoutingNumber;
                                        oClientInfo.BankName = row.BankName;
                                        oClientInfo.AccountType = row.AccountType.ToUpper() == "CHECKING" ? RAMSApi.BankAccountType.CONSUMER_DEBIT_CHECKING : RAMSApi.BankAccountType.CONSUMER_DEBIT_SAVINGS;
                                        row.MarkForSync = true;
                                        row.AcceptChanges();
                                    }

                                    if (hasChanges)
                                    {
                                        try
                                        {
                                            RAMS.UpdateClient(ref oClientInfo);
                                            if (String.IsNullOrEmpty(oClientInfo.ErrorResponse))
                                            {
                                                row.MarkForSync = false;
                                                row.LastSyncDate = DateTime.Now;
                                                row.SyncDescription += "Account changes synced with Payment Processor.";
                                            }
                                            else
                                            {
                                                row.MarkForSync = true;
                                                row.SyncDescription += oClientInfo.ErrorResponse + " ";
                                            }
                                            row.AcceptChanges();
                                        }
                                        catch (Exception ex)
                                        {
                                            row.MarkForSync = true;
                                            row.SyncDescription += oClientInfo.ErrorResponse;
                                            row.AcceptChanges();
                                        }
                                    }
                                    else
                                    {
                                        row.MarkForSync = false;
                                        row.SyncDescription += "No changes detected to account for sync";
                                        row.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    row.MarkForSync = true;
                                    row.SyncDescription += dr_clientRow[0]["Error"].ToString();
                                    row.AcceptChanges();
                                }
                            }
                        }
                    }

                    // create a new instance of the salesForce interop class to record changes to the client record
                    SFDebtClass sfC = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                    sfC.UpdateClientList(sfClients);
                    sfC.Logout();
                    RAMS.Logout();
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void UpdateEPPSClients()
        {
            try
            {
                // create a new connection to EPPS and get Logged In to EPPS System
                EPPS EPPS = new EPPS(this._ProcessorLogin, this._ProcessorPwd);

                // create a new instance of the salesForce interop class
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                TransactionDS.ClientsDataTable sfClients = sf.GetModifiedClients(this._Processor);
                sf.Logout();


                foreach (TransactionDS.ClientsRow SfRow in sfClients.Rows)
                {
                    if (SfRow.Processor == "EPPS")
                    {
                        bool hasChanges = false;
                        TransactionDS.ClientsRow cr = (TransactionDS.ClientsRow)SfRow;

                        EPPSApi.CardHolderDetail CardHolderDetails = new EPPSApi.CardHolderDetail();
                        CardHolderDetails.CardHolderID = cr.ClientIDforRAMandEPPS.ToString();
                        EPPSApi.FindCardHolder CardHolder = new EPPSApi.FindCardHolder();
                        CardHolder = EPPS.RetrieveCardHolderById(CardHolderDetails);

                        if (CardHolder.CardHolderList.Length != 0 && CardHolder.Message.Equals("Success"))
                        {
                            CardHolderDetails = CardHolder.CardHolderList.FirstOrDefault(item => item.CardHolderID == cr.ClientIDforRAMandEPPS.ToString());
                            if (SfRow.Address1 != CardHolderDetails.Address || SfRow.City != CardHolderDetails.City || SfRow.State != CardHolderDetails.State || SfRow.DateOfBirth != CardHolderDetails.DateOfBirth || SfRow.PhoneNumber.Replace("-", string.Empty) != CardHolderDetails.Phone.Replace("-", string.Empty))
                            {
                                hasChanges = true;
                                CardHolderDetails = new EPPSApi.CardHolderDetail();

                                CardHolderDetails.CardHolderID = SfRow.ClientIDforRAMandEPPS.ToString();
                                CardHolderDetails.FirstName = SfRow.FirstName;
                                CardHolderDetails.LastName = SfRow.LastName;
                                CardHolderDetails.DateOfBirth = SfRow.DateOfBirth;
                                CardHolderDetails.Phone = SfRow.PhoneNumber.Replace("-", string.Empty).Trim(); ;
                                CardHolderDetails.SSN = SfRow.SocialSecurityNumber.Replace("-", string.Empty).Trim();
                                CardHolderDetails.Address = SfRow.Address1;
                                CardHolderDetails.City = SfRow.City;
                                CardHolderDetails.State = SfRow.State;
                                CardHolderDetails.Zip = SfRow.Zip;
                                CardHolderDetails.Email = SfRow.Email;

                                SfRow.MarkForSync = true;
                                SfRow.AcceptChanges();

                                try
                                {
                                    EPPSApi.CardHolder CH = new EPPSApi.CardHolder();
                                    CH = EPPS.UpdateCardHolder(CardHolderDetails);
                                    if (CH.Message == "Success")
                                    {
                                        SfRow.MarkForSync = false;
                                        SfRow.LastSyncDate = DateTime.Now;
                                        SfRow.SyncDescription += "Account changes synced with Payment Processor.";
                                    }
                                    else
                                    {
                                        SfRow.MarkForSync = true;
                                        SfRow.SyncDescription = CH.Message;
                                    }
                                    SfRow.AcceptChanges();
                                }
                                catch (Exception ex)
                                {
                                    SfRow.MarkForSync = true;
                                    SfRow.SyncDescription += ex.Message;
                                    SfRow.AcceptChanges();
                                }
                            }
                            else
                            {
                                SfRow.MarkForSync = false;
                                SfRow.SyncDescription += "No changes detected to account for sync";
                                SfRow.AcceptChanges();
                            }
                        }
                    }
                }

                // create a new instance of the salesForce interop class to record changes to the client record
                SFDebtClass sfC = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sfC.UpdateClientList(sfClients);
                sfC.Logout();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Updates client balances in SF
        /// </summary>
        public void UpdateGCSBalances()
        {
            // create a new connection to GCS
            GCS gcs = new GCS(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);

            // retrieve the list of all active clients in GCS's system
            GcsApi.ClientsWSDS clientList = gcs.GetClients();

            // create a new instance of the salesForce interop class
            SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            TransactionDS.ClientsDataTable sfClients = sf.GetActiveClients(this._Processor);
            sf.Logout();

            // create a new client table to hold a list of all clients whose account balances need to be updated
            TransactionDS.ClientsDataTable clients = new TransactionDS.ClientsDataTable();

            foreach (GcsApi.ClientsWSDS.CLIENTSRow row in clientList.CLIENTS.Rows)
            {
                // if the account exists, retrieve the client record from GCS and store the account balance and account number
                System.Data.DataRow[] clientRows = sfClients.Select("ProcessorAccountNum = '" + row.ACCOUNT_ID + "'");

                if (clientRows.Length > 0)
                {
                    TransactionDS.ClientsRow cr = (TransactionDS.ClientsRow)clientRows[0];
                    cr.AccountBalance = (double)row.ACCOUNT_BALANCE;
                    cr.ProcessorStatus = (row.ACTIVE_FLAG == "N") ? "Inactive" : "Active";
                    cr.ProcessorAccountNum = row.ACCOUNT_ID;
                    cr.SyncDescription = "Balance reconciled";
                    cr.ClientStatus = row.STATUS;
                    cr.ClientStatusDescription = row.CLIENT_STATUS_DESCRIPTION;
                    cr.DocumentStatus = row.DOCUMENT_STATUS;
                    cr.DocumentStatusDescription = row.DOCUMENT_STATUS_DESCRIPTION;

                    //if (row.CREATION_DATE != DateTime.MinValue)
                    cr.ProccesorCreationDate = row.CREATION_DATE;

                    cr.AcceptChanges();
                }
                else
                {
                    // try to look up the client by the drc id
                    System.Data.DataRow[] clientRows2 = sfClients.Select("ImportID = '" + row.CLIENT_ID + "' OR ClientNameID = '" + row.CLIENT_ID + "'");

                    if (clientRows2.Length > 0)
                    {
                        TransactionDS.ClientsRow cr = (TransactionDS.ClientsRow)clientRows2[0];
                        cr.AccountBalance = (double)row.ACCOUNT_BALANCE;
                        cr.ProcessorStatus = (row.ACTIVE_FLAG == "N") ? "Inactive" : "Active";
                        cr.ProcessorAccountNum = row.ACCOUNT_ID;
                        cr.SyncDescription = "Balance reconciled";
                        cr.ClientStatus = row.STATUS;
                        cr.ClientStatusDescription = row.CLIENT_STATUS_DESCRIPTION;
                        cr.DocumentStatus = row.DOCUMENT_STATUS;
                        cr.DocumentStatusDescription = row.DOCUMENT_STATUS_DESCRIPTION;
                        cr.ProccesorCreationDate = row.CREATION_DATE;
                        cr.AcceptChanges();
                    }
                }
            }

            foreach (TransactionDS.ClientsRow cr in sfClients.Rows)
            {
                try
                {
                    if ((cr.IsSyncDescriptionNull() || string.IsNullOrEmpty(cr.SyncDescription)) && !cr.IsProcessorAccountNumNull() && !string.IsNullOrEmpty(cr.ProcessorAccountNum))
                    {
                        GcsApi.ClientsWSDS client = gcs.GetClientByAccountID(cr.ProcessorAccountNum);

                        if (client.CLIENTS.Rows.Count > 0)
                        {
                            GcsApi.ClientsWSDS.CLIENTSRow row = (GcsApi.ClientsWSDS.CLIENTSRow)client.CLIENTS.Rows[0];
                            cr.AccountBalance = (double)row.ACCOUNT_BALANCE;
                            cr.ProcessorStatus = (row.ACTIVE_FLAG == "N") ? "Inactive" : "Active";
                            cr.ProcessorAccountNum = row.ACCOUNT_ID;
                            cr.SyncDescription = "Balance reconciled";
                            cr.ClientStatus = row.STATUS;
                            cr.ClientStatusDescription = row.CLIENT_STATUS_DESCRIPTION;
                            cr.DocumentStatus = row.DOCUMENT_STATUS;
                            cr.DocumentStatusDescription = row.DOCUMENT_STATUS_DESCRIPTION;
                            cr.ProccesorCreationDate = row.CREATION_DATE;
                            cr.AcceptChanges();
                        }
                    }
                }
                catch { }
            }

            // re-connect to SalesForce to update the processor account numbers
            sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);

            for (int i = 0; i < sfClients.Rows.Count; )
            {
                TransactionDS.ClientsDataTable sfClientsC = new TransactionDS.ClientsDataTable();

                for (int j = 0; j < 200 && i < sfClients.Rows.Count; j++, i++)
                {
                    if (i < sfClients.Rows.Count)
                    {
                        sfClients.Rows[i]["RowNumber"] = i + 1;

                        sfClientsC.ImportRow(sfClients.Rows[i]);
                    }
                }

                sf.UpdateClientListWithStatus(sfClientsC);
                if (_IsLogEnabled.Equals(true))
                {
                    sf.WriteClientLog(sfClientsC);
                }
                this.CreatedClients = sfClientsC;
            }


            sf.Logout();
        }
        public void UpdateRAMSBalances()
        {
            // create a new connection to RAMS and get Logged In to RAM System with a valid Session and Vendor key 
            RAMS RAMS = new RAMS(this._ProcessorLogin, this._LiveEnvironment);
            RAMS.Login();

            // retrieve the list of all active clients in GCS's system
            //DataTable ClientBalances = RAMS.GetClientListToUpdateBalances(

            // create a new instance of the salesForce interop class
            SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            TransactionDS.ClientsDataTable sfClients = sf.GetActiveClients(this._Processor);
            sf.Logout();

            // create a new client table to hold a list of all clients whose account balances need to be updated
            TransactionDS.ClientsDataTable clients = new TransactionDS.ClientsDataTable();

            //Create Datatable with single column containing all Client's IDs to pass as parameter in below method
            DataTable dt_ClientIDs = new DataTable();

            System.Data.DataView view = new System.Data.DataView(sfClients);
            dt_ClientIDs = view.ToTable("Clients", false, "ClientIDforRAMandEPPS");

            string[] ClientIDs = dt_ClientIDs.AsEnumerable().Select(r => r[0].ToString()).ToArray();

            // retrieve the list of all active clients in RAMS system
            DataTable ClientBalances = RAMS.GetClientBalances(dt_ClientIDs);

            // retrieve the list of all client Information
            DataTable ClientInfo = RAMS.GetAllClients(ClientIDs);

            foreach (DataRow RetRow in ClientBalances.Rows)
            {
                // if the account exists, retrieve the client record from RAMS and store the account balance and account number
                System.Data.DataRow[] clientRows = sfClients.Select("ClientIDforRAMandEPPS = '" + RetRow["client_id"].ToString() + "'");

                if (clientRows.Length > 0)
                {
                    TransactionDS.ClientsRow cr = (TransactionDS.ClientsRow)clientRows[0];
                    if (RetRow["trustAccountBalance"].ToString() != "Error")
                    {
                        cr.AccountBalance = double.Parse(RetRow["trustAccountBalance"].ToString());
                        cr.SyncDescription = "Balance reconciled";
                    }
                    else
                    {
                        cr.SyncDescription = RetRow["Error"].ToString();
                    }

                    cr.ProcessorAccountNum = RetRow["client_id"].ToString();
                    cr.AcceptChanges();
                }
            }

            foreach (DataRow RetRow in ClientInfo.Rows)
            {
                // if the account exists, retrieve the client record 
                System.Data.DataRow[] clientRows = sfClients.Select("ClientIDforRAMandEPPS = '" + RetRow["ClientID"].ToString() + "'");

                if (clientRows.Length > 0)
                {
                    TransactionDS.ClientsRow cr = (TransactionDS.ClientsRow)clientRows[0];
                    if (RetRow["Error"].ToString() == "")
                    {
                        cr.ProcessorStatus = (RetRow["Status"].ToString() == "1") ? "Active" : "Inactive";
                    }
                    cr.AcceptChanges();
                }
            }

            // re-connect to SalesForce to update the processor account numbers
            sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);

            for (int i = 0; i < sfClients.Rows.Count; )
            {
                TransactionDS.ClientsDataTable sfClientsC = new TransactionDS.ClientsDataTable();

                for (int j = 0; j < 200 && i < sfClients.Rows.Count; j++, i++)
                {
                    if (i < sfClients.Rows.Count)
                    {
                        sfClients.Rows[i]["RowNumber"] = i + 1;

                        sfClientsC.ImportRow(sfClients.Rows[i]);
                    }
                }

                sf.UpdateClientList(sfClientsC);
                if (_IsLogEnabled.Equals(true))
                {
                    sf.WriteClientLog(sfClientsC);
                }
                this.CreatedClients = sfClientsC;
            }


            sf.Logout();
            RAMS.Logout();
        }
        public void UpdateEPPSBalances()
        {
            EPPS Epps = new EPPS(this._ProcessorLogin, this._ProcessorPwd);

            // create a new instance of the salesForce interop class
            SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
            TransactionDS.ClientsDataTable sfClients = sf.GetActiveClients(this._Processor);

            sf.Logout();

            foreach (DataRow SfRow in sfClients)
            {
                TransactionDS.ClientsRow cr = (TransactionDS.ClientsRow)SfRow;

                EPPSApi.CardHolderDetail CardHolderDetails = new EPPSApi.CardHolderDetail();
                CardHolderDetails.CardHolderID = cr.ClientIDforRAMandEPPS.ToString();
                EPPSApi.FindCardHolder CardHolder = new EPPSApi.FindCardHolder();
                CardHolder = Epps.RetrieveCardHolderById(CardHolderDetails);

                if (CardHolder.CardHolderList.Length != 0 && CardHolder.Message.Equals("Success"))
                {
                    CardHolderDetails = CardHolder.CardHolderList.FirstOrDefault(item => item.CardHolderID == cr.ClientIDforRAMandEPPS.ToString());
                    if (CardHolderDetails != null)
                    {
                        cr.AccountBalance = Double.Parse(CardHolderDetails.AccountBalance);
                        cr.ProcessorAccountNum = CardHolderDetails.CardHolderID;
                        cr.SyncDescription = "Balance reconciled";
                        //cr.ProcessorStatus = 
                    }
                }
                else
                {
                    cr.SyncDescription = "ID not found";
                }

                cr.AcceptChanges();
            }

            // re-connect to SalesForce to update the processor account numbers
            sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);

            for (int i = 0; i < sfClients.Rows.Count; )
            {
                TransactionDS.ClientsDataTable sfClientsC = new TransactionDS.ClientsDataTable();

                for (int j = 0; j < 200 && i < sfClients.Rows.Count; j++, i++)
                {
                    if (i < sfClients.Rows.Count)
                    {
                        sfClients.Rows[i]["RowNumber"] = i + 1;

                        sfClientsC.ImportRow(sfClients.Rows[i]);
                    }
                }

                sf.UpdateClientList(sfClientsC);
                if (_IsLogEnabled.Equals(true))
                {
                    sf.WriteClientLog(sfClientsC);
                }
                this.CreatedClients = sfClientsC;
            }


            sf.Logout();
        }


        /// <summary>
        /// Retrieves the list of pending transactions for a specified date range and 
        /// creates these transactions with the appropriate processor
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        public TransactionDS ViewPendingEPPSTransactions(DateTime start, DateTime endDate)
        {

            try
            {
                TransactionDS transactions = this.GetEPPSPendingPayments(start, endDate);

                return transactions;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Retrieves the list of pending transactions for a specified date range and 
        /// creates these transactions with the appropriate processor
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        public TransactionDS EPPSCreateTransactions(TransactionDS ds)
        {

            try
            {
                TransactionDS transactions = this.CreateEPPSPayments(ds);

                return transactions;
            }
            catch (Exception)
            {

                throw;
            }

        }

        private TransactionDS GetEPPSPendingPayments(DateTime start, DateTime end)
        {
            try
            {
                SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                TransactionDS ds = salesForce.GetEPPSTransactions(start, end);
                salesForce.Logout();
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private TransactionDS CreateEPPSPayments(TransactionDS ds)
        {
            try
            {
                EPPS client = new EPPS(this._ProcessorLogin, this._ProcessorPwd);
                EPPSApi.EFTChangeLogDetail eftInfo = new EPPSApi.EFTChangeLogDetail();
                //Raghuraj
                foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit)
                {
                    if (!row.IsCardNumberNull() && !row.IsCardExpDateNull())
                    {
                        EPPSApi.CreditCardDebitDetail objCCDD = new EPPSApi.CreditCardDebitDetail();
                        EPPSApi.CreditCardDebit RetVal = new EPPSApi.CreditCardDebit();
                        objCCDD.CardHolderId = row.IsProcessorAccountNull() ? "" : row.ProcessorAccount;
                        objCCDD.CreditCardNumber = row.IsCardNumberNull() ? "" : row.CardNumber;
                        objCCDD.FirstName = row.IsClientFNameNull() ? "" : row.ClientFName;
                        objCCDD.LastName = row.IsClientLNameNull() ? "" : row.ClientLName;
                        objCCDD.Address = row.IsCardAddressNull() ? "" : row.CardAddress;
                        objCCDD.City = row.IsCardCityNull() ? "" : row.CardCity;
                        objCCDD.State = row.IsCardStateNull() ? "" : row.CardState;
                        objCCDD.Zip = row.IsCardPostalNull() ? "" : row.CardPostal;

                        objCCDD.ChargeAmount = 0; // Fee Amt is passed  to ChargeAmount
                        DateTime Exp = DateTime.Parse(row.CardExpDate);
                        decimal DebitAmount = (decimal)row.Amount;

                        //What should actually passed in place of Debit Amount ? "Debit Fee greater than Debit Amount" Error occured.
                        RetVal = client.AddCreditCardDebits(objCCDD, DebitAmount, Exp.Month, Exp.Year, row.CardPin);

                        if (RetVal.Message == "Successfully Saved")
                        {
                            row.TransactionID = Convert.ToString(RetVal.CreditCardDebitID);
                            row.Status = RetVal.StatusCode;
                            row.SyncNotes = RetVal.Message;
                            row.LastSyncDate = DateTime.Now;
                            row.AcceptChanges();
                        }

                        else
                        {
                            row.TransactionID = "";
                            row.SyncNotes = RetVal.Message;
                            row.LastSyncDate = DateTime.Now;
                            row.AcceptChanges();
                        }
                    }
                    else
                    {
                        EPPSApi.EFTTransaction eftTransaction = new EPPSApi.EFTTransaction();
                        decimal eftFee = 0;
                        eftInfo.CardHolderId = row.IsProcessorAccountNull() ? "" : row.ProcessorAccount;

                        if (!row.IsDateNull())
                            eftInfo.EftDate = row.Date;
                        eftInfo.EftAmount = row.IsAmountNull() ? 0 : Convert.ToDecimal(row.Amount);
                        eftInfo.BankName = row.IsBankNameNull() ? "" : row.BankName;
                        eftInfo.BankCity = row.IsBankCityNull() ? "" : row.BankCity;
                        eftInfo.BankState = row.IsBankStateNull() ? "" : row.BankState;
                        eftInfo.AccountNumber = row.IsBankAccountNumberNull() ? "" : row.BankAccountNumber;
                        eftInfo.RoutingNumber = row.IsBankRoutingNumberNull() ? "" : row.BankRoutingNumber;
                        eftInfo.AccountType = row.IsBankAccountTypeNull() ? "" : row.BankAccountType;

                        eftInfo.Memo = "";
                        eftTransaction = client.CreateEFT(eftInfo, eftFee);
                        if (eftTransaction.EftTransactionID != 0)
                        {
                            row.TransactionID = Convert.ToString(eftTransaction.EftTransactionID);
                            row.Status = "Processing";
                            row.SyncNotes = eftTransaction.Message;
                            row.LastSyncDate = DateTime.Now;
                        }
                        else
                        {
                            row.SyncNotes = eftTransaction.Message;
                            row.LastSyncDate = DateTime.Now;
                        }
                    }
                }
                if (ds.ClientDeposit.Count > 0)
                {
                    SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                    sf.UpdateT2PTransactions(ds);
                    sf.Logout();
                }
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS ViewAuthorizeCredits(DateTime startDate, DateTime endDate)
        {
            try
            {
                SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                TransactionDS ds = salesForce.GetAutherizeTransactions(startDate, endDate);
                salesForce.Logout();

                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS CreateAuthorizeCredits(ref TransactionDS ds)
        {
            try
            {
                foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit.Rows)
                {
                    var requestInfo = CheckReaders.BuildPost();

                    #region Credit Card Type
                    requestInfo.Amount = row.IsAmountNull() ? "" : Convert.ToString(row.Amount);
                    requestInfo.FirstName = row.IsClientFNameNull() ? "" : row.ClientFName;
                    requestInfo.LastName = row.IsClientLNameNull() ? "" : row.ClientLName;
                    requestInfo.Address = row.IsCardAddressNull() ? "" : row.CardAddress;
                    requestInfo.City = row.IsCardCityNull() ? "" : row.CardCity;
                    requestInfo.ShipToCity = row.IsCardCityNull() ? "" : row.CardCity;
                    requestInfo.State = row.IsCardStateNull() ? "" : row.CardState;
                    requestInfo.Zip = row.IsCardPostalNull() ? "" : row.CardPostal;
                    requestInfo.CardNum = row.IsCardNumberNull() ? null : row.CardNumber;
                    requestInfo.CustId = row.IsClientNameNull() ? "" : row.ClientName;
                    if (!row.IsCardExpDateNull())
                    {
                        DateTime expDate = Convert.ToDateTime(row.CardExpDate);
                        string month = expDate.ToString("MM");
                        string year = expDate.ToString("yy");
                        requestInfo.ExpDate = month + year;
                    }
                    else
                        requestInfo.ExpDate = null;
                    #endregion

                    requestInfo.Login = this._AuthNetID;
                    requestInfo.TranKey = this._AuthNetPwd;
                   
                    if (requestInfo.CardNum != null && requestInfo.ExpDate != null)
                    {
                        var response = CreateTransaction(requestInfo);
                        //var response.ResponseCode
                        // if (response.TransactionID != "0")
                        if (response.ResponseCode == "1")
                        {
                            row.Status = "Cleared";
                            row.ClearedDate = DateTime.Today;
                        }
                        else
                            row.Status = "Errored";

                        string returnResponseReasonNote = Resources.ResponseReasonText.GetRRC(response.ResponseReasonCode);
                        if (!returnResponseReasonNote.Equals(string.Empty))
                            row.SyncNotes = response.Message + " { " + returnResponseReasonNote + " }";
                        else
                            row.SyncNotes = response.Message;
                        row.ResponseReasonCode =Convert.ToInt32(response.ResponseReasonCode);
                        if (response.TransactionID != "0")
                            row.TransactionID = response.TransactionID;
                    }
                    else
                    {
                        if (requestInfo.CardNum == null)
                        {
                            row.SyncNotes = "Please enter client credit card number";
                        }
                        else if (requestInfo.ExpDate == null)
                        {
                            row.SyncNotes = "Please enter client credit card expiry date";
                        }

                    }
                }
                //SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                //sf.UpdateAIMPayment(ds);
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS ViewAIMTransaction(DateTime startDate, DateTime endDate)
        {
            try
            {
                SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                TransactionDS ds = salesForce.GetAutherizeDotNetTransactions(this._Processor, startDate, endDate);
                salesForce.Logout();
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS CreateAIMTransaction(TransactionDS ds)
        {
            try
            {
                foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit.Rows)
                {
                    var requestInfo = CheckReaders.BuildPost();

                    #region Echeck Type
                    requestInfo.Amount = row.IsAmountNull() ? "" : Convert.ToString(row.Amount);
                    requestInfo.BankABACode = row.IsBankRoutingNumberNull() ? "" : row.BankRoutingNumber;
                    requestInfo.BankAccountNumber = row.IsBankAccountNumberNull() ? "" : row.BankAccountNumber;
                    requestInfo.BankAccountName = row.IsNameOnAccountNull() ? "" : row.NameOnAccount;
                    requestInfo.FirstName = row.IsClientFNameNull() ? "" : row.ClientFName;
                    requestInfo.LastName = row.IsClientLNameNull() ? "" : row.ClientLName;
                    requestInfo.Address = row.IsCardAddressNull() ? "" : row.CardAddress;
                    requestInfo.State = row.IsCardStateNull() ? "" : row.CardState;
                    requestInfo.Zip = row.IsCardPinNull() ? "" : row.CardPin;
                    requestInfo.City = row.IsBillingCityNull() ? "" : row.BillingCity;
                    requestInfo.ShipToCity = requestInfo.City;
                    requestInfo.Method = "ECHECK";
                    requestInfo.CustId = row.IsClientNameNull() ? "" : row.ClientName;
                    requestInfo.EcheckType = AuthorizeNet.EcheckType.WEB;
                    requestInfo.BankName = row.IsBankNameNull() ? "" : row.BankName;
                    if (!row.IsBankAccountTypeNull())
                    {
                        if (row.BankAccountType.Equals("Checking"))
                            requestInfo.BankAccountType = AuthorizeNet.BankAccountType.Checking;
                        else if (row.BankAccountType.Equals("Savings"))
                            requestInfo.BankAccountType = AuthorizeNet.BankAccountType.Savings;
                        else if (row.BankAccountType.Equals("BusinessChecking"))
                            requestInfo.BankAccountType = AuthorizeNet.BankAccountType.BusinessChecking;
                    }
                    #endregion

                    requestInfo.Login = this._ProcessorLogin;
                    requestInfo.TranKey = this._ProcessorPwd;

                    var response = CreateTransaction(requestInfo);
                    if (response.ResponseCode == "1")
                    {
                        row.SyncNotes = response.Message;
                        row.Status = "Cleared";
                        row.ClearedDate = DateTime.Today;
                        //row.TransactionID = response.TransactionID;
                    }
                    else
                    {
                        row.SyncNotes = response.Message;
                        row.Status = "Errored";
                        //row.ClearedDate = DateTime.Today;
                    }
                    if (response.TransactionID != "0")
                        row.TransactionID = response.TransactionID;
                }
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.UpdateAuthorizePaymentFee(ds);
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range and create transaction on first data
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS ViewFDTransaction(DateTime startDate, DateTime endDate)
        {
            try
            {
                SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                TransactionDS ds = salesForce.GetFDDeposits(this._Processor, startDate, endDate);
                salesForce.Logout();
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range and create transaction on first data
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of pending transactions </returns>
        private TransactionDS CreateFDTransaction(TransactionDS ds)
        {
            try
            {
                FDIntrop.Main fdMain = new FDIntrop.Main(this._ProcessorLogin, this._ProcessorPwd, this._LiveEnvironment);
                foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit.Rows)
                {
                    if (!row.IsClientFullNameNull())
                    {
                        fdMain.CardHoldersName = row.ClientFullName;
                    }
                    if (!row.IsAmountNull())
                    {
                        fdMain.Amount = row.Amount;
                    }
                    if (!row.IsCardNumberNull())
                    {
                        fdMain.CardNumber = row.CardNumber;
                    }
                    if (!row.IsCardExpDateNull())
                    {
                        if (!row.CardExpDate.Equals(string.Empty))
                        {
                            DateTime expDate = Convert.ToDateTime(row.CardExpDate);
                            string month = expDate.ToString("MM");
                            string year = expDate.ToString("yy");
                            fdMain.ExpiryDate = month + year;
                        }
                    }

                    fdMain.CreateTransaction(ref fdMain);

                    FDIntrop.Main response = fdMain;

                    if (!response.Status.Equals(""))
                    {
                        row.SyncNotes = response.Message;
                        row.Status = response.Status;
                        row.TransactionID = response.TransactionId;
                    }
                    else
                    {
                        row.SyncNotes = response.Message;
                        row.Status = "Errored";
                    }
                }
                SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                sf.UpdateFDPaments(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Retrieves the list of pending transactions for a specified date range and 
        /// creates these transactions with the appropriate processor
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        public TransactionDS EPPSUpdateTransactions(DateTime start, DateTime endDate)
        {
            try
            {
                TransactionDS transactions = this.GetEPPSProcessingPayments(start, endDate);
                return transactions;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieves all pending payments during a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of transactions </returns>
        private TransactionDS GetEPPSProcessingPayments(DateTime start, DateTime end)
        {
            try
            {

                SFDebtClass salesForce = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                TransactionDS ds = salesForce.GetEPPSProcessingTransactions(start, end, false);
                salesForce.Logout();
                EPPSApi.FindEFT eftInfo = new EPPSApi.FindEFT();
                EPPS client = new EPPS(this._ProcessorLogin, this._ProcessorPwd);
                eftInfo = client.RetrieveEFTByCreateDate(start, end);
                TransactionDS dsOut = new TransactionDS();

                foreach (var item in eftInfo.EFTList)
                {
                    if (!item.StatusCode.Equals("Create EFT Pending"))
                    {
                        TransactionDS.ClientDepositRow row = (from i in ds.ClientDeposit where i.TransactionID == item.EftTransactionID select i).FirstOrDefault();

                        if (row != null)
                        {
                            TransactionDS.ClientDepositRow rowdata = dsOut.ClientDeposit.NewClientDepositRow();
                            row.AmountPaid = (double)item.EftAmount;
                            row.Status = item.StatusCode;
                            row.SyncNotes = item.Memo;
                            if (!string.IsNullOrEmpty(item.SettledDate) && !string.IsNullOrWhiteSpace(item.SettledDate))
                                row.ClearedDate = Convert.ToDateTime(item.SettledDate);
                            row.AcceptChanges();
                            rowdata = row;
                            dsOut.ClientDeposit.ImportRow(rowdata);
                        }
                    }
                }

                if (dsOut.ClientDeposit.Rows.Count > 0)
                {
                    SFDebtClass sf = new SFDebtClass(this._sForceLogin, this._sForcePwd, this._sForceToken, this._LiveEnvironment);
                    sf.UpdateT2PTransactions(dsOut);
                    sf.Logout();
                }
                this.UpdatedTransactions = dsOut;
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }

        //Method to create transaction on autherize.net paymnet gateway
        public IGatewayResponse CreateTransaction(IGatewayRequest authDetail)
        {
            IGatewayResponse response = null;
            try
            {
                Gateway gateway = new Gateway(authDetail.Login, authDetail.TranKey, _Processor, _LiveEnvironment);
                response = gateway.Send(authDetail, _Processor);
                return response;

            }
            catch (Exception)
            {
                throw;
            }

        }

        public void CreateExcle(string tableName, TransactionDS ds, string option)
        {
            TransactionDS exportDS = new TransactionDS();

            if (tableName.Equals("CreateTransactions"))
            {
                foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit)
                {
                    TransactionDS.TransactionExportRow newRow = exportDS.TransactionExport.NewTransactionExportRow();
                    newRow.Row_Id = exportDS.TransactionExport.Count + 1;
                    if (!row.IsProcessorAccountNull() && row.ProcessorAccount.Length >= 4)
                    {
                        newRow.Gateway_Account_Number = string.Concat("".PadLeft(12, '*'), row.ProcessorAccount.Substring(row.ProcessorAccount.Length - 4));
                    }
                    else
                        newRow.Gateway_Account_Number = String.Empty;
                    newRow.Client_Id = row.IsClientNameNull() ? String.Empty : row.ClientName;
                    newRow.Client_Name = row.IsClientFullNameNull() ? String.Empty : row.ClientFullName;
                    newRow.Bank_Name = row.IsBankNameNull() ? String.Empty : row.BankName;
                    newRow.Bank_Account_Number = row.IsBankAccountNumberNull() ? String.Empty : row.BankAccountNumber;
                    newRow.Bank_Routing_Number = row.IsBankRoutingNumberNull() ? String.Empty : row.BankRoutingNumber;
                    if (!row.IsDateNull())
                        newRow.Scheduled_Date = row.Date;
                    newRow.Amount = row.IsAmountNull() ? 0.0 : row.Amount;
                    newRow.Transaction_Id = row.IsTransactionIDNull() ? String.Empty : row.TransactionID;
                    newRow.Transation_Status = row.IsStatusNull() ? String.Empty : row.Status;
                    newRow.Transaction_Type = row.IsTransactionTypeNull() ? String.Empty : row.TransactionType;
                    newRow.Notes = row.IsSyncNotesNull() ? String.Empty : row.SyncNotes;
                    newRow.SF_Status = row.IsSFErrorCodeNull() ? String.Empty : row.SFErrorCode;
                    newRow.SF_Error_Message = row.IsSFErrorMessageNull() ? String.Empty : row.SFErrorMessage;

                    newRow.SF_Client_Id = row.IsClientIDNull() ? string.Empty : row.ClientID;
                    exportDS.TransactionExport.AddTransactionExportRow(newRow);
                }
                exportDS.TransactionExport.Gateway_Account_NumberColumn.ColumnName = this._Processor + " A/C#";

                if (option.Equals("Automatic"))
                {
                    DateTime fileNameExt = DateTime.Now;
                    var directory = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6) + "\\Report";
                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    string fileName = "Transaction_" + fileNameExt.Month.ToString() + fileNameExt.Day.ToString() + fileNameExt.Year.ToString() + fileNameExt.Hour.ToString() + fileNameExt.Minute.ToString() + fileNameExt.Second.ToString();
                    string fullPath = directory + "\\" + fileName;
                    exportDS.TransactionExport.ExportToExcel(fullPath);
                }
                else
                    exportDS.TransactionExport.ExportToExcel(null);
            }
            else if (tableName.Equals("CreateClient"))
            {
                foreach (TransactionDS.ClientsRow row in this.CreatedClients)
                {
                    TransactionDS.ClientExportRow newRow = exportDS.ClientExport.NewClientExportRow();
                    newRow.Row_Id = exportDS.ClientExport.Count + 1;
                    if (!row.IsProcessorAccountNumNull() && row.ProcessorAccountNum.Length>=4)
                    {
                        newRow.Gateway_Account_Number = string.Concat("".PadLeft(12, '*'), row.ProcessorAccountNum.Substring(row.ProcessorAccountNum.Length - 4));
                    }
                    else
                        newRow.Gateway_Account_Number = String.Empty;
                    newRow.Client_Id = row.IsClientNameIDNull() ? String.Empty : row.ClientNameID;
                    newRow.Client_Name = (row.IsFirstNameNull() ? String.Empty : row.FirstName) + " " + (row.IsLastNameNull() ? String.Empty : row.LastName);
                    newRow.Account_Balance = row.IsAccountBalanceNull() ? 0.0 : row.AccountBalance;
                    newRow.Note = row.IsSyncDescriptionNull() ? String.Empty : row.SyncDescription;
                    newRow.SF_Status = row.IsSFErrorCodeNull() ? String.Empty : row.SFErrorCode;
                    newRow.SF_Error_Message = row.IsSFErrorMessageNull() ? String.Empty : row.SFErrorMessage;
                    newRow.SF_Client_Id = row.IsClientIDNull() ? string.Empty : row.ClientID;
                    exportDS.ClientExport.AddClientExportRow(newRow);
                }
                ds.ClientExport.Gateway_Account_NumberColumn.ColumnName = this._Processor + " A/C#";

                if (option.Equals("Automatic"))
                {
                    DateTime fileNameExt = DateTime.Now;
                    var directory = System.IO.Path.GetDirectoryName(
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Substring(6) + "\\Report";
                    if (!Directory.Exists(directory))
                        Directory.CreateDirectory(directory);

                    string fileName = "Client_" + fileNameExt.Month.ToString() + fileNameExt.Day.ToString() + fileNameExt.Year.ToString() + fileNameExt.Hour.ToString() + fileNameExt.Minute.ToString() + fileNameExt.Second.ToString();
                    string fullPath = directory + "\\" + fileName;
                    exportDS.ClientExport.ExportToExcel(fullPath);
                }
                else
                    exportDS.ClientExport.ExportToExcel(null);
            }
        }

        #endregion
    }

    public static class CheckReaders
    {
        /// <summary>
        /// This override will use HttpContext.Current - not ideal for testing
        /// </summary>
        public static IGatewayRequest BuildPost()
        {
            //validate the request first
            var request = new ReferanceRequest();
            return request;
        }
    }

    public class ReferanceRequest : GatewayRequest
    {
    }

}