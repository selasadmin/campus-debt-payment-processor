﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Services.Protocols;
using RAMSApi = RAMSInterop.RAMSProd;
using System.Data;

namespace RAMSInterop
{
    public class RAMS
    {

        /// <summary>
        /// RAM Vendor ID
        /// </summary>
        private string VendorID = string.Empty;

        /// <summary>
        /// RAM Session ID
        /// </summary>
        private System.Guid SessionID;

        /// <summary> 
        /// RAM live environment ? if no then test environment
        /// </summary>
        private Boolean prodEnvironment = false;

        private RAMSApi.RAMSGatewayVer2 ServiceClient = null;

        private void GetSoapClient()
        {
            this.ServiceClient = new RAMSApi.RAMSGatewayVer2();
            if (prodEnvironment)
                ServiceClient.Url = global::RAMSInterop.Properties.Settings.Default.RAMSInterop_RAMSProd_RAMSGatewayVer2;
            else
                ServiceClient.Url = global::RAMSInterop.Properties.Settings.Default.RAMSInterop_RAMSTest_RAMSGatewayVer2;

            ServiceClient.Timeout = 2 * 60 * 1000;         // set timeout to 2 minutes
        }


        public RAMS(string VendorID, Boolean prod)
        {
            this.VendorID = VendorID;
            this.prodEnvironment = prod;
            GetSoapClient();
        }

        public bool Login()
        {
            bool IsLoggedIn = false;
            try
            {
                this.SessionID = this.ServiceClient.StartSession(VendorID);
                if (this.SessionID != Guid.Empty)
                {
                    IsLoggedIn = true;
                }
                else
                {
                    Exception ex = new Exception("Could not Login to RAMS. Unable to generate session");
                    throw ex;
                }
            }
            catch
            {
                IsLoggedIn = false;
                throw;
            }
            return IsLoggedIn;
        }

        public string Logout()
        {
            try
            {
                this.ServiceClient.EndSession(this.SessionID);
                return "SessionEnd";
            }
            catch
            {
                throw;
            }
        }

        /// <summary> 
        /// Method to look for an existing Client in RAMS System
        /// </summary>
        public bool FindClientById(ref ClientInfo clientInfo)
        {
            bool IsSearched = false;
            //string RetVal = string.Empty;
            try
            {
                string[] resArr = null;
                resArr = this.ServiceClient.GetClientInfo(this.SessionID, clientInfo.ClientID);

                if (resArr != null && resArr[0] == "OK")
                {
                    IsSearched = true;
                }
            }
            catch (SoapException ex)
            {
                //throw ex;
                clientInfo.ErrorResponse = ex.Message;
            }
            return IsSearched;
        }


        public string[] GetClientInfobyID(int clientID)
        {
            string[] resArr = null;
            try
            {
                resArr = this.ServiceClient.GetClientInfo(this.SessionID, clientID);
            }
            catch (SoapException ex)
            {
                //throw ex;
            }
            return resArr;
        }

        public DataTable GetAllClients(string[] clientIDs)
        {
            DataTable resDt = null;
            try
            {
                resDt = this.ServiceClient.GetClientInfo_Bulk(this.SessionID, clientIDs);
            }
            catch (SoapException ex)
            {
                //throw ex;
            }
            return resDt;
        }


        /// <summary> 
        /// Method to create new Client in RAMS System
        /// </summary>
        public void AddNewClient(ref ClientInfo oClientinfo)
        {
            try
            {
                string[] resArr = this.ServiceClient.NewClient(this.SessionID, oClientinfo.ClientID, oClientinfo.Firstname, oClientinfo.Lastname, oClientinfo.Firstname, oClientinfo.Lastname, oClientinfo.emailAddress,
                                                        oClientinfo.taxID, "", oClientinfo.streetAddress, oClientinfo.city, oClientinfo.zip, oClientinfo.stateAbbreviation, "", "", "", "",
                                                        oClientinfo.Phone1, oClientinfo.Phone2, oClientinfo.Phone3, oClientinfo.Dob1, "", oClientinfo.AccountStatus, oClientinfo.AffiliateID,
                                                        oClientinfo.feeSplitGroupID, oClientinfo.AccountHoldStatus);

                oClientinfo.IsClientCreated = true;

                if (resArr != null && resArr[0] == "OK")
                {
                    this.ServiceClient.AddUpdateClientBanking(this.SessionID, oClientinfo.ClientID, oClientinfo.AccountNumber, oClientinfo.NameonAccount, oClientinfo.Routing, oClientinfo.BankName,
                                                           oClientinfo.AccountType);

                    oClientinfo.IsBankingCreated = true;
                }
            }
            catch (SoapException ex)
            {
                oClientinfo.ErrorResponse = ex.Message;

                if (this.FindClientById(ref oClientinfo))
                {
                    this.ServiceClient.RemoveUser(this.SessionID, oClientinfo.ClientID.ToString());
                }
                throw ex;
            }
        }

        public void UpdateClient(ref ClientInfo oClientinfo)
        {
            try
            {
                //Update CLient Info
                string RetValue = this.ServiceClient.UpdateClient(this.SessionID, oClientinfo.ClientID, oClientinfo.Firstname, oClientinfo.Lastname, oClientinfo.Firstname, oClientinfo.Lastname, oClientinfo.emailAddress,
                                                        oClientinfo.taxID, "", oClientinfo.streetAddress, oClientinfo.city, oClientinfo.zip, oClientinfo.stateAbbreviation, "", "", "", "",
                                                        oClientinfo.Phone1, oClientinfo.Phone2, oClientinfo.Phone3, oClientinfo.Dob1, "", oClientinfo.AccountStatus, oClientinfo.AffiliateID,
                                                        oClientinfo.feeSplitGroupID, oClientinfo.AccountHoldStatus);
                oClientinfo.IsClientCreated = true;

                //Update Banking Details 
                this.ServiceClient.AddUpdateClientBanking(this.SessionID, oClientinfo.ClientID, oClientinfo.AccountNumber, oClientinfo.NameonAccount, oClientinfo.Routing, oClientinfo.BankName,
                                                           oClientinfo.AccountType);

                oClientinfo.IsBankingCreated = true;

            }
            catch (SoapException ex)
            {
                oClientinfo.ErrorResponse = ex.Message;
                throw ex;
            }
        }

        public DataTable GetAllTransactionsByDate(DateTime StartDate, DateTime EndDate)
        {
            DataTable Transactions = null;
            try
            {
                Transactions = this.ServiceClient.GetClientTransByPaymentDateRange(this.SessionID, StartDate, EndDate);
            }
            catch (SoapException ex)
            {
                throw ex;
            }
            return Transactions;
        }

        public void GetClientTransactionsByClientID(ref ClientInfo CInfo)
        {
            CInfo.Transactions_dt = new DataTable();
            try
            {
                CInfo.Transactions_dt = this.ServiceClient.GetClientTransByClientID(this.SessionID, CInfo.ClientID);
            }
            catch (SoapException ex)
            {
            }
        }

        public DataTable GetClientTransByPaymentDateRange(DateTime startDate, DateTime Enddate)
        {
            DataTable dt = null;
            try
            {
                dt = ServiceClient.GetClientTransByPaymentDateRange(this.SessionID, startDate, Enddate);
                return dt;
            }
            catch (SoapException ex)
            {
                dt = null;
            }
            return dt;
        }

        public DataTable GetClientBalances(DataTable dt_Clients)
        {
            DataTable RetBalancesdt = null;
            try
            {
                RetBalancesdt = this.ServiceClient.GetClientSavings_Bulk(this.SessionID, dt_Clients);
            }
            catch (SoapException ex)
            {
                throw ex;
            }
            return RetBalancesdt;
        }

        /// <summary> 
        /// Method to create Transaction RAMS payment getway service
        /// </summary>
        public int CreateTransaction(ref List<TransactionInfo> list, ref object objTransactionResponce)
        {
            string empty = string.Empty;
            int transactionCreateValue = 0;
            if (Login())
            {

                foreach (var objInput in list)
                {
                    RAMSApi.PaymentLine paymentLine;
                    RAMSApi.PaymentLine[] paymentLineOut;
                    string[] addSingleOut;
                    try
                    {
                        int clientId = Convert.ToInt32(GetClientID(objInput.ClientId));

                        RAMSApi.PaymentLine[] paymentLineScheduled = RetrieveClientPaymentSchedule(SessionID, clientId);
                        List<RAMSApi.PaymentLine> paymentLineList = (List<RAMSApi.PaymentLine>)paymentLineScheduled.ToList();
                        paymentLine = CreatePaymentLine(objInput);

                        if (paymentLineList.Count > 0)
                        {
                            addSingleOut = AddSinglePaySchedule(clientId, paymentLine);
                            if (addSingleOut[0].Equals("OK"))
                            {
                                (from i in list where i.TrasactionId.Equals(objInput.TrasactionId) select i).First().ProcessStatus = true;
                                (from i in list where i.TrasactionId.Equals(objInput.TrasactionId) select i).First().PID = Convert.ToInt32(addSingleOut[1].ToString());
                                (from i in list where i.TrasactionId.Equals(objInput.TrasactionId) select i).First().TotalPaymentAmount = Convert.ToDouble(addSingleOut[2].ToString());
                            }
                        }
                        else
                        {
                            paymentLineOut = CreatePaySchedule(clientId, paymentLine);
                            if (!paymentLineOut.Equals(null))
                            {
                                (from i in list where i.TrasactionId.Equals(objInput.TrasactionId) select i).First().ProcessStatus = true;
                                (from i in list where i.TrasactionId.Equals(objInput.TrasactionId) select i).First().PID = paymentLineOut[0].PID;
                                (from i in list where i.TrasactionId.Equals(objInput.TrasactionId) select i).First().TotalPaymentAmount = paymentLineOut[0].TotalPaymentAmount;
                            }
                        }
                        (from i in list where i.TrasactionId.Equals(objInput.TrasactionId) select i).First().Note = "Transaction scheduled successfully";
                    }
                    catch (Exception Ex)
                    {
                        (from i in list where i.TrasactionId.Equals(objInput.TrasactionId) select i).First().Note = Ex.Message;

                    }
                }
                Logout();
            }

            return transactionCreateValue;
        }

        public void UpdateTransaction()
        {
            //ServiceClient.PayScheduleUpdateSingle(               
        }

        /// <summary> 
        /// Method to split sttring and get client id 
        /// </summary>
        private string GetClientID(string clientId)
        {
            int index = clientId.IndexOf("-");
            clientId = clientId.Substring(index + 1);
            return clientId;
        }

        /// <summary> 
        /// Method to retrieve client payment schedule
        /// </summary>
        private RAMSApi.PaymentLine[] RetrieveClientPaymentSchedule(Guid SessionID, int clientId)
        {
            RAMSApi.PaymentLine[] paymentLine = null;
            try
            {
                paymentLine = ServiceClient.GetClientPaymentSchedule(SessionID, clientId);

            }
            catch (Exception)
            {
                throw;
            }
            return paymentLine;
        }

        /// <summary> 
        /// Method to retrieve client Transaction by client ID
        /// </summary>
        private System.Data.DataTable RetrieveClientTransByClientID(Guid SessionID, int clientId)
        {
            System.Data.DataTable clientTransaction = new System.Data.DataTable();
            clientTransaction = ServiceClient.GetClientTransByClientID(SessionID, clientId);
            return clientTransaction;
        }

        /// <summary> 
        /// Method to add paymnet schedule
        /// </summary>
        private string[] AddSinglePaySchedule(int clientId, RAMSApi.PaymentLine inputPaymnetLIne)
        {

            string[] returnPaySchedule = null;
            returnPaySchedule = ServiceClient.PayScheduleAddSingle(this.SessionID, clientId, "",
                inputPaymnetLIne.Fee1Amount, inputPaymnetLIne.Fee2Amount, inputPaymnetLIne.Fee3Amount,
                inputPaymnetLIne.Fee4Amount, inputPaymnetLIne.Fee5Amount, inputPaymnetLIne.Fee6Amount,
                inputPaymnetLIne.Fee7Amount, inputPaymnetLIne.TotalPaymentAmount, inputPaymnetLIne.SavingsAmount, inputPaymnetLIne.SchedDate);

            return returnPaySchedule;
        }

        /// <summary> 
        /// Method to create paymnet schedule 
        /// </summary>
        private RAMSApi.PaymentLine[] CreatePaySchedule(int clientId, RAMSApi.PaymentLine inputPaymnetLIne)
        {
            //RAMSApi.PaymentLine[] paymentLine = null;
            RAMSApi.PaymentLine[] paymentLineIn = new RAMSApi.PaymentLine[1] { inputPaymnetLIne };
            RAMSApi.PaymentLine[] paymentLineOut = new RAMSApi.PaymentLine[1];
            //clientId = 9854;
            // paymentLine = CreatePaymentLine();
            paymentLineOut = ServiceClient.PayScheduleCreate(this.SessionID, clientId, paymentLineIn);
            return paymentLineOut;
        }

        /// <summary> 
        /// Method to create paymnet Line 
        /// </summary>
        private RAMSApi.PaymentLine CreatePaymentLine(TransactionInfo list)
        {
            RAMSApi.PaymentLine paymentLine = new RAMSApi.PaymentLine();

            paymentLine.SchedDate = list.ScheduleDate;
            if (!list.Fee1.Equals(0))
            {
                paymentLine.Fee1Amount = list.Fee1;
            }
            if (!list.Fee2.Equals(0))
            {
                paymentLine.Fee2Amount = list.Fee2;
            }
            if (!list.Fee3.Equals(0))
            {
                paymentLine.Fee3Amount = list.Fee3;
            }
            if (!list.Fee4.Equals(0))
            {
                paymentLine.Fee4Amount = list.Fee4;
            }
            if (!list.Fee5.Equals(0))
            {
                paymentLine.Fee5Amount = list.Fee5;
            }
            if (!list.Fee6.Equals(0))
            {
                paymentLine.Fee6Amount = list.Fee6;
            }
            if (!list.Fee7.Equals(0))
            {
                paymentLine.Fee7Amount = list.Fee7;
            }

            paymentLine.SavingsAmount = list.SavingsAmount;
            paymentLine.TotalPaymentAmount = list.TotalPaymentAmount;
            paymentLine.Paid = false;
            paymentLine.Returned = false;
            paymentLine.RAMFEE = 0;
            paymentLine.PID = 0;
            return paymentLine;
        }
    }

    public class ClientInfo
    {
        int _clientID;
        string _Firstname;
        string _Lastname;
        string _emailAddress;
        string _taxID;
        string _streetAddress;
        string _city;
        string _zip;
        string _stateAbbreviation;  //2-­‐character state abbreviation
        string _Phone1;
        string _Phone2;
        string _Phone3;
        string _Dob1;
        int _AffiliateID;
        int _feeSplitGroupID;
        double _paymentAmount;
        int _settlementID;
        string _payeeZIP;
        string _payeeName;
        DateTime _paymentDate;
        string _payeeAddress;
        string _payeeClientAccountNumber;
        string _payeeCity;
        string _payeeStateAbbrev;
        string _referenceNumber;
        string _creditorName;
        string _AccountNumber;
        string _NameonAccount;
        string _Routing;
        string _BankName;
        int _PaymentScheduleID;


        double _Savings = 0.00D;
        DataTable _ClientBalances_dt = null;
        DataTable _Transactions_dt = null;
        bool _IsClientCreated = false;
        bool _IsBankingCreated = false;
        string _ErrorResponse = string.Empty;

        RAMSApi.BankAccountType _AccountType = RAMSApi.BankAccountType.CREDIT_SAVINGS;
        RAMSApi.AccountStatus _accountStatus = RAMSApi.AccountStatus.ACTIVE;
        RAMSApi.ACHStatus _accountHoldStatus = RAMSApi.ACHStatus.ACTIVE;
        RAMSApi.SETTLEMENT_PAYMENTYPE _paymnetType;
        RAMSApi.PaymentLine[] _PaymentLinesArray;

        public DataTable Transactions_dt
        {
            get { return _Transactions_dt; }
            set { _Transactions_dt = value; }
        }

        public DataTable ClientBalances_dt
        {
            get { return _ClientBalances_dt; }
            set { _ClientBalances_dt = value; }
        }
        public RAMSApi.PaymentLine[] PaymentLinesArray
        {
            get { return _PaymentLinesArray; }
            set { _PaymentLinesArray = value; }
        }

        public int PaymentScheduleID
        {
            get { return _PaymentScheduleID; }
            set { _PaymentScheduleID = value; }
        }

        public double Savings
        {
            get { return _Savings; }
            set { _Savings = value; }
        }

        public bool IsClientCreated
        {
            get { return _IsClientCreated; }
            set { _IsClientCreated = value; }
        }

        public bool IsBankingCreated
        {
            get { return _IsBankingCreated; }
            set { _IsBankingCreated = value; }
        }

        public string ErrorResponse
        {
            get { return _ErrorResponse; }
            set { _ErrorResponse = value; }
        }

        public int ClientID
        {
            get { return _clientID; }
            set { _clientID = value; }
        }
        public string Firstname
        {
            get { return _Firstname; }
            set { _Firstname = value; }
        }
        public string Lastname
        {
            get { return _Lastname; }
            set { _Lastname = value; }
        }
        public string emailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; }
        }
        public string taxID
        {
            get { return _taxID; }
            set { _taxID = value; }
        }
        public string streetAddress
        {
            get { return _streetAddress; }
            set { _streetAddress = value; }
        }
        public string city
        {
            get { return _city; }
            set { _city = value; }
        }
        public string zip
        {
            get { return _zip; }
            set { _zip = value; }
        }
        public string stateAbbreviation
        {
            get { return _stateAbbreviation; }
            set { _stateAbbreviation = value; }
        }
        public string Phone1
        {
            get { return _Phone1; }
            set { _Phone1 = value; }
        }

        public string Phone2
        {
            get { return _Phone2; }
            set { _Phone2 = value; }
        }

        public string Phone3
        {
            get { return _Phone3; }
            set { _Phone3 = value; }
        }

        public string Dob1
        {
            get { return _Dob1; }
            set { _Dob1 = value; }
        }
        public int AffiliateID
        {
            get { return _AffiliateID; }
            set { _AffiliateID = value; }
        }
        public int feeSplitGroupID
        {
            get { return _feeSplitGroupID; }
            set { _feeSplitGroupID = value; }
        }
        public RAMSApi.BankAccountType AccountType
        {
            get { return _AccountType; }
            set { _AccountType = value; }
        }
        public RAMSApi.AccountStatus AccountStatus
        {
            get { return _accountStatus; }
            set { _accountStatus = value; }
        }
        public RAMSApi.ACHStatus AccountHoldStatus
        {
            get { return _accountHoldStatus; }
            set { _accountHoldStatus = value; }
        }


        public int SettlementID
        {
            get { return _settlementID; }
            set { _settlementID = value; }
        }
        public double PaymentAmount
        {
            get { return _paymentAmount; }
            set { _paymentAmount = value; }
        }
        public DateTime PaymentDate
        {
            get { return _paymentDate; }
            set { _paymentDate = value; }
        }
        public RAMSApi.SETTLEMENT_PAYMENTYPE PaymnetType
        {
            get { return _paymnetType; }
            set { _paymnetType = value; }
        }
        public string PayeeName
        {
            get { return _payeeName; }
            set { _payeeName = value; }
        }
        public string PayeeAddress
        {
            get { return _payeeAddress; }
            set { _payeeAddress = value; }
        }
        public string PayeeCity
        {
            get { return _payeeCity; }
            set { _payeeCity = value; }
        }
        public string PayeeStateAbbrev
        {
            get { return _payeeStateAbbrev; }
            set { _payeeStateAbbrev = value; }
        }
        public string PayeeZIP
        {
            get { return _payeeZIP; }
            set { _payeeZIP = value; }
        }
        public string PayeeClientAccountNumber
        {
            get { return _payeeClientAccountNumber; }
            set { _payeeClientAccountNumber = value; }
        }
        public string CreditorName
        {
            get { return _creditorName; }
            set { _creditorName = value; }
        }
        public string ReferenceNumber
        {
            get { return _referenceNumber; }
            set { _referenceNumber = value; }
        }
        public string AccountNumber
        {
            get { return _AccountNumber; }
            set { _AccountNumber = value; }
        }
        public string NameonAccount
        {
            get { return _NameonAccount; }
            set { _NameonAccount = value; }
        }
        public string Routing
        {
            get { return _Routing; }
            set { _Routing = value; }
        }
        public string BankName
        {
            get { return _BankName; }
            set { _BankName = value; }
        }
    }

    public class TransactionInfo
    {
        #region Field
        private string _trasactionId;
        private string _clientId;
        private DateTime scheduleDate;
        private double fee1;
        private double fee2;
        private double fee3;
        private double fee4;
        private double fee5;
        private double fee6;
        private double fee7;
        private double savingsAmount;
        private int pID;
        private double totalPaymentAmount;
        private bool paid;
        private bool returned;
        private double rAMFEE;
        private bool processStatus;
        private string _clientID_c;

        private string note;

        public string Note
        {
            get { return note; }
            set { note = value; }
        }

        #endregion

        #region Property

        public double SavingsAmount
        {
            get { return savingsAmount; }
            set { savingsAmount = value; }
        }

        public bool ProcessStatus
        {
            get { return processStatus; }
            set { processStatus = value; }
        }

        public double TotalPaymentAmount
        {
            get { return totalPaymentAmount; }
            set { totalPaymentAmount = value; }
        }


        public bool Paid
        {
            get { return paid; }
            set { paid = value; }
        }


        public bool Returned
        {
            get { return returned; }
            set { returned = value; }
        }


        public double RAMFEE
        {
            get { return rAMFEE; }
            set { rAMFEE = value; }
        }


        public int PID
        {
            get { return pID; }
            set { pID = value; }
        }

        public string TrasactionId
        {
            get { return _trasactionId; }
            set { _trasactionId = value; }
        }

        public string ClientId
        {
            get { return _clientId; }
            set { _clientId = value; }
        }



        public string ClientID_c
        {
            get { return _clientID_c; }
            set { _clientID_c = value; }
        }

        public DateTime ScheduleDate
        {
            get { return scheduleDate; }
            set { scheduleDate = value; }
        }

        public double Fee1
        {
            get { return fee1; }
            set { fee1 = value; }
        }

        public double Fee2
        {
            get { return fee2; }
            set { fee2 = value; }
        }

        public double Fee3
        {
            get { return fee3; }
            set { fee3 = value; }
        }

        public double Fee4
        {
            get { return fee4; }
            set { fee4 = value; }
        }


        public double Fee5
        {
            get { return fee5; }
            set { fee5 = value; }
        }


        public double Fee6
        {
            get { return fee6; }
            set { fee6 = value; }
        }


        public double Fee7
        {
            get { return fee7; }
            set { fee7 = value; }
        }
        #endregion
    }
}
