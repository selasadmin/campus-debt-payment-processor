﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BluePay2;

namespace BluePayLibrary
{
    public class BPClass
    {
        private List<BluePay2API> payments = new List<BluePay2API>();

        /// <summary>
        /// The bluePay URL
        /// </summary>
        private readonly string url = "https://secure.bluepay.com/interfaces/bp10emu";

        /// <summary>
        /// The mode i.e. test / live
        /// </summary>
        private readonly string mode = "TEST";

        /// <summary>
        /// The company name
        /// </summary>
        private readonly string companyName = "CDDG";

        /// <summary>
        /// The gateway account ID
        /// </summary>
        private readonly string accountID = "123412341234";

        /// <summary>
        /// The secret key
        /// </summary>
        private readonly string key = "abcdabcdabcdabcdabcdabcdabcdabcd";

        /// <summary>
        /// Constructor for BluePay Class
        /// </summary>
        public BPClass()
        {
        }

        /// <summary>
        /// The BluePay customer
        /// </summary>
        public struct BPCustomer
        {
            public string AccountNumber;
            public string RoutingNumber;
            public string CustomerName;
            public string CompanyName;
            public string AddressLine1;
            public string City;
            public string State;
            public string Zip;
            public string Phone;
            public decimal Amount = 0;
            public bool IsCompany = false;
            public bool IsWebTransaction = false;

            /// <summary>
            /// Checks that all required fields are filled in
            /// </summary>
            /// <returns>False if any field is blank, and true otherwise</returns>
            public bool CheckRequired()
            {
                return !string.IsNullOrEmpty(AccountNumber) && !string.IsNullOrEmpty(RoutingNumber) && !string.IsNullOrEmpty(CustomerName) && !string.IsNullOrEmpty(AddressLine1)
                        && !string.IsNullOrEmpty(City) && !string.IsNullOrEmpty(State) && !string.IsNullOrEmpty(Zip) && !string.IsNullOrEmpty(Phone) && this.Amount > 0 
                        && (!this.IsCompany || (this.IsCompany && !string.IsNullOrEmpty(this.CompanyName)));
            }
        }

        public void Process(SFDebt.TransactionDS tds)
        {
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(""))
            {
                // create the associated blue pay objects
                foreach (SFDebt.TransactionDS.ClientDepositRow cdR in tds.ClientDeposit.Rows)
                {
                    BPCustomer c = new BPCustomer();
                    c.AccountNumber = cdR.BankAccountNumber;
                    c.RoutingNumber = cdR.BankRoutingNumber;
                    c.CustomerName = cdR.ClientName;

                    SFDebt.TransactionDS.ClientsRow[] cr = tds.Clients.Select("ClientID = '" + cdR.ClientID + "'") as SFDebt.TransactionDS.ClientsRow[];


                }

                // 
            }
        }
    }
}
