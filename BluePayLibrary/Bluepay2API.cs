/*
 * Bluepay 2.0 .NET Sample code.
 *
 * Developed by Joel Tosi and Chris Jansen of Bluepay.
 *
 * This code is Free.  You may use it, modify it, redistribute it, post it on the bathroom wall,
 * or whatever.  If you do make modifications that are useful, Bluepay would love it if you donated
 * them back to us!
 *
 * TODO: There is no input validation.
 * TODO: add COUNTRY
 *
 */
namespace BluePay2
{
    using System;
    using System.IO;
    using System.Net;
    using System.Security.Cryptography;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;

	/// <summary>
	/// This is the BluePay2 API COM object.
	/// </summary>
	public class BluePay2API
    {
        #region Fields

        //required for every transaction
		public string BPMerchant = "";
		public string BPURL = "";
		public string BPKey = "";
		public string BPMode = "";
		public string BPDebug = "";

		//required for auth or sale
		public string BPCCNum = "";
		public string BPCVV2 = "";
		public string BPExpire = "";
		public string BPName = "";
		public string BPAddr1 = "";
		public string BPCity = "";
		public string BPState = "";
		public string BPZip = "";

		//optional for auth or sale
		public string BPAddr2 = "";
		public string BPComment = "";
		public string BPPhone = "";
		public string BPEmail = "";

		//transaction variables
		public string BPAmount = "";
		public string BPTransaction_Type = "";
		public string BPAVS_Allowed = "";
		public string BPCVV2_Allowed = "";
		public string BPAutocap = "";
		public string BPRRNO = "";

		//rebill variables
		public string BPRebilling  = "";
		public string BPReb_Amount = "";
		public string BPReb_First_Date = "";
		public string BPReb_Expr = "";
		public string BPReb_Cycles = "";

		//level2 variables
		public string BPOrder_ID = "";
		public string BPInvoice_ID = "";
		public string BPAmount_Tip = "";
		public string BPAmount_Tax = "";

		public string BPTPS = "";
		public string BPheaderstring = "";

        //variables for ach storage
        public string PAYMENT_TYPE = "CREDIT";
        public string ACH_ACCOUNT_TYPE = "";
        public string ACH_ROUTING = "";
        public string ACH_ACCOUNT = "";
        public string DOC_TYPE = "WEB";

        public string IS_CORPORATE = "0";
        public string COMPANY_NAME = "";

        //f&b variables
        public string BPAmount_Food = "";
        public string BPAmount_Misc = "";
        #endregion

        public BluePay2API(string Merchant, string URL, string Secret_key, string Mode)
		{
            BPMerchant = Merchant;
            BPURL = URL;
            BPKey = Secret_key;
            BPMode = Mode;
		}

		public void New(string Merchant, string URL, string Secret_key, string Mode)
		{
			New(Merchant,URL,Secret_key,Mode,"0");
		}

		public void New(string Merchant, string URL, string Secret_key, string Mode, string Debug)
		{
			//This should be the constructor
			//need to add logic to make sure that merchant and url are based if not throw execption
			BPMerchant = Merchant;
			BPURL = URL;
			BPKey = Secret_key;
			BPMode = Mode;
			BPDebug = Debug;
		}

		public void Set_Cust_Required(string CCNum, string CVV2, string Expire, string Name, string Addr1, string City, string State,string Zip)
		{
			BPCCNum = CCNum;
			BPCVV2 = CVV2;
			BPExpire = Expire;
			BPName = Name;
			BPAddr1 = Addr1;
			BPCity = City;
			BPState = State;
			BPZip = Zip;
		}

        public void Set_ACH_Required(string i_account, string i_routing, string i_name, 
                                 string i_addr1, string i_city, string i_state, string i_zip,
                                 string i_phone)
        {
            PAYMENT_TYPE = "ACH";
            ACH_ACCOUNT_TYPE = "C";
            ACH_ROUTING = i_routing;
            ACH_ACCOUNT = i_account;
            DOC_TYPE = "WEB";
            BPName = i_name;
            BPAddr1 = i_addr1;
            BPCity  = i_city;
            BPState = i_state;
            BPZip   = i_zip;
            BPPhone = i_phone;
        }

        // use savings account instead of checking.
        public void Set_ACH_Savings()
        {
            ACH_ACCOUNT_TYPE = "S";
        }

        public void Set_Doc_Type(string i_doc_type)
        {
            DOC_TYPE = i_doc_type;
        }

        public void Set_Corporate_Charge(string i_company_name)
        {
            IS_CORPORATE = "1";
            COMPANY_NAME = i_company_name;
        }

		public void Easy_Sale(string Amount)
		{
			BPAmount = Amount;
			BPTransaction_Type = "SALE";
		}

		public void Easy_Auth(string Amount)
		{
			BPAmount = Amount;
			BPTransaction_Type = "AUTH";
		}

		public void Easy_Add_Rebill(string Reb_Amount, string Reb_First_Date, string Reb_Expr, string Reb_Cycles)
		{
			//need to put logic in to confirm that trans_type is a sale,auth,or capture. if not then throw execption.
			BPRebilling = "1";
			BPReb_Amount = Reb_Amount;
			BPReb_First_Date = Reb_First_Date;
			BPReb_Expr = Reb_Expr;
			BPReb_Cycles = Reb_Cycles;
		}

		public void Easy_Add_AVS_Proofing(string AVS_Allowed)
		{
			//Need to make sure that trans_type is auth
			BPAVS_Allowed = AVS_Allowed;
		}

		public void Easy_Add_CVV2_Proofing(string CVV2_Allowed)
		{
			//Need to make sure that trans_type is auth
			BPCVV2_Allowed = CVV2_Allowed;
		}

		public void Easy_Add_Autocap()
		{
			BPAutocap = "1";
		}

		public void Easy_Refund(string RRNO)
		{
			BPTransaction_Type = "REFUND";
			BPRRNO = RRNO;
		}

		public void Easy_Rebcancel(string RRNO)
		{
			BPTransaction_Type = "REBCANCEL";
			BPRRNO = RRNO;
		}

		public void Easy_Capture(string RRNO)
		{
			BPTransaction_Type = "CAPTURE";
			BPRRNO = RRNO;
		}

		public void Set_Order_ID(string Order_ID)
		{
			BPOrder_ID = Order_ID;
		}

		public void Set_Level2(string Order_ID, string Invoice_ID, string Amount_Tip, string Amount_Tax)
		{
			BPOrder_ID = Order_ID;
			BPInvoice_ID = Invoice_ID;
			BPAmount_Tip = Amount_Tip;
			BPAmount_Tax = Amount_Tax;
		}

        public void Set_FnB(string Food_Amount, string Misc_Amount, string Amount_Tip, string Amount_Tax)
        {
            BPAmount_Food = Food_Amount;
            BPAmount_Misc = Misc_Amount;
            BPAmount_Tip = Amount_Tip;
            BPAmount_Tax = Amount_Tax;
        }

		public void Set_Addr2(string Addr2)
		{
			BPAddr2 = Addr2;
		}

		public void Set_Comment(string Comment)
		{
			BPComment = Comment;
		}

		public void Set_Phone(string Phone)
		{
			BPPhone = Phone;
		}

		public void Set_Email(string Email)
		{
			BPEmail = Email;
		}

		public void Set_Param(string Name, string Value)
		{
			Name = Value;
		}

		public void Calc_TPS()
		{
			string tamper_proof_seal = BPKey +
                                 BPMerchant +
                                 BPTransaction_Type +
                                 BPAmount +
                                 BPRebilling +
                                 BPReb_First_Date +
                                 BPReb_Expr +
                                 BPReb_Cycles +
                                 BPReb_Amount +
                                 BPRRNO +
                                 BPAVS_Allowed +
                                 BPAutocap +
                                 BPMode;
      
			MD5 md5 = new MD5CryptoServiceProvider();
			byte[] hash;
			ASCIIEncoding encode = new ASCIIEncoding();
			
			byte[] buffer = encode.GetBytes(tamper_proof_seal);
			hash = md5.ComputeHash(buffer);
			BPTPS = ByteArrayToString(hash);
		}

		//This is used to convert a byte array to a hex string
		static string ByteArrayToString(byte[] arrInput)
		{
			int i;
			StringBuilder sOutput = new StringBuilder(arrInput.Length);
			for (i=0;i < arrInput.Length; i++)
			{
				sOutput.Append(arrInput[i].ToString("X2"));
			}
			return sOutput.ToString();
		}

		public string Process()
		{
            // Calc the TPS
            Calc_TPS();
      
			//post data
			ASCIIEncoding encoding = new ASCIIEncoding();
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(BPURL));
			request.AllowAutoRedirect = false;

            // THIS IS DANGEROUS!  NO CERT VALIDATON WILL BE PERFORMED.
            ServicePointManager.CertificatePolicy = new FakeCertificatePolicy ();

			string serverquery = "MERCHANT=" + System.Web.HttpUtility.UrlEncode(BPMerchant) + 
				"&MISSING_URL=nu" + 
				"&APPROVED_URL=nu" +
				"&DECLINED_URL=nu" +
				"&MODE="              + HttpUtility.UrlEncode(BPMode) +
				"&TAMPER_PROOF_SEAL=" + HttpUtility.UrlEncode(BPTPS) +
				"&TRANSACTION_TYPE="  + HttpUtility.UrlEncode(BPTransaction_Type) +
				"&NAME="              + HttpUtility.UrlEncode(BPName) +
				"&CC_NUM="            + HttpUtility.UrlEncode(BPCCNum) +
				"&CVCCVV2="           + HttpUtility.UrlEncode(BPCVV2) +
				"&CC_EXPIRES="        + HttpUtility.UrlEncode(BPExpire) +
				"&AMOUNT="            + HttpUtility.UrlEncode(BPAmount) +
				"&Order_ID="          + HttpUtility.UrlEncode(BPOrder_ID) +
				"&Addr1="             + HttpUtility.UrlEncode(BPAddr1) +
				"&Addr2="             + HttpUtility.UrlEncode(BPAddr2) +
				"&CITY="              + HttpUtility.UrlEncode(BPCity) +
				"&STATE="             + HttpUtility.UrlEncode(BPState) +
				"&ZIPCODE="           + HttpUtility.UrlEncode(BPZip) +
				"&COMMENT="           + HttpUtility.UrlEncode(BPComment) +
				"&PHONE="             + HttpUtility.UrlEncode(BPPhone) +
				"&EMAIL="             + HttpUtility.UrlEncode(BPEmail) +
				"&REBILLING="         + HttpUtility.UrlEncode(BPRebilling) +
				"&REB_FIRST_DATE="    + HttpUtility.UrlEncode(BPReb_First_Date) +
				"&REB_EXPR="          + HttpUtility.UrlEncode(BPReb_Expr) +
				"&REB_CYCLES="        + HttpUtility.UrlEncode(BPReb_Cycles) +
				"&REB_AMOUNT="        + HttpUtility.UrlEncode(BPReb_Amount) +
				"&RRNO="              + HttpUtility.UrlEncode(BPRRNO) +
				"&AUTOCAP="           + HttpUtility.UrlEncode(BPAutocap) +
				"&AVS_ALLOWED="       + HttpUtility.UrlEncode(BPAVS_Allowed) +
                "&PAYMENT_TYPE="      + HttpUtility.UrlEncode(PAYMENT_TYPE) +
                "&ACH_ACCOUNT_TYPE="  + HttpUtility.UrlEncode(ACH_ACCOUNT_TYPE) +
                "&ACH_ROUTING="       + HttpUtility.UrlEncode(ACH_ROUTING) +
                "&ACH_ACCOUNT="       + HttpUtility.UrlEncode(ACH_ACCOUNT) +
                "&DOC_TYPE="          + HttpUtility.UrlEncode(DOC_TYPE) +
                "&IS_CORPORATE="      + HttpUtility.UrlEncode(IS_CORPORATE) +
                "&COMPANY_NAME="      + HttpUtility.UrlEncode(COMPANY_NAME) +
                "&INVOICE_ID="        + HttpUtility.UrlEncode(BPInvoice_ID) +
                "&AMOUNT_TIP="        + HttpUtility.UrlEncode(BPAmount_Tip) +
                "&AMOUNT_TAX="        + HttpUtility.UrlEncode(BPAmount_Tax) +
                "&AMOUNT_FOOD="       + HttpUtility.UrlEncode(BPAmount_Food) +
                "&AMOUNT_MISC="       + HttpUtility.UrlEncode(BPAmount_Misc);

			byte[] data = encoding.GetBytes(serverquery);
			
			request.Method = "POST";
			request.ContentType="application/x-www-form-urlencoded";
			request.ContentLength = data.Length;

			Stream postdata = request.GetRequestStream();
			postdata.Write(data,0,data.Length);
			postdata.Close();

			//get response
			try
			{
				HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                BPheaderstring = response.GetResponseHeader("Location");
                response.Close();
			}
			catch(WebException e)
			{
				HttpWebResponse response = (HttpWebResponse)e.Response;

				if (response != null)
				{
					return "The following WebException was raised :" + e.Message;
				}
				else
					return "Response Received from server was null";
			}

			string headerstring = Get_Status();
			return headerstring;
			
		}

		public string Get_Status()
		{
			Regex regexresult = new Regex(@"Result=([^&$]*)"); 
			string resultheaderstring = regexresult.Match(BPheaderstring).Result("$1");
			switch (resultheaderstring)
			{
				case "APPROVED":
					return "1";
				case "DECLINED":
					return "0";
				case "ERROR":
					return "E";
				case "MISSING":
					return "M";
				default:
					return "!";
			}
		}
	
		public string Get_RRNO()
		{
			Regex r = new Regex(@"RRNO=([^&$]*)"); 
            Match m = r.Match(BPheaderstring);
            if(m.Success)
                return(m.Value.Substring(5));
            else
                return "";
            //return regexrrno.Match(BPheaderstring).Result("$1");
			//Match matchrrno = regexrrno.Match(BPheaderstring);
			//return matchrrno.Value;
			//string stringrrno = matchrrno.ToString();
			//return stringrrno; //.Substring(5);
		}

		public string Get_Message()
		{
			Regex r = new Regex(@"MESSAGE=([^&$]+)");
            Match m = r.Match(BPheaderstring);
            if(m.Success)
                return(m.Value.Substring(8));
            else
                return "";
            //return regexmessage.Match(BPheaderstring).Result("$1");
            //Regex regexmessage = new Regex(@"MESSAGE=(\w+)");
			//Match matchmessage = regexmessage.Match(BPheaderstring);
			//return matchmessage.Value;
			//string stringmessage = matchmessage.ToString();
			//return stringmessage.Substring(8);
		}

		public string Get_CCV()
		{
			Regex r = new Regex(@"CVV2=([^&$]*)");
            Match m = r.Match(BPheaderstring);
            if(m.Success)
                return(m.Value.Substring(5));
            else
                return "";
                  
            //return regexcvv.Match(BPheaderstring).Result("$1");
			//Match matchcvv = regexcvv.Match(BPheaderstring);
			//return matchcvv.Value;
			//string stringcvv = matchcvv.ToString();
			//return stringcvv.Substring(5);
		}

		public string Get_AVS()
		{
			Regex r = new Regex(@"AVS=([^&$]+)");
            Match m = r.Match(BPheaderstring);
            if(m.Success)
                return(m.Value.Substring(5));
            else        
                return "";
            //return regexavs.Match(BPheaderstring).Result("$1");
			//Match matchavs = regexavs.Match(BPheaderstring);
			//return matchavs.Value;
			//string stringavs = matchavs.ToString();
			//return stringavs.Substring(5);
		}

        public string Get_AuthCode()
        {
            Regex r = new Regex(@"AUTH_CODE=([^&$]+)");
            Match m = r.Match(BPheaderstring);
            if(m.Success)
                return(m.Value.Substring(10));
            else        
                return "";

            //return regexauth.Match(BPheaderstring).Result("$1");
            //Match matchauth = regexauth.Match(BPheaderstring);
            //string stringauth = matchauth.ToString();
            //return stringauth; //.Substring(6);
        }

		public string Get_Missing()
		{
			Regex r = new Regex(@"MISSING=([^&$]+)");
            Match m = r.Match(BPheaderstring);
            if(m.Success)
                return(m.Value.Substring(5));
            else        
                return "";
		}
	}
  
  public class FakeCertificatePolicy : ICertificatePolicy 
  {
    public bool CheckValidationResult (ServicePoint sp, X509Certificate certificate, WebRequest request, int error)
    {
      // whatever the reason we do not stop the SSL connection
      return true;
    }
  }
}
