﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using System.Data;
using EPPSApi = EPPSInterop.EPPSService;
using EPPSInterop.Properties;

namespace EPPSInterop
{
    public class EPPS
    {
        #region Fields
        private EPPSApi.CardHolder oCardHolder = null;
        private EPPSApi.EFTService oEFTService = null;
        private EPPSApi.EFTTransactionDetail oEFTTransactionDetails = null;
        private EPPSApi.Fee fee = null;
        private EPPSApi.DepositTransaction depositTransaction = null;

        /// <summary>   
        /// EPPS User ID
        /// </summary>
        private string userId = string.Empty;

        /// <summary>
        /// EPPS password
        /// </summary>
        private string password = string.Empty;
        #endregion

        #region Constructor
        /// <summary>
        /// Perpose -> Constructor to set userId and password to the variables and created instance of cardholder, EFTservice, EFTTransactionDetail ect
        /// <createdBy>Karan Kushwaha</createdBy>
        /// <createdDate>April/28/2014</createdDate>
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        public EPPS(string userId, string password)
        {
            this.userId = userId;
            this.password = password;
            this.oCardHolder = new EPPSApi.CardHolder();
            this.oEFTService = new EPPSApi.EFTService();
            this.oEFTTransactionDetails = new EPPSApi.EFTTransactionDetail();
            this.fee = new EPPSApi.Fee();
            this.depositTransaction = new EPPSApi.DepositTransaction();
        }
        #endregion

        #region Property
        public EPPSApi.CardHolder CardHolder
        {
            get { return this.oCardHolder; }
            set { this.oCardHolder = value; }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Perpose -> Method to add fee on EPPS
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>April/28/2014</createdDate>
        /// </summary>
        /// <param name="fee"></param>
        /// <returns></returns>
        public EPPSApi.FeeResult FeeAdd(EPPSApi.Fee fee)
        {
            EPPSApi.FeeResult feeResult = new EPPSApi.FeeResult();
            try
            {
                feeResult = oEFTService.AddFee(userId, password, fee.CardHolderID, fee.Fee_Date, fee.FeeAmount, fee.Description, fee.FeeType, fee.PaidToName,
                                   fee.PaidToPhone, fee.PaidToStreet, fee.PaidToStreet2, fee.PaidToCity, fee.PaidToState, fee.PaidToZip, "", "");
                return feeResult;
            }
            catch (SoapException Exc)
            {

                throw Exc;
            }

        }

        /// <summary>
        /// Perpose -> Method to update fee on EPPS
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>April/28/2014</createdDate>
        /// </summary>
        /// <param name="fee"></param>
        /// <returns></returns>
        public EPPSApi.FeeResult FeeUpdate(EPPSApi.Fee fee)
        {
            EPPSApi.FeeResult feeResult = new EPPSApi.FeeResult();
            try
            {
                feeResult = oEFTService.UpdateFee(userId, password, fee.FeeID, fee.Fee_Date, fee.FeeAmount, fee.Description, fee.FeeType, fee.PaidToName,
                                      fee.PaidToPhone, fee.PaidToStreet, fee.PaidToStreet2, fee.PaidToCity, fee.PaidToState, fee.PaidToZip, "", "");

                return feeResult;
            }
            catch (SoapException Exc)
            {

                throw Exc;
            }

        }

        /// <summary>
        /// Perpose -> Method to void fee on EPPS
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>April/28/2014</createdDate>
        /// </summary>
        public void FeeVoid()
        {
            try
            {
                oEFTService.VoidFee(userId, password, fee.FeeID);
            }
            catch (SoapException Exc)
            {

                throw Exc;
            }
        }

        /// <summary>
        /// Perpose -> Method to void fee on EPPS
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>April/28/2014</createdDate>
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>EPPSApi.FindFee</returns>
        public EPPSApi.FindFee RetrieveFeeByDate(DateTime startDate, DateTime endDate)
        {
            EPPSApi.FindFee feeResult = new EPPSApi.FindFee();
            try
            {
                feeResult = oEFTService.FindFeeByDate(userId, password, startDate, endDate);
                return feeResult;
            }
            catch (SoapException Exc)
            {

                throw Exc;
            }
        }

        /// <summary>
        /// Perpose -> Method to find fee by cardholder id in EPPS
        /// <createdBy>Karan Kushwaha</createdBy>
        /// <createdDate>April/28/2014</createdDate>
        /// </summary>
        public void RetrieveFeeByCardHolderID()
        {
            try
            {
                oEFTService.FindFeeByCardHolderID(userId, password, fee.CardHolderID);
            }
            catch (SoapException Exc)
            {

                throw Exc;
            }
        }

        /// <summary>
        /// Perpose -> Method to create card holder on EPPS gateway
        /// <createdBy>Karan Kushwaha</createdBy>
        /// <createdDate>April/28/2014</createdDate>
        /// </summary>
        /// <param name="oCardHolderDetails"></param>
        /// <returns></returns>
        public EPPSApi.CardHolder CreateCardHolder(EPPSApi.CardHolderDetail oCardHolderDetails)
        {
            EPPSApi.CardHolder CardHolder = new EPPSApi.CardHolder();
            try
            {
                CardHolder = oEFTService.AddCardHolder(userId, password, oCardHolderDetails.CardHolderID, oCardHolderDetails.FirstName, oCardHolderDetails.LastName,
                                     oCardHolderDetails.DateOfBirth, oCardHolderDetails.SSN, oCardHolderDetails.Address, oCardHolderDetails.City,
                                     oCardHolderDetails.State, oCardHolderDetails.Zip, oCardHolderDetails.Phone, oCardHolderDetails.Email);
            }
            catch (SoapException Exc)
            {
                throw Exc;
            }
            return CardHolder;
        }

        /// <summary>
        /// Perpose -> Method to update card holder on EPPS gateway
        /// <createdBy>Karan Kushwaha</createdBy>
        /// <createdDate>April/28/2014</createdDate>ry>
        /// <param name="oCardHolderDetails"></param>
        /// <returns>EPPSApi.CardHolder</returns>
        public EPPSApi.CardHolder UpdateCardHolder(EPPSApi.CardHolderDetail oCardHolderDetails)
        {
            EPPSApi.CardHolder CardHolder = new EPPSApi.CardHolder();
            try
            {
                CardHolder = oEFTService.UpdateCardHolder(userId, password, oCardHolderDetails.CardHolderID, oCardHolderDetails.Address, oCardHolderDetails.City,
                                         oCardHolderDetails.State, oCardHolderDetails.Zip, oCardHolderDetails.Phone, oCardHolderDetails.Email);
            }
            catch (SoapException Exc)
            {
                throw Exc;
            }
            return CardHolder;
        }

        /// <summary>
        /// Perpose -> Method to retrieve card holder by id
        /// <createdBy>Karan Kushwaha</createdBy>
        /// <createdDate>April/30/2014</createdDate>y>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public void RetrieveCardHolderByDate(DateTime startDate, DateTime endDate)
        {
            try
            {
                oEFTService.FindCardHolderByDate(userId, password, startDate, endDate);
            }
            catch (SoapException Exc)
            {
                throw Exc;
            }
        }

        /// <summary>
        /// Perpose -> Method to create EFT
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>April/30/2014</createdDate>
        /// </summary>
        /// <param name="oEFTTransactionDetails"></param>
        /// <param name="programFee"></param>
        /// <returns>EPPSApi.EFTTransaction</returns>
        public EPPSApi.EFTTransaction CreateEFT(EPPSApi.EFTChangeLogDetail oEFTTransactionDetails, decimal programFee)
        {
            EPPSApi.EFTTransaction EFT = new EPPSApi.EFTTransaction();
            try
            {
                EPPSApi.EFTTransaction eftTransaction = new EPPSApi.EFTTransaction();
                EFT = oEFTService.AddEft(userId, password, oEFTTransactionDetails.CardHolderId, oEFTTransactionDetails.EftDate, oEFTTransactionDetails.EftAmount, programFee,
                                   oEFTTransactionDetails.BankName, oEFTTransactionDetails.BankCity, oEFTTransactionDetails.BankState,
                                   oEFTTransactionDetails.AccountNumber, oEFTTransactionDetails.RoutingNumber, oEFTTransactionDetails.AccountType, oEFTTransactionDetails.Memo);
            }
            catch (SoapException Exc)
            {
                throw Exc;
            }

            return EFT;
        }

        /// <summary>
        /// Perpose -> Method to update EFT
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>April/30/2014</createdDate>
        /// </summary>
        /// <param name="oEFTTransactionDetails"></param>
        public void EftUpdate(EPPSApi.EFTChangeLogDetail oEFTTransactionDetails)
        {
            try
            {
                oEFTService.UpdateEft(userId, password, Convert.ToInt32(oEFTTransactionDetails.EftTransactionID), oEFTTransactionDetails.EftDate, oEFTTransactionDetails.EftAmount, 0,
                                      oEFTTransactionDetails.BankName, oEFTTransactionDetails.BankCity, oEFTTransactionDetails.BankState,
                                      oEFTTransactionDetails.AccountNumber, oEFTTransactionDetails.RoutingNumber, oEFTTransactionDetails.AccountType, oEFTTransactionDetails.Memo);

            }
            catch (SoapException Exc)
            {
                throw Exc;
            }
        }

        /// <summary>
        /// Perpose -> Method to create void EFT
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>April/30/2014</createdDate>
        /// </summary>
        public void EftVoid()
        {
            try
            {
                oEFTService.VoidEft(userId, password, Convert.ToInt32(oEFTTransactionDetails.EftTransactionID));
            }
            catch (SoapException Exc)
            {
                throw Exc;
            }
        }

        /// <summary>
        /// Perpose -> Method to retrieve card holder from EPPS by id
        /// <createdBy>Karan Kushwaha</createdBy>
        /// <createdDate>April/30/2014</createdDate>
        /// </summary>
        /// <param name="oCardHolderDetails"></param>
        /// <returns>EPPSApi.FindCardHolder</returns>
        public EPPSApi.FindCardHolder RetrieveCardHolderById(EPPSApi.CardHolderDetail oCardHolderDetails)
        {
            EPPSApi.FindCardHolder CardHolder = null;
            try
            {
                CardHolder = oEFTService.FindCardHolderByID(userId, password, oCardHolderDetails.CardHolderID);
            }
            catch (SoapException Exc)
            {
                throw Exc;
            }
            return CardHolder;
        }

        /// <summary>
        /// Perpose -> Method to retrieve EFT from EPPS by id
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>April/30/2014</createdDate>
        /// </summary>
        /// <param name="cardHolderId"></param>
        /// <returns>EPPSApi.FindEFT</returns>
        public EPPSApi.FindEFT RetrieveEFTById(string cardHolderId)
        {
            EPPSApi.FindEFT EFTDetails = null;
            try
            {
                EFTDetails = oEFTService.FindEftByID(userId, password, cardHolderId);
            }
            catch (SoapException Exc)
            {
                throw Exc;
            }
            return EFTDetails;
        }

        /// <summary>
        /// Perpose -> Method to retrieve EFT from EPPS by create date
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>April/29/2014</createdDate>
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>EPPSApi.FindEFT</returns>
        public EPPSApi.FindEFT RetrieveEFTByCreateDate(DateTime startDate, DateTime endDate)
        {
            EPPSApi.FindEFT EFTDetails = new EPPSApi.FindEFT();
            try
            {
                EFTDetails = oEFTService.FindEftByCreateDate(userId, password, startDate, endDate);
            }
            catch (SoapException Exc)
            {
                throw Exc;
            }
            return EFTDetails;
        }

        /// <summary>
        /// Perpose -> Method to retrieve EFT by status date from EPPS
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>April/29/2014</createdDate>
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public void RetrieveEFTByStatusDate(DateTime startDate, DateTime endDate)
        {
            try
            {
                oEFTService.FindEftByStatusDate(userId, password, startDate, endDate);
            }
            catch (SoapException Exc)
            {
                throw Exc;
            }
        }

        /// <summary>
        /// Perpose -> Method to find EFT change by id on EPPS 
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>April/29/2014</createdDate>
        /// </summary>
        /// <param name="TransactionID"></param>
        /// <returns>EPPSApi.FindEftChange</returns>
        public EPPSApi.FindEftChange RetrieveFindEftChangebyID(string TransactionID)
        {
            EPPSApi.FindEftChange EFTChange = new EPPSApi.FindEftChange();
            try
            {
                EFTChange = oEFTService.FindEftChangeByID(userId, password, TransactionID);
            }
            catch (SoapException ex)
            {
                throw ex;
            }
            return EFTChange;
        }

        /// <summary>
        /// Perpose -> Method to add credit card debits on EPPS payment gateway
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>May/29/2014</createdDate>
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="ExpirationMonth"></param>
        /// <param name="ExpirationYear"></param>
        /// <param name="CVCode"></param>
        /// <returns>EPPSApi.CreditCardDebit</returns>
        public EPPSApi.CreditCardDebit AddCreditCardDebits(EPPSApi.CreditCardDebitDetail obj, decimal DebitAmt, int ExpirationMonth, int ExpirationYear, string CVCode)
        {
            EPPSApi.CreditCardDebit RetVal = new EPPSApi.CreditCardDebit();
            try
            {
                // Fee Amt = Payment Fee amt
                RetVal = oEFTService.AddCreditCardDebit(userId, password, obj.CardHolderId, DebitAmt, obj.ChargeAmount, long.Parse(obj.CreditCardNumber),
                                                        ExpirationMonth, ExpirationYear, CVCode, obj.FirstName,
                                                        obj.LastName, obj.Address, obj.City, obj.State, obj.Zip);
            }
            catch (SoapException ex)
            {
                throw ex;
            }
            return RetVal;
        }

        /// <summary>
        /// Perpose -> Method to find credit card debits from EPPS payment gateway by id
        /// <createdBy>Raghuraj Singh</createdBy>
        /// <createdDate>May/29/2014</createdDate>
        /// </summary>
        /// <param name="CardHolderID"></param>
        /// <returns>EPPSApi.CreditCardDebitSearchResult</returns>
        public EPPSApi.CreditCardDebitSearchResult FindCreditCardDebits(string CardHolderID)
        {
            EPPSApi.CreditCardDebitSearchResult RetVal = new EPPSApi.CreditCardDebitSearchResult();
            try
            {
                RetVal = oEFTService.FindCreditCardDebitByID(userId, password, CardHolderID);
            }
            catch (SoapException ex)
            {
                throw ex;
            }
            return RetVal;
        }
        #endregion
    }
}