﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using SelasDataClass;
using RAMSInterop;
using EPPSApi = EPPSInterop.EPPSService;

namespace SFDebt
{
    public class SFDebtClass
    {
        #region Fields

        /// <summary>
        /// An instance of the SFInterop class
        /// </summary>
        private SFInterop.SFInterop _salesForce = null;

        #endregion 

        #region Constructors

        /// <summary>
        /// Constructor for the SFDebtClass
        /// </summary>
        /// <param name="uName">SalesForce username</param>
        /// <param name="pwd">The SalesForce password</param>
        /// <param name="token">The SalesForce security token</param>
        public SFDebtClass(string uName, string pwd, string token, Boolean liveEnv)
        {
            this._salesForce = new SFInterop.SFInterop(uName, pwd, token, liveEnv);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets all transactions scheduled for processing within a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of transactions as a strongly typed dataset</returns>
        public TransactionDS GetTransactions(DateTime start, DateTime end, bool getWithdrawals, string processor)
        {
            try
            {
                TransactionDS tds = new TransactionDS();
                this.GetDeposits(start, end, tds, processor);
                return tds;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Gets all transactions scheduled for processing within a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of transactions as a strongly typed dataset</returns>
        public TransactionDS GetT2PTransactions(DateTime start, DateTime end, bool flag)
        {
            try
            {
                TransactionDS tds = new TransactionDS();
                this.GetT2PDeposits(start, end, tds, flag);
                return tds;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        public TransactionDS GetDeposits(DateTime start, DateTime end, TransactionDS tranDS, string processor)
        {
            /*TransactionDS.ClientDepositDataTable tran = tranDS.ClientDeposit;
            if (tranDS.ClientDeposit == null)
            {
                tranDS.ClientDeposit. = 
            }*/

            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT     Id, nudebt__Payment_Amount__c, nudebt__Amount_Paid__c,
                                            nudebt__Payment_Scheduled_Date__c, nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                            nudebt__Client__c, nudebt__Client_Name__c,               
                                            nudebt__Client__r.nudebt__Bank_Name__c, 
                                            nudebt__Client__r.nudebt__Routing_Number__c, 
                                            nudebt__Client__r.nudebt__Account_Type__c, 
                                            nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client__r.nudebt__Account_Number__c, 
                                            nudebt__Client__r.nudebt__Processor__r.Name,
                                            nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c,
                                            nudebt__Client__r.nudebt__Processor_Acct__c, 
                                            nudebt__Client__r.nudebt__Import_ID__c,
                                            nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c
                                FROM        nudebt__Client_Payment__c  
                                WHERE       nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} AND nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd}
                                AND         (nudebt__Payment_Status__c = 'Pending' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored') 
                                
                                AND         nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                AND         nudebt__Client__r.nudebt__Processor__r.Name='" + processor + "'";

                string query2 = @"SELECT    Id, Name, nudebt__Fee_ID__c, nudebt__Transaction_ID__c,
                                            nudebt__Amount__c, nudebt__Amount_Paid__c, 
                                            nudebt__Status__c, nudebt__Client_Payment__c,
                                            nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__c, 
                                            nudebt__Client_Payment__r.nudebt__Client_Name__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c, nudebt__Client_Payment__r.Id, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c
                                FROM        nudebt__Client_Payment_Fee__c 
                                WHERE       (nudebt__Status__c = 'Pending' OR nudebt__Status__c = 'TimedOut' OR nudebt__Status__c = 'Errored')
                                AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} 
                                AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd}                                                       
                                AND         nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                AND         nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name='" + processor + "'";


                if (start.Date > end.Date)
                {
                    query = string.Format(query, end.Date, start.Date);
                    query2 = string.Format(query2, end.Date, start.Date);
                }
                else
                {
                    query = string.Format(query, start.Date, end.Date);
                    query2 = string.Format(query2, start.Date, end.Date);
                }

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query2);
                TransactionDS.ClientDepositFeeDataTable tmpFeeTable = new TransactionDS.ClientDepositFeeDataTable();
                //qr2 = _salesForce.RunQuery(query2);
                bool isDone = false;

                do
                {
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;
                        TransactionDS.ClientDepositRow row = tranDS.ClientDeposit.NewClientDepositRow();

                        try
                        {
                            row.RecordID = draft.Id;
                            row.ClientID = draft.nudebt__Client__c;
                            row.ClientName = draft.nudebt__Client_Name__c;
                            row.BankAccountType = draft.nudebt__Client__r.nudebt__Account_Type__c;
                            row.BankAccountNumber
                                = String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number__c) ? "" : draft.nudebt__Client__r.nudebt__Account_Number__c)
                                : draft.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            row.BankRoutingNumber = draft.nudebt__Client__r.nudebt__Routing_Number__c;
                            row.BankName = draft.nudebt__Client__r.nudebt__Bank_Name__c;
                            row.Amount = draft.nudebt__Payment_Amount__c.Value;
                            if (draft.nudebt__Amount_Paid__c != null)
                                row.AmountPaid = draft.nudebt__Amount_Paid__c.Value;
                            row.Date = Convert.ToDateTime(draft.nudebt__Payment_Scheduled_Date__c);
                            row.ProcessorAccount = draft.nudebt__Client__r.nudebt__Processor_Acct__c;
                            row.TransactionID = draft.nudebt__Transaction_ID__c == null ? "" : draft.nudebt__Transaction_ID__c;
                            row.ParentDRCID = "";
                            row.Type = "A";
                            row.TypeDesc = "ACH Monthly Draft";

                            row.Status = "Pending";
                            row.ClientImportID = draft.nudebt__Client__r.nudebt__Import_ID__c;
                            row.ProcessorFee = draft.nudebt__Processing_Fee__c == null ? 0 : (double)draft.nudebt__Processing_Fee__c;
                            row.Processor = draft.nudebt__Client__r.nudebt__Processor__r.Name;
                            row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(draft.nudebt__Sync_Date__c);
                        }
                        catch
                        {
                            row.HasSyncError = "True";
                            row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            if (row.Amount > 0)
                                tranDS.ClientDeposit.Rows.Add(row);
                        }
                    }
                    for (int j = 0; j < qr2.size; j++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment_Fee__c fee = qr2.records[j] as SFInterop.SForce.nudebt__Client_Payment_Fee__c;
                        TransactionDS.ClientDepositRow rowf = tranDS.ClientDeposit.NewClientDepositRow();
                        try
                        {
                            rowf.RecordID = fee.Id;
                            rowf.ClientID = fee.nudebt__Client_Payment__r.nudebt__Client__c;
                            rowf.ClientName = fee.nudebt__Client_Payment__r.nudebt__Client_Name__c;
                            rowf.BankAccountType = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c;
                            rowf.BankAccountNumber
                                = String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c) ? "" : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c)
                                : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            rowf.BankRoutingNumber = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c;
                            rowf.BankName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c;
                            rowf.Amount = fee.nudebt__Amount__c.Value;
                            if (fee.nudebt__Amount_Paid__c != null)
                                rowf.AmountPaid = fee.nudebt__Amount_Paid__c.Value;
                            if (fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c != null)
                                rowf.Date = Convert.ToDateTime(fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c);
                            rowf.ProcessorAccount = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c;
                            rowf.TransactionID = fee.nudebt__Transaction_ID__c;
                            rowf.ParentDRCID = fee.nudebt__Client_Payment__c;
                            rowf.Type = fee.nudebt__Fee_ID__c == null ? "" : fee.nudebt__Fee_ID__c; 
                            rowf.TypeDesc = fee.Name;
                            //rowf.RecordName
                            rowf.Status = fee.nudebt__Status__c;
                            rowf.ClientImportID = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c;
                            /* TODO bt - use name its
                            if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c))
                                rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c;
                            else
                                if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name))
                            */
                            rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name;
                        }
                        catch
                        {
                            rowf.HasSyncError = "True";
                            rowf.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            if (rowf.Amount > 0)
                                tranDS.ClientDeposit.Rows.Add(rowf);
                        }
                    }

                    if (!qr.done)
                    {
                        qr = _salesForce.GetNextBatch(qr);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return tranDS;
        }

        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        public TransactionDS GetT2PDeposits(DateTime start, DateTime end, TransactionDS tranDS, bool flag)
        {
            if (_salesForce.IsValidSession)
            {

                string query = @"SELECT     Id, nudebt__Payment_Amount__c, nudebt__Amount_Paid__c,nudebt__Client_Name__c,
                                            nudebt__Payment_Scheduled_Date__c, nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                            nudebt__Client__c,            
                                            nudebt__Client__r.nudebt__Bank_Name__c, 
                                            nudebt__Client__r.nudebt__Routing_Number__c, 
                                            nudebt__Client__r.nudebt__Account_Type__c, 
                                            nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client__r.nudebt__Account_Number__c, 
                                            nudebt__Client__r.nudebt__Processor__r.Name,
                                            nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c,
                                            nudebt__Client__r.nudebt__Processor_Acct__c, 
                                            nudebt__Client__r.nudebt__Import_ID__c,nudebt__Client__r.Name,
                                            nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c,
                                             nudebt__Client__r.nudebt__CVV_Code__c, nudebt__Client__r.nudebt__Debit_Card_Number__c,
                                            nudebt__Client__r.nudebt__Debit_Card_Expires__c, nudebt__Client__r.nudebt__Sync_Description__c
                                FROM        nudebt__Client_Payment__c  
                                WHERE       nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} AND nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd}
                                AND         (nudebt__Payment_Status__c = 'Pending' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored')
                                AND         nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                AND         (nudebt__Client__r.nudebt__Processor__r.Name='T2P-Hybrid' OR nudebt__Client__r.nudebt__Processor__r.Name='T2P-ProtectionProgram')";

                string query2 = @"SELECT    Id, Name, nudebt__Fee_ID__c, nudebt__Transaction_ID__c,
                                            nudebt__Fee_Type__c,
                                            nudebt__Amount__c, nudebt__Amount_Paid__c, 
                                            nudebt__Status__c, nudebt__Client_Payment__c,
                                            nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__c, nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Sync_Description__c,
                                            nudebt__Client_Payment__r.nudebt__Client_Name__c, nudebt__Client_Payment__r.nudebt__Client__r.Name,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c, nudebt__Client_Payment__r.Id, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c
                                FROM        nudebt__Client_Payment_Fee__c 
                                WHERE       (nudebt__Status__c = 'Pending' OR nudebt__Status__c = 'TimedOut' OR nudebt__Status__c = 'Errored')
                                AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} 
                                AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd}                                                       
                                AND         nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                AND         (nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name='T2P-Hybrid' OR nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name='T2P-ProtectionProgram')";


                if (start.Date > end.Date)
                {
                    query = string.Format(query, end.Date, start.Date);
                    query2 = string.Format(query2, end.Date, start.Date);
                }
                else
                {
                    query = string.Format(query, start.Date, end.Date);
                    query2 = string.Format(query2, start.Date, end.Date);
                }

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query2);
                // TransactionDS.ClientDepositFeeDataTable tmpFeeTable = new TransactionDS.ClientDepositFeeDataTable();

                bool isDone = false;

                do
                {
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;

                        if ((draft.nudebt__Client__r.nudebt__CVV_Code__c == null || draft.nudebt__Client__r.nudebt__Debit_Card_Number__c == null || draft.nudebt__Client__r.nudebt__Debit_Card_Expires__c == null) || flag == true)
                        {
                            TransactionDS.ClientDepositRow row = tranDS.ClientDeposit.NewClientDepositRow();
                            try
                            {
                                row.RecordID = draft.Id;
                                row.ClientFullName = draft.nudebt__Client_Name__c;
                                row.ClientID = draft.nudebt__Client__c;
                                row.ClientName = draft.nudebt__Client__r.Name;
                                row.BankAccountType = draft.nudebt__Client__r.nudebt__Account_Type__c;
                                row.BankAccountNumber
                                    = String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                    ? (String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number__c) ? "" : draft.nudebt__Client__r.nudebt__Account_Number__c)
                                    : draft.nudebt__Client__r.nudebt__Account_Number_Enc__c;
                                row.ClientNote = draft.nudebt__Client__r.nudebt__Sync_Description__c;
                                row.BankRoutingNumber = draft.nudebt__Client__r.nudebt__Routing_Number__c;
                                row.BankName = draft.nudebt__Client__r.nudebt__Bank_Name__c;
                                row.Amount = draft.nudebt__Payment_Amount__c.Value;
                                if (draft.nudebt__Amount_Paid__c != null)
                                    row.AmountPaid = draft.nudebt__Amount_Paid__c.Value;
                                row.Date = Convert.ToDateTime(draft.nudebt__Payment_Scheduled_Date__c);
                                row.ProcessorAccount = draft.nudebt__Client__r.nudebt__Processor_Acct__c;
                                row.TransactionID = draft.nudebt__Transaction_ID__c == null ? "" : draft.nudebt__Transaction_ID__c;
                                row.ParentDRCID = "";
                                row.Type = "A";
                                row.TransactionType = "Payment";
                                row.TypeDesc = "ACH Monthly Draft";
                                row.RowNumber = tranDS.ClientDeposit.Count + 1;
                                row.Status = "Pending";
                                row.ClientImportID = draft.nudebt__Client__r.nudebt__Import_ID__c;
                                row.ProcessorFee = draft.nudebt__Processing_Fee__c == null ? 0 : (double)draft.nudebt__Processing_Fee__c;
                                row.Processor = draft.nudebt__Client__r.nudebt__Processor__r.Name;
                                row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(draft.nudebt__Sync_Date__c);
                            }
                            catch
                            {
                                row.HasSyncError = "True";
                                row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                            }
                            finally
                            {
                                if (row.Amount > 0)
                                    tranDS.ClientDeposit.Rows.Add(row);
                            }
                        }
                        
                    }
                    for (int j = 0; j < qr2.size; j++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment_Fee__c fee = qr2.records[j] as SFInterop.SForce.nudebt__Client_Payment_Fee__c;
                        TransactionDS.ClientDepositRow rowf = tranDS.ClientDeposit.NewClientDepositRow();
                        try
                        {
                            rowf.RecordID = fee.Id;
                            rowf.ClientFullName = fee.nudebt__Client_Payment__r.nudebt__Client_Name__c;
                            rowf.ClientID = fee.nudebt__Client_Payment__r.nudebt__Client__c;
                            rowf.ClientName = fee.nudebt__Client_Payment__r.nudebt__Client__r.Name;
                            rowf.BankAccountType = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c;
                            rowf.BankAccountNumber
                                = String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c) ? "" : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c)
                                : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c;
                            rowf.ClientNote = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Sync_Description__c;
                            rowf.BankRoutingNumber = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c;
                            rowf.BankName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c;
                            rowf.Amount = fee.nudebt__Amount__c.Value;
                            if (fee.nudebt__Amount_Paid__c != null)
                                rowf.AmountPaid = fee.nudebt__Amount_Paid__c.Value;
                            if (fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c != null)
                                rowf.Date = Convert.ToDateTime(fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c);
                            rowf.ProcessorAccount = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c;
                            //rowf.TransactionID = fee.nudebt__Transaction_ID__c;
                            rowf.TransactionID = fee.nudebt__Transaction_ID__c == null ? "" : fee.nudebt__Transaction_ID__c; 
                            rowf.ParentDRCID = fee.nudebt__Client_Payment__c;
                            //rowf.Type = fee.nudebt__Fee_ID__c;
                            rowf.Type = fee.nudebt__Fee_ID__c == null ? "" : fee.nudebt__Fee_ID__c; 
                            //rowf.Type = "T";
                            rowf.TransactionType = "Fee";
                            rowf.RowNumber = tranDS.ClientDeposit.Count + 1;
                            rowf.TypeDesc = fee.Name;
                            //rowf.RecordName
                            rowf.Status = fee.nudebt__Status__c;
                            rowf.ClientImportID = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c;
                            /* TODO bt - use name its
                            if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c))
                                rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c;
                            else
                                if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name))
                            */
                            rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name;
                        }
                        catch
                        {
                            rowf.HasSyncError = "True";
                            rowf.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            //if (rowf.Amount > 0)
                            tranDS.ClientDeposit.Rows.Add(rowf);
                        }
                    }
                    if (!qr2.done)
                    {
                        qr2 = _salesForce.GetNextBatch(qr2);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return tranDS;
        }

        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        public TransactionDS GetAutherizeDeposits(TransactionDS tranDS, DateTime starDate, DateTime endDate)
        {
            if (_salesForce.IsValidSession)
            {
                DateTime currentDate = DateTime.Now;
                if (endDate>currentDate)
                {
                    endDate = currentDate;
                }
                    string query = @"SELECT     Id, nudebt__Payment_Amount__c, nudebt__Amount_Paid__c,
            
                                                        nudebt__Client__r.nudebt__First_Name__c,
                                                        nudebt__Client__r.nudebt__Last_Name__c,
                                                        nudebt__Client__r.nudebt__Address_Line_1__c,
                                                        nudebt__Client__r.nudebt__State__c,
                                                        nudebt__Client__r.nudebt__City__c,nudebt__Client__r.Name,
                                                        nudebt__Client__r.nudebt__Postal_Code__c,
                                                        nudebt__Client__r.nudebt__CVV_Code__c,
                                                        nudebt__Client__r.nudebt__Debit_Card_Number__c,
                                                        nudebt__Client__r.nudebt__Debit_Card_Expires__c,
                                                        nudebt__Payment_Scheduled_Date__c, nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                                        nudebt__Client__c, nudebt__Client_Name__c,               
                                                        nudebt__Client__r.nudebt__Bank_Name__c, 
                                                        nudebt__Client__r.nudebt__Routing_Number__c, nudebt__Client__r.nudebt__Bank_City__c, 
                                                        nudebt__Client__r.nudebt__Bank_State__c, nudebt__Client__r.nudebt__Account_Type__c, 
                                                        nudebt__Client__r.nudebt__Account_Number_Enc__c,nudebt__Client__r.nudebt__Account_Number__c, 
                                                        nudebt__Client__r.nudebt__Processor__r.Name,nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c,
                                                        nudebt__Client__r.nudebt__Processor_Acct__c, nudebt__Client__r.nudebt__Import_ID__c,
                                                        nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c,
                                                        nudebt__Client__r.nudebt__Billing_Add1__c,                                                        
                                                        nudebt__Client__r.nudebt__Billing_City__c,
                                                        nudebt__Client__r.nudebt__Billing_ST__c,
                                                        nudebt__Client__r.nudebt__Billing_Postal__c
                                            FROM        nudebt__Client_Payment__c
                                            WHERE       (nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} AND nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd})
                                            AND         (nudebt__Payment_Status__c = 'Pending' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored')                                              
                                            AND         nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                            AND         (nudebt__Client__r.nudebt__Processor__r.Name='T2P-Hybrid' OR nudebt__Client__r.nudebt__Processor__r.Name='T2P-ProtectionProgram')";

                    //{0:yyyy-MM-dd} 
                    if (starDate.Date > endDate.Date)
                    {
                        query = string.Format(query, endDate.Date, starDate.Date);
                    }
                    else
                    {
                        query = string.Format(query, starDate.Date, endDate.Date);
                    }

                    SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                    TransactionDS.ClientDepositFeeDataTable tmpFeeTable = new TransactionDS.ClientDepositFeeDataTable();
                    bool isDone = false;

                    do
                    {
                        for (int i = 0; i < qr.size; i++)
                        {
                            SFInterop.SForce.nudebt__Client_Payment__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;

                            TransactionDS.ClientDepositRow row = tranDS.ClientDeposit.NewClientDepositRow();
                            if (draft.nudebt__Client__r.nudebt__CVV_Code__c != null && draft.nudebt__Client__r.nudebt__Debit_Card_Number__c != null && draft.nudebt__Client__r.nudebt__Debit_Card_Expires__c!=null)
                            {
                                try
                                {
                                    row.RecordID = draft.Id;
                                    row.ClientFullName = draft.nudebt__Client_Name__c;
                                    row.ClientID = draft.nudebt__Client__c;
                                    row.ClientName = draft.nudebt__Client__r.Name;
                                    row.BankAccountType = draft.nudebt__Client__r.nudebt__Account_Type__c;
                                    row.BankAccountNumber
                                        = String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                        ? (String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number__c) ? "" : draft.nudebt__Client__r.nudebt__Account_Number__c)
                                        : draft.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                                    row.BankCity = draft.nudebt__Client__r.nudebt__Bank_City__c;
                                    row.BankState = draft.nudebt__Client__r.nudebt__Bank_State__c;
                                    row.TransactionType = "Payment";
                                    row.BankRoutingNumber = draft.nudebt__Client__r.nudebt__Routing_Number__c;
                                    row.BankName = draft.nudebt__Client__r.nudebt__Bank_Name__c;
                                    row.Amount = draft.nudebt__Payment_Amount__c.Value;
                                    if (draft.nudebt__Amount_Paid__c != null)
                                        row.AmountPaid = draft.nudebt__Amount_Paid__c.Value;
                                    row.Date = Convert.ToDateTime(draft.nudebt__Payment_Scheduled_Date__c);
                                    row.ProcessorAccount = draft.nudebt__Client__r.nudebt__Processor_Acct__c;
                                    row.TransactionID = draft.nudebt__Transaction_ID__c == null ? "" : draft.nudebt__Transaction_ID__c;
                                    row.ParentDRCID = "";
                                    row.Type = "A";
                                    row.TypeDesc = "ACH Monthly Draft";

                                                                    row.Status = "Pending";
                                    row.ClientImportID = draft.nudebt__Client__r.nudebt__Import_ID__c;
                                    row.ClientFName = draft.nudebt__Client__r.nudebt__First_Name__c;
                                    row.ClientLName = draft.nudebt__Client__r.nudebt__Last_Name__c;
                                    // TODO bt - Need to pickup the Billing address
                                    row.CardAddress = draft.nudebt__Client__r.nudebt__Billing_Add1__c;
                                    row.CardCity = draft.nudebt__Client__r.nudebt__Billing_City__c;
                                    row.CardState = draft.nudebt__Client__r.nudebt__Billing_ST__c;
                                    row.CardPostal = draft.nudebt__Client__r.nudebt__Billing_Postal__c;
                                    row.CardNumber = draft.nudebt__Client__r.nudebt__Debit_Card_Number__c;
                                    row.CardPin = draft.nudebt__Client__r.nudebt__CVV_Code__c;
                                    row.CardExpDate = Convert.ToString(draft.nudebt__Client__r.nudebt__Debit_Card_Expires__c);
                                   // row.RowNumber = tranDS.ClientDeposit.Count + 1;
                                    row.ProcessorFee = draft.nudebt__Processing_Fee__c == null ? 0 : (double)draft.nudebt__Processing_Fee__c;
                                    row.Processor = draft.nudebt__Client__r.nudebt__Processor__r.Name;
                                    row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(draft.nudebt__Sync_Date__c);
                                }
                                catch
                                {
                                    row.HasSyncError = "True";
                                    row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                                }
                                finally
                                {
                                    if (row.Amount > 0)
                                        tranDS.ClientDeposit.Rows.Add(row);
                                }
                            }
                        }
                        if (!qr.done)
                        {
                            qr = _salesForce.GetNextBatch(qr);
                        }
                        else
                        {
                            isDone = true;
                        }
                    }
                    while (!isDone);
                }
                else
                {

                }           
            return tranDS;

        }
        
        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        public TransactionDS GetAutherizeDotNetDeposits(TransactionDS tranDS, string processor, DateTime starDate, DateTime endDate)
        {
            if (_salesForce.IsValidSession)
            {
                //DateTime currentDate = DateTime.Now;
                //if (endDate > currentDate)
                //{
                //    endDate = currentDate;
                //}
                string query = @"SELECT     Id, nudebt__Payment_Amount__c, nudebt__Amount_Paid__c,
                                                        nudebt__Client__r.Name,
                                                        nudebt__Client__r.nudebt__First_Name__c,
                                                        nudebt__Client__r.nudebt__Last_Name__c,
                                                        nudebt__Client__r.nudebt__Address_Line_1__c,
                                                        nudebt__Client__r.nudebt__State__c,
                                                        nudebt__Client__r.nudebt__City__c,
                                                        nudebt__Client__r.nudebt__Postal_Code__c,
                                                        nudebt__Client__r.nudebt__CVV_Code__c,
                                                        nudebt__Client__r.nudebt__Debit_Card_Number__c,
                                                        nudebt__Client__r.nudebt__Debit_Card_Expires__c,
                                                        nudebt__Payment_Scheduled_Date__c, nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                                        nudebt__Client__c, nudebt__Client_Name__c,               
                                                        nudebt__Client__r.nudebt__Bank_Name__c, 
                                                        nudebt__Client__r.nudebt__Routing_Number__c, nudebt__Client__r.nudebt__Bank_City__c, 
                                                        nudebt__Client__r.nudebt__Bank_State__c, nudebt__Client__r.nudebt__Account_Type__c, 
                                                        nudebt__Client__r.nudebt__Account_Number_Enc__c,nudebt__Client__r.nudebt__Account_Number__c, 
                                                        nudebt__Client__r.nudebt__Processor__r.Name,nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c,
                                                        nudebt__Client__r.nudebt__Processor_Acct__c, nudebt__Client__r.nudebt__Import_ID__c,
                                                        nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c,
                                                        nudebt__Client__r.nudebt__Billing_Add1__c,                                                        
                                                        nudebt__Client__r.nudebt__Billing_City__c,
                                                        nudebt__Client__r.nudebt__Billing_ST__c,
                                                        nudebt__Client__r.nudebt__Billing_Postal__c
                                            FROM        nudebt__Client_Payment__c
                                            WHERE       (nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} AND nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd})
                                            AND         (nudebt__Payment_Status__c = 'Pending' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored')                                              
                                            AND         nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                            AND         nudebt__Client__r.nudebt__Processor__r.Name='" + processor + "'";

                string query2 = @"SELECT     Id, nudebt__Amount__c, nudebt__Amount_Paid__c,
                                                        nudebt__Client_Payment__r.nudebt__Client__r.Name,
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__First_Name__c,
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Last_Name__c, 
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Address_Line_1__c,
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__State__c,
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__City__c,
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Postal_Code__c,
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__CVV_Code__c, 
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Debit_Card_Number__c, 
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Debit_Card_Expires__c,
                                                        nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c, 
                                                        nudebt__Transaction_ID__c, nudebt__Status__c,
                                                        nudebt__Client_Payment__r.nudebt__Client__c,  nudebt__Client_Payment__r.nudebt__Client_Name__c,               
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c,  
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c, 
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_City__c, 
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_State__c, 
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c, 
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c,
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c, 
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name,
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c, 
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c, 
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c,
                                                        nudebt__Sync_Date__c, nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_Add1__c,                                                        
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_City__c,
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_ST__c,
                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_Postal__c
                                            FROM        nudebt__Client_Payment_Fee__c 
                                            WHERE       (nudebt__Status__c = 'Pending' OR nudebt__Status__c = 'TimedOut' OR nudebt__Status__c = 'Errored')
                                            AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} 
                                            AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd}                                                       
                                            AND         nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                            AND         nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name='" + processor + "'";
                //{0:yyyy-MM-dd} 
                if (starDate.Date > endDate.Date)
                {
                    query = string.Format(query, endDate.Date, starDate.Date);
                    query2 = string.Format(query2, endDate.Date, starDate.Date);
                }
                else
                {
                    query = string.Format(query, starDate.Date, endDate.Date);
                    query2 = string.Format(query2, starDate.Date, endDate.Date);
                }

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query2);

                TransactionDS.ClientDepositFeeDataTable tmpFeeTable = new TransactionDS.ClientDepositFeeDataTable();
                bool isDone = false;

                do
                {
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;

                        TransactionDS.ClientDepositRow row = tranDS.ClientDeposit.NewClientDepositRow();
                        //if (draft.nudebt__Client__r.nudebt__CVV_Code__c != null && draft.nudebt__Client__r.nudebt__Debit_Card_Number__c != null && draft.nudebt__Client__r.nudebt__Debit_Card_Expires__c != null)
                        //{
                        try
                        {
                            row.RecordID = draft.Id;
                            string x = draft.Name;
                            row.ClientID = draft.nudebt__Client__c;
                            row.ClientName = draft.nudebt__Client__r.Name;
                            row.BankAccountType = draft.nudebt__Client__r.nudebt__Account_Type__c;
                            row.BankAccountNumber
                                = String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number__c) ? "" : draft.nudebt__Client__r.nudebt__Account_Number__c)
                                : draft.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            row.BankCity = draft.nudebt__Client__r.nudebt__Bank_City__c;
                            row.BankState = draft.nudebt__Client__r.nudebt__Bank_State__c;

                            row.BankRoutingNumber = draft.nudebt__Client__r.nudebt__Routing_Number__c;
                            row.BankName = draft.nudebt__Client__r.nudebt__Bank_Name__c;
                            row.Amount = draft.nudebt__Payment_Amount__c.Value;
                            if (draft.nudebt__Amount_Paid__c != null)
                                row.AmountPaid = draft.nudebt__Amount_Paid__c.Value;
                            row.Date = Convert.ToDateTime(draft.nudebt__Payment_Scheduled_Date__c);
                            row.ProcessorAccount = draft.nudebt__Client__r.nudebt__Processor_Acct__c;
                            row.TransactionID = draft.nudebt__Transaction_ID__c == null ? "" : draft.nudebt__Transaction_ID__c;
                            row.ParentDRCID = "";
                            row.Type = "A";
                            row.TypeDesc = "ACH Monthly Draft";

                            row.Status = "Pending";
                            row.ClientImportID = draft.nudebt__Client__r.nudebt__Import_ID__c;
                            row.ClientFName = draft.nudebt__Client__r.nudebt__First_Name__c;
                            row.ClientLName = draft.nudebt__Client__r.nudebt__Last_Name__c;
                            // TODO bt - Need to pickup the Billing address
                            row.CardAddress = draft.nudebt__Client__r.nudebt__Billing_Add1__c;
                            row.CardCity = draft.nudebt__Client__r.nudebt__Billing_City__c;
                            row.CardState = draft.nudebt__Client__r.nudebt__Billing_ST__c;
                            row.CardPostal = draft.nudebt__Client__r.nudebt__Billing_Postal__c;
                            row.CardNumber = draft.nudebt__Client__r.nudebt__Debit_Card_Number__c;
                            row.CardPin = draft.nudebt__Client__r.nudebt__CVV_Code__c;
                            row.CardExpDate = Convert.ToString(draft.nudebt__Client__r.nudebt__Debit_Card_Expires__c);

                            row.ProcessorFee = draft.nudebt__Processing_Fee__c == null ? 0 : (double)draft.nudebt__Processing_Fee__c;
                            row.Processor = draft.nudebt__Client__r.nudebt__Processor__r.Name;
                            row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(draft.nudebt__Sync_Date__c);
                        }
                        catch
                        {
                            row.HasSyncError = "True";
                            row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            if (row.Amount > 0)
                                tranDS.ClientDeposit.Rows.Add(row);
                        }
                    }
                    //}
                    for (int j = 0; j < qr2.size; j++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment_Fee__c fee = qr2.records[j] as SFInterop.SForce.nudebt__Client_Payment_Fee__c;
                        TransactionDS.ClientDepositRow rowf = tranDS.ClientDeposit.NewClientDepositRow();
                        try
                        {
                            rowf.RecordID = fee.Id;
                            rowf.ClientID = fee.nudebt__Client_Payment__r.nudebt__Client__c;
                            rowf.ClientName = fee.nudebt__Client_Payment__r.nudebt__Client__r.Name;
                            rowf.BankAccountType = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c;
                            rowf.BankAccountNumber
                                = String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c) ? "" : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c)
                                : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            rowf.BankCity = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_City__c;
                            rowf.BankState = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_State__c;

                            rowf.BankRoutingNumber = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c;
                            rowf.BankName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c;
                            rowf.Amount = fee.nudebt__Amount__c.Value;
                            if (fee.nudebt__Amount_Paid__c != null)
                                rowf.AmountPaid = fee.nudebt__Amount_Paid__c.Value;
                            if (fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c != null)
                                rowf.Date = Convert.ToDateTime(fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c);
                            rowf.ProcessorAccount = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c;
                            //rowf.TransactionID = fee.nudebt__Transaction_ID__c;
                            rowf.TransactionID = fee.nudebt__Transaction_ID__c == null ? "" : fee.nudebt__Transaction_ID__c;
                            rowf.ParentDRCID = fee.nudebt__Client_Payment__c;
                             rowf.Type = fee.nudebt__Fee_ID__c == null ? "" : fee.nudebt__Fee_ID__c; 
                            //rowf.Type = "T";
                            rowf.TypeDesc = fee.Name;
                            //rowf.RecordName
                            rowf.Status = fee.nudebt__Status__c;
                            rowf.ClientImportID = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c;
                            /* TODO bt - use name its
                            if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c))
                                rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c;
                            else
                                if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name))
                            */
                            rowf.ClientFName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__First_Name__c;
                            rowf.ClientLName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Last_Name__c;
                            // TODO bt - Need to pickup the Billing address
                            rowf.CardAddress = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_Add1__c;
                            rowf.CardCity = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_City__c;
                            rowf.CardState = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_ST__c;
                            rowf.CardPostal = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_Postal__c;
                            rowf.CardNumber = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Debit_Card_Number__c;
                            rowf.CardPin = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__CVV_Code__c;
                            rowf.CardExpDate = Convert.ToString(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Debit_Card_Expires__c);

                            //rowf.ProcessorFee = fee.nudebt__Client_Payment__r.nudebt__Processing_Fee__c == null ? 0 : (double)fee.nudebt__Client_Payment__r.nudebt__Processing_Fee__c;
                            rowf.LastSyncDate = fee.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(fee.nudebt__Sync_Date__c);
                            rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name;
                        }
                        catch
                        {
                            rowf.HasSyncError = "True";
                            rowf.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            //if (rowf.Amount > 0)
                            tranDS.ClientDeposit.Rows.Add(rowf);
                        }
                    }
                    if (!qr.done)
                    {
                        qr = _salesForce.GetNextBatch(qr);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }
            else
            {

            }
            return tranDS;

        }

        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        public TransactionDS GetFDDeposits(string processor, DateTime starDate, DateTime endDate)
        {
            TransactionDS tranDS = new TransactionDS();
            if (_salesForce.IsValidSession)
            {
                string query = string.Empty;
                if (processor.Equals("AffinityProgram"))
                {
                    query = @"SELECT     Id, nudebt__Payment_Amount__c, nudebt__Amount_Paid__c,   nudebt__affinity_payment__c,      
                                                        nudebt__Client__r.nudebt__First_Name__c,nudebt__Client__r.Name,
                                                        nudebt__Client__r.nudebt__Last_Name__c,
                                                        nudebt__Client__r.nudebt__Address_Line_1__c,
                                                        nudebt__Client__r.nudebt__State__c,
                                                        nudebt__Client__r.nudebt__City__c,
                                                        nudebt__Client__r.nudebt__Postal_Code__c,
                                                        nudebt__Client__r.nudebt__CVV_Code__c,
                                                        nudebt__Client__r.nudebt__Debit_Card_Number__c,
                                                        nudebt__Client__r.nudebt__Debit_Card_Expires__c,
                                                        nudebt__Payment_Scheduled_Date__c, nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                                        nudebt__Client__c, nudebt__Client_Name__c,               
                                                        nudebt__Client__r.nudebt__Bank_Name__c, 
                                                        nudebt__Client__r.nudebt__Routing_Number__c, nudebt__Client__r.nudebt__Bank_City__c, 
                                                        nudebt__Client__r.nudebt__Bank_State__c, nudebt__Client__r.nudebt__Account_Type__c, 
                                                        nudebt__Client__r.nudebt__Account_Number_Enc__c,nudebt__Client__r.nudebt__Account_Number__c, 
                                                        nudebt__Client__r.nudebt__Processor__r.Name,nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c,
                                                        nudebt__Client__r.nudebt__Processor_Acct__c, nudebt__Client__r.nudebt__Import_ID__c,
                                                        nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c,
                                                        nudebt__Client__r.nudebt__Billing_Add1__c,                                                        
                                                        nudebt__Client__r.nudebt__Billing_City__c,
                                                        nudebt__Client__r.nudebt__Billing_ST__c,
                                                        nudebt__Client__r.nudebt__Billing_Postal__c
                                            FROM        nudebt__Client_Payment__c
                                            WHERE       (nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} AND nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd})
                                            AND         (nudebt__Payment_Status__c = 'Pending' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored')                                              
                                            AND         nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                            AND         nudebt__Affinity_Flag__c = true";
                    //draft.nudebt__Amount_Paid__c
                    //AND         nudebt__Client__r.nudebt__Processor__r.Name='T2P-ProtectionProgram'
                    //AND         nudebt__Affinity_Flag__c = true
                }
                else
                {                
                query = @"SELECT     Id, nudebt__Payment_Amount__c, nudebt__Amount_Paid__c,
                                                        nudebt__Client__r.Name,
                                                        nudebt__Client__r.nudebt__First_Name__c,
                                                        nudebt__Client__r.nudebt__Last_Name__c,
                                                        nudebt__Client__r.nudebt__Address_Line_1__c,
                                                        nudebt__Client__r.nudebt__State__c,
                                                        nudebt__Client__r.nudebt__City__c,
                                                        nudebt__Client__r.nudebt__Postal_Code__c,
                                                        nudebt__Client__r.nudebt__CVV_Code__c,
                                                        nudebt__Client__r.nudebt__Debit_Card_Number__c,
                                                        nudebt__Client__r.nudebt__Debit_Card_Expires__c,
                                                        nudebt__Payment_Scheduled_Date__c, nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                                        nudebt__Client__c, nudebt__Client_Name__c,               
                                                        nudebt__Client__r.nudebt__Bank_Name__c, 
                                                        nudebt__Client__r.nudebt__Routing_Number__c, nudebt__Client__r.nudebt__Bank_City__c, 
                                                        nudebt__Client__r.nudebt__Bank_State__c, nudebt__Client__r.nudebt__Account_Type__c, 
                                                        nudebt__Client__r.nudebt__Account_Number_Enc__c,nudebt__Client__r.nudebt__Account_Number__c, 
                                                        nudebt__Client__r.nudebt__Processor__r.Name,nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c,
                                                        nudebt__Client__r.nudebt__Processor_Acct__c, nudebt__Client__r.nudebt__Import_ID__c,
                                                        nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c,
                                                        nudebt__Client__r.nudebt__Billing_Add1__c,                                                        
                                                        nudebt__Client__r.nudebt__Billing_City__c,
                                                        nudebt__Client__r.nudebt__Billing_ST__c,
                                                        nudebt__Client__r.nudebt__Billing_Postal__c
                                            FROM        nudebt__Client_Payment__c
                                            WHERE       (nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} AND nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd})
                                            AND         (nudebt__Payment_Status__c = 'Pending' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored')                                              
                                            AND         nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                            AND         nudebt__Client__r.nudebt__Processor__r.Name='" + processor + "'";
                }
//                string query2 = @"SELECT     Id, nudebt__Amount__c, nudebt__Amount_Paid__c,
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__First_Name__c,
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Last_Name__c, 
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Address_Line_1__c,
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__State__c,
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__City__c,
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Postal_Code__c,
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__CVV_Code__c, 
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Debit_Card_Number__c, 
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Debit_Card_Expires__c,
//                                                        nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c, 
//                                                        nudebt__Transaction_ID__c, nudebt__Status__c,
//                                                        nudebt__Client_Payment__r.nudebt__Client__c,  nudebt__Client_Payment__r.nudebt__Client_Name__c,               
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c,  
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c, 
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_City__c, 
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_State__c, 
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c, 
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c,
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c, 
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name,
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c, 
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c, 
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c,
//                                                        nudebt__Sync_Date__c, nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_Add1__c,                                                        
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_City__c,
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_ST__c,
//                                                        nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_Postal__c
//                                            FROM        nudebt__Client_Payment_Fee__c 
//                                            WHERE       (nudebt__Status__c = 'Pending' OR nudebt__Status__c = 'TimedOut' OR nudebt__Status__c = 'Errored')
//                                            AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} 
//                                            AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd}                                                       
//                                            AND         nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
//                                            AND         nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name='" + processor + "'";
                //{0:yyyy-MM-dd} 
                if (starDate.Date > endDate.Date)
                {
                    query = string.Format(query, endDate.Date, starDate.Date);
                    //query2 = string.Format(query2, endDate.Date, starDate.Date);
                }
                else
                {
                    query = string.Format(query, starDate.Date, endDate.Date);
                    //query2 = string.Format(query2, starDate.Date, endDate.Date);
                }

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                //SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query2);

                TransactionDS.ClientDepositFeeDataTable tmpFeeTable = new TransactionDS.ClientDepositFeeDataTable();
                bool isDone = false;

                do
                {
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;

                        TransactionDS.ClientDepositRow row = tranDS.ClientDeposit.NewClientDepositRow();
                        //if (draft.nudebt__Client__r.nudebt__CVV_Code__c != null && draft.nudebt__Client__r.nudebt__Debit_Card_Number__c != null && draft.nudebt__Client__r.nudebt__Debit_Card_Expires__c != null)
                        //{
                        try
                        {
                            row.RecordID = draft.Id;
                            string x = draft.Name;
                            row.ClientID = draft.nudebt__Client__c;
                            row.ClientName = draft.nudebt__Client__r.Name;
                            row.ClientFullName = draft.nudebt__Client_Name__c;
                            row.BankAccountType = draft.nudebt__Client__r.nudebt__Account_Type__c;
                            row.BankAccountNumber
                                = String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number__c) ? "" : draft.nudebt__Client__r.nudebt__Account_Number__c)
                                : draft.nudebt__Client__r.nudebt__Account_Number_Enc__c;
                            
                            row.BankCity = draft.nudebt__Client__r.nudebt__Bank_City__c;
                            row.BankState = draft.nudebt__Client__r.nudebt__Bank_State__c;
                            
                            row.BankRoutingNumber = draft.nudebt__Client__r.nudebt__Routing_Number__c;
                            row.BankName = draft.nudebt__Client__r.nudebt__Bank_Name__c;
                            if (processor.Equals("AffinityProgram"))
                                row.Amount = draft.nudebt__affinity_payment__c.Value;
                            else
                                row.Amount = draft.nudebt__Payment_Amount__c.Value;
                            
                            if (draft.nudebt__Amount_Paid__c != null)
                                row.AmountPaid = draft.nudebt__Amount_Paid__c.Value;
                            row.Date = Convert.ToDateTime(draft.nudebt__Payment_Scheduled_Date__c);
                            row.ProcessorAccount = draft.nudebt__Client__r.nudebt__Processor_Acct__c;
                            row.TransactionID = draft.nudebt__Transaction_ID__c == null ? "" : draft.nudebt__Transaction_ID__c;
                            row.ParentDRCID = "";
                            row.Type = "A";
                            row.TypeDesc = "ACH Monthly Draft";

                            row.Status = "Pending";
                            row.ClientImportID = draft.nudebt__Client__r.nudebt__Import_ID__c;
                            row.ClientFName = draft.nudebt__Client__r.nudebt__First_Name__c;
                            row.ClientLName = draft.nudebt__Client__r.nudebt__Last_Name__c;
                            // TODO bt - Need to pickup the Billing address
                            row.CardAddress = draft.nudebt__Client__r.nudebt__Billing_Add1__c;
                            row.CardCity = draft.nudebt__Client__r.nudebt__Billing_City__c;
                            row.CardState = draft.nudebt__Client__r.nudebt__Billing_ST__c;
                            row.CardPostal = draft.nudebt__Client__r.nudebt__Billing_Postal__c;
                            row.CardNumber = draft.nudebt__Client__r.nudebt__Debit_Card_Number__c;
                            row.CardPin = draft.nudebt__Client__r.nudebt__CVV_Code__c;
                            row.CardExpDate = Convert.ToString(draft.nudebt__Client__r.nudebt__Debit_Card_Expires__c);
                            row.RowNumber = tranDS.ClientDeposit.Count + 1;
                            row.TransactionType = "Payment";
                            row.ProcessorFee = draft.nudebt__Processing_Fee__c == null ? 0 : (double)draft.nudebt__Processing_Fee__c;
                            row.Processor = draft.nudebt__Client__r.nudebt__Processor__r.Name;
                            row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(draft.nudebt__Sync_Date__c);
                        }
                        catch
                        {
                            row.HasSyncError = "True";
                            row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            if (row.Amount > 0)
                                tranDS.ClientDeposit.Rows.Add(row);
                        }
                    }
                    //}
                    //for (int j = 0; j < qr2.size; j++)
                    //{
                    //    SFInterop.SForce.nudebt__Client_Payment_Fee__c fee = qr2.records[j] as SFInterop.SForce.nudebt__Client_Payment_Fee__c;
                    //    TransactionDS.ClientDepositRow rowf = tranDS.ClientDeposit.NewClientDepositRow();
                    //    try
                    //    {
                    //        rowf.RecordID = fee.Id;
                    //        rowf.ClientID = fee.nudebt__Client_Payment__r.nudebt__Client__c;
                    //        rowf.ClientName = fee.nudebt__Client_Payment__r.nudebt__Client_Name__c;
                    //        rowf.BankAccountType = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c;
                    //        rowf.BankAccountNumber
                    //            = String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                    //            ? (String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c) ? "" : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c)
                    //            : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                    //        rowf.BankCity = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_City__c;
                    //        rowf.BankState = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_State__c;

                    //        rowf.BankRoutingNumber = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c;
                    //        rowf.BankName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c;
                    //        rowf.Amount = fee.nudebt__Amount__c.Value;
                    //        if (fee.nudebt__Amount_Paid__c != null)
                    //            rowf.AmountPaid = fee.nudebt__Amount_Paid__c.Value;
                    //        if (fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c != null)
                    //            rowf.Date = Convert.ToDateTime(fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c);
                    //        rowf.ProcessorAccount = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c;
                    //        //rowf.TransactionID = fee.nudebt__Transaction_ID__c;
                    //        rowf.TransactionID = fee.nudebt__Transaction_ID__c == null ? "" : fee.nudebt__Transaction_ID__c;
                    //        rowf.ParentDRCID = fee.nudebt__Client_Payment__c;
                    //        rowf.Type = fee.nudebt__Fee_ID__c == null ? "" : fee.nudebt__Fee_ID__c;
                    //        //rowf.Type = "T";
                    //        rowf.TypeDesc = fee.Name;
                    //        //rowf.RecordName
                    //        rowf.Status = fee.nudebt__Status__c;
                    //        rowf.ClientImportID = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c;
                    //        /* TODO bt - use name its
                    //        if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c))
                    //            rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c;
                    //        else
                    //            if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name))
                    //        */
                    //        rowf.ClientFName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__First_Name__c;
                    //        rowf.ClientLName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Last_Name__c;
                    //        // TODO bt - Need to pickup the Billing address
                    //        rowf.CardAddress = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_Add1__c;
                    //        rowf.CardCity = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_City__c;
                    //        rowf.CardState = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_ST__c;
                    //        rowf.CardPostal = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Billing_Postal__c;
                    //        rowf.CardNumber = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Debit_Card_Number__c;
                    //        rowf.CardPin = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__CVV_Code__c;
                    //        rowf.CardExpDate = Convert.ToString(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Debit_Card_Expires__c);

                    //        //rowf.ProcessorFee = fee.nudebt__Client_Payment__r.nudebt__Processing_Fee__c == null ? 0 : (double)fee.nudebt__Client_Payment__r.nudebt__Processing_Fee__c;
                    //        rowf.LastSyncDate = fee.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(fee.nudebt__Sync_Date__c);
                    //        rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name;
                    //    }
                    //    catch
                    //    {
                    //        rowf.HasSyncError = "True";
                    //        rowf.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                    //    }
                    //    finally
                    //    {
                    //        //if (rowf.Amount > 0)
                    //        tranDS.ClientDeposit.Rows.Add(rowf);
                    //    }
                    //}
                    if (!qr.done)
                    {
                        qr = _salesForce.GetNextBatch(qr);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }
            else
            {

            }
            return tranDS;

        }

        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        public TransactionDS GetDraftsForUpdate(ref TransactionDS tran, string processor)
        {
            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT     nudebt__Client__c, nudebt__Client_Name__c, Id, nudebt__Client__r.nudebt__Import_ID__c,
                                            nudebt__Client__r.nudebt__Bank_Name__c,
                                            nudebt__Client__r.nudebt__Routing_Number__c,
                                            nudebt__Client__r.nudebt__Account_Type__c, 
                                            nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client__r.nudebt__Account_Number__c,
                                            nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c, 
                                            nudebt__Client__r.nudebt__Processor__r.Name, nudebt__Client__r.nudebt__Processor_Acct__c,
                                            nudebt__Payment_Amount__c, nudebt__Payment_Scheduled_Date__c, nudebt__Amount_Paid__c,
                                            nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                            nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c, nudebt__Setup_Fee_Transaction_ID__c, 
                                            nudebt__Maintenance_Fee_Transaction_ID__c
                                FROM        nudebt__Client_Payment__c  
                                WHERE       nudebt__Payment_Scheduled_Date__c <= TODAY 
                                AND         (nudebt__Payment_Status__c = 'Pending' OR nudebt__Payment_Status__c = 'Processing' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored') 
                                
                                AND         nudebt__Client__r.nudebt__Processor_Acct__c != ''
                                AND         nudebt__Client__r.nudebt__Processor__r.Name='" + processor + "'";

                string query2 = @"SELECT    Id, Name, 
                                            nudebt__Amount__c, nudebt__Amount_Paid__c, nudebt__Fee_ID__c, nudebt__Transaction_ID__c, 
                                            nudebt__Client_Payment__c, nudebt__Client_Payment__r.Id,
                                            nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c, nudebt__Status__c,
                                            nudebt__Client_Payment__r.nudebt__Client__c,
                                            nudebt__Client_Payment__r.nudebt__Client_Name__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c
                                FROM        nudebt__Client_Payment_Fee__c  
                                
                                WHERE       (nudebt__Status__c = 'Pending' OR nudebt__Status__c = 'TimedOut' OR nudebt__Status__c = 'Errored' OR nudebt__Status__c = 'Processing')
                                AND         nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                
                                AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c <= TODAY
                                ANd         nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name = '" + processor + "'";

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query2);
                bool isDone = false;

                do
                {
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;

                        TransactionDS.ClientDepositRow row = tran.ClientDeposit.NewClientDepositRow();
                        // TransactionDS.ClientDepositFeeDataTable tmpFeeTable = new TransactionDS.ClientDepositFeeDataTable();
                        try
                        {
                            row.RecordID = draft.Id;
                            row.ClientID = draft.nudebt__Client__c;
                            row.ClientName = draft.nudebt__Client_Name__c;
                            row.BankAccountType = draft.nudebt__Client__r.nudebt__Account_Type__c;
                            row.BankAccountNumber
                                = String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number_Enc__c) 
                                    ? (String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number__c) ? "" : draft.nudebt__Client__r.nudebt__Account_Number__c)
                                    : draft.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            row.BankRoutingNumber = draft.nudebt__Client__r.nudebt__Routing_Number__c;
                            row.BankName = draft.nudebt__Bank_Name__c;
                            if (draft.nudebt__Amount_Paid__c != null)
                                row.AmountPaid = draft.nudebt__Amount_Paid__c.Value;
                            row.Amount = draft.nudebt__Payment_Amount__c.Value;
                            row.Date = Convert.ToDateTime(draft.nudebt__Payment_Scheduled_Date__c);
                            row.ProcessorAccount = draft.nudebt__Client__r.nudebt__Processor_Acct__c;
                            row.TransactionID = draft.nudebt__Transaction_ID__c;
                            row.ParentDRCID = "";
                            row.Type = "A";
                            row.TypeDesc = "ACH Monthly Draft";

                            row.Status = draft.nudebt__Payment_Status__c;
                            row.ClientImportID = draft.nudebt__Client__r.nudebt__Import_ID__c;
                            row.ProcessorFee = draft.nudebt__Processing_Fee__c == null ? 0 : (double)draft.nudebt__Processing_Fee__c;
                            row.Processor = draft.nudebt__Client__r.nudebt__Processor__r.Name;
                            row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(draft.nudebt__Sync_Date__c);

                        }
                        catch
                        {
                            row.HasSyncError = "True";
                            row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            // int lg = row.GetClientDepositFeeRows().Length;
                            tran.ClientDeposit.Rows.Add(row);
                        }
                    }
                    for (int j = 0; j < qr2.size; j++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment_Fee__c fee = qr2.records[j] as SFInterop.SForce.nudebt__Client_Payment_Fee__c;
                        TransactionDS.ClientDepositRow rowf = tran.ClientDeposit.NewClientDepositRow();
                        try
                        {
                            rowf.RecordID = fee.Id;
                            rowf.ClientID = fee.nudebt__Client_Payment__r.nudebt__Client__c;
                            rowf.ClientName = fee.nudebt__Client_Payment__r.nudebt__Client_Name__c;
                            rowf.BankAccountType = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c;
                            rowf.BankAccountNumber
                                = String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c) ?  "" : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c)
                                : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            rowf.BankRoutingNumber = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c;
                            rowf.BankName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c;
                            rowf.Amount = fee.nudebt__Amount__c.Value;
                            if (fee.nudebt__Amount_Paid__c != null)
                                rowf.AmountPaid = fee.nudebt__Amount_Paid__c.Value;
                            if (fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c != null)
                                rowf.Date = Convert.ToDateTime(fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c);
                            rowf.ProcessorAccount = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c;
                            rowf.TransactionID = fee.nudebt__Transaction_ID__c;
                            rowf.ParentDRCID = fee.nudebt__Client_Payment__c;
                            //rowf.Type = fee.nudebt__Fee_ID__c;
                            rowf.Type = fee.nudebt__Fee_ID__c == null ? "" : fee.nudebt__Fee_ID__c; 
                            rowf.TypeDesc = fee.Name;
                            //rowf.RecordName
                            rowf.Status = fee.nudebt__Status__c;
                            rowf.ClientImportID = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c;
                            /* TODO bt - use name its
                            if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c))
                                rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c;
                            else
                                if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name))
                             */
                                    rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name;
                        }
                        catch
                        {
                            rowf.HasSyncError = "True";
                            rowf.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            tran.ClientDeposit.Rows.Add(rowf);
                        }
                    }

                    if (!qr.done)
                    {
                        qr = _salesForce.GetNextBatch(qr);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return tran;
        }

        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        public TransactionDS GetT2PForUpdate(ref TransactionDS tran, string processor, DateTime start)
        {
            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT     nudebt__Client__c, nudebt__Client_Name__c, Id, nudebt__Client__r.nudebt__Import_ID__c,
                                            nudebt__Client__r.nudebt__Bank_Name__c,nudebt__Mark_for_Sync__c,
                                            nudebt__Client__r.Name,
                                            nudebt__Payment_Cleared_Date__c,
                                            nudebt__Client__r.nudebt__Routing_Number__c,
                                            nudebt__Client__r.nudebt__Account_Type__c, 
                                            nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client__r.nudebt__Account_Number__c,
                                            nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c, 
                                            nudebt__Client__r.nudebt__Processor__r.Name, nudebt__Client__r.nudebt__Processor_Acct__c,
                                            nudebt__Payment_Amount__c, nudebt__Payment_Scheduled_Date__c, nudebt__Amount_Paid__c,
                                            nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                            nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c, nudebt__Setup_Fee_Transaction_ID__c, 
                                            nudebt__Maintenance_Fee_Transaction_ID__c
                                FROM        nudebt__Client_Payment__c  
                                WHERE       nudebt__Client__r.nudebt__Processor_Acct__c != 'Active'
                                AND         nudebt__Payment_Status__c='Processing'
                                AND         ((nudebt__Payment_Scheduled_Date__c >= LAST_N_DAYS:29 AND nudebt__Payment_Scheduled_Date__c <= TODAY  )or nudebt__Mark_for_Sync__c=True)
                                AND         (nudebt__Client__r.nudebt__Processor__r.Name = 'T2P-Hybrid' OR nudebt__Client__r.nudebt__Processor__r.Name = 'T2P-ProtectionProgram')";
                //nudebt__Payment_Scheduled_Date__c <= TODAY
                //                AND         
                string query2 = @"SELECT    Id, Name, 
                                            nudebt__Amount__c, nudebt__Amount_Paid__c, nudebt__Fee_ID__c, nudebt__Transaction_ID__c, 
                                            nudebt__Client_Payment__c, nudebt__Client_Payment__r.Id,nudebt__Client_Payment__r.nudebt__Mark_for_Sync__c,
                                            nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c, nudebt__Status__c,
                                            nudebt__Client_Payment__r.nudebt__Client__c,
                                            nudebt__Client_Payment__r.nudebt__Client_Name__c, 
nudebt__Client_Payment__r.nudebt__Client__r.Name,
nudebt__Cleared_Date__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c
                                FROM        nudebt__Client_Payment_Fee__c  
                                
                                WHERE       nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                AND         ((nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c >= LAST_N_DAYS:29 
                                            AND nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c <= TODAY  )
                                            OR nudebt__Client_Payment__r.nudebt__Mark_for_Sync__c=True) 
                                AND         nudebt__Status__c = 'Processing'
                                AND         (nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name = 'T2P-Hybrid' OR nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name = 'T2P-ProtectionProgram')";
                //AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c <= TODAY
                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query2);
                bool isDone = false;

                do
                {
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;

                        TransactionDS.ClientDepositRow row = tran.ClientDeposit.NewClientDepositRow();
                        // TransactionDS.ClientDepositFeeDataTable tmpFeeTable = new TransactionDS.ClientDepositFeeDataTable();
                        try
                        {
                            row.RecordID = draft.Id;
                            row.ClientID = draft.nudebt__Client__c;
                            row.ClientFullName = draft.nudebt__Client_Name__c;
                            row.BankAccountType = draft.nudebt__Client__r.nudebt__Account_Type__c;
                            row.BankAccountNumber
                                = String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                    ? (String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number__c) ? "" : draft.nudebt__Client__r.nudebt__Account_Number__c)
                                    : draft.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            row.BankRoutingNumber = draft.nudebt__Client__r.nudebt__Routing_Number__c;
                            row.BankName = draft.nudebt__Bank_Name__c;
                            if (draft.nudebt__Amount_Paid__c != null)
                                row.AmountPaid = draft.nudebt__Amount_Paid__c.Value;
                            row.Amount = draft.nudebt__Payment_Amount__c.Value;
                            row.Date = Convert.ToDateTime(draft.nudebt__Payment_Scheduled_Date__c);
                            row.ProcessorAccount = draft.nudebt__Client__r.nudebt__Processor_Acct__c;
                            row.TransactionID = draft.nudebt__Transaction_ID__c;
                            row.ParentDRCID = "";
                            row.Status = draft.nudebt__Payment_Status__c;
                            row.Type = "A";
                            row.TypeDesc = "ACH Monthly Draft";
                            row.TransactionType = "Payment";
                            row.ClientImportID = draft.nudebt__Client__r.nudebt__Import_ID__c;
                            row.ProcessorFee = draft.nudebt__Processing_Fee__c == null ? 0 : (double)draft.nudebt__Processing_Fee__c;
                            row.Processor = draft.nudebt__Client__r.nudebt__Processor__r.Name;
                            row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(draft.nudebt__Sync_Date__c);
                            row.ClientName = draft.nudebt__Client__r.Name == null ? "" : draft.nudebt__Client__r.Name;
                            if (draft.nudebt__Payment_Cleared_Date__c != null)
                                row.ClearedDate = Convert.ToDateTime(draft.nudebt__Payment_Cleared_Date__c);
                            row.MarkForSync = (bool)draft.nudebt__Mark_for_Sync__c;
                        }
                        catch
                        {
                            row.HasSyncError = "True";
                            row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            // int lg = row.GetClientDepositFeeRows().Length;
                            tran.ClientDeposit.Rows.Add(row);
                        }
                    }
                    for (int j = 0; j < qr2.size; j++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment_Fee__c fee = qr2.records[j] as SFInterop.SForce.nudebt__Client_Payment_Fee__c;
                        TransactionDS.ClientDepositRow rowf = tran.ClientDeposit.NewClientDepositRow();
                        try
                        {
                            rowf.RecordID = fee.Id;
                            rowf.ClientID = fee.nudebt__Client_Payment__r.nudebt__Client__c;
                            rowf.ClientFullName = fee.nudebt__Client_Payment__r.nudebt__Client_Name__c;
                            rowf.BankAccountType = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c;
                            rowf.BankAccountNumber
                                = String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c) ? "" : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c)
                                : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            rowf.BankRoutingNumber = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c;
                            rowf.BankName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c;
                            //rowf.Amount = fee.nudebt__Amount__c.Value;
                            rowf.Amount = (double)fee.nudebt__Amount__c;
                            if (fee.nudebt__Amount_Paid__c != null)
                                rowf.AmountPaid = fee.nudebt__Amount_Paid__c.Value;
                            if (fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c != null)
                                rowf.Date = Convert.ToDateTime(fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c);
                            rowf.ProcessorAccount = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c;
                            rowf.TransactionID = fee.nudebt__Transaction_ID__c;
                            rowf.ParentDRCID = fee.nudebt__Client_Payment__c;
                            rowf.Type = fee.nudebt__Fee_ID__c == null ? "" : fee.nudebt__Fee_ID__c;
                            rowf.TypeDesc = fee.Name;
                            rowf.TransactionType = "Fee";
                            //rowf.RecordName
                            rowf.Status = fee.nudebt__Status__c;
                            rowf.ClientImportID = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c;
                            /* TODO bt - use name its
                            if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c))
                                rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c;
                            else
                                if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name))
                             */
                            rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name;
                            rowf.ClientName = fee.nudebt__Client_Payment__r.nudebt__Client__r.Name == null ? "" : fee.nudebt__Client_Payment__r.nudebt__Client__r.Name;
                            if (fee.nudebt__Cleared_Date__c != null)
                                rowf.ClearedDate = Convert.ToDateTime(fee.nudebt__Cleared_Date__c);
                            rowf.MarkForSync = (bool)fee.nudebt__Client_Payment__r.nudebt__Mark_for_Sync__c;
                        }
                        catch
                        {
                            rowf.HasSyncError = "True";
                            rowf.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            tran.ClientDeposit.Rows.Add(rowf);
                        }
                    }

                    if (!qr.done)
                    {
                        qr = _salesForce.GetNextBatch(qr);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return tran;

        }

        /// <summary>
        /// Returns a list of pending payments out to creditors that fall within a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of debt settlement payments to creditors</returns>
        public TransactionDS.OfferPaymentDataTable GetWithdrawals(DateTime start, DateTime end, TransactionDS.OfferPaymentDataTable tran)
        {
            if (tran == null)
            {
                tran = new TransactionDS.OfferPaymentDataTable();
            }

            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT     nudebt__Transaction_ID__c, Id,
                                            nudebt__Offer__c, nudebt__Offer__r.nudebt__Client_Debt__c,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__c,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Account_Number_Enc__c,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Account_Number__c, nudebt__Scheduled_Date__c,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Social_Security_Number__c,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__SSN_ENC__c,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Processor_Acct__c, nudebt__Sync_Date__c,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Processor__r.Name,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Processor__r.nudebt__Company_Name__c,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Original_Creditor__r.Id,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Original_Creditor__r.Name, nudebt__Payment_Amount__c,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Original_Creditor__r.nudebt__Processor_Acct__c,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Original_Creditor__r.nudebt__Processor_Bank__c,
                                            nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Original_Creditor__r.nudebt__Processor_Addr__c 
                                FROM        nudebt__Offer_Payment__c 
                                WHERE       nudebt__Scheduled_Date__c >= {0:yyyy-MM-dd} AND nudebt__Scheduled_Date__c <= {1:yyyy-MM-dd} 
                                AND         (nudebt__Status__c = 'Pending' OR nudebt__Status__c = 'TimedOut' OR nudebt__Status__c = 'Errored')
                                AND         nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Processor_Status__c = 'Active'";
                                            // TODO bt - Client status is too variable
                                            // nudebt__Client__r.nudebt__Processor_Status__c = 'Active'


                query = string.Format(query, start.Date, end.Date);

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                bool isDone = false;

                do
                {
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.nudebt__Offer_Payment__c payment = qr.records[i] as SFInterop.SForce.nudebt__Offer_Payment__c;
                        TransactionDS.OfferPaymentRow row = tran.NewOfferPaymentRow();

                        try
                        {
                            row.RecordID = payment.Id;
                            row.ClientID = payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__c;
                            row.DebtID = payment.nudebt__Offer__r.nudebt__Client_Debt__c;
                            row.OfferID = payment.nudebt__Offer__c;
                            row.Payee = payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Original_Creditor__r.Name;
                            row.Amount = payment.nudebt__Payment_Amount__c.Value;
                            row.ProcessorAccount = payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Processor_Acct__c;
                            row.AccountNumber
                                = String.IsNullOrEmpty(payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Account_Number__c) ? ""
                                                      : payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Account_Number__c)
                                : payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Account_Number_Enc__c;

                            row.Date = payment.nudebt__Scheduled_Date__c.Value;
                            row.Processor = payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Processor__r.Name;

                            row.SocialSecurityNumber 
                                = String.IsNullOrEmpty(payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__SSN_ENC__c)
                                ? (String.IsNullOrEmpty(payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Social_Security_Number__c)
                                                      ? "" 
                                                      : payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__Social_Security_Number__c)
                                : payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_ID__r.nudebt__SSN_ENC__c;

                            if (payment.nudebt__Transaction_ID__c != null)
                                row.TransactionID = payment.nudebt__Transaction_ID__c;
                            row.PayeeID = payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Original_Creditor__r.nudebt__Processor_Acct__c;
                            if (!String.IsNullOrEmpty(payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Original_Creditor__r.nudebt__Processor_Addr__c))
                                row.PayeeAddressID = payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Original_Creditor__r.nudebt__Processor_Addr__c;
                            else
                                row.PayeeAddressID = "";
                            if (!String.IsNullOrEmpty(payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Original_Creditor__r.nudebt__Processor_Bank__c))
                                row.PayeeBankID = payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Original_Creditor__r.nudebt__Processor_Bank__c;
                            else
                                row.PayeeBankID = "";
                            row.ClientName = payment.nudebt__Offer__r.nudebt__Client_Debt__r.nudebt__Client_Full_Name__c;
                            row.Status = "Pending";
                            row.LastSyncDate = payment.nudebt__Sync_Date__c == null ? new DateTime() : payment.nudebt__Sync_Date__c.Value;
                            row.HasSyncError = false;

                            if (string.IsNullOrEmpty(row.ProcessorAccount) || string.IsNullOrEmpty(row.AccountNumber) || row.Amount <= 0)
                            {
                                throw new Exception();
                            }
                        }
                        catch
                        {
                            row.HasSyncError = true;
                            row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            if (row.Amount > 0)
                                tran.Rows.Add(row);
                        }
                    }

                    if (!qr.done)
                    {
                        qr = _salesForce.GetNextBatch(qr);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return tran;
        }

        /// <summary>
        /// Converts a list of strings to a comma separated string
        /// </summary>
        /// <param name="list">The list of strings</param>
        /// <returns>A single string containing all the items in the original list separated by commas</returns>
        private string ListToString(List<string> list)
        {
            string s = string.Empty;

            foreach (string st in list)
            {
                s += "'" + st + "', ";
            }

            if (list.Count > 0)
            {
                s = s.Remove(s.Length - 2);
            }

            return s;
        }

        /// <summary>
        /// Retrieves a list of creditors from salesForce
        /// </summary>
        /// <returns>The list of accounts</returns>
        public DataTable GetCreditors()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Id");
            dt.Columns.Add("Name");
            dt.Columns.Add("BillingCity");
            dt.Columns.Add("BillingCountry");
            dt.Columns.Add("BillingPostalCode");
            dt.Columns.Add("BillingState");
            dt.Columns.Add("BillingStreet");
            dt.Columns.Add("BillingStreet2");
            dt.Columns.Add("ProcessorAccount");
            dt.Columns.Add("ProcessorAddress");
            dt.Columns.Add("ProcessorBank");
            dt.Columns.Add("BankAccount");
            dt.Columns.Add("BankAccountType");
            dt.Columns.Add("BankRouting");
            dt.Columns.Add("BankType");

            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT     Id, Name, nudebt__City__c, nudebt__Country__c, nudebt__Postal_Code__c, nudebt__State__c, nudebt__Address_Line_1__c, nudebt__Address_Line_2__c, 
                                            nudebt__Bank_Account__c, nudebt__Bank_Account_Type__c, nudebt__Bank_Routing__c, nudebt__Bank_Type__c, 
                                            nudebt__Processor_Bank__c, nudebt__Processor_Addr__c, nudebt__Processor_Acct__c
                                FROM        Account
                                WHERE       (Type = 'Creditor' OR Type = '') 
                                AND         ((nudebt__Processor_Acct__c = '' OR nudebt__Processor_Acct__c = NULL) OR 
                                             ((nudebt__Processor_Addr__c = '' OR nudebt__Processor_Addr__c = NULL) AND nudebt__City__c != '' AND nudebt__Country__c != '' AND nudebt__Postal_Code__c != '' AND nudebt__State__c != '' AND nudebt__Address_Line_1__c != '') OR
                                             ((nudebt__Processor_Bank__c = '' OR nudebt__Processor_Bank__c = NULL) AND nudebt__Bank_Account__c != '' AND nudebt__Bank_Account_Type__c != '' AND nudebt__Bank_Routing__c != '' AND nudebt__Bank_Type__c != ''))";

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                bool isDone = false;

                do
                {

                    // iterate through every contact record returned by salesForce and retrieve the contact information for the primary contact
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.Account acct = qr.records[i] as SFInterop.SForce.Account;
                        DataRow row = dt.NewRow();
                        row["Name"] = acct.Name;
                        row["Id"] = acct.Id;
                        row["BillingCity"] = acct.nudebt__City__c == "" || acct.nudebt__City__c == null ? "" : acct.nudebt__City__c;
                        // row["BillingCountry"] = acct.Country__c == "" || acct.Country__c == null ? "" : acct.Country__c;
                        row["BillingCountry"] = "US";
                        row["BillingPostalCode"] = acct.nudebt__Postal_Code__c == "" || acct.nudebt__Postal_Code__c == null ? "" : acct.nudebt__Postal_Code__c;
                        row["BillingState"] = acct.nudebt__State__c == "" || acct.nudebt__State__c == null ? "" : acct.nudebt__State__c;
                        row["BillingStreet"] = acct.nudebt__Address_Line_1__c == "" || acct.nudebt__Address_Line_1__c == null ? "" : acct.nudebt__Address_Line_1__c;
                        row["BillingStreet2"] = acct.nudebt__Address_Line_2__c == "" || acct.nudebt__Address_Line_2__c == null ? "" : acct.nudebt__Address_Line_2__c;
                        row["ProcessorAccount"] = acct.nudebt__Processor_Acct__c == "" || acct.nudebt__Processor_Acct__c == null ? "" : acct.nudebt__Processor_Acct__c;
                        row["ProcessorAddress"] = acct.nudebt__Processor_Addr__c == "" || acct.nudebt__Processor_Addr__c == null ? "" : acct.nudebt__Processor_Addr__c;
                        row["ProcessorBank"] = acct.nudebt__Processor_Bank__c == "" || acct.nudebt__Processor_Bank__c == null ? "" : acct.nudebt__Processor_Bank__c;
                        row["BankAccount"] = acct.nudebt__Bank_Account__c == "" || acct.nudebt__Bank_Account__c == null ? "" : acct.nudebt__Bank_Account__c;
                        row["BankAccountType"] = acct.nudebt__Bank_Account_Type__c == "" || acct.nudebt__Bank_Account_Type__c == null ? "" : acct.nudebt__Bank_Account_Type__c;
                        row["BankRouting"] = acct.nudebt__Bank_Routing__c == "" || acct.nudebt__Bank_Routing__c == null ? "" : acct.nudebt__Bank_Routing__c;
                        row["BankType"] = acct.nudebt__Bank_Type__c == "" || acct.nudebt__Bank_Type__c == null ? "" : acct.nudebt__Bank_Type__c;
                        dt.Rows.Add(row);
                    }

                    if (!qr.done)
                    {
                        qr = _salesForce.GetNextBatch(qr);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return dt;
        }

        /// <summary>
        /// Retrieves the API settings from SalesForce
        /// </summary>
        /// <returns>The settings</returns>
        public TransactionDS.SettingsDataTable GetSettings()
        {
            TransactionDS.SettingsDataTable settingsTable = new TransactionDS.SettingsDataTable();

            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT     Id, nudebt__Default_Processor__r.nudebt__Company_Name__c, nudebt__Process_Payments_Enabled__c, nudebt__Update_Client_Enabled__c,
                                            nudebt__Update_Payment_Status_Enabled__c, nudebt__GCS_Account__c, nudebt__GCS_Account_Pwd__c, nudebt__Number_of_Days_Ahead_to_Process__c
                                FROM        nudebt__API_Setting__c
                                WHERE       nudebt__Is_Active_Setting__c = True";

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);

                // iterate through every NuDebt API settings record returned and 
                for (int i = 0; i < qr.records.Length; i++)
                {
                    SFInterop.SForce.nudebt__API_Setting__c acct = qr.records[i] as SFInterop.SForce.nudebt__API_Setting__c;

                    TransactionDS.SettingsRow row = settingsTable.NewSettingsRow();
                    row.RecordID = acct.Id;
                    row.ProcessPaymentsEnabled = acct.nudebt__Process_Payments_Enabled__c.HasValue ? acct.nudebt__Process_Payments_Enabled__c.Value : false;
                    row.UpdateClientEnabled = acct.nudebt__Update_Client_Enabled__c.HasValue ? acct.nudebt__Update_Client_Enabled__c.Value : false;
                    row.UpdateStatusEnabled = acct.nudebt__Update_Payment_Status_Enabled__c.HasValue ? acct.nudebt__Update_Payment_Status_Enabled__c.Value : false;
                    row.GCSLogin = acct.nudebt__GCS_Account__c;
                    row.GCSPassword = acct.nudebt__GCS_Account_Pwd__c;
                    row.NumberOfDaysAhead = acct.nudebt__Number_of_Days_Ahead_to_Process__c.HasValue ? (int)acct.nudebt__Number_of_Days_Ahead_to_Process__c.Value : 0;
                    row.DefaultProcessor = string.IsNullOrEmpty(acct.nudebt__Default_Processor__r.nudebt__Company_Name__c) ? "GCS" : acct.nudebt__Default_Processor__r.nudebt__Company_Name__c;
                    settingsTable.Rows.Add(row);
                }
            }

            return settingsTable;
        }

        public List<string> SetCreditorAccount(DataTable dt)
        {
            List<SFInterop.SForce.sObject> accounts = new List<SFInterop.SForce.sObject>();

            foreach (DataRow dr in dt.Rows)
            {
                SFInterop.SForce.Account acct = new SFInterop.SForce.Account() { Id = dr["Id"].ToString() };
                acct.nudebt__Processor_Acct__c = dr["ProcessorAccount"].ToString();
                acct.nudebt__Processor_Addr__c = dr["ProcessorAddress"].ToString();
                acct.nudebt__Processor_Bank__c = dr["ProcessorBank"].ToString();
                accounts.Add(acct);
            }

            List<string> resultList = new List<string>();
            List<string[]> errorList = new List<string[]>();
            SFInterop.SForce.SaveResult[] results = this._salesForce.UpdateBatch(accounts);

            if (results != null)
            {
                foreach (SFInterop.SForce.SaveResult result in results)
                {
                    this.ProcessResult(result, "Account", ref resultList, ref errorList, result.id);
                }
            }

            this.WriteError(errorList);
            return resultList;
        }

        /// <summary>
        /// Updates the status of client payment and offer payment transactions in SalesForce
        /// based on the results from the processor's system
        /// </summary>
        /// <param name="deposits">The deposit record to update</param>
        /// <param name="payments">The offer payment records to update</param>
        /// <returns>The results of the update operation</returns>
        public List<string> SetStatus(TransactionDS ds)
        {
            TransactionDS.ClientDepositDataTable deposits = ds.ClientDeposit;
            TransactionDS.ClientDepositFeeDataTable depositFees = ds.ClientDepositFee;
            TransactionDS.OfferPaymentDataTable payments = ds.OfferPayment;

            List<SFInterop.SForce.sObject> updateOfferList = new List<SFInterop.SForce.sObject>();
            List<SFInterop.SForce.sObject> paymentList = new List<SFInterop.SForce.sObject>();
            List<SFInterop.SForce.sObject> paymentFeeList = new List<SFInterop.SForce.sObject>();

            foreach (TransactionDS.ClientDepositRow row in deposits.Rows)
            {
                if (row.Type == "A")
                    paymentList.Add(this.CreateDepositRow(row));
                else
                    paymentFeeList.Add(this.CreateDepositFeeRow(row));
            }

            foreach (TransactionDS.OfferPaymentRow row in payments.Rows)
            {
                if (!String.IsNullOrEmpty(row.TransactionID))
                    updateOfferList.Add(this.CreatePaymentRow(row));
            }

            List<SFInterop.SForce.SaveResult> results = this.MakeBatches(paymentList, 80);
            List<SFInterop.SForce.SaveResult> resultFees = this.MakeBatches(paymentFeeList, 80);
            SFInterop.SForce.SaveResult[] resultsO = _salesForce.UpdateBatch(updateOfferList);

            List<string> resultList = new List<string>();
            List<string[]> errorList = new List<string[]>();

            if (results != null)
            {
                foreach (SFInterop.SForce.SaveResult result in results) 
                {
                    this.ProcessResult(result, "nudebt__Client_Payment__c", ref resultList, ref errorList);
                }
            }

            if (resultFees != null)
            {
                foreach (SFInterop.SForce.SaveResult result in resultFees)
                {
                    this.ProcessResult(result, "nudebt__Client_Payment_Fee__c", ref resultList, ref errorList);
                }
            }

            if (resultsO != null)
            {
                foreach (SFInterop.SForce.SaveResult result in resultsO)
                {
                    this.ProcessResult(result, "nudebt__Offer_Payment__c", ref resultList, ref errorList);
                }
            }

            ///For Salesforce Responce
            for (int i = 0; i < errorList.Count; i++)
            {
                TransactionDS.ClientDepositRow[] rows = ds.ClientDeposit.Select("RecordID = '" + errorList[i][2] + "'") as TransactionDS.ClientDepositRow[];
                if (rows.Length > 0)
                {
                    rows[0].SFErrorMessage = errorList[i][0];

                    rows[0].SFErrorCode = "Error";
                }
            }
            if (errorList.Count > 0)
                this.WriteError(errorList);
            return resultList;
        }

        /// <summary>
        /// Breaks up a list of sObjects into multiple lists of fixed size batches to prevent timeouts.
        /// </summary>
        /// <param name="listToUpdate">The list of all sObjects</param>
        /// <param name="batchSize">The batch size</param>
        /// <returns>The group of batches</returns>
        private List<SFInterop.SForce.SaveResult> MakeBatches(List<SFInterop.SForce.sObject> listToUpdate, int batchSize)
        {
            List<SFInterop.SForce.SaveResult> results = new List<SFInterop.SForce.SaveResult>();

            for (int i = 0; i < listToUpdate.Count; )
            {
                List<SFInterop.SForce.sObject> batch = new List<SFInterop.SForce.sObject>();

                for (int j = 0; i < listToUpdate.Count && j < batchSize; j++, i++)
                {
                    batch.Add(listToUpdate[i]);
                }

                foreach (SFInterop.SForce.SaveResult sr in _salesForce.UpdateBatch(batch))
                {
                    results.Add(sr);
                }
            }

            return results;
        }

        /// <summary>
        /// Updates the GCS account number for those clients that do not have one
        /// </summary>
        public Hashtable UpdateClientList(TransactionDS.ClientsDataTable clients)
        {
            Hashtable ht = new Hashtable();
            List<string> clientIDs = new List<string>();

            if (clients.Rows.Count > 0)
            {
                List<string[]> errorList = new List<string[]>();
                List<string> resultList = new List<string>();

                List<SFInterop.SForce.sObject> clientsToUpdate = new List<SFInterop.SForce.sObject>();

                foreach (TransactionDS.ClientsRow client in clients.Rows)
                {
                    // if the account had not been stored to salesForce
                    // add to the list to be updated
                    SFInterop.SForce.nudebt__Client__c updateClient = new SFInterop.SForce.nudebt__Client__c() { Id = client.ClientID };

                    updateClient.nudebt__Account_Balance__cSpecified = true;

                    if (client.ProcessorAccountNum != null)
                        updateClient.nudebt__Processor_Acct__c = client.ProcessorAccountNum;
                    if (!client.IsAccountBalanceNull())    
                        updateClient.nudebt__Account_Balance__c = client.AccountBalance;
                    if (client.ProcessorStatus != null)
                        updateClient.nudebt__Processor_Status__c = client.ProcessorStatus;
                    if (client.SyncDescription != null)
                        updateClient.nudebt__Sync_Description__c = client.SyncDescription;
                    else
                        updateClient.nudebt__Sync_Description__c = String.Empty;                // clear out what might be there
                    updateClient.nudebt__Mark_for_Sync__c = client.MarkForSync;
                    updateClient.nudebt__Mark_for_Sync__cSpecified = true;
                    updateClient.nudebt__Sync_Date__c = DateTime.Now;
                    updateClient.nudebt__Sync_Date__cSpecified = true;

                    clientsToUpdate.Add(updateClient);

                    client.IsNewAccount = false;
                    clientIDs.Add(client.ClientID);
                    ht.Add(client.ClientID, client);

                    if (clientsToUpdate.Count >= 100)
                    {
                        SFInterop.SForce.SaveResult[] results = this._salesForce.UpdateBatch(clientsToUpdate);
                        this.ProcessResults(results, " nudebt__Client__c", ref resultList, ref errorList, clientIDs);
                        clientsToUpdate.Clear();
                    }
                }

                if (clientsToUpdate.Count > 0)
                {
                    SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(clientsToUpdate);
                    this.ProcessResults(results2, " nudebt__Client__c", ref resultList, ref errorList, clientIDs);
                }

                ///For Salesforce Responce
                for (int i = 0; i < errorList.Count; i++)
                {
                    TransactionDS.ClientsRow[] rows = clients.Select("ClientID = '" + errorList[i][2] + "'") as TransactionDS.ClientsRow[];
                    if (rows.Length > 0)
                    {
                        rows[0].SFErrorMessage = errorList[i][0];

                        rows[0].SFErrorCode = "Error";
                    }
                }
                if (errorList.Count > 0)
                    this.WriteError(errorList);
            }

            return ht;
        }

        /// <summary>
        /// Updates the GCS account number for those clients that do not have one
        /// </summary>
        public Hashtable UpdateFDClientList(TransactionDS.ClientsDataTable clients)
        {
            Hashtable ht = new Hashtable();
            List<string> clientIDs = new List<string>();

            if (clients.Rows.Count > 0)
            {
                List<string[]> errorList = new List<string[]>();
                List<string> resultList = new List<string>();

                List<SFInterop.SForce.sObject> clientsToUpdate = new List<SFInterop.SForce.sObject>();

                foreach (TransactionDS.ClientsRow client in clients.Rows)
                {
                    // if the account had not been stored to salesForce
                    // add to the list to be updated
                    SFInterop.SForce.nudebt__Client__c updateClient = new SFInterop.SForce.nudebt__Client__c() { Id = client.ClientID };

                    updateClient.nudebt__Account_Balance__cSpecified = true;

                    if (client.ProcessorAccountNum != null)
                        updateClient.nudebt__Processor_Acct__c = client.ProcessorAccountNum;
                    if (updateClient.nudebt__Account_Balance__c != null)
                        updateClient.nudebt__Account_Balance__c = client.AccountBalance;
                    if (client.ProcessorStatus != null)
                        updateClient.nudebt__Processor_Status__c = client.ProcessorStatus;
                    if (client.SyncDescription != null)
                        updateClient.nudebt__Sync_Description__c = client.SyncDescription;
                    else
                        updateClient.nudebt__Sync_Description__c = String.Empty;                // clear out what might be there
                    updateClient.nudebt__Mark_for_Sync__c = client.MarkForSync;
                    updateClient.nudebt__Mark_for_Sync__cSpecified = true;
                    updateClient.nudebt__Sync_Date__c = DateTime.Now;
                    updateClient.nudebt__Sync_Date__cSpecified = true;

                    clientsToUpdate.Add(updateClient);

                    client.IsNewAccount = false;
                    clientIDs.Add(client.ClientID);
                    ht.Add(client.ClientID, client);

                    if (clientsToUpdate.Count >= 100)
                    {
                        SFInterop.SForce.SaveResult[] results = this._salesForce.UpdateBatch(clientsToUpdate);
                        this.ProcessResults(results, " nudebt__Client__c", ref resultList, ref errorList, clientIDs);
                        clientsToUpdate.Clear();
                    }
                }

                if (clientsToUpdate.Count > 0)
                {
                    SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(clientsToUpdate);
                    this.ProcessResults(results2, " nudebt__Client__c", ref resultList, ref errorList, clientIDs);
                }

                ///For Salesforce Responce
                for (int i = 0; i < errorList.Count; i++)
                {
                    TransactionDS.ClientsRow[] rows = clients.Select("ClientID = '" + errorList[i][2] + "'") as TransactionDS.ClientsRow[];
                    if (rows.Length > 0)
                    {
                        rows[0].SFErrorMessage = errorList[i][0];

                        rows[0].SFErrorCode = "Error";
                    }
                }
                if (errorList.Count > 0)
                    this.WriteError(errorList);
            }

            return ht;
        }

        /// <summary>
        /// Retrieves SF Clients that are Active in SF and Active in Pmt. Processor company
        /// </summary>
        /// <returns>The client records as a strongly typed Datatable</returns>
        public TransactionDS.ClientsDataTable GetActiveClients(string Processor)
        {
            TransactionDS.ClientsDataTable clients = new TransactionDS.ClientsDataTable();

            if (_salesForce.IsValidSession)
            {
                string query = string.Empty;
                if(Processor.Equals("T2P"))
                    query = @"SELECT     Id, Name, nudebt__Processor_Acct__c, nudebt__Import_ID__c,nudebt__Processor__r.Name, nudebt__Processor_Name__c, 
                                            nudebt__Account_Balance__c, nudebt__Processor_Status__c, nudebt__First_Name__c, nudebt__Last_Name__c, nudebt__Mark_For_Sync__c, 
                                            nudebt__Fee_Template_Meta__c, nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c, 
                                            nudebt_client_status__c, nudebt_client_status_description__c, nudebt_document_status__c, nudebt_document_status_description__c,
                                            Nudebt_Processor_Created_Date__c                           
                                FROM        nudebt__Client__c
                                WHERE       nudebt__Processor_Status__c = 'Active'
                                AND         nudebt__Processor_Acct__c <> ''
                                AND         (nudebt__Processor__r.Name = 'T2P-Hybrid' OR nudebt__Processor__r.Name = 'T2P-ProtectionProgram')";
                else
                    query = @"SELECT     Id, Name, nudebt__Processor_Acct__c, nudebt__Import_ID__c,nudebt__Processor__r.Name, nudebt__Processor_Name__c, 
                                            nudebt__Account_Balance__c, nudebt__Processor_Status__c, nudebt__First_Name__c, nudebt__Last_Name__c, nudebt__Mark_For_Sync__c, 
                                            nudebt__Fee_Template_Meta__c, nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c, 
                                            nudebt_client_status__c, nudebt_client_status_description__c, nudebt_document_status__c, nudebt_document_status_description__c,
                                            Nudebt_Processor_Created_Date__c                           
                                FROM        nudebt__Client__c
                                WHERE       nudebt__Processor_Status__c = 'Active'
                                AND         nudebt__Processor_Acct__c <> ''
                                AND         nudebt__Processor__r.Name = '" + Processor + "'";

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                bool isDone = false;

                do
                {
                    if (qr != null && qr.records != null)
                    {
                        // iterate through every contact record returned by salesForce and retrieve the contact information for the primary contact
                        for (int i = 0; i < qr.records.Length; i++)
                        {
                            SFInterop.SForce.nudebt__Client__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client__c;

                            TransactionDS.ClientsRow row = clients.NewClientsRow();

                            row.FirstName = draft.nudebt__First_Name__c;
                            row.LastName = draft.nudebt__Last_Name__c;
                            row.DateOfBirth = new DateTime();
                            row.Address1 = string.Empty;
                            row.City = string.Empty;
                            row.State = string.Empty;
                            row.Country = string.Empty;
                            row.Zip = string.Empty;
                            row.PhoneNumber = string.Empty;
                            row.BankAccountNumber = string.Empty;
                            row.BankRoutingNumber = string.Empty;
                            row.AccountType = string.Empty;
                            row.IsNewAccount = false;
                            row.ExistsInSalesForce = true;
                            row.Status = "Active";
                            row.ProcessorStatus = "Active";
                            row.ProcessorAccountNum = string.Empty;
                            row.ImportID = string.Empty;
                            row.MarkForSync = draft.nudebt__Mark_for_Sync__c.HasValue ? draft.nudebt__Mark_for_Sync__c.Value : false;
                            if (draft.nudebt__Fee_Template_Meta__r != null && !String.IsNullOrEmpty(draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c))
                                row.PolicyID = draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c;
                            else
                                row.PolicyID = "";
                            row.ClientID = draft.Id;
                            row.Processor = draft.nudebt__Processor__r.Name;
                            row.ImportID = draft.nudebt__Import_ID__c;
                            row.AccountBalance = draft.nudebt__Account_Balance__c == null ? 0 : draft.nudebt__Account_Balance__c.Value;
                            row.ProcessorAccountNum = draft.nudebt__Processor_Acct__c;
                            row.ProcessorStatus = draft.nudebt__Processor_Status__c;
                            row.ClientIDforRAMandEPPS = int.Parse(String.Concat("0", draft.Name.Split(new Char[] { '-' })[1]));
                            row.ClientNameID = draft.Name;
                            row.ClientStatus = draft.nudebt__Client_Status__c == null ? "" : draft.nudebt__Client_Status__c;
                            row.ClientStatusDescription = draft.nudebt_client_status_description__c == null ? "" : draft.nudebt_client_status_description__c;
                            row.DocumentStatus = draft.nudebt_document_status__c == null ? "" : draft.nudebt_document_status__c;
                            row.DocumentStatusDescription = draft.nudebt_document_status_description__c == null ? "" : draft.nudebt_document_status_description__c;

                            if (draft.Nudebt_Processor_Created_Date__c != null)
                            {
                                row.ProccesorCreationDate = draft.Nudebt_Processor_Created_Date__c.Value;
                            }
                            else
                            {
                                row.ProccesorCreationDate = new DateTime();
                            }
                            clients.Rows.Add(row);

                        }

                        if (!qr.done)
                        {
                            qr = _salesForce.GetNextBatch(qr);
                        }
                        else
                        {
                            isDone = true;
                        }
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return clients;
        }

        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        public TransactionDS.ClientsDataTable GetModifiedClients(string ProcessorName)
        {
            TransactionDS.ClientsDataTable clients = new TransactionDS.ClientsDataTable();
            clients = this.GetClientsMarkedForSync(clients, ProcessorName );
            return clients;
        }

        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        /// 
        public TransactionDS.ClientsDataTable GetPendingClients(string processor)
        {
            TransactionDS.ClientsDataTable clients = new TransactionDS.ClientsDataTable();
             
            List<string> completed = new List<string>();

            if (_salesForce.IsValidSession)
            {
                string query = string.Empty;
                if(processor.Equals("T2P"))
                {
                    query = @"SELECT Id, Name, nudebt__First_Name__c, nudebt__Last_Name__c, nudebt__Date_of_Birth__c,
                                        nudebt__Address_Line_1__c, nudebt__City__c, nudebt__State__c, nudebt__Postal_Code__c, 
                                        nudebt__Best_Phone__c, nudebt__Home_Phone__c, nudebt__Cell_phone__c,
                                        nudebt__Social_Security_Number__c, nudebt__SSN_ENC__c,
                                        nudebt__Account_Number__c, nudebt__Account_Number_Enc__c,
                                        nudebt__Routing_Number__c, nudebt__Account_Type__c, 
                                        nudebt__Fee_Template_Meta__c, nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c,
                                        nudebt__Processor__c,nudebt__Processor__r.nudebt__Company_Name__c, nudebt__Processor__r.Name,
                                        nudebt__Processor_Acct__c, nudebt__Processor_Status__c, nudebt__Sync_Date__c, nudebt__Email_Address__c,
                                        nudebt__Name_On_Account__c, nudebt__Bank_Name__c
                                FROM    nudebt__Client__c
                                WHERE   nudebt__Processor__c != NULL AND nudebt__Processor_Status__c = 'Pending Creation' 
                                        AND (nudebt__Processor__r.Name='T2P-Hybrid' OR nudebt__Processor__r.Name='T2P-ProtectionProgram')";
                }
                else
                {
                  query = @"SELECT Id, Name, nudebt__First_Name__c, nudebt__Last_Name__c, nudebt__Date_of_Birth__c,
                                        nudebt__Address_Line_1__c, nudebt__City__c, nudebt__State__c, nudebt__Postal_Code__c, 
                                        nudebt__Best_Phone__c, nudebt__Home_Phone__c, nudebt__Cell_phone__c,
                                        nudebt__Social_Security_Number__c, nudebt__SSN_ENC__c,
                                        nudebt__Account_Number__c, nudebt__Account_Number_Enc__c,
                                        nudebt__Routing_Number__c, nudebt__Account_Type__c, 
                                        nudebt__Fee_Template_Meta__c, nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c,
                                        nudebt__Processor__c,nudebt__Processor__r.nudebt__Company_Name__c, nudebt__Processor__r.Name,
                                        nudebt__Processor_Acct__c, nudebt__Processor_Status__c, nudebt__Sync_Date__c, nudebt__Email_Address__c,
                                        nudebt__Name_On_Account__c, nudebt__Bank_Name__c
                                FROM    nudebt__Client__c
                                WHERE   nudebt__Processor__c != NULL AND nudebt__Processor_Status__c = 'Pending Creation' AND nudebt__Processor__r.Name='" + processor + "'";
                }
                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                bool isDone = false;

                do
                {

                    if (qr != null && qr.records != null)
                    {
                        // iterate through every contact record returned by salesForce and retrieve the contact information for the primary contact
                        for (int i = 0; i < qr.size; i++)
                        {
                            SFInterop.SForce.nudebt__Client__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client__c;

                            if (!completed.Contains(draft.Id) && draft.nudebt__Processor__c != null)
                            {
                                TransactionDS.ClientsRow row = clients.NewClientsRow();

                                // Using 'Name' field in for RAM Client ID preceded with zero as 5 digits are essential in RAM.
                                row.ClientID = draft.Id;
                                row.FirstName = draft.nudebt__First_Name__c;
                                row.LastName = draft.nudebt__Last_Name__c;
                                if (draft.nudebt__Date_of_Birth__c != null)
                                {
                                    row.DateOfBirth = draft.nudebt__Date_of_Birth__c.Value;
                                }
                                //row.DateOfBirth = draft.nudebt__Date_of_Birth__c == null ?   DateTime.Now : draft.nudebt__Date_of_Birth__c.Value;
                                row.Address1 = draft.nudebt__Address_Line_1__c == null ? string.Empty : draft.nudebt__Address_Line_1__c;
                                //row.Address2 = draft.Address_Line_1__c == null ? string.Empty : draft.Address_Line_1__c;
                                row.City = draft.nudebt__City__c == null ? string.Empty : draft.nudebt__City__c;
                                row.State = draft.nudebt__State__c == null ? string.Empty : draft.nudebt__State__c;
                                row.Country = "US";
                                //row.Country = draft.County__c == null ? string.Empty : draft.County__c;
                                row.Zip = draft.nudebt__Postal_Code__c == null ? string.Empty : draft.nudebt__Postal_Code__c;
                                row.Processor = draft.nudebt__Processor__r.Name == null ? "" : draft.nudebt__Processor__r.Name;
                                row.ProcessorAccountNum = draft.nudebt__Processor_Acct__c;

                                row.SocialSecurityNumber
                                    = String.IsNullOrEmpty(draft.nudebt__SSN_ENC__c)
                                        ? (String.IsNullOrEmpty(draft.nudebt__Social_Security_Number__c) ? null : draft.nudebt__Social_Security_Number__c)
                                        : draft.nudebt__SSN_ENC__c;

                                row.BankAccountNumber
                                    = String.IsNullOrEmpty(draft.nudebt__Account_Number_Enc__c)
                                    ? (String.IsNullOrEmpty(draft.nudebt__Account_Number__c) ? null : draft.nudebt__Account_Number__c)
                                    : draft.nudebt__Account_Number_Enc__c;

                                row.BankRoutingNumber = draft.nudebt__Routing_Number__c;
                                ////Credit Card Info Validation
                                //row.DebitCardNumber = draft.nudebt__Debit_Card_Number__c;
                                //row.CVVNumber = draft.nudebt__CVV_Code__c;
                                //row.CardExpiaryDate = draft.nudebt__Debit_Card_Expires__c.Value;
                                row.AccountType = draft.nudebt__Account_Type__c;
                                if (draft.nudebt__Fee_Template_Meta__r != null && !String.IsNullOrEmpty(draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c))
                                    row.PolicyID = draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c;
                                else
                                    row.PolicyID = "";
                                row.IsNewAccount = true;
                                row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : draft.nudebt__Sync_Date__c.Value;
                                row.Status = "Active";
                                row.ProcessorStatus = "Pending Creation";
                                row.LastStatus = row.ProcessorStatus;

                                //Added New Columns
                                row.Email = draft.nudebt__Email_Address__c;
                                //row.FeeSplitGroupID = draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c == null ? string.Empty : draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c;

                                //row.FeeSplitGroupID = draft.nudebt__Fee_Template_Meta__r == null ? string.Empty : (draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c == null ? string.Empty : draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c);

                                row.FeeSplitGroupID = draft.nudebt__Fee_Template_Meta__r == null ? null : (draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c == null ? null : draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c);
                                row.NameonAccount = draft.nudebt__Name_On_Account__c == null ? string.Empty : draft.nudebt__Name_On_Account__c;
                                row.BankName = draft.nudebt__Bank_Name__c == null ? null : draft.nudebt__Bank_Name__c;
                                row.ClientIDforRAMandEPPS = int.Parse(String.Concat("0", draft.Name.Split(new Char[] { '-' })[1]));
                                row.ClientNameID = draft.Name;
                                //row.AffiateID = draft.nudebt__Fee_Template_Meta__c;
                                row.PhoneNumber = string.Empty;
                                row.Phone2 = string.Empty;
                                row.Phone3 = string.Empty;

                                if (draft.nudebt__Home_Phone__c != null)
                                {
                                    row.Phone3 = draft.nudebt__Home_Phone__c;
                                    
                                }
                                if (draft.nudebt__Cell_phone__c != null)
                                {
                                    row.Phone2 = draft.nudebt__Cell_phone__c;
                                    
                                }
                                if (draft.nudebt__Best_Phone__c != null)
                                {
                                    row.PhoneNumber = draft.nudebt__Best_Phone__c;
                                    row.PhoneNumber = this.FormatPhone(row.PhoneNumber);
                                }

                                row.Phone2 = this.FormatPhone(row.Phone2);
                                row.Phone3 = this.FormatPhone(row.Phone3);
                                row.RowNumber = clients.Count + 1;
                                if (draft.nudebt__Fee_Template_Meta__c != null && processor.Equals("RAMS") )
                                {
                                    //Find and ADD Affilliate ID 
                                    query = @"SELECT nudebt__Processor_Acct__c FROM Account WHERE Id IN 
                                        (SELECT nudebt__Account__c FROM nudebt__Maintenance_Fee__c WHERE nudebt__Fee_ID__c = '1' 
                                        AND nudebt__Fee_Template__c = '" + draft.nudebt__Fee_Template_Meta__c + "')";

                                    SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query);
                                    if (qr2 != null && qr2.records != null)
                                    {
                                        SFInterop.SForce.Account draft2 = qr2.records[0] as SFInterop.SForce.Account;
                                        row.AffiateID = draft2.nudebt__Processor_Acct__c;
                                    }
                                }
                                else
                                {
                                    row.AffiateID = null;
                                }

                                clients.Rows.Add(row);
                                completed.Add(draft.Id);
                            }
                        }

                        if (!qr.done)
                        {
                            qr = _salesForce.GetNextBatch(qr);
                        }
                        else
                        {
                            isDone = true;
                        }
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return clients;
        }

        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        /// 
        public TransactionDS.ClientsDataTable GetAuthorizePendingClients(string processor)
        {
            TransactionDS.ClientsDataTable clients = new TransactionDS.ClientsDataTable();

            List<string> completed = new List<string>();

            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT Id, Name, nudebt__First_Name__c, nudebt__Last_Name__c, nudebt__Date_of_Birth__c,
                                        nudebt__Address_Line_1__c, nudebt__City__c, nudebt__State__c, nudebt__Postal_Code__c, 
                                        nudebt__Best_Phone__c, nudebt__Home_Phone__c, nudebt__Cell_phone__c,
                                        nudebt__Social_Security_Number__c, nudebt__SSN_ENC__c,
                                        nudebt__Account_Number__c, nudebt__Account_Number_Enc__c,
                                        nudebt__Routing_Number__c, nudebt__Account_Type__c, 
                                        nudebt__Fee_Template_Meta__c, nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c,
                                        nudebt__Processor__c,nudebt__Processor__r.nudebt__Company_Name__c, nudebt__Processor__r.Name,
                                        nudebt__Processor_Acct__c, nudebt__Processor_Status__c, nudebt__Sync_Date__c, nudebt__Email_Address__c,
                                        nudebt__Name_On_Account__c, nudebt__Bank_Name__c,
                                        nudebt__Billing_Add1__c, nudebt__Billing_City__c,nudebt__Billing_ST__c,nudebt__Billing_Postal__c
                            FROM        nudebt__Client__c
                            WHERE   nudebt__Processor__c != NULL AND (nudebt__Processor_Status__c = 'Pending Creation' OR nudebt__Processor_Status__c = 'Errored') AND nudebt__Processor__r.Name='" + processor + "'";

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                bool isDone = false;

                do
                {

                    if (qr != null && qr.records != null)
                    {
                        // iterate through every contact record returned by salesForce and retrieve the contact information for the primary contact
                        for (int i = 0; i < qr.size; i++)
                        {
                            SFInterop.SForce.nudebt__Client__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client__c;

                            if (!completed.Contains(draft.Id) && draft.nudebt__Processor__c != null)
                            {
                                TransactionDS.ClientsRow row = clients.NewClientsRow();

                                // Using 'Name' field in for RAM Client ID preceded with zero as 5 digits are essential in RAM.
                                row.ClientID = draft.Id;
                                row.FirstName = String.IsNullOrEmpty(draft.nudebt__First_Name__c) ? string.Empty : draft.nudebt__First_Name__c;
                                row.LastName = String.IsNullOrEmpty(draft.nudebt__Last_Name__c) ? string.Empty : draft.nudebt__Last_Name__c;
                                if (draft.nudebt__Date_of_Birth__c != null)
                                {
                                    row.DateOfBirth = draft.nudebt__Date_of_Birth__c.Value;
                                }
                                //row.DateOfBirth = draft.nudebt__Date_of_Birth__c == null ?   DateTime.Now : draft.nudebt__Date_of_Birth__c.Value;
                                row.Address1 = draft.nudebt__Billing_Add1__c == null ? string.Empty : draft.nudebt__Billing_Add1__c;
                                //row.Address2 = draft.Address_Line_1__c == null ? string.Empty : draft.Address_Line_1__c;
                                row.City = draft.nudebt__Billing_City__c == null ? string.Empty : draft.nudebt__Billing_City__c;
                                row.State = draft.nudebt__Billing_ST__c == null ? "NY" : draft.nudebt__Billing_ST__c;
                                row.Country = "US";
                                //row.Country = draft.County__c == null ? string.Empty : draft.County__c;
                                row.Zip = draft.nudebt__Billing_Postal__c == null ? string.Empty : draft.nudebt__Billing_Postal__c;
                                row.Processor = processor;
                                row.ProcessorAccountNum = draft.nudebt__Processor_Acct__c;

                                row.SocialSecurityNumber
                                    = String.IsNullOrEmpty(draft.nudebt__SSN_ENC__c)
                                        ? (String.IsNullOrEmpty(draft.nudebt__Social_Security_Number__c) ? null : draft.nudebt__Social_Security_Number__c)
                                        : draft.nudebt__SSN_ENC__c;

                                row.BankAccountNumber
                                    = String.IsNullOrEmpty(draft.nudebt__Account_Number_Enc__c)
                                    ? (String.IsNullOrEmpty(draft.nudebt__Account_Number__c) ? string.Empty : draft.nudebt__Account_Number__c)
                                    : draft.nudebt__Account_Number_Enc__c;

                                row.BankRoutingNumber = String.IsNullOrEmpty(draft.nudebt__Routing_Number__c) ? string.Empty : draft.nudebt__Routing_Number__c;
                                ////Credit Card Info Validation
                                //row.DebitCardNumber = draft.nudebt__Debit_Card_Number__c;
                                //row.CVVNumber = draft.nudebt__CVV_Code__c;
                                //row.CardExpiaryDate = draft.nudebt__Debit_Card_Expires__c.Value;
                                row.AccountType = draft.nudebt__Account_Type__c;
                                if (draft.nudebt__Fee_Template_Meta__r != null && !String.IsNullOrEmpty(draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c))
                                    row.PolicyID = draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c;
                                else
                                    row.PolicyID = "";
                                row.IsNewAccount = true;
                                row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : draft.nudebt__Sync_Date__c.Value;
                                row.Status = "Active";
                                row.ProcessorStatus = "Pending Creation";
                                row.LastStatus = row.ProcessorStatus;

                                //Added New Columns
                                row.Email = draft.nudebt__Email_Address__c;
                                //row.FeeSplitGroupID = draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c == null ? string.Empty : draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c;

                                //row.FeeSplitGroupID = draft.nudebt__Fee_Template_Meta__r == null ? string.Empty : (draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c == null ? string.Empty : draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c);

                                row.FeeSplitGroupID = draft.nudebt__Fee_Template_Meta__r == null ? null : (draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c == null ? null : draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c);
                                row.NameonAccount = draft.nudebt__Name_On_Account__c == null ? string.Empty : draft.nudebt__Name_On_Account__c;
                                row.BankName = draft.nudebt__Bank_Name__c == null ? string.Empty : draft.nudebt__Bank_Name__c;
                                row.ClientIDforRAMandEPPS = int.Parse(String.Concat("0", draft.Name.Split(new Char[] { '-' })[1]));
                                row.AffiateID = draft.nudebt__Fee_Template_Meta__c;
                                row.PhoneNumber = string.Empty;
                                row.Phone2 = string.Empty;
                                row.Phone3 = string.Empty;

                                if (draft.nudebt__Home_Phone__c != null)
                                {
                                    row.Phone3 = draft.nudebt__Home_Phone__c;

                                }
                                if (draft.nudebt__Cell_phone__c != null)
                                {
                                    row.Phone2 = draft.nudebt__Cell_phone__c;

                                }
                                if (draft.nudebt__Best_Phone__c != null)
                                {
                                    row.PhoneNumber = draft.nudebt__Best_Phone__c;
                                    row.PhoneNumber = this.FormatPhone(row.PhoneNumber);
                                }

                                row.Phone2 = this.FormatPhone(row.Phone2);
                                row.Phone3 = this.FormatPhone(row.Phone3);

                                if (draft.nudebt__Fee_Template_Meta__c != null)
                                {
                                    //Find and ADD Affilliate ID 
                                    query = @"SELECT nudebt__Processor_Acct__c FROM Account WHERE Id IN 
                                        (SELECT nudebt__Account__c FROM nudebt__Maintenance_Fee__c WHERE nudebt__Fee_ID__c = '1' 
                                        AND nudebt__Fee_Template__c = '" + draft.nudebt__Fee_Template_Meta__c + "')";

                                    SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query);
                                    if (qr2 != null && qr2.records != null)
                                    {
                                        SFInterop.SForce.Account draft2 = qr2.records[0] as SFInterop.SForce.Account;
                                        row.AffiateID = draft2.nudebt__Processor_Acct__c;
                                    }
                                }
                                else
                                {
                                    row.AffiateID = null;
                                }

                                clients.Rows.Add(row);
                                completed.Add(draft.Id);
                            }
                        }

                        if (!qr.done)
                        {
                            qr = _salesForce.GetNextBatch(qr);
                        }
                        else
                        {
                            isDone = true;
                        }
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return clients;
        }

        /// <summary>
        /// Retrieves clients related to first data
        /// </summary>
        /// <param name="processor">Patment processor</param>
        public TransactionDS.ClientsDataTable GetFDClients(string processor)
        {
            TransactionDS.ClientsDataTable clients = new TransactionDS.ClientsDataTable();

            List<string> completed = new List<string>();

            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT Id, Name, nudebt__First_Name__c, nudebt__Last_Name__c, nudebt__Date_of_Birth__c,
                                        nudebt__Address_Line_1__c, nudebt__City__c, nudebt__State__c, nudebt__Postal_Code__c, 
                                        nudebt__Best_Phone__c, nudebt__Home_Phone__c, nudebt__Cell_phone__c,
                                        nudebt__Social_Security_Number__c, nudebt__SSN_ENC__c,
nudebt__Debit_Card_Expires__c,
nudebt__Client_Full_Name__c,
nudebt__Debit_Card_Number__c,
                                        nudebt__Account_Number__c, nudebt__Account_Number_Enc__c,
                                        nudebt__Routing_Number__c, nudebt__Account_Type__c, 
                                        nudebt__Fee_Template_Meta__c, nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c,
                                        nudebt__Processor__c,nudebt__Processor__r.nudebt__Company_Name__c, nudebt__Processor__r.Name,
                                        nudebt__Processor_Acct__c, nudebt__Processor_Status__c, nudebt__Sync_Date__c, nudebt__Email_Address__c,
                                        nudebt__Name_On_Account__c, nudebt__Bank_Name__c,
                                        nudebt__Billing_Add1__c, nudebt__Billing_City__c,nudebt__Billing_ST__c,nudebt__Billing_Postal__c
                            FROM        nudebt__Client__c
                            WHERE   nudebt__Processor__c != NULL AND (nudebt__Processor_Status__c = 'Pending Creation' OR nudebt__Processor_Status__c = 'Errored') AND nudebt__Processor__r.Name='" + processor + "'";

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                bool isDone = false;

                do
                {

                    if (qr != null && qr.records != null)
                    {
                        // iterate through every contact record returned by salesForce and retrieve the contact information for the primary contact
                        for (int i = 0; i < qr.size; i++)
                        {
                            SFInterop.SForce.nudebt__Client__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client__c;

                            if (!completed.Contains(draft.Id) && draft.nudebt__Processor__c != null)
                            {
                                TransactionDS.ClientsRow row = clients.NewClientsRow();

                                // Using 'Name' field in for RAM Client ID preceded with zero as 5 digits are essential in RAM.
                                row.ClientID = draft.Id;
                                row.FirstName = String.IsNullOrEmpty(draft.nudebt__First_Name__c) ? string.Empty : draft.nudebt__First_Name__c;
                                row.LastName = String.IsNullOrEmpty(draft.nudebt__Last_Name__c) ? string.Empty : draft.nudebt__Last_Name__c;
                                if (draft.nudebt__Date_of_Birth__c != null)
                                {
                                    row.DateOfBirth = draft.nudebt__Date_of_Birth__c.Value;
                                }

                                row.ClientNameID = draft.nudebt__Client_Full_Name__c;
                                row.CardExpiaryDate = Convert.ToDateTime(draft.nudebt__Debit_Card_Expires__c);
                                row.DebitCardNumber = draft.nudebt__Debit_Card_Number__c == null ? string.Empty : draft.nudebt__Debit_Card_Number__c;

                                row.Address1 = draft.nudebt__Billing_Add1__c == null ? string.Empty : draft.nudebt__Billing_Add1__c;
                                row.City = draft.nudebt__Billing_City__c == null ? string.Empty : draft.nudebt__Billing_City__c;
                                row.State = draft.nudebt__Billing_ST__c == null ? "NY" : draft.nudebt__Billing_ST__c;
                                row.Country = "US";
                                row.Zip = draft.nudebt__Billing_Postal__c == null ? string.Empty : draft.nudebt__Billing_Postal__c;
                                row.Processor = processor;
                                row.ProcessorAccountNum = draft.nudebt__Processor_Acct__c;

                                row.SocialSecurityNumber
                                    = String.IsNullOrEmpty(draft.nudebt__SSN_ENC__c)
                                        ? (String.IsNullOrEmpty(draft.nudebt__Social_Security_Number__c) ? null : draft.nudebt__Social_Security_Number__c)
                                        : draft.nudebt__SSN_ENC__c;

                                row.BankAccountNumber
                                    = String.IsNullOrEmpty(draft.nudebt__Account_Number_Enc__c)
                                    ? (String.IsNullOrEmpty(draft.nudebt__Account_Number__c) ? string.Empty : draft.nudebt__Account_Number__c)
                                    : draft.nudebt__Account_Number_Enc__c;

                                row.BankRoutingNumber = String.IsNullOrEmpty(draft.nudebt__Routing_Number__c) ? string.Empty : draft.nudebt__Routing_Number__c;
                                ////Credit Card Info Validation
                                //row.DebitCardNumber = draft.nudebt__Debit_Card_Number__c;
                                //row.CVVNumber = draft.nudebt__CVV_Code__c;
                                //row.CardExpiaryDate = draft.nudebt__Debit_Card_Expires__c.Value;
                                row.AccountType = draft.nudebt__Account_Type__c;
                                if (draft.nudebt__Fee_Template_Meta__r != null && !String.IsNullOrEmpty(draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c))
                                    row.PolicyID = draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c;
                                else
                                    row.PolicyID = "";
                                row.IsNewAccount = true;
                                row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : draft.nudebt__Sync_Date__c.Value;
                                row.Status = "Active";
                                row.ProcessorStatus = "Pending Creation";
                                row.LastStatus = row.ProcessorStatus;

                                //Added New Columns
                                row.Email = draft.nudebt__Email_Address__c;
                                //row.FeeSplitGroupID = draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c == null ? string.Empty : draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c;

                                //row.FeeSplitGroupID = draft.nudebt__Fee_Template_Meta__r == null ? string.Empty : (draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c == null ? string.Empty : draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c);

                                row.FeeSplitGroupID = draft.nudebt__Fee_Template_Meta__r == null ? null : (draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c == null ? null : draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c);
                                row.NameonAccount = draft.nudebt__Name_On_Account__c == null ? string.Empty : draft.nudebt__Name_On_Account__c;
                                row.BankName = draft.nudebt__Bank_Name__c == null ? string.Empty : draft.nudebt__Bank_Name__c;
                                row.ClientIDforRAMandEPPS = int.Parse(String.Concat("0", draft.Name.Split(new Char[] { '-' })[1]));
                                row.AffiateID = draft.nudebt__Fee_Template_Meta__c;
                                row.PhoneNumber = string.Empty;
                                row.Phone2 = string.Empty;
                                row.Phone3 = string.Empty;

                                if (draft.nudebt__Home_Phone__c != null)
                                {
                                    row.Phone3 = draft.nudebt__Home_Phone__c;

                                }
                                if (draft.nudebt__Cell_phone__c != null)
                                {
                                    row.Phone2 = draft.nudebt__Cell_phone__c;

                                }
                                if (draft.nudebt__Best_Phone__c != null)
                                {
                                    row.PhoneNumber = draft.nudebt__Best_Phone__c;
                                    row.PhoneNumber = this.FormatPhone(row.PhoneNumber);
                                }
                                row.ClientNameID = draft.Name;
                                row.Phone2 = this.FormatPhone(row.Phone2);
                                row.Phone3 = this.FormatPhone(row.Phone3);
                                row.RowNumber = clients.Count + 1;
                                if (draft.nudebt__Fee_Template_Meta__c != null)
                                {
                                    //Find and ADD Affilliate ID 
                                    query = @"SELECT nudebt__Processor_Acct__c FROM Account WHERE Id IN 
                                        (SELECT nudebt__Account__c FROM nudebt__Maintenance_Fee__c WHERE nudebt__Fee_ID__c = '1' 
                                        AND nudebt__Fee_Template__c = '" + draft.nudebt__Fee_Template_Meta__c + "')";

                                    SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query);
                                    if (qr2 != null && qr2.records != null)
                                    {
                                        SFInterop.SForce.Account draft2 = qr2.records[0] as SFInterop.SForce.Account;
                                        row.AffiateID = draft2.nudebt__Processor_Acct__c;
                                    }
                                }
                                else
                                {
                                    row.AffiateID = null;
                                }

                                clients.Rows.Add(row);
                                completed.Add(draft.Id);
                            }
                        }

                        if (!qr.done)
                        {
                            qr = _salesForce.GetNextBatch(qr);
                        }
                        else
                        {
                            isDone = true;
                        }
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return clients;
        }

        /// <summary>
        /// Writes a list of errors to the SalesForce Errors object
        /// </summary>
        /// <param name="errorList">The error list</param>
        public void WriteSuccessLog(TransactionDS.ClientDepositDataTable errorList)
        {
            try
            {
                List<SFInterop.SForce.sObject> errors = new List<SFInterop.SForce.sObject>();
                foreach (TransactionDS.ClientDepositRow row in errorList.Rows)
                {
                    SFInterop.SForce.nudebt__Error__c error = new SFInterop.SForce.nudebt__Error__c();
                    error.nudebt__Error_description__c = "Record Id: " + row.RecordID + ", Client Id: " + row.ClientID + ", Message: " + row.SyncNotes;
                    error.nudebt__Error_Date__c = DateTime.Now;
                    error.nudebt__Object_ID__c = row.RecordID;
                    if (row.Type.Equals("A"))
                    {
                        error.nudebt__Client_Payment__c = error.nudebt__Object_ID__c;
                        error.nudebt__Related_Object__c = "nudebt__Client_Payment__c";
                    }
                    else
                    {
                        error.nudebt__Related_Object__c = "nudebt__Client_Payment_Fee__c";
                        error.nudebt__Client_Payment_Fee__c = error.nudebt__Object_ID__c;
                    }

                    errors.Add(error);
                }


                //this._salesForce.InsertBatch(errors);
                SFInterop.SForce.SaveResult[] results2 = this._salesForce.InsertBatch(errors);
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Writes a list of errors to the SalesForce Errors object
        /// </summary>
        /// <param name="errorList">The error list</param>
        public void WriteClientLog(TransactionDS.ClientsDataTable errorList)
        {
            try
            {
                List<SFInterop.SForce.sObject> errors = new List<SFInterop.SForce.sObject>();

                foreach (TransactionDS.ClientsRow row in errorList.Rows)
                {
                    SFInterop.SForce.nudebt__Error__c error = new SFInterop.SForce.nudebt__Error__c();
                    error.nudebt__Error_description__c = "Record Id: " + row.ClientID + ", Message: " + row.SyncDescription;
                    error.nudebt__Error_Date__c = DateTime.Now;
                    error.nudebt__Related_Object__c = "nudebt__Client__c";

                    error.nudebt__Object_ID__c = row.ClientID;
                    error.nudebt__Client__c = error.nudebt__Object_ID__c;

                    errors.Add(error);
                }
                this._salesForce.InsertBatch(errors);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieves a list of client records that were modified within the last week
        /// date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        private TransactionDS.ClientsDataTable GetClientsMarkedForSync(TransactionDS.ClientsDataTable clients, string Processor)
        {
            if (_salesForce.IsValidSession)
            {
                string query = string.Empty;
                if (Processor.Equals("T2P"))
                query = @"SELECT     Id, Name, nudebt__First_Name__c, nudebt__Last_Name__c, nudebt__Cell_phone__c, nudebt__Date_of_Birth__c, nudebt__Mark_for_Sync__c, 
                                            nudebt__Social_Security_Number__c, nudebt__Address_Line_1__c, nudebt__City__c, nudebt__State__c,
                                            nudebt__Postal_Code__c, nudebt__Home_Phone__c, nudebt__Account_Number__c, nudebt__Processor__r.nudebt__Company_Name__c,
                                            nudebt__Routing_Number__c, nudebt__Best_Phone__c, nudebt__Sync_Date__c, nudebt__Import_ID__c, nudebt__Processor__r.Name, 
                                            nudebt__Processor_Acct__c, nudebt__Account_Type__c, nudebt__Account_Balance__c, nudebt__Fee_Template_Meta__c, nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c,
                                            nudebt__Email__c, nudebt__Name_On_Account__c, nudebt__Bank_Name__c, nudebt__Account_Number_Enc__c, nudebt__SSN_ENC__c
                                FROM        nudebt__Client__c
                                WHERE       nudebt__Mark_for_Sync__c = TRUE
                                AND         nudebt__Processor_Status__c = 'Active'
                                AND         nudebt__Processor_Acct__c <> ''
                                AND         (nudebt__Processor__r.Name = 'T2P-Hybrid' OR nudebt__Processor__r.Name = 'T2P-ProtectionProgram')";
                else
                    query = @"SELECT     Id, Name, nudebt__First_Name__c, nudebt__Last_Name__c, nudebt__Cell_phone__c, nudebt__Date_of_Birth__c, nudebt__Mark_for_Sync__c, 
                                            nudebt__Social_Security_Number__c, nudebt__Address_Line_1__c, nudebt__City__c, nudebt__State__c,
                                            nudebt__Postal_Code__c, nudebt__Home_Phone__c, nudebt__Account_Number__c, nudebt__Processor__r.nudebt__Company_Name__c,
                                            nudebt__Routing_Number__c, nudebt__Best_Phone__c, nudebt__Sync_Date__c, nudebt__Import_ID__c, nudebt__Processor__r.Name,
                                            nudebt__Processor_Acct__c, nudebt__Account_Type__c, nudebt__Account_Balance__c, nudebt__Fee_Template_Meta__c, nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c,
                                            nudebt__Email__c, nudebt__Name_On_Account__c, nudebt__Bank_Name__c, nudebt__Account_Number_Enc__c, nudebt__SSN_ENC__c
                                FROM        nudebt__Client__c
                                WHERE       nudebt__Mark_for_Sync__c = TRUE
                                AND         nudebt__Processor_Status__c = 'Active'
                                AND         nudebt__Processor_Acct__c <> ''
                                AND         nudebt__Processor__r.Name = '" + Processor + "'";


                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                bool isDone = false;

                do
                {
                    if (qr != null && qr.records != null)
                    {
                        // iterate through every contact record returned by salesForce and retrieve the contact information for the primary contact
                        for (int i = 0; i < qr.records.Length; i++)
                        {
                            SFInterop.SForce.nudebt__Client__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client__c;

                            TransactionDS.ClientsRow row = clients.NewClientsRow();
                            row.ClientID = draft.Id;
                            row.ClientIDforRAMandEPPS = int.Parse(String.Concat("0", draft.Name.Split(new Char[] { '-' })[1]));
                            row.ClientNameID = draft.Name;
                            row.Email = draft.nudebt__Email__c == null ? string.Empty : draft.nudebt__Email__c;
                            if (draft.nudebt__Fee_Template_Meta__r != null)
                            {
                                row.FeeSplitGroupID = draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c == null ? string.Empty : draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c;
                            }
                            else
                            {
                                row.FeeSplitGroupID = string.Empty;
                            }

                            row.NameonAccount = draft.nudebt__Name_On_Account__c == null ? string.Empty : draft.nudebt__Name_On_Account__c;
                            row.BankName = draft.nudebt__Bank_Name__c == null ? string.Empty : draft.nudebt__Bank_Name__c;

                            row.SocialSecurityNumber
                               = String.IsNullOrEmpty(draft.nudebt__SSN_ENC__c)
                                   ? (String.IsNullOrEmpty(draft.nudebt__Social_Security_Number__c) ? "" : draft.nudebt__Social_Security_Number__c)
                                   : draft.nudebt__SSN_ENC__c;

                            row.FirstName = draft.nudebt__First_Name__c;
                            row.LastName = draft.nudebt__Last_Name__c;

                            //row.DateOfBirth = draft.nudebt__Date_of_Birth__c == null ? new DateTime() : draft.nudebt__Date_of_Birth__c.Value;
                            if (draft.nudebt__Date_of_Birth__c != null)
                            {
                                row.DateOfBirth = draft.nudebt__Date_of_Birth__c.Value;
                            }
                            row.Address1 = draft.nudebt__Address_Line_1__c == null ? string.Empty : draft.nudebt__Address_Line_1__c;
                            // row.Address2 = draft.nudebt__Address_Line_2__c == null ? string.Empty : draft.nudebt__Address_Line_2__c;
                            row.City = draft.nudebt__City__c == null ? string.Empty : draft.nudebt__City__c;
                            row.State = draft.nudebt__State__c == null ? string.Empty : draft.nudebt__State__c;
                            //row.Country = draft.MailingCountry == null ? string.Empty : draft.MailingCountry;
                            row.Country = "US";
                            row.Zip = draft.nudebt__Postal_Code__c == null ? string.Empty : draft.nudebt__Postal_Code__c;
                            // TODO bt - use name its
                            // row.Processor = draft.nudebt__Processor__r.nudebt__Company_Name__c; 
                            row.Processor = draft.nudebt__Processor__r.Name;
                            row.PhoneNumber = string.Empty;

                            row.BankAccountNumber
                                = String.IsNullOrEmpty(draft.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(draft.nudebt__Account_Number__c) ? null : draft.nudebt__Account_Number__c)
                                : draft.nudebt__Account_Number_Enc__c;

                            row.BankRoutingNumber = draft.nudebt__Routing_Number__c;
                            row.AccountBalance = draft.nudebt__Account_Balance__c == null ? 0 : (double)draft.nudebt__Account_Balance__c;
                            row.AccountType = draft.nudebt__Account_Type__c;
                            if (draft.nudebt__Fee_Template_Meta__r != null && !String.IsNullOrEmpty(draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c))
                                row.PolicyID = draft.nudebt__Fee_Template_Meta__r.nudebt__Program_ID__c;
                            else
                                row.PolicyID = "";
                            row.IsNewAccount = false;
                            row.ExistsInSalesForce = true;
                            row.Status = "Active";
                            row.ProcessorStatus = "Active";
                            row.ProcessorAccountNum = draft.nudebt__Processor_Acct__c;
                            row.ImportID = draft.nudebt__Import_ID__c;
                            row.MarkForSync = true;

                            //if (draft.nudebt__Home_Phone__c != null)
                            //{
                            //    row.PhoneNumber = draft.nudebt__Home_Phone__c;
                            //}
                            //else if (draft.nudebt__Cell_phone__c != null)
                            //{
                            //    row.PhoneNumber = draft.nudebt__Cell_phone__c;
                            //}
                            //else if (draft.nudebt__Best_Phone__c != null)
                            //{
                            //    row.PhoneNumber = draft.nudebt__Best_Phone__c;
                            //}

                            //row.PhoneNumber = this.FormatPhone(row.PhoneNumber);

                            row.Phone2 = string.Empty;
                            row.Phone3 = string.Empty;

                            if (draft.nudebt__Home_Phone__c != null)
                            {
                                row.Phone3 = draft.nudebt__Home_Phone__c;

                            }
                            if (draft.nudebt__Cell_phone__c != null)
                            {
                                row.Phone2 = draft.nudebt__Cell_phone__c;
                            }
                            if (draft.nudebt__Best_Phone__c != null)
                            {
                                row.PhoneNumber = draft.nudebt__Best_Phone__c;
                                row.PhoneNumber = this.FormatPhone(row.PhoneNumber);
                            }

                            row.Phone2 = this.FormatPhone(row.Phone2);
                            row.Phone3 = this.FormatPhone(row.Phone3);

                            if (draft.nudebt__Fee_Template_Meta__c != null)
                            {
                                //Find and ADD Affilliate ID 
                                query = @"SELECT nudebt__Processor_Acct__c FROM Account WHERE Id IN 
                                        (SELECT nudebt__Account__c FROM nudebt__Maintenance_Fee__c WHERE nudebt__Fee_ID__c = '1' 
                                        AND nudebt__Fee_Template__c = '" + draft.nudebt__Fee_Template_Meta__c + "')";

                                SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query);
                                if (qr2 != null && qr2.records != null)
                                {
                                    SFInterop.SForce.Account draft2 = qr2.records[0] as SFInterop.SForce.Account;
                                    row.AffiateID = draft2.nudebt__Processor_Acct__c;
                                }
                            }
                            else
                            {
                                row.AffiateID = null;
                            }

                            clients.Rows.Add(row);
                        }

                        if (!qr.done)
                        {
                            qr = _salesForce.GetNextBatch(qr);
                        }
                        else
                        {
                            isDone = true;
                        }
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return clients;
        }

        /// <summary>
        /// Formats a telephone number in the way the GCS system expects
        /// </summary>
        /// <param name="phoneNumber">The raw unformatted telephone number</param>
        /// <returns>Re-formatted telephone number</returns>
        private string FormatPhone(string phoneNumber)
        {
            phoneNumber = phoneNumber.Replace("(", string.Empty).Replace(")", string.Empty).Replace(" ", string.Empty).Replace("-", string.Empty);

            if (phoneNumber.Length == 10)
            {
                phoneNumber = phoneNumber.Substring(0, 3) + "-" + phoneNumber.Substring(3, 3) + "-" + phoneNumber.Substring(6);
            }
            else if (phoneNumber.Length > 10)
            {
                phoneNumber = phoneNumber.Substring(phoneNumber.Length - 10);
            }

            return phoneNumber;
        }

        /// <summary>
        /// Builds an error list from a SalesForce SaveResult 
        /// </summary>
        /// <param name="result">The result of a SalesForce create/update transaction</param>
        /// <param name="objectName">The name of the SalesForce object whose record was updated/created</param>
        /// <param name="resultList">The existing list of errors</param>
        /// <param name="errorList">The existing list of results</param>
        private void ProcessResults(SFInterop.SForce.SaveResult[] results, string objectName, ref List<string> resultList, ref List<string[]> errorList, List<string> clientIDs)
        {
            for (int i = 0; i < results.Length; i++)
            {
                //this.ProcessResult(results[i], objectName, ref resultList, ref errorList, clientIDs[i]);
                SFInterop.SForce.SaveResult result=results[i];
                this.ProcessResult(result, objectName, ref resultList, ref errorList, clientIDs[i]);
            } 
        }

        /// <summary>
        /// Builds an error list from a SalesForce SaveResult 
        /// </summary>
        /// <param name="result">The result of a SalesForce create/update transaction</param>
        /// <param name="objectName">The name of the SalesForce object whose record was updated/created</param>
        /// <param name="resultList">The existing list of errors</param>
        /// <param name="errorList">The existing list of results</param>
        private void ProcessResult(SFInterop.SForce.SaveResult result, string objectName, ref List<string> resultList, ref List<string[]> errorList, string id = "")
        {
            if (result.success)
            {
                resultList.Add(objectName + " with Record Id: " + result.id + " updated successfully!");
            }
            else
            {
                string error = string.Empty;

                foreach (SFInterop.SForce.Error err in result.errors)
                {
                    error += err.message + "  ";
                }

                resultList.Add(objectName + " with Record Id: " + id + " update failed!  " + error);
                errorList.Add(new string[] { error, objectName, id });
            }
        }

        /// <summary>
        /// Writes a single exception to the SalesForce error object
        /// </summary>
        /// <param name="ex">The system exception</param>
        public void WriteError(Exception ex, string id)
        {
            SFInterop.SForce.nudebt__Error__c error = new SFInterop.SForce.nudebt__Error__c();
            error.nudebt__Error_description__c = ex.Source + " " + ex.Message + "\r\n" + ex.StackTrace;
            error.nudebt__Error_Date__c = DateTime.Now;

            List<SFInterop.SForce.sObject> errors = new List<SFInterop.SForce.sObject>();
            errors.Add(error);
            this._salesForce.InsertBatch(errors);

            throw ex;
        }

        /// <summary>
        /// Writes a single exception to the SalesForce error object
        /// </summary>
        /// <param name="ex">The system exception</param>
        public void WriteCriticalError(Exception ex, string id)
        {
            SFInterop.SForce.nudebt__Error__c error = new SFInterop.SForce.nudebt__Error__c();
            error.nudebt__Error_description__c = "Critical: " + ex.Source + " " + ex.Message + "\r\n" + ex.StackTrace;
            error.nudebt__Error_Date__c = DateTime.Now;

            List<SFInterop.SForce.sObject> errors = new List<SFInterop.SForce.sObject>();
            errors.Add(error);
            this._salesForce.InsertBatch(errors);

            //throw ex;
        }

        /// <summary>
        /// Writes a list of errors to the SalesForce Errors object
        /// </summary>
        /// <param name="errorList">The error list</param>
        public void WriteError(List<string[]> errorList)
        {
            List<SFInterop.SForce.sObject> errors = new List<SFInterop.SForce.sObject>();

            foreach (string[] s in errorList)
            {
                SFInterop.SForce.nudebt__Error__c error = new SFInterop.SForce.nudebt__Error__c();
                error.nudebt__Error_description__c = s[0];
                error.nudebt__Error_Date__c = DateTime.Now;
                error.nudebt__Related_Object__c = s[1];
                error.nudebt__Object_ID__c = s[2];

                if (s[1] == null)
                {
                    error.nudebt__Payment_History__c = s[2];
                }
                else if (s[1].Trim() == "nudebt__Client__c")
                {
                    error.nudebt__Client__c = s[2];
                }
                else if (s[1].Trim() == "nudebt__Offer_Payment__c")
                {
                    error.nudebt__Offer_Payment__c = s[2];
                }
                else if (s[1].Trim() == "Account")
                {
                    error.nudebt__Related_Object__c += s[2];
                }
                else if (s[1].Trim() == "nudebt__Client_Payment_Fee__c")
                {
                    error.nudebt__Client_Payment_Fee__c = s[2];
                }
                else if (s[1].Trim() == "nudebt__Client_Payment__c")
                {
                    error.nudebt__Client_Payment__c = s[2];
                }
                else
                {
                    error.nudebt__Payment_History__c = s[2];
                }

                errors.Add(error);
            }

            this._salesForce.InsertBatch(errors);
        }

        /// <summary>
        /// Writes a list of errors to the SalesForce Errors object
        /// </summary>
        /// <param name="errorList">The error list</param>
        public void WriteError(System.Data.DataTable errorList, string objectName)
        {
            List<SFInterop.SForce.sObject> errors = new List<SFInterop.SForce.sObject>();
            string initialObjectName = (string) objectName.Clone();

            foreach (System.Data.DataRow row in errorList.Rows)
            {
                SFInterop.SForce.nudebt__Error__c error = new SFInterop.SForce.nudebt__Error__c();
                error.nudebt__Error_description__c = row["COLUMN_ID"].ToString() + " " + row["ERROR_TEXT"].ToString();
                error.nudebt__Error_Date__c = DateTime.Now;
                error.nudebt__Related_Object__c = objectName;
                if (!String.IsNullOrEmpty(error.nudebt__Object_ID__c) || row["ERROR_TEXT"].ToString().Contains("SFID: "))
                {
                    if (String.IsNullOrEmpty(error.nudebt__Object_ID__c))
                        error.nudebt__Object_ID__c = row["ERROR_TEXT"].ToString().Substring(row["ERROR_TEXT"].ToString().LastIndexOf("SFID: ") + 6);
                    switch (objectName.Trim())
                    {
                        case "nudebt__Client__c":
                        case "nudebt__Clients__c":
                            error.nudebt__Client__c = error.nudebt__Object_ID__c;
                            break;
                        case "nudebt__Offer_Payments__c":
                        case "nudebt__Offer_Payment__c":
                            error.nudebt__Offer_Payment__c = error.nudebt__Object_ID__c;
                            break;
                        case "nudebt__Client_Payments__c":
                        case "nudebt__Client_Payment__c":
                            if (row["ERROR_TYPE"].ToString() == "A")
                                error.nudebt__Client_Payment__c = error.nudebt__Object_ID__c;
                            else
                                error.nudebt__Client_Payment_Fee__c = error.nudebt__Object_ID__c;
                            break;
                        case "nudebt__Client_Payments_Fee__c":
                        case "nudebt__Client_Payment_Fee__c":
                            error.nudebt__Client_Payment_Fee__c = error.nudebt__Object_ID__c;
                            break;
                    }
                }
                
                errors.Add(error);
            }

            this._salesForce.InsertBatch(errors);
        }

        /// <summary>
        /// Creates a ClientPayment record for updates that need to be made to SalesForce
        /// </summary>
        /// <param name="row">The row containing the updated data</param>
        /// <returns>The new client payment record</returns>
        public SFInterop.SForce.nudebt__Client_Payment__c CreateDepositRow(TransactionDS.ClientDepositRow row)
        {
            SFInterop.SForce.nudebt__Client_Payment__c deposit = new SFInterop.SForce.nudebt__Client_Payment__c() { Id = row.RecordID };
            deposit.nudebt__Sync_Date__c = DateTime.Now;
            deposit.nudebt__Sync_Date__cSpecified = true;
            deposit.nudebt__Sync_Notes__c = row.SyncNotes;

            if (!row.IsAmountPaidNull())
            {
                deposit.nudebt__Amount_Paid__c = row.AmountPaid;
                deposit.nudebt__Amount_Paid__cSpecified = true;
            }
            else
            {
                if (!row.IsAmountNull())
                {
                    deposit.nudebt__Amount_Paid__c = row.Amount;
                    deposit.nudebt__Amount_Paid__cSpecified = true;
                }
            }

            if (!row.IsTransactionIDNull())
            {
                deposit.nudebt__Transaction_ID__c = row.TransactionID;
            }

            if (!row.IsStatusNull() && row.Status != null && !string.IsNullOrEmpty(row.Status))
            {
                // if the status of the payment has been updated, then set the payment status
                deposit.nudebt__Payment_Status__c = row.Status;
            }

            if (!row.IsClearedDateNull())
            {
                // if the payment is cleared
                deposit.nudebt__Payment_Cleared_Date__c = row.ClearedDate;
                deposit.nudebt__Payment_Cleared_Date__cSpecified = true;
            }

            return deposit;
        }

        /// <summary>
        /// Creates a ClientPayment record for updates that need to be made to SalesForce
        /// </summary>
        /// <param name="row">The row containing the updated data</param>
        /// <returns>The new client payment record</returns>
        public SFInterop.SForce.nudebt__Client_Payment_Fee__c CreateDepositFeeRow(TransactionDS.ClientDepositRow row)
        {
            SFInterop.SForce.nudebt__Client_Payment_Fee__c deposit = new SFInterop.SForce.nudebt__Client_Payment_Fee__c() { Id = row.RecordID };
            deposit.nudebt__Sync_Date__c = DateTime.Now;
            deposit.nudebt__Sync_Date__cSpecified = true;
            deposit.nudebt__Sync_Notes__c = row.SyncNotes;
            deposit.nudebt__Client_Payment__c = null;

            if (!row.IsTransactionIDNull())
            {
                deposit.nudebt__Transaction_ID__c = row.TransactionID;
            }
            if (!row.IsStatusNull() && row.Status != null && !string.IsNullOrEmpty(row.Status))
            {
                // if the status of the payment has been updated, then set the payment status
                deposit.nudebt__Status__c = row.Status;
            }
            if (!row.IsAmountPaidNull())
            {
                deposit.nudebt__Amount_Paid__c = row.AmountPaid;
            }
            else
                if (!row.IsAmountNull())
                    deposit.nudebt__Amount_Paid__c = row.Amount;

            if (!row.IsClearedDateNull())
            {
                // if the payment is cleared
                deposit.nudebt__Cleared_Date__c = row.ClearedDate;
                deposit.nudebt__Cleared_Date__cSpecified = true;
            }

            return deposit;
        }

        public SFInterop.SForce.sObject CreatePaymentRow(TransactionDS.OfferPaymentRow row)
        {
            SFInterop.SForce.nudebt__Offer_Payment__c payment = new SFInterop.SForce.nudebt__Offer_Payment__c() { Id = row.RecordID };
            payment.nudebt__Transaction_ID__c = row.TransactionID;
            payment.nudebt__Status__c = row.Status;
            payment.nudebt__Sync_Date__c = DateTime.Today;
            payment.nudebt__Sync_Date__cSpecified = true;

            return payment;
        }

        public void Logout()
        {
            try
            {
                this._salesForce.Logout();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Gets all transactions scheduled for processing within a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of transactions as a strongly typed dataset</returns>
        public TransactionDS GetRAMSTransactions(DateTime start, DateTime end, bool getWithdrawals, ref List<TransactionInfo> list, ref TransactionDS feeTemplate)
        {
            try
            {
                TransactionDS tds = new TransactionDS();
                this.GetRAMSDeposits(start, end, tds, ref list, ref feeTemplate);
                return tds;
            }
            catch
            {
                throw;
            }
        }
       
        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        public TransactionDS GetRAMSDeposits(DateTime start, DateTime end, TransactionDS tranDS, ref List<TransactionInfo> objList, ref TransactionDS feeTemplateOut)
        {
            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT     Id, nudebt__Payment_Amount__c, nudebt__Amount_Paid__c,
                                            nudebt__Payment_Scheduled_Date__c, nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                            nudebt__Client__c, nudebt__Client_Name__c,               
                                            nudebt__Client__r.nudebt__Bank_Name__c, 
                                            nudebt__Client__r.nudebt__Routing_Number__c, 
                                            nudebt__Client__r.nudebt__Account_Type__c, 
                                            nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client__r.nudebt__Account_Number__c, 
                                            nudebt__Client__r.nudebt__Processor__r.Name,
                                            nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c,
                                            nudebt__Client__r.nudebt__Processor_Acct__c, 
                                            nudebt__Client__r.nudebt__Import_ID__c,
                                            nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c
                                FROM        nudebt__Client_Payment__c  
                                WHERE       nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} AND nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd}
                                AND         (nudebt__Payment_Status__c = 'Pending' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored') 
                                AND         nudebt__Client__r.nudebt__Processor__r.Name='RAMS'
                                AND         nudebt__Client__r.nudebt__Processor_Status__c = 'Active'";

                string query2 = @"SELECT    Id, Name, nudebt__Fee_ID__c, nudebt__Transaction_ID__c,
                                            nudebt__Amount__c, nudebt__Amount_Paid__c, 
                                            nudebt__Status__c, nudebt__Client_Payment__c,
                                            nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__c, 
                                            nudebt__Client_Payment__r.nudebt__Client_Name__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c, nudebt__Client_Payment__r.Id, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c
                                FROM        nudebt__Client_Payment_Fee__c 
                                WHERE       (nudebt__Status__c = 'Pending' OR nudebt__Status__c = 'TimedOut' OR nudebt__Status__c = 'Errored')
                                AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} 
                                AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd}
                                AND         nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Status__c = 'Active'";


                string query3 = @"SELECT   Id, nudebt__Client_ID__c, nudebt__Payment_Scheduled_Date__c,nudebt__Processing_Fee__c,
                                           nudebt__Payment_Amount__c,nudebt__Amount_to_Savings__c, nudebt__Client__c FROM nudebt__Client_Payment__c                                           
                               WHERE       nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd}  
                               AND         nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd}  
                               AND         nudebt__Client__r.nudebt__Processor__r.Name='RAMS'
                               AND         (nudebt__Payment_Status__c='Pending' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored')";

                if (start.Date > end.Date)
                {
                    query = string.Format(query, end.Date, start.Date);
                    query2 = string.Format(query2, end.Date, start.Date);
                    query3 = string.Format(query3, end.Date, start.Date);
                }
                else
                {
                    query = string.Format(query, start.Date, end.Date);
                    query2 = string.Format(query2, start.Date, end.Date);
                    query3 = string.Format(query3, start.Date, end.Date);
                }

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query2);
                SFInterop.SForce.QueryResult qr3 = _salesForce.RunQuery(query3);
                bool isDone = false;

                do
                {
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;

                        TransactionDS.ClientDepositRow row = tranDS.ClientDeposit.NewClientDepositRow();

                        try
                        {
                            row.RecordID = draft.Id;
                            string x = draft.Name;
                            row.ClientID = draft.nudebt__Client__c;
                            row.ClientName = draft.nudebt__Client_Name__c;
                            row.BankAccountType = draft.nudebt__Client__r.nudebt__Account_Type__c;
                            row.BankAccountNumber
                                = String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number__c) ? "" : draft.nudebt__Client__r.nudebt__Account_Number__c)
                                : draft.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            row.BankRoutingNumber = draft.nudebt__Client__r.nudebt__Routing_Number__c;
                            row.BankName = draft.nudebt__Client__r.nudebt__Bank_Name__c;
                            row.Amount = draft.nudebt__Payment_Amount__c.Value;
                            if (draft.nudebt__Amount_Paid__c != null)
                                row.AmountPaid = draft.nudebt__Amount_Paid__c.Value;
                            row.Date = Convert.ToDateTime(draft.nudebt__Payment_Scheduled_Date__c);
                            row.ProcessorAccount = draft.nudebt__Client__r.nudebt__Processor_Acct__c;
                            row.TransactionID = draft.nudebt__Transaction_ID__c == null ? "" : draft.nudebt__Transaction_ID__c;
                            row.ParentDRCID = "";
                            row.Type = "A";
                            row.TypeDesc = "ACH Monthly Draft";

                            row.Status = "Pending";
                            row.ClientImportID = draft.nudebt__Client__r.nudebt__Import_ID__c;
                            row.ProcessorFee = draft.nudebt__Processing_Fee__c == null ? 0 : (double)draft.nudebt__Processing_Fee__c;
                            row.Processor = draft.nudebt__Client__r.nudebt__Processor__r.Name;
                            row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(draft.nudebt__Sync_Date__c);
                        }
                        catch
                        {
                            row.HasSyncError = "True";
                            row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            if (row.Amount > 0)
                                tranDS.ClientDeposit.Rows.Add(row);
                        }
                    }
                    for (int j = 0; j < qr2.size; j++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment_Fee__c fee = qr2.records[j] as SFInterop.SForce.nudebt__Client_Payment_Fee__c;
                        TransactionDS.ClientDepositRow rowf = feeTemplateOut.ClientDeposit.NewClientDepositRow();
                        try
                        {
                            rowf.RecordID = fee.Id;
                            string x = fee.Name;
                            rowf.ClientID = fee.nudebt__Client_Payment__r.nudebt__Client__c;
                            rowf.ClientName = fee.nudebt__Client_Payment__r.nudebt__Client_Name__c;
                            rowf.BankAccountType = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c;
                            rowf.BankAccountNumber
                                = String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c) ? "" : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c)
                                : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            rowf.BankRoutingNumber = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c;
                            rowf.BankName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c;
                            rowf.Amount = fee.nudebt__Amount__c.Value;
                            if (fee.nudebt__Amount_Paid__c != null)
                                rowf.AmountPaid = fee.nudebt__Amount_Paid__c.Value;
                            if (fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c != null)
                                rowf.Date = Convert.ToDateTime(fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c);
                            rowf.ProcessorAccount = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c;
                            rowf.TransactionID = fee.nudebt__Transaction_ID__c;
                            rowf.ParentDRCID = fee.nudebt__Client_Payment__c;
                            rowf.Type = fee.nudebt__Fee_ID__c == null ? "" : fee.nudebt__Fee_ID__c; 
                            rowf.TypeDesc = fee.Name;
                            //rowf.RecordName
                            rowf.Status = fee.nudebt__Status__c;
                            rowf.ClientImportID = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c;
                            /* TODO bt - use name its
                            if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c))
                                rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c;
                            else
                                if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name))
                            */
                            rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name;
                        }
                        catch
                        {
                            rowf.HasSyncError = "True";
                            rowf.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            if (rowf.Amount > 0)
                                feeTemplateOut.ClientDeposit.Rows.Add(rowf);
                        }
                    }

                    try
                    {
                        List<TransactionInfo> list = new List<TransactionInfo>();


                        for (int i = 0; i < qr3.size; i++)
                        {
                            SFInterop.SForce.nudebt__Client_Payment__c transactionDetail = qr3.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;
                            TransactionInfo transactionInfo = new TransactionInfo();
                            transactionInfo.TotalPaymentAmount = transactionDetail.nudebt__Payment_Amount__c.Value;
                            transactionInfo.TrasactionId = transactionDetail.Id;
                            transactionInfo.SavingsAmount = transactionDetail.nudebt__Processing_Fee__c == null ? 0 : (double)transactionDetail.nudebt__Processing_Fee__c;
                            transactionInfo.ClientId = transactionDetail.nudebt__Client_ID__c;
                            transactionInfo.ClientID_c = transactionDetail.nudebt__Client__c;
                            transactionInfo.ScheduleDate = Convert.ToDateTime(transactionDetail.nudebt__Payment_Scheduled_Date__c);
                            string query4 = @"SELECT nudebt__Amount__c,  nudebt__Client_Payment__c,  nudebt__Fee_ID__c  FROM nudebt__Client_Payment_Fee__c
                            where nudebt__Client_Payment__c='" + transactionDetail.Id + "' order by nudebt__Fee_ID__c";
                            query4 = string.Format(query4, end.Date, start.Date);

                            SFInterop.SForce.QueryResult qr4 = _salesForce.RunQuery(query4);


                            for (int j = 0; j < qr4.size; j++)
                            {
                                SFInterop.SForce.nudebt__Client_Payment_Fee__c feeTemplate = qr4.records[j] as SFInterop.SForce.nudebt__Client_Payment_Fee__c;
                                switch (j)
                                {
                                    case 0:
                                        transactionInfo.Fee1 = (double)feeTemplate.nudebt__Amount__c;
                                        break;
                                    case 1:
                                        transactionInfo.Fee2 = (double)feeTemplate.nudebt__Amount__c;
                                        break;
                                    case 2:
                                        transactionInfo.Fee3 = (double)feeTemplate.nudebt__Amount__c;
                                        break;
                                    case 3:
                                        transactionInfo.Fee4 = (double)feeTemplate.nudebt__Amount__c;
                                        break;
                                    case 4:
                                        transactionInfo.Fee5 = (double)feeTemplate.nudebt__Amount__c;
                                        break;
                                    case 5:
                                        transactionInfo.Fee6 = (double)feeTemplate.nudebt__Amount__c;
                                        break;
                                    case 6:
                                        transactionInfo.Fee7 = (double)feeTemplate.nudebt__Amount__c;
                                        break;
                                }

                            }
                            list.Add(transactionInfo);
                        }
                        objList = list;

                    }
                    catch (Exception)
                    {

                        throw;
                    }

                    if (!qr.done)
                    {
                        qr = _salesForce.GetNextBatch(qr);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return tranDS;
        }

        /// <summary>
        /// method to update payment status and description on SalesForce.
        /// </summary>
        public Hashtable UpdateRAMSPayment(List<TransactionInfo> list)
        {
            Hashtable ht = new Hashtable();
            List<string> clientIDs = new List<string>();
            List<string> clientIDsDesc = new List<string>();

            if (list.Count > 0)
            {
                List<string[]> errorList = new List<string[]>();
                List<string> resultList = new List<string>();

                List<SFInterop.SForce.sObject> clientsToUpdate = new List<SFInterop.SForce.sObject>();
                List<SFInterop.SForce.sObject> clientsSyncToUpdate = new List<SFInterop.SForce.sObject>();

                foreach (var transaction in list)
                {

                    SFInterop.SForce.nudebt__Client_Payment__c updatePayment = new SFInterop.SForce.nudebt__Client_Payment__c() { Id = transaction.TrasactionId };
                    SFInterop.SForce.nudebt__Client__c updateClient = new SFInterop.SForce.nudebt__Client__c() { Id = transaction.ClientID_c };

                    if (transaction.ProcessStatus == true)
                    {
                        updatePayment.nudebt__Payment_Status__c = "Processing";
                        updatePayment.nudebt__Payment_Scheduled_Date__c = transaction.ScheduleDate;
                        updatePayment.nudebt__Payment_Amount__c = transaction.TotalPaymentAmount;
                        updatePayment.nudebt__Sync_Notes__c = transaction.Note;
                        if (!transaction.PID.Equals(0))
                            updatePayment.nudebt__Transaction_ID__c = Convert.ToString(transaction.PID);
                    }
                    else
                    {
                        updatePayment.nudebt__Sync_Notes__c = transaction.Note;
                    }

                    clientsToUpdate.Add(updatePayment);
                    clientIDs.Add(transaction.TrasactionId);

                    updateClient.nudebt__Sync_Description__c = "     ";
                    clientsSyncToUpdate.Add(updateClient);
                    clientIDsDesc.Add(transaction.ClientID_c);
                }

                if (clientsToUpdate.Count > 0)
                {
                    SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(clientsToUpdate);
                    this.ProcessResults(results2, " nudebt__Client_Payment__c", ref resultList, ref errorList, clientIDs);
                }

                if (clientsSyncToUpdate.Count > 0)
                {
                    SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(clientsSyncToUpdate);
                    this.ProcessResults(results2, " nudebt__Client__c", ref resultList, ref errorList, clientIDsDesc);
                }
                this.WriteError(errorList);
            }

            return ht;
        }
       
        /// <summary>
        /// method to update payment fee table status and description on SalesForce.
        /// </summary>
        public Hashtable UpdateRAMSPaymentFee(TransactionDS ds)
        {
            Hashtable ht = new Hashtable();
            List<string> TransactionIDs = new List<string>();

            List<string[]> errorList = new List<string[]>();
            List<string> resultList = new List<string>();


            List<SFInterop.SForce.sObject> feeToUpdate = new List<SFInterop.SForce.sObject>();
            foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit.Rows)
            {
                SFInterop.SForce.nudebt__Client_Payment_Fee__c updateFee = new SFInterop.SForce.nudebt__Client_Payment_Fee__c() { Id = row.RecordID };
                if (row.Status == "Processing")
                {
                    updateFee.nudebt__Status__c = "Processing";
                    updateFee.nudebt__Sync_Notes__c = row.SyncNotes;
                    if (!row.TransactionID.Equals(0))
                        updateFee.nudebt__Transaction_ID__c = Convert.ToString(row.TransactionID);
                }
                else
                {
                    updateFee.nudebt__Sync_Notes__c = row.SyncNotes;
                }
                feeToUpdate.Add(updateFee);
                TransactionIDs.Add(row.RecordID);

            }

            if (feeToUpdate.Count > 0)
            {
                SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(feeToUpdate);
                this.ProcessResults(results2, " nudebt__Client_Payment_Fee__c", ref resultList, ref errorList, TransactionIDs);
            }

            ///For Salesforce Responce
            for (int i = 0; i < errorList.Count; i++)
            {
                TransactionDS.ClientDepositRow[] rows = ds.ClientDeposit.Select("RecordID = '" + errorList[i][2] + "'") as TransactionDS.ClientDepositRow[];
                if (rows.Length > 0)
                {
                    rows[0].SFErrorMessage = errorList[i][0];

                    rows[0].SFErrorCode = "Error";
                }
            }
            if (errorList.Count > 0)
                this.WriteError(errorList);

            return ht;
        }

        /// <summary>
        /// method to update payment and fee table status, Cleared Date and  on SalesForce.
        /// </summary>
        public List<string> UpdateT2PTransactions(TransactionDS ds)
        {
            try
            {
                List<string> paymentIDs = new List<string>();
                List<string> feeIDs = new List<string>();
                List<string> clientIDs = new List<string>();

                List<string[]> errorList = new List<string[]>();
                List<string> resultList = new List<string>();

                List<SFInterop.SForce.sObject> feeToUpdate = new List<SFInterop.SForce.sObject>();
                List<SFInterop.SForce.sObject> paymentToUpdate = new List<SFInterop.SForce.sObject>();
                List<SFInterop.SForce.sObject> clientToUpdate = new List<SFInterop.SForce.sObject>();

                List<List<SFInterop.SForce.sObject>> feeToUpdatebatch = new List<List<SFInterop.SForce.sObject>>();
                List<List<SFInterop.SForce.sObject>> paymentToUpdatebatch = new List<List<SFInterop.SForce.sObject>>();
                List<List<SFInterop.SForce.sObject>> clientToUpdatebatch = new List<List<SFInterop.SForce.sObject>>();
                List<List<string>> paymentIDsbatch = new List<List<string>>();
                List<List<string>> feeIDsbatch = new List<List<string>>();
                List<List<string>> clientIDsbatch = new List<List<string>>();
                //int count = 0;
                
                foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit.Rows)
                {

                    if (feeToUpdate.Count == 1)
                    {
                        feeToUpdatebatch.Add(feeToUpdate);
                        feeIDsbatch.Add(feeIDs);
                        feeIDs = new List<string>();
                        feeToUpdate = new List<SFInterop.SForce.sObject>();
                    }
                    if (paymentToUpdate.Count == 1)
                    {
                        paymentToUpdatebatch.Add(paymentToUpdate);
                        paymentIDsbatch.Add(paymentIDs);
                        paymentIDs = new List<string>();
                        paymentToUpdate = new List<SFInterop.SForce.sObject>();
                    }

                    if (clientToUpdate.Count == 1)
                    {
                        clientToUpdatebatch.Add(clientToUpdate);
                        clientIDsbatch.Add(clientIDs);
                        clientIDs = new List<string>();
                        clientToUpdate = new List<SFInterop.SForce.sObject>();
                    }

                    #region batch1
                    SFInterop.SForce.nudebt__Client_Payment_Fee__c updateFee = new SFInterop.SForce.nudebt__Client_Payment_Fee__c() { Id = row.RecordID };
                    SFInterop.SForce.nudebt__Client_Payment__c updatePayment = new SFInterop.SForce.nudebt__Client_Payment__c() { Id = row.RecordID };
                    SFInterop.SForce.nudebt__Client__c updateClient = new SFInterop.SForce.nudebt__Client__c() { Id = row.ClientID };
                    if (row.Type.Equals("A"))
                    {
                        updatePayment.nudebt__Sync_Date__c = DateTime.Now;
                        updatePayment.nudebt__Sync_Date__cSpecified = true;

                        if (!row.IsSyncNotesNull())
                        {
                            updatePayment.nudebt__Sync_Notes__c = row.SyncNotes;
                        }
                        //aaded noew entery
                        if (!row.IsResponseReasonCodeNull())
                        {
                            updatePayment.Response_Reason_Code__c = row.ResponseReasonCode;
                            updatePayment.Response_Reason_Code__cSpecified = true; 
                        }

                        if (!row.IsStatusNull() && (row.Status == "Cleared" || row.Status == "Approved"))
                        {
                            if (!row.IsAmountPaidNull())
                            {
                                updatePayment.nudebt__Amount_Paid__c = row.AmountPaid;
                                updatePayment.nudebt__Amount_Paid__cSpecified = true;
                            }
                            else
                            {
                                if (!row.IsAmountNull())
                                {
                                    updatePayment.nudebt__Amount_Paid__c = row.Amount;
                                    updatePayment.nudebt__Amount_Paid__cSpecified = true;
                                }
                            }
                        }


                        if (!row.IsTransactionIDNull())
                        {
                            updatePayment.nudebt__Transaction_ID__c = row.TransactionID;
                        }

                        if (!row.IsStatusNull() && row.Status != null && !string.IsNullOrEmpty(row.Status))
                        {
                            // if the status of the payment has been updated, then set the payment status
                            updatePayment.nudebt__Payment_Status__c = row.Status;
                        }

                        if (!row.IsClearedDateNull())
                        {
                            // if the payment is cleared
                            updatePayment.nudebt__Payment_Cleared_Date__c = row.ClearedDate;
                            updatePayment.nudebt__Payment_Cleared_Date__cSpecified = true;
                        }
                        paymentToUpdate.Add(updatePayment);
                        paymentIDs.Add(row.RecordID);
                    }
                    else
                    {
                        updateFee.nudebt__Sync_Date__c = DateTime.Now;
                        updateFee.nudebt__Sync_Date__cSpecified = true;

                        if (!row.IsSyncNotesNull())
                        {
                            updateFee.nudebt__Sync_Notes__c = row.SyncNotes;
                        }
                        if (!row.IsStatusNull() && (row.Status == "Cleared" || row.Status == "Approved"))
                        {
                            if (!row.IsAmountPaidNull())
                            {
                                updateFee.nudebt__Amount_Paid__c = row.AmountPaid;
                                updateFee.nudebt__Amount_Paid__cSpecified = true;
                            }
                            else
                            {
                                if (!row.IsAmountNull())
                                {
                                    updateFee.nudebt__Amount_Paid__c = row.Amount;
                                    updateFee.nudebt__Amount_Paid__cSpecified = true;
                                }
                            }
                        }


                        if (!row.IsTransactionIDNull())
                        {
                            updateFee.nudebt__Transaction_ID__c = row.TransactionID;
                        }

                        if (!row.IsStatusNull() && row.Status != null && !string.IsNullOrEmpty(row.Status))
                        {
                            // if the status of the fee has been updated, then set the fee status
                            updateFee.nudebt__Status__c = row.Status;
                        }

                        if (!row.IsClearedDateNull())
                        {
                            // if the fee is cleared
                            updateFee.nudebt__Cleared_Date__c = row.ClearedDate;
                            updateFee.nudebt__Cleared_Date__cSpecified = true;
                        }
                        feeToUpdate.Add(updateFee);
                        feeIDs.Add(row.RecordID);
                    }
                    #region In Construction
                    if (!row.IsClientNoteNull())
                    {
                        bool flag = true;
                        if (!string.IsNullOrEmpty(row.ClientNote) && !string.IsNullOrWhiteSpace(row.ClientNote))
                        {
                            if (clientIDsbatch.Count == 0)
                            {
                                if (clientIDs.Count > 0)
                                {
                                    for (int j = 0; j < clientIDs.Count; j++)
                                    {
                                        if (clientIDs[j] == row.ClientID)
                                        {
                                            flag = false;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0; i < clientIDsbatch.Count; i++)
                                {
                                    for (int j = 0; j < clientIDsbatch[i].Count; j++)
                                    {
                                        if (clientIDsbatch[i][j] == row.ClientID)
                                        {
                                            flag = false;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        if (flag)
                        {
                            updateClient.nudebt__Sync_Description__c = "     ";
                            clientToUpdate.Add(updateClient);
                            clientIDs.Add(row.ClientID);
                        }
                    }

                    #endregion
                    #endregion
                }

                feeToUpdatebatch.Add(feeToUpdate);
                feeIDsbatch.Add(feeIDs);
                paymentToUpdatebatch.Add(paymentToUpdate);
                paymentIDsbatch.Add(paymentIDs);
                clientToUpdatebatch.Add(clientToUpdate);
                clientIDsbatch.Add(clientIDs);

                for (int i = 0; i < paymentToUpdatebatch.Count; i++)
                {
                    if (paymentToUpdatebatch[i].Count > 0)
                    {
                        SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(paymentToUpdatebatch[i]);
                        this.ProcessResults(results2, " nudebt__Client_Payment__c", ref resultList, ref errorList, paymentIDsbatch[i]);
                    }
                }
                for (int i = 0; i < feeToUpdatebatch.Count; i++)
                {
                    if (feeToUpdatebatch[i].Count > 0)
                    {
                        SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(feeToUpdatebatch[i]);
                        this.ProcessResults(results2, " nudebt__Client_Payment_Fee__c", ref resultList, ref errorList, feeIDsbatch[i]);
                    }
                }
                for (int i = 0; i < clientToUpdatebatch.Count; i++)
                {
                    if (clientToUpdatebatch[i].Count > 0)
                    {
                        SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(clientToUpdatebatch[i]);
                        this.ProcessResults(results2, " nudebt__Client__c", ref resultList, ref errorList, clientIDsbatch[i]);
                    }
                }
                ///For Salesforce Responce
                for (int i = 0; i < errorList.Count; i++)
                {
                    TransactionDS.ClientDepositRow[] rows = ds.ClientDeposit.Select("RecordID = '" + errorList[i][2] + "'") as TransactionDS.ClientDepositRow[];
                    if (rows.Length > 0)
                    {
                        rows[0].SFErrorMessage = errorList[i][0];

                        rows[0].SFErrorCode = "Error";
                    }
                }
                if (errorList.Count > 0)
                    this.WriteError(errorList);
                return resultList;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// method to update payment fee table status and description on SalesForce.
        /// </summary>
        public Hashtable UpdateAuthorizePaymentFee(TransactionDS ds)
        {
            Hashtable ht = new Hashtable();
            List<string> TransactionIDs = new List<string>();

            List<string[]> errorList = new List<string[]>();
            List<string> resultList = new List<string>();


            List<SFInterop.SForce.sObject> feeToUpdate = new List<SFInterop.SForce.sObject>();
            foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit.Rows)
            {
                SFInterop.SForce.nudebt__Client_Payment_Fee__c updateFee = new SFInterop.SForce.nudebt__Client_Payment_Fee__c() { Id = row.RecordID };
                if (row.Status == "Cleared" || row.Status == "Errored")
                {
                    updateFee.nudebt__Status__c = row.Status;
                    updateFee.nudebt__Sync_Notes__c = row.SyncNotes;
                    updateFee.nudebt__Sync_Date__c = DateTime.Now;
                    updateFee.nudebt__Sync_Date__cSpecified = true;
                    if (!row.TransactionID.Equals(0))
                        updateFee.nudebt__Transaction_ID__c = Convert.ToString(row.TransactionID);
                }
                else
                {
                    updateFee.nudebt__Sync_Notes__c = row.SyncNotes;
                    updateFee.nudebt__Sync_Date__c = DateTime.Now;
                    updateFee.nudebt__Sync_Date__cSpecified = true;
                }
                feeToUpdate.Add(updateFee);
                TransactionIDs.Add(row.RecordID);

            }

            if (feeToUpdate.Count > 0)
            {
                SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(feeToUpdate);
                this.ProcessResults(results2, " nudebt__Client_Payment_Fee__c", ref resultList, ref errorList, TransactionIDs);
            }

            ///For Salesforce Responce
            for (int i = 0; i < errorList.Count; i++)
            {
                TransactionDS.ClientDepositRow[] rows = ds.ClientDeposit.Select("RecordID = '" + errorList[i][2] + "'") as TransactionDS.ClientDepositRow[];
                if (rows.Length > 0)
                {
                    rows[0].SFErrorMessage = errorList[i][0];

                    rows[0].SFErrorCode = "Error";
                }
            }
            if (errorList.Count > 0)
                this.WriteError(errorList);
            this.UpdateAuthorizePayment(ds);
            return ht;
        }
     
        /// <summary>
        /// method to update payment and fee table status, Cleared Date and  on SalesForce.
        /// </summary>
        public List<string> UpdateFDPaments(TransactionDS ds)
        {
            try
            {
                #region Parameters
                List<string> paymentIDs = new List<string>();
                List<string> feeIDs = new List<string>();
                List<string> clientIDsDesc = new List<string>();

                List<string[]> errorList = new List<string[]>();
                List<string> resultList = new List<string>();

                List<SFInterop.SForce.sObject> feeToUpdate = new List<SFInterop.SForce.sObject>();
                List<SFInterop.SForce.sObject> paymentToUpdate = new List<SFInterop.SForce.sObject>();
                List<SFInterop.SForce.sObject> clientsSyncToUpdate = new List<SFInterop.SForce.sObject>();

                List<List<SFInterop.SForce.sObject>> feeToUpdatebatch = new List<List<SFInterop.SForce.sObject>>();
                List<List<SFInterop.SForce.sObject>> paymentToUpdatebatch = new List<List<SFInterop.SForce.sObject>>();
                List<List<SFInterop.SForce.sObject>> clientsSyncToUpdatebatch = new List<List<SFInterop.SForce.sObject>>();
                List<List<string>> paymentIDsbatch = new List<List<string>>();
                List<List<string>> feeIDsbatch = new List<List<string>>();
                List<List<string>> clientIDsDescbatch = new List<List<string>>();
                #endregion
                foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit.Rows)
                {

                    if (paymentToUpdate.Count == 1)
                    {
                        paymentToUpdatebatch.Add(paymentToUpdate);
                        paymentIDsbatch.Add(paymentIDs);
                        paymentIDs = new List<string>();
                        paymentToUpdate = new List<SFInterop.SForce.sObject>();
                    }
                    if (feeToUpdate.Count == 1)
                    {
                        feeToUpdatebatch.Add(feeToUpdate);
                        feeIDsbatch.Add(feeIDs);
                        feeIDs = new List<string>();
                        feeToUpdate = new List<SFInterop.SForce.sObject>();
                    }
                    if (clientsSyncToUpdate.Count == 1)
                    {
                        clientsSyncToUpdatebatch.Add(clientsSyncToUpdate);
                        clientIDsDescbatch.Add(clientIDsDesc);
                        clientIDsDesc = new List<string>();
                        clientsSyncToUpdate = new List<SFInterop.SForce.sObject>();
                    }
                    #region batch1
                    SFInterop.SForce.nudebt__Client_Payment_Fee__c updateFee = new SFInterop.SForce.nudebt__Client_Payment_Fee__c() { Id = row.RecordID };
                    SFInterop.SForce.nudebt__Client_Payment__c updatePayment = new SFInterop.SForce.nudebt__Client_Payment__c() { Id = row.RecordID };
                    SFInterop.SForce.nudebt__Client__c updateClient = new SFInterop.SForce.nudebt__Client__c() { Id = row.ClientID };
                                       
                    if (row.Type.Equals("A"))
                    {
                        updatePayment.nudebt__Sync_Date__c = DateTime.Now;
                        updatePayment.nudebt__Sync_Date__cSpecified = true;
                        if (!row.IsSyncNotesNull())
                        {
                            updatePayment.nudebt__Sync_Notes__c = row.SyncNotes;
                        }
                        if (!row.IsStatusNull() && row.Status != null && !string.IsNullOrEmpty(row.Status))
                        {
                            // if the status of the payment has been updated, then set the payment status
                            updatePayment.nudebt__Payment_Status__c = row.Status;
                        }
                        if (!row.IsTransactionIDNull())
                        {
                            updatePayment.nudebt__Transaction_ID__c = row.TransactionID;
                        }                      
                        paymentToUpdate.Add(updatePayment);
                        paymentIDs.Add(row.RecordID);

                        if (row.Status.Equals("Approved"))
                        {
                            updateClient.nudebt__Sync_Description__c = "     ";                           
                        }
                        else
                        {
                            updateClient.nudebt__Sync_Description__c = row.SyncNotes;
                        }
                        clientsSyncToUpdate.Add(updateClient);
                        clientIDsDesc.Add(row.ClientID);
                    }
                    else
                    {
                        updateFee.nudebt__Sync_Date__c = DateTime.Now;
                        updateFee.nudebt__Sync_Date__cSpecified = true;
                        if (!row.IsSyncNotesNull())
                        {
                            updateFee.nudebt__Sync_Notes__c = row.SyncNotes;
                        }

                        if (!row.IsTransactionIDNull())
                        {
                            updateFee.nudebt__Transaction_ID__c = row.TransactionID;
                        }

                        if (!row.IsStatusNull() && row.Status != null && !string.IsNullOrEmpty(row.Status))
                        {
                            // if the status of the fee has been updated, then set the fee status
                            updateFee.nudebt__Status__c = row.Status;
                        }
                        feeToUpdate.Add(updateFee);
                        feeIDs.Add(row.RecordID);
                    }
                    #endregion                    
                }

                feeToUpdatebatch.Add(feeToUpdate);
                paymentToUpdatebatch.Add(paymentToUpdate);
                paymentIDsbatch.Add(paymentIDs);
                feeIDsbatch.Add(feeIDs);
                clientsSyncToUpdatebatch.Add(clientsSyncToUpdate);
                clientIDsDescbatch.Add(clientIDsDesc);
                for (int i = 0; i < paymentToUpdatebatch.Count; i++)
                {
                    if (paymentToUpdatebatch[i].Count > 0)
                    {
                        SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(paymentToUpdatebatch[i]);
                        this.ProcessResults(results2, " nudebt__Client_Payment__c", ref resultList, ref errorList, paymentIDsbatch[i]);
                    }
                }
                for (int i = 0; i < feeToUpdatebatch.Count; i++)
                {
                    if (feeToUpdatebatch[i].Count > 0)
                    {
                        SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(feeToUpdatebatch[i]);
                        this.ProcessResults(results2, " nudebt__Client_Payment_Fee__c", ref resultList, ref errorList, feeIDsbatch[i]);
                    }
                }
                for (int i = 0; i < clientsSyncToUpdatebatch.Count; i++)
                {
                    if (clientsSyncToUpdatebatch[i].Count > 0)
                    {
                        SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(clientsSyncToUpdatebatch[i]);
                        this.ProcessResults(results2, " nudebt__Client_Payment_Fee__c", ref resultList, ref errorList, clientIDsDescbatch[i]);
                    }
                }
                ///For Salesforce Responce
                for (int i = 0; i < errorList.Count; i++)
                {
                    TransactionDS.ClientDepositRow[] rows = ds.ClientDeposit.Select("RecordID = '" + errorList[i][2] + "'") as TransactionDS.ClientDepositRow[];
                    if (rows.Length > 0)
                    {
                        rows[0].SFErrorMessage = errorList[i][0];

                        rows[0].SFErrorCode = "Error";
                    }
                }
                if (errorList.Count > 0)
                    this.WriteError(errorList);
                return resultList;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// Gets all transactions scheduled for processing within a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of transactions as a strongly typed dataset</returns>
        public TransactionDS GetEPPSTransactions(DateTime start, DateTime end)
        {
            try
            {
                TransactionDS tds = new TransactionDS();
                this.GetEPPSDeposits(start, end, tds);
                return tds;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Gets all transactions scheduled for processing within a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of transactions as a strongly typed dataset</returns>
        public TransactionDS GetAutherizeTransactions( DateTime startDate, DateTime endDate)
        {
            try
            {
                TransactionDS tds = new TransactionDS();
                this.GetAutherizeDeposits(tds, startDate, endDate);
                return tds;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Gets all transactions scheduled for processing within a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of transactions as a strongly typed dataset</returns>
        public TransactionDS GetAutherizeDotNetTransactions(string processor, DateTime startDate, DateTime endDate)
        {
            try
            {
                TransactionDS tds = new TransactionDS();
                this.GetAutherizeDotNetDeposits(tds, processor, startDate, endDate);
                return tds;
            }
            catch
            {
                throw;
            }
        }

        public TransactionDS GetEPPSDeposits(DateTime start, DateTime end, TransactionDS tranDS)
        {
            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT     Id, nudebt__Payment_Amount__c, nudebt__Amount_Paid__c,
                                            nudebt__Payment_Scheduled_Date__c, nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                            nudebt__Client__c, nudebt__Client_Name__c,               
                                            nudebt__Client__r.nudebt__Bank_Name__c, 
                                            nudebt__Client__r.nudebt__Routing_Number__c, nudebt__Client__r.nudebt__Bank_City__c, 
                                            nudebt__Client__r.nudebt__Bank_State__c, nudebt__Client__r.nudebt__Account_Type__c, 
                                            nudebt__Client__r.nudebt__Account_Number_Enc__c,nudebt__Client__r.nudebt__Account_Number__c, 
                                            nudebt__Client__r.nudebt__Processor__r.Name,nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c,
                                            nudebt__Client__r.nudebt__Processor_Acct__c, nudebt__Client__r.nudebt__Import_ID__c,
                                            nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c,
                                            nudebt__Client__r.nudebt__Billing_Add1__c,                                                        
                                            nudebt__Client__r.nudebt__Billing_City__c,
                                            nudebt__Client__r.nudebt__Billing_ST__c,
                                            nudebt__Client__r.nudebt__Billing_Postal__c,
                                            nudebt__Client__r.nudebt__CVV_Code__c, 
                                            nudebt__Client__r.nudebt__Debit_Card_Number__c, 
                                            nudebt__Client__r.nudebt__Debit_Card_Expires__c,
                                            nudebt__Client__r.nudebt__First_Name__c,
                                            nudebt__Client__r.nudebt__Last_Name__c
                                FROM        nudebt__Client_Payment__c
                                WHERE       nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} AND nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd}
                                AND         (nudebt__Payment_Status__c = 'Pending' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored')
                                AND         nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                AND         nudebt__Client__r.nudebt__Processor__r.Name='EPPS'";


               
                if (start.Date > end.Date)
                {
                    query = string.Format(query, end.Date, start.Date);
                }
                else
                {
                    query = string.Format(query, start.Date, end.Date);
                }

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);

                TransactionDS.ClientDepositFeeDataTable tmpFeeTable = new TransactionDS.ClientDepositFeeDataTable();
                bool isDone = false;

                do
                {
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;

                        TransactionDS.ClientDepositRow row = tranDS.ClientDeposit.NewClientDepositRow();

                        try
                        {
                            row.RecordID = draft.Id;
                            string x = draft.Name;
                            row.ClientID = draft.nudebt__Client__c;
                            row.ClientName = draft.nudebt__Client_Name__c;
                            row.BankAccountType = draft.nudebt__Client__r.nudebt__Account_Type__c;
                            row.BankAccountNumber
                                = String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number__c) ? "" : draft.nudebt__Client__r.nudebt__Account_Number__c)
                                : draft.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            row.BankCity = draft.nudebt__Client__r.nudebt__Bank_City__c;
                            row.BankState = draft.nudebt__Client__r.nudebt__Bank_State__c;

                            row.BankRoutingNumber = draft.nudebt__Client__r.nudebt__Routing_Number__c;
                            row.BankName = draft.nudebt__Client__r.nudebt__Bank_Name__c;
                            row.Amount = draft.nudebt__Payment_Amount__c.Value;
                            if (draft.nudebt__Amount_Paid__c != null)
                                row.AmountPaid = draft.nudebt__Amount_Paid__c.Value;
                            row.Date = Convert.ToDateTime(draft.nudebt__Payment_Scheduled_Date__c);
                            row.ProcessorAccount = draft.nudebt__Client__r.nudebt__Processor_Acct__c;
                            row.TransactionID = draft.nudebt__Transaction_ID__c == null ? "" : draft.nudebt__Transaction_ID__c;
                            row.ParentDRCID = "";
                            row.Type = "A";
                            row.TypeDesc = "ACH Monthly Draft";

                            row.Status = "Pending";
                            row.ClientImportID = draft.nudebt__Client__r.nudebt__Import_ID__c;
                            string query4 = @"SELECT nudebt__Amount__c  FROM nudebt__Client_Payment_Fee__c
                            where  nudebt__Fee_Type__c = 'Program' AND nudebt__Client_Payment__c='" + draft.Id + "'";

                            query4 = string.Format(query4, end.Date, start.Date);
                            SFInterop.SForce.QueryResult qr4 = _salesForce.RunQuery(query4);
                            double programFee = 0;
                            for (int j = 0; j < qr4.size; j++)
                            {
                                SFInterop.SForce.nudebt__Client_Payment_Fee__c feeTemplate = qr4.records[j] as SFInterop.SForce.nudebt__Client_Payment_Fee__c;
                                programFee = programFee + (double)feeTemplate.nudebt__Amount__c;
                            }

                            row.ProgramFee = Convert.ToString(programFee);
                            row.ProcessorFee = draft.nudebt__Processing_Fee__c == null ? 0 : (double)draft.nudebt__Processing_Fee__c;
                            row.Processor = draft.nudebt__Client__r.nudebt__Processor__r.Name;
                            row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(draft.nudebt__Sync_Date__c);
                            row.ParentDRCID = "";
                            row.PaymentID = "";
                            
                            ////
                            row.ClientFName = draft.nudebt__Client__r.nudebt__First_Name__c;
                            row.ClientLName = draft.nudebt__Client__r.nudebt__Last_Name__c;
                            // TODO bt - Need to pickup the Billing address
                            row.CardAddress = draft.nudebt__Client__r.nudebt__Billing_Add1__c == null ? string.Empty : draft.nudebt__Client__r.nudebt__Billing_Add1__c;
                            row.CardCity = draft.nudebt__Client__r.nudebt__Billing_City__c == null ? string.Empty : draft.nudebt__Client__r.nudebt__Billing_City__c;
                            row.CardState = draft.nudebt__Client__r.nudebt__Billing_ST__c == null ? "NY" : draft.nudebt__Client__r.nudebt__Billing_ST__c;
                            row.CardPostal = draft.nudebt__Client__r.nudebt__Billing_Postal__c == null ? string.Empty : draft.nudebt__Client__r.nudebt__Billing_Postal__c;
                            row.CardNumber = draft.nudebt__Client__r.nudebt__Debit_Card_Number__c;
                            row.CardPin = draft.nudebt__Client__r.nudebt__CVV_Code__c == null ? string.Empty : draft.nudebt__Client__r.nudebt__CVV_Code__c;
                            row.CardExpDate =  Convert.ToString(draft.nudebt__Client__r.nudebt__Debit_Card_Expires__c);
                        }
                        catch
                        {
                            row.HasSyncError = "True";
                            row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            if (row.Amount > 0)
                                tranDS.ClientDeposit.Rows.Add(row);
                        }
                    }

                   
                    if (!qr.done)
                    {
                        qr = _salesForce.GetNextBatch(qr);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return tranDS;
        }

        /// <summary>
        /// method to update payment fee table status and description on SalesForce.
        /// </summary>
        public Hashtable UpdateAuthorizePayment(TransactionDS ds)
        {
            Hashtable ht = new Hashtable();
            List<string> transcationIDs = new List<string>();
            List<string> clientIDsDesc = new List<string>();

            List<string[]> errorList = new List<string[]>();
            List<string> resultList = new List<string>();


            List<SFInterop.SForce.sObject> feeToUpdate = new List<SFInterop.SForce.sObject>();
            List<SFInterop.SForce.sObject> clientsSyncToUpdate = new List<SFInterop.SForce.sObject>();

            foreach (TransactionDS.ClientDepositRow row in ds.ClientDeposit.Rows)
            {
                SFInterop.SForce.nudebt__Client_Payment__c updateFee = new SFInterop.SForce.nudebt__Client_Payment__c() { Id = row.RecordID };
                SFInterop.SForce.nudebt__Client__c updateClient = new SFInterop.SForce.nudebt__Client__c() { Id = row.ClientID };

                if (row.Status == "Cleared" || row.Status == "Errored")
                {
                    updateFee.nudebt__Payment_Status__c = row.Status;
                    updateFee.nudebt__Sync_Notes__c = row.SyncNotes;
                    updateFee.nudebt__Sync_Date__c = DateTime.Now;
                    updateFee.nudebt__Sync_Date__cSpecified = true;
                    if (!row.TransactionID.Equals(0))
                        updateFee.nudebt__Transaction_ID__c = Convert.ToString(row.TransactionID);

                    if (row.TransactionID != "" && row.TransactionID != "")
                    {
                        updateClient.nudebt__Sync_Description__c = "     ";
                        clientsSyncToUpdate.Add(updateClient);
                        clientIDsDesc.Add(row.ClientID);
                    }
                }
                else
                {
                    updateFee.nudebt__Payment_Status__c = row.Status;
                    updateFee.nudebt__Sync_Notes__c = row.SyncNotes;
                    updateFee.nudebt__Sync_Date__c = DateTime.Now;
                    updateFee.nudebt__Sync_Date__cSpecified = true;
                    updateClient.nudebt__Sync_Description__c = row.SyncNotes;
                    clientsSyncToUpdate.Add(updateClient);
                    clientIDsDesc.Add(row.ClientID);
                }
                feeToUpdate.Add(updateFee);
                transcationIDs.Add(row.RecordID);

            }

            if (feeToUpdate.Count > 0)
            {
                SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(feeToUpdate);
                this.ProcessResults(results2, " nudebt__Client_Payment__c", ref resultList, ref errorList, transcationIDs);
            }

            if (clientsSyncToUpdate.Count > 0)
            {
                SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(clientsSyncToUpdate);
                this.ProcessResults(results2, " nudebt__Client__c", ref resultList, ref errorList, clientIDsDesc);
            }

            ///For Salesforce Responce
            for (int i = 0; i < errorList.Count; i++)
            {
                TransactionDS.ClientDepositRow[] rows = ds.ClientDeposit.Select("RecordID = '" + errorList[i][2] + "'") as TransactionDS.ClientDepositRow[];
                if (rows.Length > 0)
                {
                    rows[0].SFErrorMessage = errorList[i][0];

                    rows[0].SFErrorCode = "Error";
                }
            }
            if (errorList.Count > 0)
                this.WriteError(errorList);

            return ht;
        }
         
        /// <summary>
        /// Gets all transactions scheduled for processing within a specified date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The list of transactions as a strongly typed dataset</returns>
        public TransactionDS GetEPPSProcessingTransactions(DateTime start, DateTime end, bool getWithdrawals)
        {
            try
            {
                TransactionDS tds = new TransactionDS();
                this.GetEPPSProcessingDeposits(start, end, tds);
                return tds;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <param name="start">The start date</param>
        /// <param name="end">The end date</param>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        public TransactionDS GetEPPSProcessingDeposits(DateTime start, DateTime end, TransactionDS tranDS)
        {
            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT     Id, nudebt__Payment_Amount__c, nudebt__Amount_Paid__c,
                                            nudebt__Payment_Scheduled_Date__c, nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                            nudebt__Client__c, nudebt__Client_Name__c,               
                                            nudebt__Client__r.nudebt__Bank_Name__c, 
                                            nudebt__Client__r.nudebt__Routing_Number__c, 
                                            nudebt__Client__r.nudebt__Account_Type__c, 
                                            nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client__r.nudebt__Account_Number__c, 
                                            nudebt__Client__r.nudebt__Processor__r.Name,
                                            nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c,
                                            nudebt__Client__r.nudebt__Processor_Acct__c, 
                                            nudebt__Client__r.nudebt__Import_ID__c,
                                            nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c
                                FROM        nudebt__Client_Payment__c  
                                WHERE       nudebt__Payment_Scheduled_Date__c >= {0:yyyy-MM-dd} AND nudebt__Payment_Scheduled_Date__c <= {1:yyyy-MM-dd}                            
                                AND         nudebt__Payment_Status__c = 'Processing'
                                AND         nudebt__Client__r.nudebt__Processor__r.Name='EPPS'
                                AND         nudebt__Client__r.nudebt__Processor_Status__c = 'Active'";
                 //AND         nudebt__Client__r.nudebt__Processor__r.Name='EPPS'
                 //AND         (nudebt__Payment_Status__c = 'Processing' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored') 
                if (start.Date > end.Date)
                {
                    query = string.Format(query, end.Date, start.Date);
                }
                else
                {
                    query = string.Format(query, start.Date, end.Date);
                }

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                TransactionDS.ClientDepositFeeDataTable tmpFeeTable = new TransactionDS.ClientDepositFeeDataTable();
                bool isDone = false;

                do
                {
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;

                        TransactionDS.ClientDepositRow row = tranDS.ClientDeposit.NewClientDepositRow();

                        try
                        {
                            row.RecordID = draft.Id;
                            string x = draft.Name;
                            row.ClientID = draft.nudebt__Client__c;
                            row.ClientName = draft.nudebt__Client_Name__c;
                            row.BankAccountType = draft.nudebt__Client__r.nudebt__Account_Type__c;
                            row.BankAccountNumber
                                = String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number__c) ? "" : draft.nudebt__Client__r.nudebt__Account_Number__c)
                                : draft.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            row.BankRoutingNumber = draft.nudebt__Client__r.nudebt__Routing_Number__c;
                            row.BankName = draft.nudebt__Client__r.nudebt__Bank_Name__c;
                            row.Amount = draft.nudebt__Payment_Amount__c.Value;
                            if (draft.nudebt__Amount_Paid__c != null)
                                row.AmountPaid = draft.nudebt__Amount_Paid__c.Value;
                            row.Date = Convert.ToDateTime(draft.nudebt__Payment_Scheduled_Date__c);
                            row.ProcessorAccount = draft.nudebt__Client__r.nudebt__Processor_Acct__c;
                            row.TransactionID = draft.nudebt__Transaction_ID__c == null ? "" : draft.nudebt__Transaction_ID__c;
                            row.ParentDRCID = "";
                            row.Type = "A";
                            row.TypeDesc = "ACH Monthly Draft";

                            row.Status = draft.nudebt__Payment_Status__c;
                            row.ClientImportID = draft.nudebt__Client__r.nudebt__Import_ID__c;
                            row.ProcessorFee = draft.nudebt__Processing_Fee__c == null ? 0 : (double)draft.nudebt__Processing_Fee__c;
                            row.Processor = draft.nudebt__Client__r.nudebt__Processor__r.Name;
                            row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(draft.nudebt__Sync_Date__c);
                        }
                        catch
                        {
                            row.HasSyncError = "True";
                            row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            if (row.Amount > 0)
                                tranDS.ClientDeposit.Rows.Add(row);
                        }
                    }
                    if (!qr.done)
                    {
                        qr = _salesForce.GetNextBatch(qr);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return tranDS;
        }

        /// <summary>
        /// Retrieves payments that should be processed from clients within the specified
        /// date range
        /// </summary>
        /// <returns>The payment records as a strongly typed Datatable</returns>
        public TransactionDS GetDraftsRAMSForUpdate(ref TransactionDS tran)
        {
            if (_salesForce.IsValidSession)
            {
                string query = @"SELECT     nudebt__Client__c, nudebt__Client_Name__c, Id, nudebt__Client__r.nudebt__Import_ID__c,
                                            nudebt__Client__r.nudebt__Bank_Name__c,
                                            nudebt__Client__r.nudebt__Routing_Number__c,
                                            nudebt__Client__r.nudebt__Account_Type__c, 
                                            nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client__r.nudebt__Account_Number__c,
                                            nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c, 
                                            nudebt__Client__r.nudebt__Processor__r.Name, nudebt__Client__r.nudebt__Processor_Acct__c,
                                            nudebt__Payment_Amount__c, nudebt__Payment_Scheduled_Date__c, nudebt__Amount_Paid__c,
                                            nudebt__Transaction_ID__c, nudebt__Payment_Status__c,
                                            nudebt__Processing_Fee__c, nudebt__Setup_Fee__c, nudebt__Maintenance_Fee__c, nudebt__Sync_Date__c, nudebt__Setup_Fee_Transaction_ID__c, 
                                            nudebt__Maintenance_Fee_Transaction_ID__c
                                FROM        nudebt__Client_Payment__c  
                                WHERE       nudebt__Payment_Scheduled_Date__c <= TODAY 
                                AND         (nudebt__Payment_Status__c = 'Pending' OR nudebt__Payment_Status__c = 'Processing' OR nudebt__Payment_Status__c = 'TimedOut' OR nudebt__Payment_Status__c = 'Errored') 
                                AND         nudebt__Client__r.nudebt__Processor__r.Name='RAMS'
                                AND         nudebt__Client__r.nudebt__Processor_Acct__c != ''";

                string query2 = @"SELECT    Id, Name, 
                                            nudebt__Amount__c, nudebt__Amount_Paid__c, nudebt__Fee_ID__c, nudebt__Transaction_ID__c, 
                                            nudebt__Client_Payment__c, nudebt__Client_Payment__r.Id,
                                            nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c, nudebt__Status__c,
                                            nudebt__Client_Payment__r.nudebt__Client__c,
                                            nudebt__Client_Payment__r.nudebt__Client_Name__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__c,
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name, 
                                            nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c
                                FROM        nudebt__Client_Payment_Fee__c  
                                
                                WHERE       (nudebt__Status__c = 'Pending' OR nudebt__Status__c = 'TimedOut' OR nudebt__Status__c = 'Errored' OR nudebt__Status__c = 'Processing')
                                AND         nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Status__c = 'Active'
                                AND nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name = 'RAMS'
                                AND         nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c <= TODAY";

                SFInterop.SForce.QueryResult qr = _salesForce.RunQuery(query);
                SFInterop.SForce.QueryResult qr2 = _salesForce.RunQuery(query2);
                bool isDone = false;

                do
                {
                    for (int i = 0; i < qr.size; i++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment__c draft = qr.records[i] as SFInterop.SForce.nudebt__Client_Payment__c;

                        TransactionDS.ClientDepositRow row = tran.ClientDeposit.NewClientDepositRow();
                        // TransactionDS.ClientDepositFeeDataTable tmpFeeTable = new TransactionDS.ClientDepositFeeDataTable();
                        try
                        {
                            row.RecordID = draft.Id;
                            row.ClientID = draft.nudebt__Client__c;
                            row.ClientName = draft.nudebt__Client_Name__c;
                            row.BankAccountType = draft.nudebt__Client__r.nudebt__Account_Type__c;
                            row.BankAccountNumber
                                = String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                    ? (String.IsNullOrEmpty(draft.nudebt__Client__r.nudebt__Account_Number__c) ? "" : draft.nudebt__Client__r.nudebt__Account_Number__c)
                                    : draft.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            row.BankRoutingNumber = draft.nudebt__Client__r.nudebt__Routing_Number__c;
                            row.BankName = draft.nudebt__Client__r.nudebt__Bank_Name__c;
                            if (draft.nudebt__Amount_Paid__c != null)
                                row.AmountPaid = draft.nudebt__Amount_Paid__c.Value;
                            row.Amount = draft.nudebt__Payment_Amount__c.Value;
                            row.Date = Convert.ToDateTime(draft.nudebt__Payment_Scheduled_Date__c);
                            row.ProcessorAccount = draft.nudebt__Client__r.nudebt__Processor_Acct__c;
                            row.TransactionID = draft.nudebt__Transaction_ID__c;
                            row.ParentDRCID = "";
                            row.Type = "A";
                            row.TypeDesc = "ACH Monthly Draft";
                            //row.ClientIDForRAM = int.Parse(String.Concat("0", draft.nudebt__Transaction_ID__c.Split(new Char[] { '-' })[1]));
                            row.Status = draft.nudebt__Payment_Status__c;
                            row.ClientImportID = draft.nudebt__Client__r.nudebt__Import_ID__c;
                            row.ProcessorFee = draft.nudebt__Processing_Fee__c == null ? 0 : (double)draft.nudebt__Processing_Fee__c;
                            row.Processor = draft.nudebt__Client__r.nudebt__Processor__r.Name;
                            row.LastSyncDate = draft.nudebt__Sync_Date__c == null ? new DateTime() : Convert.ToDateTime(draft.nudebt__Sync_Date__c);

                        }
                        catch
                        {
                            row.HasSyncError = "True";
                            row.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            // int lg = row.GetClientDepositFeeRows().Length;
                            tran.ClientDeposit.Rows.Add(row);
                        }
                    }
                    for (int j = 0; j < qr2.size; j++)
                    {
                        SFInterop.SForce.nudebt__Client_Payment_Fee__c fee = qr2.records[j] as SFInterop.SForce.nudebt__Client_Payment_Fee__c;
                        TransactionDS.ClientDepositRow rowf = tran.ClientDeposit.NewClientDepositRow();
                        try
                        {
                            rowf.RecordID = fee.Id;
                            rowf.ClientID = fee.nudebt__Client_Payment__r.nudebt__Client__c;
                            rowf.ClientName = fee.nudebt__Client_Payment__r.nudebt__Client_Name__c;
                            rowf.BankAccountType = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Type__c;
                            rowf.BankAccountNumber
                                = String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c)
                                ? (String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c) ? "" : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number__c)
                                : fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Account_Number_Enc__c;

                            rowf.BankRoutingNumber = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Routing_Number__c;
                            rowf.BankName = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Bank_Name__c;
                            rowf.Amount = fee.nudebt__Amount__c.Value;
                            if (fee.nudebt__Amount_Paid__c != null)
                                rowf.AmountPaid = fee.nudebt__Amount_Paid__c.Value;
                            if (fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c != null)
                                rowf.Date = Convert.ToDateTime(fee.nudebt__Client_Payment__r.nudebt__Payment_Scheduled_Date__c);
                            rowf.ProcessorAccount = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor_Acct__c;
                            rowf.TransactionID = fee.nudebt__Transaction_ID__c;
                            rowf.ParentDRCID = fee.nudebt__Client_Payment__c;
                            rowf.Type = fee.nudebt__Fee_ID__c == null ? "" : fee.nudebt__Fee_ID__c; 
                            rowf.TypeDesc = fee.Name;
                            //rowf.RecordName
                            rowf.Status = fee.nudebt__Status__c;
                            rowf.ClientImportID = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Import_ID__c;
                            /* TODO bt - use name its
                            if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c))
                                rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.nudebt__Company_Name__c;
                            else
                                if (!String.IsNullOrEmpty(fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name))
                             */
                            rowf.Processor = fee.nudebt__Client_Payment__r.nudebt__Client__r.nudebt__Processor__r.Name;
                        }
                        catch
                        {
                            rowf.HasSyncError = "True";
                            rowf.SyncNotes = "Missing Required Information - Please Verify & Update in SalesForce";
                        }
                        finally
                        {
                            tran.ClientDeposit.Rows.Add(rowf);
                        }
                    }

                    if (!qr.done)
                    {
                        qr = _salesForce.GetNextBatch(qr);
                    }
                    else
                    {
                        isDone = true;
                    }
                }
                while (!isDone);
            }

            return tran;
        }

        /// <summary>
        /// Created new method for updating the Documents and its status along with client Status.. Created New method as it is required only at Client/Balance Update option
        /// </summary>
        public Hashtable UpdateClientListWithStatus(TransactionDS.ClientsDataTable clients)
        {
            Hashtable ht = new Hashtable();
            List<string> clientIDs = new List<string>();

            if (clients.Rows.Count > 0)
            {
                List<string[]> errorList = new List<string[]>();
                List<string> resultList = new List<string>();

                List<SFInterop.SForce.sObject> clientsToUpdate = new List<SFInterop.SForce.sObject>();

                foreach (TransactionDS.ClientsRow client in clients.Rows)
                {
                    // if the account had not been stored to salesForce
                    // add to the list to be updated
                    SFInterop.SForce.nudebt__Client__c updateClient = new SFInterop.SForce.nudebt__Client__c() { Id = client.ClientID };

                    updateClient.nudebt__Account_Balance__cSpecified = true;

                    if (client.ProcessorAccountNum != null)
                        updateClient.nudebt__Processor_Acct__c = client.ProcessorAccountNum;
                    if (client.AccountBalance != null)
                        updateClient.nudebt__Account_Balance__c = client.AccountBalance;
                    if (client.ProcessorStatus != null)
                        updateClient.nudebt__Processor_Status__c = client.ProcessorStatus;
                    if (client.SyncDescription != null)
                        updateClient.nudebt__Sync_Description__c = client.SyncDescription;
                    else
                        updateClient.nudebt__Sync_Description__c = String.Empty;                // clear out what might be there

                    //Added four New fields
                    if (client.ClientStatus != null)
                        updateClient.nudebt_client_status__c = client.ClientStatus;
                    if (client.ClientStatusDescription != null)
                        updateClient.nudebt_client_status_description__c = client.ClientStatusDescription;
                    if (client.DocumentStatus != null)
                        updateClient.nudebt_document_status__c = client.DocumentStatus;
                    if (client.DocumentStatusDescription != null)
                        updateClient.nudebt_document_status_description__c = client.DocumentStatusDescription;

                    // TO DO ::
                    if (client.ProccesorCreationDate != null)
                    {
                        updateClient.Nudebt_Processor_Created_Date__c = client.ProccesorCreationDate;
                        updateClient.Nudebt_Processor_Created_Date__cSpecified = true;
                    }
                    updateClient.nudebt__Mark_for_Sync__c = client.MarkForSync;
                    updateClient.nudebt__Mark_for_Sync__cSpecified = true;
                    updateClient.nudebt__Sync_Date__c = DateTime.Now;
                    updateClient.nudebt__Sync_Date__cSpecified = true;

                    clientsToUpdate.Add(updateClient);

                    client.IsNewAccount = false;
                    clientIDs.Add(client.ClientID);
                    ht.Add(client.ClientID, client);

                    if (clientsToUpdate.Count >= 100)
                    {
                        SFInterop.SForce.SaveResult[] results = this._salesForce.UpdateBatch(clientsToUpdate);
                        this.ProcessResults(results, " nudebt__Client__c", ref resultList, ref errorList, clientIDs);
                        clientsToUpdate.Clear();
                    }
                }

                if (clientsToUpdate.Count > 0)
                {
                    SFInterop.SForce.SaveResult[] results2 = this._salesForce.UpdateBatch(clientsToUpdate);
                    this.ProcessResults(results2, " nudebt__Client__c", ref resultList, ref errorList, clientIDs);
                }

                ///For Salesforce Responce
                for (int i = 0; i < errorList.Count; i++)
                {
                    TransactionDS.ClientsRow[] rows = clients.Select("ClientID = '" + errorList[i][2] + "'") as TransactionDS.ClientsRow[];
                    if (rows.Length > 0)
                    {
                        rows[0].SFErrorMessage = errorList[i][0];

                        rows[0].SFErrorCode = "Error";
                    }
                }
                if (errorList.Count > 0)
                    this.WriteError(errorList);
            }

            return ht;
        }

        #endregion 
    }
}